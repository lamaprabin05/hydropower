<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstallmentInformation extends Model
{
    protected  $table = "installment_information";
    public $timestamps = false;

    public function formshare()
    {
        return $this->belongsTo('App\FormShare');
    }
}
