<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\FormShare;

class NotificationPayment extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->data = $input;
        $this->order = FormShare::find($this->data['id']);
//        dd($this->order);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('shareholder.email.notification_payment')
                     ->subject('Payment Notificaton');
//                     ->with([
//                         'message' => $this->data['message']
//                     ]);
    }
}
