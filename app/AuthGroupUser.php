<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthGroupUser extends Model
{
    protected $table = "auth_group_user";
    public $timestamps = false;
}
