<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecificationName extends Model
{
    protected  $table= 'specification_name';
     public $timestamps = false;
}
