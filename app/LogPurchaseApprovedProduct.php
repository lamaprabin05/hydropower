<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogPurchaseApprovedProduct extends Model
{
    protected  $table= 'log_purchase_approved_product';
    public $timestamps = false;
}
