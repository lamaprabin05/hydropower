<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubBlock extends Model
{
     protected  $table= 'sub_blocks';
     public $timestamps = false;
}
