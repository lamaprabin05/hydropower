<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetName extends Model
{
    protected $table = 'asset_name';
    public $timestamps = false;

    public function specification()
    {
        return $this->hasMany('App\SpecificationName','asset_name_id','id');
    }
}
