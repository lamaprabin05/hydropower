<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodsPurchaseForm extends Model
{
    protected  $table= 'goods_purchase_form';
    public $timestamps = false;

   

    //relations
    public function product(){
    	return $this->belongsTo('App\Product','product_id');
    }

    public function vendor(){
    	return $this->belongsTo('App\ProductVendor','id');
    }

	public function user(){
	    	return $this->belongsTo('App\User','id');
    }

    public function goods_requisition_product(){
        return $this->belongsTo('App\GoodsRequisitionProduct','goods_requisition_product_id');
    }

    public function requisition_user(){
        return $this->belongsTo('App\User','requisition_by');
    }

    public function checker_user(){
        return $this->belongsTo('App\User','checked_by');
    }

    public function approver_user(){
        return $this->belongsTo('App\User','approved_by');
    }



}
