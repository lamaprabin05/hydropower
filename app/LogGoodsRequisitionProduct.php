<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogGoodsRequisitionProduct extends Model
{
    protected  $table = 'log_goods_requisition_product';
    public  $timestamps = false;
}
