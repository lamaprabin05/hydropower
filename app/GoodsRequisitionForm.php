<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodsRequisitionForm extends Model
{
    protected $table = 'goods_requisition_form';

    public function requisition()
    {
        return $this->hasMany('App\GoodsRequisitionProduct','goods_requisition_form_id','requisition_number');
    }

    public function log_goods_requisition()
    {
        return $this->hasMany('App\LogGoodsRequisitionProduct','goods_requisition_form_id','requisition_number')->orderBy('id','DESC');
    }

    
}