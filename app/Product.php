<?php

namespace App;
use App\AssetType;
use App\ProductVendor;
use App\Block;
use App\SubBlock;
use App\AssetName;
use App\SpecificationName;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected  $table= 'products';
    public $timestamps = false;


    public function asset_types(){
    	return $this->belongsTo('App\AssetType','asset_type_id');
    }


    public function product_vendors(){
    	return $this->belongsTo('App\ProductVendor','vendor_id');
    }

    public function product_block(){
    	return $this->belongsTo('App\Block','block_id');
    }

    public function product_sub_block(){
    	return $this->belongsTo('App\SubBlock','sub_block_id');
    }

    public function asset_name(){
        return $this->belongsTo('App\AssetName','asset_name_id');
    }

    public function specification_name(){
        return $this->belongsTo('App\SpecificationName','specification_name_id');
    }

    




    
}
