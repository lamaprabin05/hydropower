<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormShare extends Model
{
    protected $table = 'form_share';
    public $timestamps = false;

    public function payment_information()
    {
        return $this->hasOne('App\InstallmentInformation', 'share_form_id');
    }

}

