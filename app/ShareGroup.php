<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareGroup extends Model
{
    protected  $table = 'share_group';
    public $timestamps = false;

    protected $fillable = ['id'];



}
