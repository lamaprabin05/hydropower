<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DispatchProduct extends Model
{
    protected  $table = 'dispatch_product';

    public function product(){
        return $this->belongsTo('App\Product','product_id');
    }

    public function goods_requisition_product(){
        return $this->belongsTo('App\GoodsRequisitionProduct','goods_requisition_product_id');
    }

}
