<?php
namespace App\Helper;


use App\FormShare;
use App\GoodsRequisitionForm;
use App\GoodsPurchaseForm;
use App\GoodsRequisitionFile;
use App\InstallmentInformation;
use App\GoodsRequisitionProduct;
use Illuminate\Support\Facades\DB;

/**
 *
 */
class Tools
{

    public static  function getCount()
    {

        $payment_info = FormShare::with('payment_information')->get(['form_status', 'id', 'notification', 'proceeded_for']);
//        dd($payment_info);
        //initializing count for badge
        $count = 0;

        foreach ($payment_info as $value) {
            $total_installment = $value->payment_information->no_of_installment;
            $temp = $value->proceeded_for . '_expiry_date';

            for ($i = 1; $i <= $total_installment; $i++) {
                if ($value->notification == 1) {
                    if ($value->form_status == $i) {
                        $payment = InstallmentInformation::where('share_form_id', $value->id)->first();
                        $days_ago = date('Y-m-d', strtotime('-7 days', strtotime($payment->$temp)));

                        if (date('Y-m-d') >= $days_ago) {
                            $count = $count + 1;

                        }
                    }
                }
            }
        }

        return $count;
    }


    public static function setResponse($type,$message,$data,$meta)
    {
        if ($type=='success') {
            $error = false;
        }
        elseif($type=='fail'){
            $error = true;
        }

        $responseData = [
            'error' => $error,
            'message'=>$message,
            'data' => $data,
            'meta' => $meta
        ];
        return $responseData;

    }

    public static function getInstallmentDetail($id,$form_status){

        switch ($form_status){

            case 1:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('first_installment_amt as installment','first_expiry_date as expiry_date')->first();
                break;
            case 2:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('second_installment_amt as installment','second_expiry_date as expiry_date')->first();
                break;
            case 3:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('third_installment_amt as installment','third_expiry_date as expiry_date')->first();
                break;
            case 4:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('fourth_installment_amt as installment','fourth_expiry_date as expiry_date')->first();
                break;
            case 5:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('fifth_installment_amt as installment','fifth_expiry_date as expiry_date')->first();
                break;
            case 6:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('sixth_installment_amt as installment','sixth_expiry_date as expiry_date')->first();
                break;
            case 7:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('seventh_installment_amt as installment','seventh_expiry_date as expiry_date')->first();
                break;
            case 8:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('eighth_installment_amt as installment','eighth_expiry_date as expiry_date')->first();
                break;
            case 9:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('ninth_installment_amt as installment','ninth_expiry_date as expiry_date')->first();
                break;
            case 10:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('tenth_installment_amt as installment','tenth_expiry_date as expiry_date')->first();
                break;
            case 11:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('eleventh_installment_amt as installment','eleventh_expiry_date as expiry_date')->first();
                break;
            case 12:
                return $data = DB::table('installment_information')->where('share_form_id',$id)->select('twelveth_installment_amt as installment','twelveth_exiry_date as expiry_date')->first();
                break;
        }
    }

    public static function count_file(){
        $data['rejected'] = GoodsRequisitionForm::where('is_rejected',1)
            ->where('created_by',\Auth::user()->id)
            ->where('proceeded_to','requestor')
            ->where('form_status',0)
            ->count();

        $data['requestor'] = GoodsRequisitionForm::
        where('is_rejected',0)
            ->where('created_by',\Auth::user()->id)
            ->count();

        $data['checker'] = GoodsRequisitionForm::
        where('is_rejected',0)
            ->where('form_status',1)
            ->count();

        $data['approver'] = GoodsRequisitionForm::
            where('form_status',2)
           ->where('is_rejected',0)
           ->count();

        $data['goods_requisition_request'] = GoodsRequisitionProduct::
            where('proceed_status',1)
            ->count();

       /* $data['purchase_checker'] = GoodsPurchaseForm::
        where('is_rejected',0)
        ->where('form_status',1)
        ->where('proceeded_to','checker')
        ->count();

        $data['purchase_approver'] = GoodsPurchaseForm::
        where('is_rejected',0)
        ->where('form_status',2)
        ->where('proceeded_to','approver')
        ->count();

        $data['approve_list'] = GoodsPurchaseForm::
        where('is_rejected',0)
        ->where('form_status',3)
        ->where('proceeded_to','store')
        ->count();

        $data['goods_requisition_file'] = GoodsRequisitionFile::
        whereRaw('dispatched_qty != requested_qty')
        ->count();

        $data['purchase_rejected_files'] = GoodsPurchaseForm::
        where('is_rejected',1)
        ->count();*/
    

        return $data;
    }




}
