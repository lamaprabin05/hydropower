<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\AssetName;
use App\SpecificationName;

class GoodsRequisitionFile extends Model
{
    protected $table = 'good_requisition_files';

    public function product(){
    	return $this->belongsTo('App\Product','product_id');
    }

    public function asset_name(){
    	return $this->belongsTo('App\AssetName','product_id');
    }

    public function specification_name(){
    	return $this->belongsTo('App\SpecificationName','product_id');
    }
}
