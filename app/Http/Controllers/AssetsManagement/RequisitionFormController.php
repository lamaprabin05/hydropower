<?php

namespace App\Http\Controllers\AssetsManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PurchaseRequisitionForm;
use Auth;
use Session;

class RequisitionFormController extends Controller
{
    public function addPurchaseForm(Request $request){

       $user=Auth::user()->name;
      // dd($request->all());
    

       for($i=1;$i<=$request->number;$i++){
        $name = 'name'.$i;
        $description = 'description'.$i;
        $requisition_number = 'requisition_number'.$i;
        $cost = 'cost'.$i;
          if($request->name==null || $request->$description==null || $request->$requisition_number==null || $request->$cost==null ){
           Session::flash('error','Sorry Data is not Inserted Properly');
            return redirect()->back();
          }
          
       }

       for($i=1;$i<=$request->number;$i++){
        $name = 'name'.$i;
        $description = 'description'.$i;
        $requisition_number = 'requisition_number'.$i;
        $cost = 'cost'.$i;
        $requisition_form=new PurchaseRequisitionForm;
        $requisition_form->product_name = $request->$name;
        $requisition_form->product_description = $request->$description;
        $requisition_form->requisition_number=$request->$requisition_number;
        $requisition_form->estimated_cost = $request->$cost;
        $requisition_form->created_by = $user;
        $requisition_form->created_at = date('Y-m-d H:i:s');
        $requisition_form->save();
       }
       Session::flash('success','Successfully Inserted');
       return redirect()->back();
    }
}
