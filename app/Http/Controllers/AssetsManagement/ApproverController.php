<?php

namespace App\Http\Controllers\AssetsManagement;

use App\GoodsRequisitionForm;
use App\GoodsRequisitionProduct;
use App\GoodsRequisitionScan;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApproverController extends Controller
{
    public function index(){
        $data['form'] = GoodsRequisitionForm::where('proceeded_to','approver')->where('form_status',2)->where('is_rejected',0)->where('user_id',Auth::user()->id)->get();
        return view('assets.approver.goodsrequisition.index',$data);
    }

    public function viewGoodsRequisition($id){
    
        $data['form'] = GoodsRequisitionForm::where('id',$id)->with('requisition.product','log_goods_requisition')->first();
        $data['product_count'] = $data['form']->requisition->count();

        //for sorting them on basis of ascending order. (filtering data)
        $log = array();
        $temp = 0;
        foreach ($data['form']->log_goods_requisition as $value){
            if($data['product_count'] > $temp){
                $log[] = $value;
            }
            $temp++;
        }
        asort($log);
        $data['log'] = $log;

        //to get user from created_by field
        $user = array();
        foreach ($data['form']->log_goods_requisition as $value){
            $user_info = User::find($value->updated_by);
            $user[$value->updated_by] = $user_info->name;
        }
        $data['user'] = $user;
        return view('assets.approver.goodsrequisition.proceed_goods_requisition',$data);
    }

    public function changeGoodsRequisitionStatus(Request $request){

       
         $request->validate([
            'user_id' => 'required',
            'form_id' => 'required',
            'approver_remarks' => 'required',
        ]);

        $requisition = GoodsRequisitionForm::find($request->form_id);
        $requisition->approver_remarks=$request->approver_remarks;
        $product_requisition_number=$requisition->requisition_number;
        $requisition->form_status = 3;
        $requisition->proceeded_to='store';
        $requisition->save();

        GoodsRequisitionProduct::where('goods_requisition_form_id',$product_requisition_number)->update(['proceed_status' => 1]);
        //GoodsRequisitionProduct::where('goods_requisition_form_id',$product_requisition_number)->update(['check_status' => 1]);

        \Session::flash('success','Goods Requisition Form Proceeded to Store Successfully');
        return redirect()->route('assets.approver.view_goods_requisition',$request->form_id);
    }


    public function getRejectGoodsRequisitionForm($id){
        $data['form'] = GoodsRequisitionForm::where('id',$id)->with('requisition')->first();
        return view('assets.approver.goodsrequisition.reject_goods_requisition',$data);
    }

    public function postRejectGoodsRequisitionForm(Request $request){
        $this->validate($request,[
            'requisition_id' => 'required',
            'reject_reason' => 'required',
        ]);

        //updating for status if user changes it.
        //get total product from requisition form id.
        $data = GoodsRequisitionForm::where('id',$request->requisition_id)->with('requisition')->first();
        $product_count = $data->requisition->count();

        for($i = 1; $i<=$product_count; $i++)
        {
            $switch = 'switch'.$i;
            $product = 'product_id'.$i;
            $goods_requisition_product = GoodsRequisitionProduct::find($request->$product);

            if(isset($request->$switch)){
                $goods_requisition_product->is_rejected = 0;
            }
            else{
                $goods_requisition_product->is_rejected = 1;
            }

            $goods_requisition_product->save();
        }

        $data->is_rejected = 1;
        $data->rejected_reason = $request->reject_reason;

        $data->proceeded_to = 'requestor';
        $data->form_status = 0;

        $data->updated_at = date('Y-m-d H:i:s');
        $data->updated_by = \Auth::user()->id;
        $data->save();

        return redirect()->route('assets.approver.get_all_goods_requisition');
    }
}
