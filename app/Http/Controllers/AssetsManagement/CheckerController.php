<?php

namespace App\Http\Controllers\AssetsManagement;

use App\GoodsRequisitionForm;
use App\GoodsRequisitionProduct;
use App\GoodsRequisitionScan;
use App\LogGoodsRequisitionProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Auth;
use App\User;

class CheckerController extends Controller
{
    public function index(){
        $data['form'] = GoodsRequisitionForm::where('proceeded_to','checker')->where('form_status',1)->where('is_rejected',0)->where('user_id',Auth::user()->id)->get();
//        dd($data);
        return view('assets.checker.goodsrequisition.index',$data);
    }

    public function viewGoodsRequisition($id){
        $data['form'] = GoodsRequisitionForm::where('id',$id)->with('requisition.product')->first();
        $data['approver_list']=User::where('user_designation',3)->get();
//       dd($data);
        return view('assets.checker.goodsrequisition.proceed_goods_requisition',$data);
    }

    //change form status and procced it to approver.
    public function changeGoodsRequisitionStatus(Request $request){

        
        $request->validate([
            'user_id' => 'required',
            'form_id' => 'required',
            'checker_remarks' => 'required',
        ]);

        $requisition = GoodsRequisitionForm::find($request->form_id);
        $requisition->form_status = 2;
        $requisition->proceeded_to='approver';
        //for user approver logged checked
        $requisition->user_id=$request->user_id;

        $requisition->approver_id=$request->user_id;
        $requisition->checker_remarks=$request->checker_remarks;
        $requisition->save();

        \Session::flash('success','Goods Requisition Form Proceeded to Approver Successfully');
        return redirect()->route('assets.checker.view_goods_requisition',$request->form_id);
    }


    public function getEditGoodsRequisitionForm($id){

        $data['form'] = GoodsRequisitionForm::where('id',$id)->with('requisition')->first();
        $data['product_list']=Product::get();
//        dd($data);

        return view('assets.checker.goodsrequisition.edit_goods_requisition',$data);
    }

    public function editGoodsRequisition(Request $request){
//        dd($request->all());

        $this->validate($request,[
            'requisition_id' => 'required',
            'purpose' => 'required',
        ]);

        //get total product from requisition form id.
        $data = GoodsRequisitionForm::where('id',$request->requisition_id)->with('requisition')->first();
        $product_count = $data->requisition->count();

        //validating each product.
        for($i = 1; $i<=$product_count; $i++)
        {
            $this->validate($request,[
                'name'.$i => 'required',
                'description'.$i => 'required',
                'quantity'.$i => 'required',
                'requirement_date'.$i => 'required',
            ]);
        }

        //update into goods_requisition_form table.
        $data = GoodsRequisitionForm::where('id',$request->requisition_id)->with('requisition')->first();

        if($request->purpose == 'others'){
            $data->purpose = $request->others;
        }
        else {
            $data->purpose = $request->purpose;
        }

        $data->reason = $request->reason;

        $data->form_status = 1;
        $data->proceeded_to = 'checker';

        $data->updated_at = date('Y-m-d H:i:s');
        $data->updated_by = \Auth::user()->id;

        $data->save();

        //saving log data for goods requisition product before update
        for($i = 1; $i<=$product_count; $i++)
        {
            $product_id = 'goods_requisition_product_id'.$i;

            $goods = GoodsRequisitionProduct::find($request->$product_id);
            $log_goods = new LogGoodsRequisitionProduct();
            $log_goods->goods_requisition_product_id = $goods->id;

            $log_goods->goods_requisition_form_id = $goods->goods_requisition_form_id;
            $log_goods->name = $goods->name;

            $log_goods->description = $goods->description;

            $log_goods->quantity = $goods->quantity;
            $log_goods->requirement_date = $goods->requirement_date;

            $log_goods->is_rejected = $goods->is_rejected;

            $log_goods->updated_by = \Auth::user()->id;
            $log_goods->updated_at = date('Y-m-d H:i:s');

            $log_goods->save();

        }

        // update goods_requisition_product.
        for($i = 1; $i<=$product_count; $i++)
        {
            $name = 'name'.$i;
            $description = 'description'.$i;
            $quantity = 'quantity'.$i;
            $requirement_date = 'requirement_date'.$i;
            $grp_id = 'goods_requisition_product_id'.$i;

            /*//this is from store product id.
            $product_id = 'product_id'.$i;*/


            //for switch
            $switch = 'switch'.$i;

            $goods = GoodsRequisitionProduct::find($request->$grp_id);

            if(isset($request->$switch)){
                $goods->is_rejected = 0;
            }
            else{
                $goods->is_rejected = 1;
            }

            /*if($request->$product_id != 0){
                $store_product = Product::find($request->$product_id);
                $goods->name = $store_product->name;
                $goods->product_id = $request->$product_id;
            }*/

            $goods->name = $request->$name;

            $goods->description = $request->$description;

            $goods->quantity = $request->$quantity;
            $goods->requirement_date = $request->$requirement_date;

            $goods->save();

        }

        \Session::flash('success','Goods Requisition Form Edited Successfully');
        return redirect()->route('assets.checker.view_goods_requisition',$data->id);
    }

    public function getRejectGoodsRequisitionForm($id){
        $data['form'] = GoodsRequisitionForm::where('id',$id)->with('requisition')->first();
        return view('assets.checker.goodsrequisition.reject_goods_requisition',$data);
    }

    public function postRejectGoodsRequisitionForm(Request $request){
//        dd($request->all());

        $this->validate($request,[
            'requisition_id' => 'required',
            'reject_reason' => 'required',
        ]);

        //updating info from switch
        //getting count number for forloop.
        $goods_requisition_form = GoodsRequisitionForm::where('id',$request->requisition_id)->with('requisition')->first();
        $product_count = $goods_requisition_form->requisition->count();

        for($i = 1; $i<=$product_count; $i++)
        {
            $switch = 'switch'.$i;
            $product = 'product_id'.$i;
            $goods_requisition_product = GoodsRequisitionProduct::find($request->$product);

            if(isset($request->$switch)){
                $goods_requisition_product->is_rejected = 0;
            }
            else{
                $goods_requisition_product->is_rejected = 1;
            }

            $goods_requisition_product->save();
        }
        $data = GoodsRequisitionForm::where('id',$request->requisition_id)->with('requisition')->first();
        $data->is_rejected = 1;
        $data->rejected_reason = $request->reject_reason;

        $data->proceeded_to = 'requestor';
        $data->form_status = 0;

        $data->updated_at = date('Y-m-d H:i:s');
        $data->updated_by = \Auth::user()->id;
        $data->save();

        \Session::flash('warning','Goods Requisition Form Rejected and proceeded to Requestor');
        return redirect()->route('assets.checker.get_all_goods_requisition');
    }
}
