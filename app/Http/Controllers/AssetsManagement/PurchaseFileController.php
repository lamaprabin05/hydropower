<?php

namespace App\Http\Controllers\AssetsManagement;

use App\GoodsPurchaseForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Product;
use Session;

class PurchaseFileController extends Controller
{
    public function purchaseFileList(){

        $data['file_list']=GoodsPurchaseForm::where('proceeded_to','requestor')->where('form_status',1)->with('file_products')->get();
//       dd($data);
        return view('assets.PurchaseFile.purchasefile',$data);
    }

    public function purchaseFileView($id){
//        dd($id);
        $data['file_view']=GoodsPurchaseForm::where('id',$id)->where('form_status',1)->with('file_products')->first();
//        dd($data);
        return view('assets.PurchaseFile.purchasefileview',$data);
    }

    public function purchaseFileEdit($id){

        $data['file_edit']=GoodsPurchaseForm::where('id',$id)->where('form_status',1)->with('file_products')->first();
        $data['product_list']=Product::get();
        return view('assets.PurchaseFile.purchasefileupdate',$data);
    }

    public function purchaseFileUpdate(Request $request){
       $goods_purchase_form=GoodsPurchaseForm::where('id',$request->goods_purchase_form_id)->first();

      if($request->product_name==null){
          $goods_purchase_form->product_name=null;
          $goods_purchase_form->product_id=$request->product_id;
      }

      if($request->product_id==null){
          $goods_purchase_form->product_name=$request->product_name;
          $goods_purchase_form->product_id=null;
      }

       $goods_purchase_form->product_description=$request->product_description;
       $goods_purchase_form->product_qty=$request->product_qty;
       $goods_purchase_form->estimated_cost=$request->estimated_cost;
       $goods_purchase_form->save();
       return redirect()->back();

    }


    public function purchaseFileDelete(Request $request){
       $delete =  GoodsPurchaseForm::where('id',$request->id)->delete();
    }

    public function purchaseFileScan(Request $request){
//        dd($request->all());
      $list_value=$request->list_value;
      $date=date('y_m_d_h_i_s');
      foreach($list_value as $value){
              if($request->purchase_file_scan){
            $path = $request->file('purchase_file_scan')->store('files/assets/purchase/purchase/'.$date,'public');
              }
        $goods_purchase_form=GoodsPurchaseForm::where('id',$value)->first();
        $goods_purchase_form->purchase_file_scan=$path;
        $goods_purchase_form->form_status=1;
        $goods_purchase_form->save();
        
       }
        
       return redirect()->route('store.purchase_file_list');
     }

    public function purchaseFileCheckList(Request $request){
//    dd($request->all());
      $id_check_list=$request->check_list;

      if($id_check_list==null){
        Session::flash('error','Sorry no items are checked');
        return redirect()->back();
      }
       foreach($id_check_list as $element){
           $good_purchase_form=GoodsPurchaseForm::where('id',$element)->first();
           $good_purchase_form->proceeded_to='checker';
           $good_purchase_form->form_status=2;
           $good_purchase_form->save();
       }
      return redirect()->back();
    }

    public function purchaseRejectedFile(){
      
        $data['purchase_rejected_file']=GoodsPurchaseForm::where('rejected_status',1)->with('user')->get();

        return view('assets.PurchaseReject.purchase_rejected_file_list',$data);
    }


}
