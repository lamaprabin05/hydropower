<?php

namespace App\Http\Controllers\AssetsManagement;

use Illuminate\Http\Request;
use Auth;
use Session;

use App\Http\Controllers\Controller;
use App\GoodsRequisitionProduct;
use App\GoodsPurchaseForm;
use App\User;

class PurchaseRequisitionController extends Controller
{

    public function purchaseForm($id){
        //data will be fetched via post method whether same specification name is required or different needed
        $data['product'] = GoodsRequisitionProduct::with('asset_name','specification_name')->find($id);
        $data['checker_list']=User::where('user_designation',2)->get();
        return view('assets.store.purchaserequisition.purchase_requisition_form',$data);
    }

    /*public function purchaseForm1(Request $request){*/
    public function addPurchaseForm(Request $request){
        $request->validate([
            'goods_requisition_product_id' => 'required',
            'quantity' => 'required',
            'requestor_remarks' => 'required',
            'user_id' => 'required',
        ]);


        $goods_requisition = GoodsRequisitionProduct::with('asset_name','specification_name')->find($request->goods_requisition_product_id);

        $requisition_form= new GoodsPurchaseForm;


        $requisition_form->goods_requisition_product_id=$request->goods_requisition_product_id;
        $requisition_form->goods_requisition_number=$goods_requisition->goods_requisition_form_id;
        $requisition_form->assets_name=$goods_requisition->asset_name->asset_name;
        $requisition_form->specification_name=$goods_requisition->specification_name->specification_name;

        $requisition_form->quantity=$request->quantity;
        $requisition_form->user_id=$request->user_id;
        $requisition_form->requisition_by= \Auth::user()->id;

        $requisition_form->requestor_remarks=$request->requestor_remarks;

        $requisition_form->proceeded_to='checker';
        $requisition_form->form_status=2;

        $requisition_form->save();

        //update goods requisition product table so that purchase order cannot be twice.
        $goods_requisition->purchase_requisition = 1;
        $goods_requisition->save();
        return redirect()->route('assets.store.get_all_goods_requisition');

    }


    public function showAllPurchaseRequisition(){
        $data['files'] = GoodsPurchaseForm::orderBy('id','desc')->get();
        return view('assets.store.purchaserequisition.index',$data);
    }

    public function viewPurchaseRequisition($id){
        $data['files'] = GoodsPurchaseForm::with('requisition_user','checker_user','approver_user')->find($id);
//        dd($data);
        return view('assets.store.purchaserequisition.view_purchase_requisition',$data);

    }
}
