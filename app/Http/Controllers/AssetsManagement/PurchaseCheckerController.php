<?php

namespace App\Http\Controllers\AssetsManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GoodsPurchaseForm;
use App\Product;
use App\User;
use Auth;

class PurchaseCheckerController extends Controller

{
    public function index(){

        $data['goods_purchase_form']=GoodsPurchaseForm::where('form_status',2)->where('proceeded_to','checker')->where('rejected_status',0)->where('user_id',Auth::user()->id)->get();
        return view('assets.checker.purchaserequisition.index',$data);
    }

  public function viewPurchaseFile($id){
        $data['goods_purchase_form']=GoodsPurchaseForm::where('form_status',2)->where('proceeded_to','checker')->where('rejected_status',0)->where('user_id',Auth::user()->id)->where('id',$id)->first();
        $data['approver_list']=User::where('user_designation',3)->get();

        return view('assets.checker.purchaserequisition.view_purchase_requisition',$data);
    }

    public function purchaseFileEdit($id){

        $data['purchasing_edit']=GoodsPurchaseForm::where('form_status',2)->where('proceeded_to','checker')->where('id',$id)->where('rejected_status',0)->first();
        // dd($data);
        $data['product_list']=product::get();
        return view('assets.checker.purchaserequisition.purchaseFileEdit',$data);
    }

    public function updatePurchaseFile(Request $request){
       
     
         $request->validate([
            'goods_purchase_form_id' => 'required',
            'quantity' => 'required',
        ]);
         
        $requisition_form=GoodsPurchaseForm::where('form_status',2)->where('proceeded_to','checker')->where('id',$request->goods_purchase_form_id)->where('rejected_status',0)->first();
        $requisition_form->quantity=$request->quantity;
        $requisition_form->save();
        return redirect()->back();
    }

    public function rejectPurchaseFile(Request $request){
        $reject=GoodsPurchaseForm::where('id',$request->purchase_form_id)->where('rejected_status',0)->first();
        $reject->rejected_status=1;
        $reject->rejected_by=$request->user_id;
        $reject->rejected_reason=$request->reject_reason;

        $reject->form_status = 1;
        $reject->proceeded_to = 'store';

        $reject->save();
        return redirect()->route('assets.checker.purchase_requisition');
    }

    public function proceedPurchaseFile(Request $request){
      $request->validate([
          'purchase_form_id' => 'required',
          'checker_remarks' => 'required',
          'approver_id' => 'required',
        ]);


        $approve=GoodsPurchaseForm::where('id',$request->purchase_form_id)->first();
        $approve->checked_by = \Auth::user()->id;
        $approve->user_id=$request->approver_id;
        $approve->checker_remarks=$request->checker_remarks;
        $approve->proceeded_to='approver';
        $approve->form_status=3;
        $approve->save();
        return redirect()->route('assets.checker.purchase_requisition');
    }

}
