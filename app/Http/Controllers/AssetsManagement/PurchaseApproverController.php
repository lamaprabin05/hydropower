<?php

namespace App\Http\Controllers\AssetsManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GoodsPurchaseForm;
use App\Product;
use App\User;
use Auth;
use mysql_xdevapi\Session;


class PurchaseApproverController extends Controller
{
    public function fetchGoodsPurchaseRequisition(){
        $data['goods_purchase_form']=GoodsPurchaseForm::where('form_status',3)->where('proceeded_to','approver')->where('rejected_status',0)->where('user_id',Auth::user()->id)->get();
       
        return view('assets.approver.purchaserequisition.index',$data);
    }

   public function viewPurchaseRequisiton($id){

   		$data['viewGoodsPurchaseRequisiton']=GoodsPurchaseForm::where('id',$id)->where('proceeded_to','approver')->where('form_status',3)->where('rejected_status',0)->where('user_id',Auth::user()->id)->first();

        return view('assets.approver.purchaserequisition.view_purchase_requisition',$data);
   }

   public function editGoodsPurchaseRequisiton($goods_requisition_product_id){
      
   	    $data['editGoodsPurchaseRequisiton']=GoodsPurchaseForm::where('id',$goods_requisition_product_id)->where('proceeded_to','approver')->where('form_status',3)->where('rejected_status',0)->first();

        return view('assets.approver.purchaserequisition.purchaseFileEdit',$data);
   }

   public function updateGoodsPurchaseRequisiton(Request $request){
    
        $update=GoodsPurchaseForm::where('id',$request->goods_purchase_form_id)->where('proceeded_to','approver')->where('form_status',3)->where('rejected_status',0)->first();
        $update->quantity=$request->quantity;
        $update->save();

       return redirect()->back();
    }


   public function rejectPurchaseRequisition(Request $request){


//        if($request->reject_reason == null){
//            \Session::flash('error','Reject Reason is empty');
//            return redirect()->back();
//
//        }
        $reject=GoodsPurchaseForm::where('id',$request->purchase_form_id)->with('user')->first();
        $reject->rejected_status=1;
        $reject->rejected_by= \Auth::user()->id;
        $reject->rejected_reason=$request->reject_reason;
        $reject->save();
        return redirect()->route('assets.approver.fetch_goods_purchase_requisition');
    }

    public function proceedPurchaseRequisition(Request $request){
        $request->validate([
            'purchase_form_id' => 'required',
            'approver_remarks' => 'required',
        ]);

//        $proceed = GoodsPurchaseForm::where('id',$request->purchase_form_id)->first();

        $proceed = GoodsPurchaseForm::where('id',$request->purchase_form_id)->first();
        $proceed->approver_remarks=$request->approver_remarks;
        $proceed->user_id=$proceed->requisition_by;

        $proceed->approved_by= \Auth::user()->id;
        $proceed->proceeded_to='store';
        $proceed->form_status=4;
        $proceed->save();

        return redirect()->route('assets.approver.fetch_goods_purchase_requisition');

    }
    
}
