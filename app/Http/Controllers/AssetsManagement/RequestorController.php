<?php

namespace App\Http\Controllers\AssetsManagement;

use App\GoodsRequisitionForm;
use App\GoodsRequisitionProduct;
use App\GoodsRequisitionScan;
use App\AssetName;
use App\Helper\Tools;
use App\LogGoodsRequisitionProduct;
use App\Product;
use App\RequisitionForm;
use App\SpecificationName;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Validator;
use Session;

class RequestorController extends Controller
{
    public  function getRequisitionForm(){

        $data['product'] = Product::with('asset_name')->distinct()->get(['asset_name_id']);
        // $data['product'] = Product::with('asset_name','specification_name')->get();
        //here asset_name only should be passed as ueser should ask specification by description.
        //added for specific user checker
        $data['checker_list']=User::where('user_designation',2)->get();
        return view('assets.requestor.goodsrequisition.goods_requisition_form_old',$data);
    }

    public  function storeRequisitionForm(Request $request){

        $request->validate([
            'purpose' => 'required',
            'reason' => 'required',
            'proceeded_user_id' => 'required',
        ]);


        if($request->counter == 1){
            return redirect()->back()->withErrors(['error_empty' => 'Please Insert At Least One Product']);
        }



       //validating empty field
        for($i=1;$i<$request->counter;$i++){
  
            $asset_name_id = 'asset_name_id'.$i;
            $manual_asset_name = 'name'.$i;

            if(isset($request->$asset_name_id)){
                if(empty($request->$asset_name_id)){
                    Session::flash('error','Sorry Data is not Inserted Properly');
                    return redirect()->back();
                }

            }
            else{
                if(empty($request->$manual_asset_name)){
                    Session::flash('error','Sorry Data is not Inserted Properly');
                    return redirect()->back();
                }
            }



            $description = 'description'.$i;
            $quantity = 'quantity'.$i;
            $requirement_date = 'requirement_date'.$i;

              if( $request->$description==null || $request->$quantity==null || $request->$requirement_date==null ){
                Session::flash('error','Sorry Data is not Inserted Properly');
                return redirect()->back();
              }

       }


       /* if($request->name0 || $request->)*/

        //insert into goods_requisition_form table.
        $data = new GoodsRequisitionForm();


        if($request->purpose == 'others'){
            $data->purpose = $request->others;
        }
        else {
            $data->purpose = $request->purpose;
        }

        $data->reason = $request->reason;

        $data->is_rejected = 0;

        $data->form_status = 1;
        $data->proceeded_to = 'checker';
       //added for specific user checker
        $data->user_id = $request->proceeded_user_id;
        $data->checker_id = $request->proceeded_user_id;

        $data->created_at = date('Y-m-d H:i:s');
        $data->created_by = Auth::user()->id;
        $data->save();

        //adding unique id to requisition number
        $form = GoodsRequisitionForm::find($data->id);
        $form->requisition_number = date('ydjmh').$form->id;
        $form->save();

        // insert into goods_requisition_product.
        for($i=1; $i<$request->counter; $i++){

            $asset_name_id = 'asset_name_id'.$i;
            $manual_asset_name = 'name'.$i;

            $description = 'description'.$i;
            $quantity = 'quantity'.$i;
            $requirement_date = 'requirement_date'.$i;

            $goods = new GoodsRequisitionProduct();


            if(isset($request->$asset_name_id)){
                //retrieve assets name
                $asset_name = AssetName::find($request->$asset_name_id);

                $goods->name=$asset_name->asset_name;
                $goods->asset_name_id=$request->$asset_name_id;
            }
            else{
                $goods->name=$request->$manual_asset_name;
            }


            $goods->goods_requisition_form_id =$form->requisition_number;
            $goods->description = $request->$description;
            $goods->quantity = $request->$quantity;
            $goods->requirement_date = $request->$requirement_date;
            $goods->save();
        }

        \Session::flash('success','Goods Requisition Form Uploaded Successfully');
        return redirect()->route('assets.requestor.view_all_requisition',$form->id);
    }


    public function getAllRequisitionFile(Request $request){
        $data['form'] = GoodsRequisitionForm::where('created_by',Auth::user()->id)->where('is_rejected',0)->get();
        return view('assets.requestor.goodsrequisition.files',$data);

    }

    public function viewRequisitionFile($id){

        // dd($id);
        $data['form'] = GoodsRequisitionForm::where('id',$id)->with('requisition.product','log_goods_requisition')->first();
        $data['product_count'] = $data['form']->requisition->count();

        //for sorting them on basis of ascending order. (filtering data)
        $log = array();
        $temp = 0;
        foreach ($data['form']->log_goods_requisition as $value){
            if($data['product_count'] > $temp){
                $log[] = $value;
            }
            $temp++;
        }
        asort($log);
        $data['log'] = $log;

        $user = array();
        foreach ($data['form']->log_goods_requisition as $value){
            $user_info = User::find($value->updated_by);
            $user[$value->updated_by] = $user_info->name;
        }
        $data['user'] = $user;

        return view('assets.requestor.goodsrequisition.view_requisition',$data);
    }

    public function editRequisitionFile($id){
        $data['form'] = GoodsRequisitionForm::where('id',$id)->with('requisition.product')->first();
        $data['product_list']=Product::get();
        return view('assets.requestor.goodsrequisition.edit_requisition',$data);
    }

    //using both product as well as goods requistiion to update and used by two views: rejected one and edit one from requestor.
    public function updateRequisitionFile(Request $request){
        $this->validate($request,[
            'requisition_id' => 'required',
            'purpose' => 'required',
        ]);

        //get total product from requisition form id.
        $data = GoodsRequisitionForm::where('id',$request->requisition_id)->with('requisition')->first();
        $product_count = $data->requisition->count();

        //validating each product.
        for($i = 1; $i<=$product_count; $i++)
        {
            $this->validate($request,[
                //product id
//                'product_id'.$i => 'required',

                //table: goods_requisition_product data types
                'goods_requisition_product_id'.$i => 'required',
                'name'.$i => 'required',
                'description'.$i => 'required',
                'quantity'.$i => 'required',
                'requirement_date'.$i => 'required',
            ]);
        }
        //update into goods_requisition_form table.
        $data = GoodsRequisitionForm::where('id',$request->requisition_id)->with('requisition')->first();

        if($request->purpose == 'others'){
            $data->purpose = $request->others;
        }
        else {
            $data->purpose = $request->purpose;
        }

        $data->reason = $request->reason;

        if($request->action == 'reject_redirect'){
            $data->form_status = 1;
            $data->proceeded_to = 'checker';
            $data->is_rejected = 0;
            $data->rejected_reason = null;
        }

        $data->updated_at = date('Y-m-d H:i:s');
        $data->updated_by = \Auth::user()->id;

        $data->save();

        // update goods_requisition_product.
        for($i = 1; $i<=$product_count; $i++)
        {
            $name = 'name'.$i;
            $description = 'description'.$i;
            $quantity = 'quantity'.$i;
            $requirement_date = 'requirement_date'.$i;
            $grp_id = 'goods_requisition_product_id'.$i;

            //this is from store product id.


            $goods = GoodsRequisitionProduct::find($request->$grp_id);


//            if($request->$product_id != 0){
//            $product_id = 'product_id'.$i;
//                $store_product = Product::find($request->$product_id);
//                $goods->name = $store_product->name;
//                $goods->product_id = $request->$product_id;
//            }
            $goods->name = $request->$name;
            $goods->description = $request->$description;

            $goods->quantity = $request->$quantity;
            $goods->requirement_date = $request->$requirement_date;
            $goods->is_rejected = 0;

            $goods->save();

        }

        \Session::flash('success','Goods Requisition Updated Successfully');

        //if  submitted from rejected form
//        if($request->action == 'reject_redirect'){
//            return redirect()->route('')
//        }
//        else{
//            return redirect()->back();
//        }
        return redirect()->back();

    }

    //ajax delete
    public function deleteRequisitionFile(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse('fail', 'Missing Parameter', '', '');
            $status = 200;

            //have to be 422 unprocessable untity but not working now.
            return response()->json($responseData,$status);
        }

        $requisition = GoodsRequisitionForm::where('id',$request->id)->with('requisition')->first();

        GoodsRequisitionProduct::where('goods_requisition_form_id',$requisition->requisition_number)->delete();

        GoodsRequisitionForm::where('id',$request->id)->delete();


        $responseData = Tools::setResponse('success', 'Requisition File Deleted Successfully', '', '');
        $status = 200;

        return response($responseData,$status);
    }

    //get all rejected requisition files.
    public function getAllRejectedRequisitionFile(){
        $data['form'] = GoodsRequisitionForm::where('proceeded_to','requestor')
            ->where('form_status',0)
            ->where('is_rejected',1)
            ->where('proceeded_to','requestor')
            ->get();
//        dd($data);
        return view('assets.requestor.goodsrequisition.rejected_requisition',$data);
    }

    //get rejected requisition file by id.
    public function getRejectedRequisitionFile($id){
        $data['form'] = GoodsRequisitionForm::where('id',$id)->with('requisition.product')->first();
        $data['rejected_user'] = User::find($data['form']->updated_by);
        $data['product_list']=Product::get();
//        dd($data);
        return view('assets.requestor.goodsrequisition.view_rejected_requisition',$data);
    }
}
