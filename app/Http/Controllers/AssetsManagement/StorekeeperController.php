<?php

namespace App\Http\Controllers\AssetsManagement;

use App\AssetName;
use App\DispatchProduct;
use App\GoodsPurchaseForm;
use App\GoodsRequisitionForm;
use App\GoodsRequisitionProduct;
use App\GoodsRequisitionFile;
use App\LogPurchaseApprovedProduct;
use App\Product;
use App\ProductLog;
use App\ProductVendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;

class StorekeeperController extends Controller
{
    public function index(){
        $data['goods_requisition_product'] = GoodsRequisitionProduct::where('proceed_status',1)
                                                ->where('is_rejected',0)
                                                ->where('quantity','!=',0)
                                                ->with('asset_name','asset_name.specification')
                                                ->get();

        $data['product_name']=Product::with('asset_name','specification_name')->get();
        $data['asset_name'] = AssetName::with('specification')->get();

        //old
//        $data['goods_requisition_product'] = GoodsRequisitionProduct::where('proceed_status',1)->where('is_rejected',0)->get();
        return view('assets.store.goodsrequisition.index',$data);
    }

    //proceed to dispatch form
    public function proceedGoodsRequisitonForm(Request $request){

        $validation = Validator::make($request->all(),[
            'product_id' => 'required',
            'token' => 'required',
            'specification_id' => 'required',
        ]);

        if($validation->fails()){
            \Session::flash('error','Please Fill All Forms');
            return redirect()->back();


        }

        if($request->token == "auto"){
            $validation = Validator::make($request->all(),[
                'specification_id' => 'required',
            ]);
        }
        else{
            $validation = Validator::make($request->all(),[
                'asset_name_id' => 'required',
                'specification_id' => 'required',
            ]);
        }

        if($validation->fails()){
            \Session::flash('error','Please Fill All Forms');
            return redirect()->back();


        }
            $product = GoodsRequisitionProduct::find($request->product_id);
            $product->specification_name_id = $request->specification_id;


            if($request->token == 'manual'){
                $product->asset_name_id = $request->asset_name_id;
            }
            $product->save();

            return redirect()->route('store.get_proceed_form',$request->product_id);
    }

    public function dispatchGoodsRequest(Request $request){

        $request->validate([
            'store_id' => 'required',
            'dispatch_quantity' => 'required',
            'requested_quantity' => 'required',
            'goods_requisition_product_id' => 'required'
        ]);

//        update store quantity
        $store=Product::where('id',$request->store_id)->first();


        //check if requested quantity is equal to dispathed quantity
        if(($request->requested_quantity < $request->dispatch_quantity)){
            return redirect()->back()->withErrors(['dispatch_quantity' => 'Sorry ! You are dispatching more than requested']);
        }

        //check if dispathed quantity is available in stock or not.
        if(($request->dispatch_quantity > $store->quantity)){
            return redirect()->back()->withErrors(['dispatch_quantity' => 'Sorry ! You are dispatching more than in store']);
        }



        //before update store quantity keep log data into product_log
        $product_log = new ProductLog();

        $product_log->product_id = $store->id;
        $product_log->batch_id = $store->batch_id;

        $product_log->batch_number = $store->batch_number;
        $product_log->asset_type_id = $store->asset_type_id;
        $product_log->block_id = $store->block_id;

        $product_log->sub_block_id = $store->sub_block_id;
        $product_log->asset_name_id = $store->asset_name_id;
        $product_log->specification_name_id = $store->specification_name_id;
        $product_log->vendor_id = $store->vendor_id;

        $product_log->unit = $store->unit;
        $product_log->unit_cost = $store->unit_cost;
        $product_log->quantity = $store->quantity;
        $product_log->amount = $store->amount;

        $product_log->discount = $store->discount;
        $product_log->vat = $store->vat;
        $product_log->expected_life = $store->expected_life;
        $product_log->depreciation_method = $store->depreciation_method;

        $product_log->depreciation_value = $store->depreciation_value;
        $product_log->residual_price = $store->residual_price;
        $product_log->bill_issue_date = $store->bill_issue_date;
        $product_log->ready_to_use_date = $store->ready_to_use_date;

        $product_log->image = $store->image;
        $product_log->bill = $store->bill;
        $product_log->created_at = $store->created_at;
        $product_log->nepali_date = $store->nepali_date;

        $product_log->save();

        $store->quantity=$store->quantity - $request->dispatch_quantity;
        $store->save();

        $product=GoodsRequisitionProduct::where('id',$request->goods_requisition_product_id)->first();
        $product->quantity = $product->quantity - $request->dispatch_quantity;
        $product->save();

        //after dispatch save the dispatched information to dispatch table.
            $dispatch = new DispatchProduct();
            $dispatch->product_id = $request->store_id;
            $dispatch->goods_requisition_product_id = $request->goods_requisition_product_id;

            $dispatch->dispatched_qty = $request->dispatch_quantity;
            $dispatch->save();

        return redirect()->route('store.get_proceed_form',$request->goods_requisition_product_id);
    }

    public function getAssetNameId(Request $request){
        $product_id=AssetName::where('asset_name',$request->asset_name)->pluck('id');
        $data['product_list']=Product::with('asset_name','specification_name')->get();

        return view('assets.store.goodsrequisition.index',$data);
    }

//    goods requisition form after proceed.. show dispatched form with other info
    public function getProceedForm($id){
        $product = GoodsRequisitionProduct::find($id);
        $store = Product::where('asset_name_id',$product->asset_name_id)->where('specification_name_id',$product->specification_name_id)->where('quantity','>',0)->with('asset_name','specification_name')->first();
//        $info = Product::where(;
//        dd($store);

        $data['store'] = $store;
        $data['product'] =$product;

        //if  required quantity is 0
        if($product->quantity == 0){
            return redirect()->route('store.get_all_goods_requisition');
        }
        else{
            return view('assets.store.goodsrequisition.dispatch_goods_requisition',$data);
        }
    }
    
    public function goodsRequisitionFile(Request $request){
            $goods_requisition_product=GoodsRequisitionProduct::where('id',$request->product_requisition_product_id)->first();
            $goods_requisition_product->proceed_status=0;
            $goods_requisition_product->save();

            $goods_requisition_file=new GoodsRequisitionFile;
            $goods_requisition_file->product_requisition_number=$request->product_requisition_number;
            $goods_requisition_file->product_id=$request->product_id;
            $goods_requisition_file->requested_qty=$request->product_quantity;
            $goods_requisition_file->dispatched_qty=0;
            $goods_requisition_file->required_date=$request->product_required_date;
            $goods_requisition_file->save();
     		// return redirect()->route('store.goods_requisition_file_status');
            return redirect()->back();
    }

    public function goodsRequisitionFileStatus(){

        $data['goods_requisition_file'] = GoodsRequisitionFile::where('dispatch_status',0)
            ->whereRaw('dispatched_qty != requested_qty')
            ->with('product.asset_name','product.specification_name')->get();
//        dd($data);
         return view('assets.store.goodsrequisitionstatus.index',$data);
    }

    public function goodsDispatch(Request $request){
        // dd($request->all());
       $request->validate([  
            'product_id' => 'required',
            'dispatched_qty' => 'required',
            'requisition_file_id' => 'required',
            'pending_qty' => 'required',

        ]);

       $product=Product::where('id',$request->product_id)->first();
        
       if(($request->dispatched_qty > $request->pending_qty)){
            Session::flash('error','Sorry You are dispatching more than required');
            return redirect()->back();
        }

        if(($product->quantity-$request->dispatched_qty)<0){
            Session::flash('error','Sorry You are dispatching more than available quantity');
            return redirect()->back();
        }
        
        $product->quantity=$product->quantity - $request->dispatched_qty;
        $product->save();

        $goodRequisitionFile=GoodsRequisitionFile::where('id',$request->requisition_file_id)->first();
        $goodRequisitionFile->dispatched_qty=$goodRequisitionFile->dispatched_qty+$request->dispatched_qty;
        $goodRequisitionFile->save();
        return redirect()->back();
    }

    public function purchaseApprovedList(){
        $data['purchaseApprovedList']=GoodsPurchaseForm::where('proceeded_to','store')->where('form_status',3)->where('rejected_status',0)->get();
        $data['vendor_list']=ProductVendor::get();
        return view('assets.purchaseApproveList.index',$data);
    }

    public function purchaseApprovedProductUpdate(Request $request){

        $request->validate([
            'product_unit_cost' => 'required',
            'product_qty' => 'required',
            'product_id' => 'required',
            'product_estimated_cost' => 'required',
            'goods_purchase_form_id' => 'required',
            'product_image' => 'required|max:100000|mimes:jpeg,png,jpg',
            'product_bill_image' => 'required|max:100000|mimes:jpeg,png,jpg',
            'vendor_id' => 'required'
        ]);

        $goods_requisition_file=GoodsRequisitionFile::where('id',$request->goods_requisition_file_id)->first();
        $goods_requisition_file->request_check_status=0;
        $goods_requisition_file->save();

        $approved_log=new LogPurchaseApprovedProduct;
        $approved_log->product_id=$request->product_id;
        $approved_log->vendor_id=$request->vendor_id;
        $approved_log->product_unit_cost=$request->product_unit_cost;
        $approved_log->product_qty=$request->product_qty;
        $approved_log->product_estimated_cost=$request->product_estimated_cost;

        $date=date('Y_m_d');

        if ($request->has('product_bill_image')){
            $path = $request->file('product_bill_image')->store('files/assets/approved_products/bill_image/'.$date,'public');
            $approved_log->product_bill_image= $path;
        }else{
            $approved_log->product_bill_image= '';

        }
        if ($request->has('product_image')){
            $path = $request->file('product_image')->store('files/assets/approved_products/product_image/'.$date,'public');
            $approved_log->product_image = $path;
        }else{
            $approved_log->product_image = '';
        }
        $approved_log->save();
        $goods_purchase_form=GoodsPurchaseForm::where('id',$request->goods_purchase_form_id)->where('form_status',3)->where('proceeded_to','store')->first();
        $goods_purchase_form->form_status=4;
        $goods_purchase_form->save();

        if(!empty($request->product_id)){
            $product=Product::where('id',$request->product_id)->first();
            $product->unit_cost=$request->product_unit_cost;
            $product->quantity=$request->product_qty;
            $product->amount=$request->product_estimated_cost;
            $product->vendor_id=$request->vendor_id;
            $product->save();
        }
       return redirect()->route('store.goods_requisition_file_status');
    }
}
