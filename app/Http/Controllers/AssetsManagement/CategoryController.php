<?php

namespace App\Http\Controllers\AssetsManagement;

use App\AssetName;
use App\SpecificationName;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function getAllAssetName(){
        $asset_name = AssetName::all();
        return json_encode($asset_name);
    }

    public function getAllSpecification(){
        $specification = SpecificationName::all();
        return json_encode($specification);
    }
}
