<?php
namespace App\Http\Controllers\AssetsManagement;
use App\GoodsPurchaseForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\AssetType;
use App\ProductLog;
use App\PurchaseRequisitionForm;
use App\ProductVendor;
use App\Block;
use App\SubBlock;
use App\ProductName;
use App\AssetName;
use App\SpecificationName;
use Session;

class ProductController extends Controller
{
    public function addProduct(){
        $data['asset_type']=AssetType::all();
        $data['vendor_list']=ProductVendor::all();
//        $data['product_name_list']=ProductName::all();
//        dd($data);
//        dd('helo');

        return view('assets.store.product.add_product',$data);
    }

    public function assetTypeAddList(){
        $data['asset_type_list']=AssetType::get();
        return view('asset.asset_type_add_list',$data);
    }
    
    public function addAssetType(Request $request){
        $add_asset_type=new AssetType;
        $add_asset_type->name=$request->asset_type_name;
        $add_asset_type->save();
        return redirect()->back();
    }

    public function addBlockTypeList(){
        $data['block_type_list']=Block::get();
        $data['asset_type']=AssetType::get();
        return view('asset.block_type_add_list',$data);
    }

    public function addBlockType(Request $request){
        $add_block_type=new Block;
        $add_block_type->category_id=$request->asset_type_id;
        $add_block_type->block_name=$request->asset_type_name;
        $add_block_type->save();
        return redirect()->back();
    }

    public function addSubBlockList(Request $request){
        $data['block_list']=Block::get();
        $data['sub_block_list']=SubBlock::get();
        return view('asset.sub_block_type_add_list',$data);
    }

    public function addSubBlockType(Request $request){
       $add_sub_block_type=new SubBlock;
       $add_sub_block_type->block_id=$request->block_type_id;
       $add_sub_block_type->sub_block_name=$request->sub_block_type_name;
       $add_sub_block_type->save();
       return redirect()->back();
    }

    public function assetNameAddList(){
      $data['sub_block_list']=SubBlock::get();
      $data['asset_name_list']=AssetName::get();
      return view('asset.asset_name_add_list',$data);
    }

    public function assetName(Request $request){
       $add_asset_name=new AssetName;
       $add_asset_name->sub_block_id=$request->sub_block_type_id;
       $add_asset_name->asset_name=$request->asset_name;
       $add_asset_name->save();
       return redirect()->back();
    }

    public function specificationNameAddList(){
      $data['asset_name_list']=AssetName::get();
      $data['specification_name_list']=SpecificationName::get();
      return view('asset.specification_name_add_list',$data);
    }

    public function addSpecificationName(Request $request){
       $add_asset_name=new SpecificationName;
       $add_asset_name->asset_name_id=$request->asset_name_id;
       $add_asset_name->specification_name=$request->specification_name;
       $add_asset_name->save();
       return redirect()->back();
    }

    public function editSpecificationName(Request $request){
       $edit_specification_name=SpecificationName::where('id',$request->specification_id)->first();
       $edit_specification_name->specification_name=$request->specification_name;
       $edit_specification_name->save();
       return redirect()->back();
    }


    public function editAssetName(Request $request){

       $edit_asset_name=AssetName::where('id',$request->asset_id)->first();
       $edit_asset_name->asset_name=$request->asset_name;
       $edit_asset_name->save();
       return redirect()->back();
    }


    public function editSubBlockName(Request $request){
       $edit_sub_block_name=SubBlock::where('id',$request->sub_block_id)->first();
       $edit_sub_block_name->sub_block_name=$request->sub_block_name;
       $edit_sub_block_name->save();
       return redirect()->back();
    }

    public function editBlockName(Request $request){
       
       $edit_block_name=Block::where('id',$request->block_id)->first();
       $edit_block_name->block_name=$request->block_name;
       $edit_block_name->save();
       return redirect()->back();
    }

    public function editAssetTypeName(Request $request){
       
       $edit_asset_type_name=AssetType::where('id',$request->asset_type_id)->first();
       $edit_asset_type_name->name=$request->asset_type_name;
       $edit_asset_type_name->save();
       return redirect()->back();
    }

    //fetching through ajax for category and sub category.
    public function blockList(Request $request){

        $data=Block::where('category_id',$request->asset_type_id)->get();
        return json_encode($data);
    }

    public function subBlockList(Request $request){
        $data=SubBlock::where('block_id',$request->block_id)->get();
        return json_encode($data);
    }

    public function assetNameList(Request $request){
        $data=AssetName::where('sub_block_id',$request->sub_block_id)->get();
        return json_encode($data);
    }

    public function specificationNameList(Request $request){
        $data=SpecificationName::where('asset_name_id',$request->asset_name_id)->get();
        return json_encode($data);
    }

    public function specificationIdCount(Request $request){

        $total_id_count=Product::where('specification_name_id',$request->get_specification_id_count)->count();
        return json_encode($total_id_count);
    }


    public function productForm(Request $request){
        $request->validate([
            'asset_type_id' => 'required',
            'block_id'=>'required',
            'sub_block_id'=>'required',

            'asset_name_id'=>'required',
            'specification_name_id' => 'required',
            'vendor_id' => 'required',

            'unit' => 'required',
            'unit_cost' => 'required',
            'quantity' => 'required',

            'amount' => 'required',
            'vat' => 'required',
            'expected_life' => 'required',

            'depreciation_method' => 'required',
//            'depreciation_value' => 'required',
            'residual_price' => 'required',


            'nepali_date' => 'required',
    
//            'bill' => 'required|max:100000|mimes:jpeg,png,jpg',
//            'image' => 'required|max:100000|mimes:jpeg,png,jpg',
        ]);

        //if both date are empty. one date must be filled.
        if(!isset($request->bill_date) && !isset($request->ready_to_use_date)){
            return redirect()->back()->withErrors(['required_date' => 'Please enter value of Bill Issued date or Ready to use date ']);
        }

        if(isset($request->bill)){
            $request->validate([
                'bill' => 'required|max:5120|mimes:jpeg,png,jpg'
            ]);
        }

        if(isset($request->image)){
            $request->validate([
                'image' => 'required|max:5120|mimes:jpeg,png,jpg',
            ]);
        }

        $product = new Product;

        $product->batch_id = $request->asset_type_id.$request->block_id.$request->sub_block_id.$request->asset_name_id.$request->specification_name_id;
        $product->batch_number=$request->batch_number;


        $product->asset_type_id=$request->asset_type_id;
        $product->block_id=$request->block_id;
        $product->sub_block_id = $request->sub_block_id;

        $product->asset_name_id=$request->asset_name_id;
        $product->specification_name_id = $request->specification_name_id;
        $product->vendor_id=$request->vendor_id;


        $product->unit=$request->unit;
        $product->unit_cost=$request->unit_cost;
        $product->quantity=$request->quantity;

        $product->amount=$request->amount;
        $product->discount = $request->discount;
        $product->vat = $request->vat;

        $product->expected_life=$request->expected_life;
        $product->depreciation_method = $request->depreciation_method;
        $product->depreciation_value=100;

    
        $product->residual_price=$request->residual_price;
        $product->bill_issue_date=date('Y-m-d');
        $product->ready_to_use_date=date('Y-m-d');


        $product->created_at=date('Y-m-d');
        $product->nepali_date=$request->nepali_date;
        


        if ($request->has('bill')){
            $path = $request->file('bill')->store('files/assets/product/bill/'.$product->id,'public');
            $product->bill= $path;
        }else{
            $product->bill= '';

        }
        if ($request->has('image')){
            $path = $request->file('image')->store('files/assets/product/product_image/'.$product->id,'public');
            $product->image = $path;
        }else{
            $product->image = '';
        }


        $product->save();

        \Session::flash('success','Product Added Successfully');
        return redirect()->route('store.list_product');
    }

    public function productList(){
        $data['list_assets']=Product::with('asset_types','product_vendors','asset_name','specification_name')->get();
        return view('assets.store.product.index',$data);
    }

    public function editProduct($id){

        $data['asset_list']=AssetType::get();
        $data['vendor_list']=ProductVendor::get();
        $data['products']=Product::where('id',$id)->with('asset_types','product_vendors','product_block','product_sub_block','asset_name','specification_name')->first();
        return view('asset.edit_asset',$data);
    }

    public function updateProduct(Request $request){
        $date=date('Y-m-d H:i:s');
        $image_date=date('Y_m_d');
        dd($request->all());
        if($request->select_name =='' && $request->input_name ==''){
            return redirect()->back();
        }

        $id=$request->id;

        /*$vendor=ProductVendor::where('id',)*/

        $product=Product::where('id',$id)->first();

        $vendor=ProductVendor::where('id',$request->vendor_id)->first();
        $vendor->name=$request->vendor_name;
        $vendor->company=$request->vendor_company_name;
        $vendor->address=$request->vendor_company_address;
        $vendor->contact=$request->vendor_contact;
        $vendor->created_at=$date;
        $vendor->updated_at=$date;
        $vendor->save();


        $product_log=new ProductLog;

        $product_log->name=$product->name;
        $product_log->description=$product->description;
        $product_log->asset_type_id=$product->asset_type_id;
        $product_log->quantity=$product->quantity;
        $product_log->unit_cost=$product->unit_cost;
        $product_log->amount=$product->amount;
        $product_log->bill= $product->bill;
        $product_log->image=$product->image;
        $product_log->vendor_name=$vendor->name;
        $product_log->vendor_company_name=$vendor->company;
        $product_log->vendor_company_address=$vendor->address;
        $product_log->vendor_contact=$vendor->contact;
        $product_log->created_at=$vendor->created_at;
        $product_log->nepali_date=$product->nepali_date;
        $product_log->save();




        if($request->input_name!=''){
            $product->name=$request->input_name;

        }else{
            $product->name=$request->select_name;
        }


        if($request->description==''){

            $product->description=$product->description;

        }else{
            $product->description=$request->description;

        }

        if($request->asset_type_id==''){

            $product->asset_type_id=$product->asset_type_id;

        }else{
            $product->asset_type_id=$request->asset_type_id;

        }

        if($request->quantity==''){

            $product->quantity=$product->quantity;

        }else{
            $product->quantity=$request->quantity;

        }

        if($request->unit_cost==''){

            $product->unit_cost=$product->unit_cost;

        }else{
            $product->unit_cost=$request->unit_cost;

        }

        if($request->amount==''){
            $product->amount=$product->amount;
        }else{
            $product->amount=$request->amount;
        }

        if($request->vendor_id==''){
            $product->vendor_id=$product->vendor_id;
        }else{
            $product->vendor_id=$request->vendor_id;
        }

        $product->created_at=$date;


        if($request->nepali_date==''){
            $product->nepali_date=$product->nepali_date;
        }else{
            $product->nepali_date=$request->nepali_date;
        }

        if ($request->hasFile('bill')){
            $path = $request->file('bill')->store('files/assets/bill/'.$image_date,'public');
            $product->bill= $path;

        }else{
            $product->bill=$product->bill;

        }
        if ($request->hasFile('image')){
            $path = $request->file('image')->store('files/assets/image/'.$image_date,'public');
            $product->image = $path;
        }else{
            $product->image=$product->image;
        }

        $product->save();
        Session::flash('success','Successfully Updated');

        /*return redirect()->route('asset.list_asset');*/
        return redirect()->back();

    }

    public function deleteProduct(Request $request){

        $id=$request->id;

        $product=Product::where('id',$id)->first();

        $product_log=new ProductLog;
        $product_log->name=$product->name;
        $product_log->description=$product->description;
        $product_log->asset_type_id=$product->asset_type_id;
        $product_log->quantity=$product->quantity;
        $product_log->unit_cost=$product->unit_cost;
        $product_log->amount=$product->amount;
        $product_log->bill= $product->bill;
        $product_log->image=$product->image;
        $product_log->vendor_name=$product->vendor_name;
        $product_log->vendor_company_name=$product->vendor_company_name;
        $product_log->vendor_company_address=$product->vendor_company_address;
        $product_log->vendor_contact=$product->vendor_contact;
        $product_log->created_at=$product->created_at;
        $product_log->nepali_date=$product->nepali_date;
        $product_log->save();
        $delete =  Product::where('id',$id)->delete();
        Session::flash('delete','Deleted Successfully');
        return redirect()->back();

    }

    public function viewProduct($id){
        $data['product']=Product::where('id',$id)->with('asset_types','product_vendors','product_block','product_sub_block','asset_name','specification_name')->first();
        return view('assets.store.product.view_product',$data);
    }

    public function vendorForm(){
        return view('productVendor.vendor_form');
    }

    public function vendorAdd(Request $request){
   
    
        $request->validate([
            'vendor_name' => 'required',
            'vendor_company_address' => 'required',
            'vendor_contact' => 'required',
            'pan_vat_number' => 'required',
            'email' => 'required',
            'mobile_number' => 'required',
        ]);

        $date=date('Y-m-d');
        $vendor=new ProductVendor;
        $vendor->name=$request->vendor_name;
    
        $vendor->address=$request->vendor_company_address;
        $vendor->contact=$request->vendor_contact;
        $vendor->pan_vat_number=$request->pan_vat_number;
        $vendor->email=$request->email;
        $vendor->mobile_number=$request->mobile_number;
        $vendor->updated_at=$date;
        $vendor->save();
        Session::flash('success','Vendor Successfully Added');
        return redirect()->route('store.list_vendor');

    }

    public function vendorList(){

        $data['vendor_list']=ProductVendor::get();

        return view('productVendor.vendor_list',$data);

    }

    public function viewVendor($id){
        $data['vendor']=ProductVendor::where('id',$id)->first();
        return view('productVendor.view_vendor',$data);
    }

    public function editVendor($id){
        $data['vendor']=ProductVendor::where('id',$id)->first();
        return view('productVendor.edit_vendor',$data);

    }

    public function updateVendor(Request $request){

        $request->validate([
            'id'=>'required',
            'vendor_name' => 'required',
            'vendor_company_address' => 'required',
            'vendor_contact' => 'required',
            'vendor_email' => 'required',
            'mobile_number' => 'required',
            'pan_vat_number' => 'required',
        ]);
        $date=date('Y-m-d');
        $id=$request->id;
        $vendor=ProductVendor::where('id',$id)->first();
        $vendor->name=$request->vendor_name;
        $vendor->address=$request->vendor_company_address;
        $vendor->contact=$request->vendor_contact;
        $vendor->email=$request->vendor_email;
        $vendor->mobile_number=$request->mobile_number;
        $vendor->pan_vat_number=$request->pan_vat_number;
        $vendor->updated_at=$date;
        $vendor->save();
        Session::flash('success','Vendor Updated Successfully ');
        return redirect()->back();
    }

    public function deletVendor(Request $request){
        $id=$request->id;
        $vendor =  ProductVendor::where('id',$id)->delete();
        Session::flash('delete','Vendor Deleted Successfully');
        return redirect()->back();
    }

    //retrieve product form after purchase form
    public function addProductAfterPurchaseForm($id){
        $purchase = GoodsPurchaseForm::find($id);

        $data['asset_type']=AssetType::all();
        $data['vendor_list']=ProductVendor::all();

        return view('assets.store.product.add_product',$data);
    }



}


