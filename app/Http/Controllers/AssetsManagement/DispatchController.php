<?php

namespace App\Http\Controllers\AssetsManagement;

use App\DispatchProduct;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DispatchController extends Controller
{
    public function showAllDispatchedProduct(){
        $dispatch = DispatchProduct::with('product','product.asset_name','product.specification_name','goods_requisition_product')->orderBy('id','desc')->get();
//        dd($dispatch);
        return view('assets.store.dispatchedProduct.index')->with('dispatch',$dispatch);
    }

    public function handOverForm($id){
        $data['dispatch'] = DispatchProduct::where('id',$id)->with('product','product.asset_name','product.specification_name','goods_requisition_product')->orderBy('id','desc')->first();
        // retrieve user requestor i.e. 1 .
        $data['users'] = User::where('user_designation','1')->get();

        //returning two views
        // 1. to show dispatch form  2. to show all information about dispatch info.
        if(empty($data['dispatch']->handover_id)){
            return view('assets.store.dispatchedProduct.handover_form',$data);
        }
        else{
            $data['custodian_name'] = User::find($data['dispatch']->custodian_id);
            return view('assets.store.dispatchedProduct.view_handover',$data);
        }
    }

    public function storeHandOverForm(Request $request){
        $request->validate([
            'dispatch_product_id' => 'required',
            'custodian_id' => 'required',
            'handover_date' => 'required',
            'returned_date' => 'required',
//            'remarks' => 'required'
        ]);

        $user = User::find($request->custodian_id);

        $handover = DispatchProduct::find($request->dispatch_product_id);

        $handover->handover_id = 'HO-'.$handover->id;
        $handover->custodian_id = $request->custodian_id;

        $handover->handover_date = $request->handover_date;
        $handover->returned_date = $request->returned_date;

        $handover->remarks = $request->remarks;

        $handover->save();

        \Session::flash('success','Product Hand Over to '.ucfirst($user->name).' Successfull');
        return redirect()->route('store.show_dispatch_product');


    }
}
