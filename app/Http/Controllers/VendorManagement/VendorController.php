<?php

namespace App\Http\Controllers\VendorManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VendorController extends Controller
{
    public function vendorForm(){

        return view('productVendor.vendor_form');
    }

    public function vendorList(){
        return view('productVendor.vendor_list');

    }
}
