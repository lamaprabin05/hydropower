<?php

namespace App\Http\Controllers\Auth;

use App\AuthGroup;
use App\AuthGroupUser;
use App\User;
use App\UserPermission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Session;

use Auth;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*dd("I am here in construct");*/
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
            
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'usertype' => ['required', 'integer'    ],
            'user_designation' => ['required', 'integer'    ],
            'auth_group' => ['required'],
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        
         $request = request();
         if ($request->has('user_avatar')){
            $path = $request->file('user_avatar')->store('files/user_images/','public');
    
        }else{
             $path='';

        }

        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_superadmin' => 0,
            'usertype' => $data['usertype'],
            'user_designation' => $data['user_designation'],
            'user_avatar' => $path,
            'created_by' => Auth::user()->id,
        ]);
//         assigning group to a user.
         $auth_group = new AuthGroupUser();
         $auth_group->user_id = $user->id;
         $auth_group->group_id = $data['auth_group'];
         $auth_group->save();


         /*
          assigning permission to individual user.
          * $permission_info = Permission::find($data['permission']);

         $permission = new UserPermission();
         $permission->user_id = $user->id;
         $permission->permission_id = $data['permission'];
         $permission->permission = $permission_info->code_name;
         $permission->save();*/

         return $user;

    }

     protected function showRegistrationForm()
    {
        $data['group'] = AuthGroup::all();
        return view('auth.register',$data);
        
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));


        Session::flash('success','User Created Successfully');
        return redirect()->route('admin.list_user');
        /*return $this->registered($request, $user)
            ?: redirect($this->redirectPath());*/
    }

  /*  protected function redirectTo()
    {
        return route('admin.list_user');
    }*/

}
