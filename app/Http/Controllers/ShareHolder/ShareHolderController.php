<?php

namespace App\Http\Controllers\ShareHolder;

use App\Helper\Tools;
use App\FormShare;
use App\ShareGroup;
use App\InstallmentInformation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Storage;
use Session;

class ShareHolderController extends Controller
{
    public function index()
    {
        $data['group']= ShareGroup::where('slug',1)->get();
        return view('shareholder.shareform.add_form_shareholder',$data);
    }

    public function process(Request $request)
    {
//        return json_encode($request->all());


        $form = new FormShare();

        //if account type is individual
        if($request->account_type == 1){

            $form->shareholder_name = $request->individual_name;
            $form->temporary_address = $request->temporary_address;
            $form->permanent_address = $request->individual_permanent_address;

            $form->citizenship_number = $request->citizenship_number;
            $form->citizenship_issued_place = $request->citizenship_issued_place;
            $form->citizenship_issued_date = $request->citizenship_issued_date;

            $form->date_of_birth = $request->date_of_birth;
            $form->contact = $request->individual_contact;
            $form->email_address = $request->individual_email_address;

            $form->father_name = $request->father_name;
            $form->grandfather_name = $request->grandfather_name;
            $form->wife_name = $request->wife_name;

            $form->relation = $request->relation;
            $form->birth_place = $request->birth_place;
            $form->husband_name = $request->husband_name;
            $form->nominee_name = $request->nominee_name;

            $form->nominee_address = $request->nominee_address;
            $form->nominee_contact_number = $request->nominee_contact_number;



            //set default value to null for company info
            $form->registration_number = null;
            $form->pan_number = null;
            $form->website = null;
        }
        //account type is company
        else{

            $form->shareholder_name = $request->shareholder_name;
            $form->registration_number = $request->registration_number;

            $form->permanent_address = $request->company_permanent_address;
            $form->contact = $request->company_contact;

            $form->pan_number = $request->pan_number;
            $form->website = $request->website;

            $form->email_address = $request->company_email_address;

            //setting other field to null
            $form->temporary_address = null;
            $form->citizenship_number = null;
            $form->citizenship_issued_place = null;

            $form->citizenship_issued_date = null;
            $form->date_of_birth = null;
            $form->father_name = null;

            $form->grandfather_name = null;
            $form->wife_name = null;
            $form->relation = null;

            $form->birth_place = null;
            $form->husband_name = null;
            $form->nominee_name = null;

        }

        //both individual and group common attributes.
        $form->notification = 1;
        $form->account_type = $request->account_type;
        $form->group_type = $request->group_type;
        $form->created_at = date('Y-m-d h:i:s');
        $form->created_by = Auth()->user()->id;

        //check group type
        if($request->group_type == 'group'){
            $form->share_group_id = $request->share_group_id;

        }
        else{
            //insert into share group with slug 2.\
            $group = new ShareGroup();
            $group->name = $request->individual_group_name;
            $group->slug = 2;
            $group->created_at = date('Y-m-d h:i:s');
            $group->created_by = Auth::user()->id;
            $group->save();

            $form->share_group_id = $group->id;

        }

        $form->save();
        $form_id = $form->id;

        //inserting documents
        if (isset($request->citizenship_scan)){

            $citizenship_scan = $request->file('citizenship_scan')->store('files/shareholder/'.$form_id.'/citizenship_scan', 'public');
        }else{

            $citizenship_scan = '';

        }

        if (isset($request->avatar)){
            $avatar = $request->file('avatar')->store('files/shareholder/'.$form_id.'/avatar', 'public');
        }else{
            $avatar = '';

        }

        if (isset($request->birth_certificate)){
            $birth_certificate = $request->file('birth_certificate')->store('files/shareholder/'.$form_id.'/birth_certificate', 'public');
        }else{
            $birth_certificate = '';

        }

        if (isset($request->protected_citizenship)){
            $protected_citizenship = $request->file('protected_citizenship')->store('files/shareholder/'.$form_id.'/protected_citizenship', 'public');
        }else{
            $protected_citizenship = '';

        }

        $form = FormShare::firstOrNew(['id'=>$form_id]);
        $form->citizenship_scan = $citizenship_scan;
        $form->avatar = $avatar;
        $form->birth_certificate = $birth_certificate;
        $form->protected_citizenship = $protected_citizenship;


        //creating unique id
        $unique_id = date('yjmh').$form_id;

        //for form share tracking
        $form->unique_id = $unique_id;
        $form->form_status = 1;
        $form->proceeded_for = 'first';

        $form->save();


        //inserting data in table: installment_information
        $payment = new InstallmentInformation();

        $payment->share_form_id = $form_id;
        $payment->no_of_installment = $request->no_of_installment;

        $payment->total_paid_amount = 0;

        $payment->remaining_installment = $request->no_of_installment;
        $payment->committed_amount = $request->committed_amount ;

        if($request->no_of_installment == 1 ){
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;
        }
        else if($request->no_of_installment == 2 ) {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;
        }
        else if($request->no_of_installment == 3 ) {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;

            $payment->third_installment_amt = $request->third_installment_amt;
            $payment->third_expiry_date = $request->third_expiry_date;
        }
        else if($request->no_of_installment == 4 ) {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;

            $payment->third_installment_amt = $request->third_installment_amt;
            $payment->third_expiry_date = $request->third_expiry_date;

            $payment->fourth_installment_amt = $request->fourth_installment_amt;
            $payment->fourth_expiry_date = $request->fourth_expiry_date;
        }

        else if($request->no_of_installment == 5 ) {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;

            $payment->third_installment_amt = $request->third_installment_amt;
            $payment->third_expiry_date = $request->third_expiry_date;

            $payment->fourth_installment_amt = $request->fourth_installment_amt;
            $payment->fourth_expiry_date = $request->fourth_expiry_date;

            $payment->fifth_installment_amt = $request->fifth_installment_amt;
            $payment->fifth_expiry_date = $request->fifth_expiry_date;
        }

        else if($request->no_of_installment == 6 ) {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;

            $payment->third_installment_amt = $request->third_installment_amt;
            $payment->third_expiry_date = $request->third_expiry_date;

            $payment->fourth_installment_amt = $request->fourth_installment_amt;
            $payment->fourth_expiry_date = $request->fourth_expiry_date;

            $payment->fifth_installment_amt = $request->fifth_installment_amt;
            $payment->fifth_expiry_date = $request->fifth_expiry_date;

            $payment->sixth_installment_amt = $request->sixth_installment_amt;
            $payment->sixth_expiry_date = $request->sixth_expiry_date;
        }

        else if($request->no_of_installment == 7 ) {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;

            $payment->third_installment_amt = $request->third_installment_amt;
            $payment->third_expiry_date = $request->third_expiry_date;

            $payment->fourth_installment_amt = $request->fourth_installment_amt;
            $payment->fourth_expiry_date = $request->fourth_expiry_date;

            $payment->fifth_installment_amt = $request->fifth_installment_amt;
            $payment->fifth_expiry_date = $request->fifth_expiry_date;

            $payment->sixth_installment_amt = $request->sixth_installment_amt;
            $payment->sixth_expiry_date = $request->sixth_expiry_date;

            $payment->seventh_installment_amt = $request->seventh_installment_amt;
            $payment->seventh_expiry_date = $request->seventh_expiry_date;
        }

        else if($request->no_of_installment == 8 ) {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;

            $payment->third_installment_amt = $request->third_installment_amt;
            $payment->third_expiry_date = $request->third_expiry_date;

            $payment->fourth_installment_amt = $request->fourth_installment_amt;
            $payment->fourth_expiry_date = $request->fourth_expiry_date;

            $payment->fifth_installment_amt = $request->fifth_installment_amt;
            $payment->fifth_expiry_date = $request->fifth_expiry_date;

            $payment->sixth_installment_amt = $request->sixth_installment_amt;
            $payment->sixth_expiry_date = $request->sixth_expiry_date;

            $payment->seventh_installment_amt = $request->seventh_installment_amt;
            $payment->seventh_expiry_date = $request->seventh_expiry_date;

            $payment->eighth_installment_amt = $request->eighth_installment_amt;
            $payment->eighth_expiry_date = $request->eighth_expiry_date;
        }

        else if($request->no_of_installment == 9 ) {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;

            $payment->third_installment_amt = $request->third_installment_amt;
            $payment->third_expiry_date = $request->third_expiry_date;

            $payment->fourth_installment_amt = $request->fourth_installment_amt;
            $payment->fourth_expiry_date = $request->fourth_expiry_date;

            $payment->fifth_installment_amt = $request->fifth_installment_amt;
            $payment->fifth_expiry_date = $request->fifth_expiry_date;

            $payment->sixth_installment_amt = $request->sixth_installment_amt;
            $payment->sixth_expiry_date = $request->sixth_expiry_date;

            $payment->seventh_installment_amt = $request->seventh_installment_amt;
            $payment->seventh_expiry_date = $request->seventh_expiry_date;

            $payment->eighth_installment_amt = $request->eighth_installment_amt;
            $payment->eighth_expiry_date = $request->eighth_expiry_date;

            $payment->ninth_installment_amt = $request->ninth_installment_amt;
            $payment->ninth_expiry_date = $request->ninth_expiry_date;
        }

        else if($request->no_of_installment == 10 ) {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;

            $payment->third_installment_amt = $request->third_installment_amt;
            $payment->third_expiry_date = $request->third_expiry_date;

            $payment->fourth_installment_amt = $request->fourth_installment_amt;
            $payment->fourth_expiry_date = $request->fourth_expiry_date;

            $payment->fifth_installment_amt = $request->fifth_installment_amt;
            $payment->fifth_expiry_date = $request->fifth_expiry_date;

            $payment->sixth_installment_amt = $request->sixth_installment_amt;
            $payment->sixth_expiry_date = $request->sixth_expiry_date;

            $payment->seventh_installment_amt = $request->seventh_installment_amt;
            $payment->seventh_expiry_date = $request->seventh_expiry_date;

            $payment->eighth_installment_amt = $request->eighth_installment_amt;
            $payment->eighth_expiry_date = $request->eighth_expiry_date;

            $payment->ninth_installment_amt = $request->ninth_installment_amt;
            $payment->ninth_expiry_date = $request->ninth_expiry_date;

            $payment->tenth_installment_amt = $request->tenth_installment_amt;
            $payment->tenth_expiry_date = $request->tenth_expiry_date;
        }

        else if($request->no_of_installment == 11 ) {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;

            $payment->third_installment_amt = $request->third_installment_amt;
            $payment->third_expiry_date = $request->third_expiry_date;

            $payment->fourth_installment_amt = $request->fourth_installment_amt;
            $payment->fourth_expiry_date = $request->fourth_expiry_date;

            $payment->fifth_installment_amt = $request->fifth_installment_amt;
            $payment->fifth_expiry_date = $request->fifth_expiry_date;

            $payment->sixth_installment_amt = $request->sixth_installment_amt;
            $payment->sixth_expiry_date = $request->sixth_expiry_date;

            $payment->seventh_installment_amt = $request->seventh_installment_amt;
            $payment->seventh_expiry_date = $request->seventh_expiry_date;

            $payment->eighth_installment_amt = $request->eighth_installment_amt;
            $payment->eighth_expiry_date = $request->eighth_expiry_date;

            $payment->ninth_installment_amt = $request->ninth_installment_amt;
            $payment->ninth_expiry_date = $request->ninth_expiry_date;

            $payment->tenth_installment_amt = $request->tenth_installment_amt;
            $payment->tenth_expiry_date = $request->tenth_expiry_date;

            $payment->eleventh_installment_amt = $request->eleventh_installment_amt;
            $payment->eleventh_expiry_date = $request->eleventh_expiry_date;
        }

        else {
            $payment->first_installment_amt = $request->first_installment_amt;
            $payment->first_expiry_date = $request->first_expiry_date ;

            $payment->second_installment_amt = $request->second_installment_amt ;
            $payment->second_expiry_date = $request->second_expiry_date ;

            $payment->third_installment_amt = $request->third_installment_amt;
            $payment->third_expiry_date = $request->third_expiry_date;

            $payment->fourth_installment_amt = $request->fourth_installment_amt;
            $payment->fourth_expiry_date = $request->fourth_expiry_date;

            $payment->fifth_installment_amt = $request->fifth_installment_amt;
            $payment->fifth_expiry_date = $request->fifth_expiry_date;

            $payment->sixth_installment_amt = $request->sixth_installment_amt;
            $payment->sixth_expiry_date = $request->sixth_expiry_date;

            $payment->seventh_installment_amt = $request->seventh_installment_amt;
            $payment->seventh_expiry_date = $request->seventh_expiry_date;

            $payment->eighth_installment_amt = $request->eighth_installment_amt;
            $payment->eighth_expiry_date = $request->eighth_expiry_date;

            $payment->ninth_installment_amt = $request->ninth_installment_amt;
            $payment->ninth_expiry_date = $request->ninth_expiry_date;

            $payment->tenth_installment_amt = $request->tenth_installment_amt;
            $payment->tenth_expiry_date = $request->tenth_expiry_date;

            $payment->eleventh_installment_amt = $request->eleventh_installment_amt;
            $payment->eleventh_expiry_date = $request->eleventh_expiry_date;

            $payment->twelveth_installment_amt = $request->twelveth_installment_amt;
            $payment->twelveth_exiry_date = $request->twelveth_exiry_date;
        }

        $payment->created_at = date('Y-m-d h:i:s');
        $payment->created_by = Auth::user()->id;
        $payment->save();


        return['error' => 'false'];
        $responseData = Tools::setResponse('success', 'Form Share  Created Successfully', '', '');
        return response()->json($responseData,200);


    }


    public function list()
    {
        $data['form'] = FormShare::all();
//        dd($data);
        return view('shareholder.shareform.list_form_shareholder',$data);

    }

    public function view(Request $request)
    {
        $data['form'] = FormShare::where('id',$request->id)->first();
        $data['installment'] = InstallmentInformation::where('share_form_id',$request->id)->first();
       
        return view('shareholder.shareform.view_form_shareholder',$data);

    }

    public function editMenu(Request $request)
    {

        $data['form'] = FormShare::where('id',$request->id)->first();
        return view('shareholder.shareform.edit_form_shareholder',$data);

    }

    public function editForm(Request $request)
    {
        $data['form'] = FormShare::where('id',$request->id)->first();
        $data['installment'] = InstallmentInformation::where('share_form_id',$request->id)->first();
        $data['share_group'] = ShareGroup::where('id',$data['form']['share_group_id'])->where('slug',2)->first();
        $data['group'] = ShareGroup::where('slug',1)->get();


        return view('shareholder.shareform.edit_detail_form_shareholder',$data);

    }

    public function posteditForm(Request $request){
        if($request->account_type == 'Individual'){
            $this->validate($request,[
                'form_id'  => ['required', 'integer'],
                'shareholder_name' => ['required','string'],
                'date_of_birth' => ['required','string'],
                'birth_place' => ['required','string'],
                'email_address' => ['required', 'string', 'email', 'max:255'],

                'contact' => ['required','string','min:7','max:12'],
                'temporary_address' => ['required','string'],
                'permanent_address' => ['required','string'],
                'citizenship_number' => 'required',
                'citizenship_issued_date' => 'required',

                'citizenship_issued_place' => ['required','string'],
                'father_name' => ['required','string'],
                'grandfather_name' => ['required','string'],
                'husband_name' => ['required','string'],
                'wife_name' => ['required','string'],

                'nominee_name' => ['required','string'],
                'nominee_contact_number' => 'required|integer',
                'nominee_address' => ['required','string'],
                'relation' => ['required','string'],


            ]);

        }
        else {
//            dd($request->all());
            $this->validate($request,[
                'form_id'  => 'required',
                'company_name' => 'required',
                'registration_number' => 'required|integer',

                'company_permanent_address' => 'required',
                'pan_number' => 'required|integer',
                'company_contact_name' => 'required',

                'company_contact' => 'required',
                'website' => 'required',
                'company_email' => ['required', 'string', 'email', 'max:255'],

            ]);

        }

        $form = FormShare::where('id',$request->form_id)->first();
        //if account type is individual
        if($request->account_type == 'Individual'){

            $form->shareholder_name = $request->shareholder_name;
            $form->temporary_address = $request->temporary_address;
            $form->permanent_address = $request->permanent_address;

            $form->citizenship_number = $request->citizenship_number;
            $form->citizenship_issued_place = $request->citizenship_issued_place;
            $form->citizenship_issued_date = $request->citizenship_issued_date;

            $form->date_of_birth = $request->date_of_birth;
            $form->contact = $request->contact;
            $form->email_address = $request->email_address;

            $form->father_name = $request->father_name;
            $form->grandfather_name = $request->grandfather_name;
            $form->wife_name = $request->wife_name;

            $form->relation = $request->relation;
            $form->birth_place = $request->birth_place;
            $form->husband_name = $request->husband_name;

            $form->nominee_name = $request->nominee_name;
            $form->nominee_address = $request->nominee_address;
            $form->nominee_contact_number = $request->nominee_contact_number;


            //set default value to null for company info
            /*$form->registration_number = null;
            $form->pan_number = null;
            $form->website = null;*/
        }
        //account type is company
        else{
            $form->shareholder_name = $request->company_name;
            $form->registration_number = $request->registration_number;

            $form->permanent_address = $request->company_permanent_address;
            $form->contact = $request->company_contact;

            $form->pan_number = $request->pan_number;
            $form->website = $request->website;

            $form->email_address = $request->company_email;
//            $form->save();
//            return redirect()->back();

            //contact person name remaiining
        }

        //both individual and group common attributes.
        $form->group_type = $request->group_type;
        /*$form->created_at = date('Y-m-d h:i:s');
        $form->created_by = Auth()->user()->id;*/

        //check group type
        if($request->group_type == 'group'){
            //if new group is selected old one is deleted which is remaining.
            $form->share_group_id = $request->share_group_id;
        }
        else{
            //insert into share group with slug 2.\
            $group = ShareGroup::firstOrNew(['id' => $request->individual_id]);
            $group->name = $request->individual_group_name;
            $group->slug = 2;
            $group->created_at = date('Y-m-d');
            $group->created_by = Auth::user()->id;
            $group->save();

            $form->share_group_id = $group->id;

        }

        $form->save();
        Session::flash('success','Share Form Updated Successfully');
//        return redirect()->route('shareholder.edit',$request->form_id);
        return redirect()->back();
    }


    public function edit_document(Request $request)
    {

        $data['form'] = FormShare::where('id',$request->id)->first();
        $data['installment'] = InstallmentInformation::where('share_form_id',$request->id)->first();
//        dd($data);
        return view('shareholder.shareform.edit_document_form_shareholder',$data);

    }

    public function edit_document_form(Request $request){

        $document = FormShare::find($request->id);

        if (isset($request->avatar)){
            Storage::delete('public/'.$document->avatar);

            $document->avatar = $request->file('avatar')->store('files/shareholder/'.$request->id.'/avatar', 'public');
        }else{
            $document->avatar= $document->avatar;

        }

        if (isset($request->birth_certificate)){
            Storage::delete('public/'.$document->birth_certificate);

            $document->birth_certificate = $request->file('birth_certificate')->store('files/shareholder/'.$request->id.'/birth_certificate', 'public');
        }else{
            $document->birth_certificate = $document->birth_certificate;

        }


        if (isset($request->citizenship_scan)){
            Storage::delete('public/'.$document->citizenship_scan);

            $document->citizenship_scan = $request->file('citizenship_scan')->store('files/shareholder/'.$request->id.'/citizenship_scan', 'public');
        }else{
            $document->citizenship_scan = $document->citizenship_scan;

        }


        if (isset($request->protected_citizenship)){
            Storage::delete('public/'.$document->protected_citizenship);

            $document->protected_citizenship = $request->file('protected_citizenship')->store('files/shareholder/'.$request->id.'/protected_citizenship', 'public');
        }else{
            $document->protected_citizenship = $document->protected_citizenship;

        }

        $document->save();
        Session::flash('success','Document Updated Successfully');
        return redirect()->back();

    }











}
