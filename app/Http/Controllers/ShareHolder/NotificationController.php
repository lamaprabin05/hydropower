<?php

namespace App\Http\Controllers\ShareHolder;

use App\FormShare;
use App\Helper\Tools;
use App\InstallmentInformation;
use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Mail\NotificationPayment;
use Symfony\Component\Console\Helper\Helper;

class NotificationController extends Controller
{
    public function index()
    {
        $payment_info = FormShare::with('payment_information')->get(['form_status','id','notification','proceeded_for']);
//        dd($payment_info);
//        $data = array();

        //initializing count for badge
        $count = 0;

        foreach ($payment_info as $value){
            $total_installment = $value->payment_information->no_of_installment;
            $temp = $value->proceeded_for . '_expiry_date';

            for($i = 1; $i <= $total_installment; $i ++) {

                if ($value->notification == 1) {

                    if ($value->form_status == $i) {
                        $payment = InstallmentInformation::where('share_form_id', $value->id)->first();
                        //                    $days_ago = date('Y-m-d', strtotime('-7 days', strtotime($payment->first_expiry_date)));
                        $days_ago = date('Y-m-d', strtotime('-7 days', strtotime($payment->$temp)));

                        if (date('Y-m-d') >= $days_ago) {
                            $count = $count + 1;

                            $data['form'] [] = FormShare::where('id', $value->id)->with('payment_information')->first();

                        }
                    }
                }
            }

//            if($value->notification == 1){
//                $check[] = $count;
//                echo $count;
//
//                if($value->form_status == $count){
//                    $payment = InstallmentInformation::where('share_form_id',$value->id)->first();
////                    $days_ago = date('Y-m-d', strtotime('-7 days', strtotime($payment->first_expiry_date)));
//                    $temp = $value->proceeded_for.'_expiry_date';
//                    $days_ago = date('Y-m-d', strtotime('-7 days', strtotime($payment->$temp)));
//
//                    if(date('Y-m-d') >= $days_ago){
//
//                        $data['form'] [] = FormShare::where('id',$value->id)->with('payment_information')->first();
//
//                    }
//                }
//                /*else if($value->form_status == 2){
//
//                    $payment = InstallmentInformation::where('share_form_id',$value->id)->first();
//                    $days_ago = date('Y-m-d', strtotime('-7 days', strtotime($payment->second_expiry_date)));
//
//
//                    if(date('Y-m-d') >= $days_ago){
//                        $data['form'] [] = FormShare::where('id',$value->id)->with('payment_information')->first();
//
//                    }
//                }*/
//            }

        }



        if(!isset($data)){
            $data['token'] = 0;
        }
        else{
            $data['token'] = 1;

        }

        return view('shareholder.Notification.index',$data);

    }

    public function showMessageForm($id){
        $data['installment'] = FormShare::where('id',$id)->with('payment_information')->first();

        $form_status= $data['installment']->form_status;
        $data['installment_detail'] = Tools::getInstallmentDetail($id,$form_status);

        $expiry_date = new \DateTime($data['installment_detail']->expiry_date);
        $current_date = new \DateTime();
        $interval = $expiry_date->diff($current_date);
        if($expiry_date > $current_date){
            $data['remaining_days']= $interval->days + 1;
        }
        else{
            $data['overdue_days']= $interval->days ;

        }
//        dd($data);

        return view('shareholder.Notification.form_message',$data);
    }

    public function post_message(Request $request){
        $this->validate($request,[
            'message' => 'required|min:10',
            'user_email' => 'required|email'
        ]);
        $data = $request->all();

        //call mailable class and send mail.
        \Mail::to($request->user_email)->send(new NotificationPayment($data));

        //after that update form_share table notification columnt to 0
        $id = $request->id;

        $form = FormShare::find($id);

        //updating notification to 0 to not show it again
        $form->notification = 0;
        $form->save();

        \Session::flash('success','Notification Sent Successfully');
        return redirect()->route('shareholder.notification');
    }
}
