<?php

namespace App\Http\Controllers\ShareHolder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ShareGroup;
use App\Helper\Tools;
use Illuminate\Support\Facades\Validator;
use Session;

class ShareGroupController extends Controller
{
    public function index(){

        $data['group'] = ShareGroup::orderBy('id','DESC')->where('slug',1)->get();
        return view('shareholder.ShareGroup.list_group',$data);
    }

    public function editGroup($id){
        $data['group'] = ShareGroup::find($id);

        if(empty($data['group'])) {
            abort(404);
        }

        return view('shareholder.ShareGroup.edit_group',$data);
    }

    public function addGroup(Request $request){

        $this->validate($request,[
            'group_name' => 'required',
            'action' => 'required'
        ]);
//        dd($request->all());

        //slug 1 = group
        //slug 2 = individual

        if($request->action == 'add'){
            $group = new ShareGroup();
            $group->slug = 1;
            $group->created_at = date('Y-m-d h:i:s');
            $group->created_by = \Auth::user()->id;
        }
        else
        {
            $group =  ShareGroup::find($request->id);
        }


        $group->name = $request->group_name;

        $group->save();

        if($request->action == 'add'){
            \Session::flash('success','Group Created Successfully');
            return redirect()->back();

        }
        else{
            \Session::flash('success','Group Updated Successfully');
            return redirect()->route('shareholder.list_group');

        }
    }

    public function deleteGroup(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            \Session::flash('error','Error in Deleting Group');
            $responseData = Tools::setResponse('fail', 'Missing Parameter', '', '');
            $status = 422;

            return response()->json($responseData,$status);
        }

        $data = ShareGroup::find($request->id);
        $data->delete();

        \Session::flash('success','Group Deleted Successfully');
        return redirect()->back();
    }
}
