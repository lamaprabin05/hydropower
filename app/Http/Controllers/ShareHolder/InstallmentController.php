<?php

namespace App\Http\Controllers\Shareholder;

use App\FormShare;
use App\Helper\Tools;
use App\ShareGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payment;
use App\InstallmentInformation;
use Session;
use DB;

class InstallmentController extends Controller
{
    public function index(){

        if (isset($_GET['search_type'])){
            //show data on basis of search
            $search = $_GET['search_type'];
            $data['token'] = $search;


            $data['form_share'] =FormShare::where('group_type',$search)
                                ->with('payment_information')
                                ->get();
        }
        else{
            //just show all data
            $data['token'] = 'all';
            $data['form_share'] = FormShare::with('payment_information')->get();
        }

        return view('shareholder.Installment.index',$data);
    }



    public function add_installment($id){
       $form_id=$id;
       $data['installment'] = FormShare::where('id',$form_id)->with('payment_information')->first();

       $form_status= $data['installment']->form_status;
       $data['installment_detail'] = Tools::getInstallmentDetail($id,$form_status);

        return view('shareholder.Installment.add_installment',$data);

    }


    public function process_installment(Request $request){

       

      if($request->installment_number == "Select Installment"){
          \Session::flash('error','Error While Adding Installment');
            return redirect()->back()->withErrors(['installment_number' => 'Please Choose Installment ']);
        }
        $request->validate([
            'applicant_name' => 'required',
            'form_id' => 'required',
            'bank_name' => 'required',
            'branch_name' => 'required',
            'account_number' => 'required',
            'installment_paid_amount' => 'required',
            /*'advance_payment' => 'required',*/
            'installment_number' => 'required',
            'installment_eng_date' => 'required',
            'installment_nep_date' => 'required',
            'bill_image' => 'required|max:10000|mimes:jpeg,png,jpg'
        ]);

        $payment = new Payment;
        $payment->applicant_name=$request->applicant_name;
        $payment->form_id=$request->form_id;
        $payment->bank_name=$request->bank_name;
        $payment->branch_name=$request->branch_name;
        $payment->account_number=$request->account_number;

        //some condition on installment number
        $payment->installment_number=$request->installment_number;

        //update status and proceeded for in form share table
        $form = FormShare::find($request->form_id);
        $form->notification = 1;

        if($request->installment_number == 1){
            $form->form_status = 2;
            $form->proceeded_for = 'second';
            $payment->installment_name = 'first';
        }
        else if ($request->installment_number == 2){
            $form->form_status  = 3;
            $form->proceeded_for = 'third';
            $payment->installment_name = 'second';

        }
        else if ($request->installment_number == 3){
            $form->form_status  = 4;
            $form->proceeded_for = 'fourth';
            $payment->installment_name = 'third';

        }
        else if ($request->installment_number == 4){
            $form->form_status  = 5;
            $form->proceeded_for = 'fifth';
            $payment->installment_name = 'fourth';

        }
        else if ($request->installment_number == 5){
            $form->form_status  = 6;
            $form->proceeded_for = 'sixth';
            $payment->installment_name = 'fifth';

        }
        else if ($request->installment_number == 6){
            $form->form_status  = 7;
            $form->proceeded_for = 'seventh';
            $payment->installment_name = 'sixth';

        }
        else if ($request->installment_number == 7){
            $form->form_status  = 8;
            $form->proceeded_for = 'eighth';
            $payment->installment_name = 'seventh';

        }
        else if ($request->installment_number == 8){
            $form->form_status  = 9;
            $form->proceeded_for = 'ninth';
            $payment->installment_name = 'eighth';

        }
        else if ($request->installment_number == 9){
            $form->form_status  = 10;
            $form->proceeded_for = 'tenth';
            $payment->installment_name = 'ninth';

        }
        else if ($request->installment_number == 10){
            $form->form_status  = 11;
            $form->proceeded_for = 'eleventh';
            $payment->installment_name = 'tenth';
        }
        else if ($request->installment_number == 11){
            $form->form_status  = 12;
            $form->proceeded_for = 'twelveth';
            $payment->installment_name = 'eleventh';
        }
        else if ($request->installment_number == 12){
            $payment->installment_name = 'twelveth';
        }
        else{
            $form->form_status  = 0;
            $form->proceeded_for = 'completed';
        }

        $form->save();

        $payment_information = Tools::getInstallmentDetail($request->form_id,$request->installment_number);

        if($request->installment_paid_amount == $payment_information->installment){
            $advance_payment = $request->installment_paid_amount - $payment_information->installment;
            $less_payment = 0;
        }
        elseif($request->installment_paid_amount > $payment_information->installment) {
            $advance_payment = $request->installment_paid_amount -$payment_information->installment;
            $less_payment = 0;

        }
        else{
            $less_payment = $payment_information->installment- $request->installment_paid_amount ;
            $advance_payment = 0;

        }


        $payment->installment_eng_date=$request->installment_eng_date;
        $payment->installment_nep_date=$request->installment_nep_date;
        $payment->installment_paid_amount=$request->installment_paid_amount;

        //for advance check paid amount and check diff with our payment information amount

        $payment->advance_payment=$advance_payment;
        $payment->less_payment = $less_payment;
        $payment->installment_paid_amount=$request->installment_paid_amount;

        $payment->created_by = \Auth::user()->id;
        $payment->created_at = date('Y-m-d h:i:s');

        if ($request->has('bill_image')){
            $path = $request->file('bill_image')->store('files/shareholder/'.$request->form_id.'/payments','public');
            $payment->bill_image = $path;
        }else{
             $payment->bill_image = '';

        }
        
        $payment->save();

        //update remaining installment
        $installment= InstallmentInformation::where('share_form_id',$request->form_id)->first();
        $installment->remaining_installment = $installment->remaining_installment-1;
        $installment->total_paid_amount = $installment->total_paid_amount + $request->installment_paid_amount;
        $installment->save();

        \Session::flash('success','Installment Added Successfully');
        return redirect()->route('shareholder.getSingleInstallment',$payment->id);


    }

    public function view_installment($id){
        $data['info'] = FormShare::where('id',$id)->with('payment_information')->first();
//        dd($data);
        $data['group_name'] = ShareGroup::find($data['info']->share_group_id);

//        dd($data);
        $data['payments'] = Payment::where('form_id',$id)->orderBy('id','asc')->get();
        return view('shareholder.Installment.view_installment',$data);
    }

    public function getSingleInstallment($id){
       $data['payment'] = Payment::find($id);
//       dd($data);
       return view('shareholder.Installment.view_single_installment',$data);
    }

    //Edit Installment Information

    public function getInstallmentInformation(Request $request)
    {
        $data['form'] = FormShare::where('id',$request->id)->first();
        $data['installment'] = InstallmentInformation::where('share_form_id',$request->id)->first();

        return view('shareholder.Installment.edit_installment_information',$data);

    }

    public function editInstallmentInformation(Request $request,$id){

      
       $installment = InstallmentInformation::find($id);

       $installment->no_of_installment=$request->no_of_installment;
       $installment->committed_amount=$request->committed_amount;
       
       if($request->no_of_installment==1){
        $installment->no_of_installment=$request->no_of_installment;
        $installment->committed_amount=$request->committed_amount;

       $installment->second_installment_amt=null;
       $installment->second_expiry_date=null;
       $installment->third_installment_amt=null;
       $installment->third_expiry_date=null;
       $installment->fourth_installment_amt=null;
       $installment->fourth_expiry_date=null;
       $installment->fifth_installment_amt=null;
       $installment->fifth_expiry_date=null;
       $installment->sixth_installment_amt=null;
       $installment->sixth_expiry_date=null;
       $installment->seventh_installment_amt=null;
       $installment->seventh_expiry_date=null;
       $installment->eighth_installment_amt=null;
       $installment->eighth_expiry_date=null;
       $installment->ninth_installment_amt=null;
       $installment->ninth_expiry_date=null;
       $installment->tenth_installment_amt=null;
       $installment->tenth_expiry_date=null;
       $installment->eleventh_installment_amt=null;
       $installment->eleventh_expiry_date=null;
       $installment->twelveth_installment_amt=null;
       $installment->twelveth_exiry_date=null;

       }elseif($request->no_of_installment==2){
        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;

       $installment->third_installment_amt=null;
       $installment->third_expiry_date=null;
       $installment->fourth_installment_amt=null;
       $installment->fourth_expiry_date=null;
       $installment->fifth_installment_amt=null;
       $installment->fifth_expiry_date=null;
       $installment->sixth_installment_amt=null;
       $installment->sixth_expiry_date=null;
       $installment->seventh_installment_amt=null;
       $installment->seventh_expiry_date=null;
       $installment->eighth_installment_amt=null;
       $installment->eighth_expiry_date=null;
       $installment->ninth_installment_amt=null;
       $installment->ninth_expiry_date=null;
       $installment->tenth_installment_amt=null;
       $installment->tenth_expiry_date=null;
       $installment->eleventh_installment_amt=null;
       $installment->eleventh_expiry_date=null;
       $installment->twelveth_installment_amt=null;
       $installment->twelveth_exiry_date=null;


       }elseif($request->no_of_installment==3){
        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;
        $installment->third_installment_amt=$request->third_installment_amt;
        $installment->third_expiry_date=$request->third_expiry_date;

       $installment->fourth_installment_amt=null;
       $installment->fourth_expiry_date=null;
       $installment->fifth_installment_amt=null;
       $installment->fifth_expiry_date=null;
       $installment->sixth_installment_amt=null;
       $installment->sixth_expiry_date=null;
       $installment->seventh_installment_amt=null;
       $installment->seventh_expiry_date=null;
       $installment->eighth_installment_amt=null;
       $installment->eighth_expiry_date=null;
       $installment->ninth_installment_amt=null;
       $installment->ninth_expiry_date=null;
       $installment->tenth_installment_amt=null;
       $installment->tenth_expiry_date=null;
       $installment->eleventh_installment_amt=null;
       $installment->eleventh_expiry_date=null;
       $installment->twelveth_installment_amt=null;
       $installment->twelveth_exiry_date=null;

       }elseif($request->no_of_installment==4){
        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;
        $installment->third_installment_amt=$request->third_installment_amt;
        $installment->third_expiry_date=$request->third_expiry_date;
        $installment->fourth_installment_amt=$request->fourth_installment_amt;
        $installment->fourth_expiry_date=$request->fourth_expiry_date;

       $installment->fifth_installment_amt=null;
       $installment->fifth_expiry_date=null;
       $installment->sixth_installment_amt=null;
       $installment->sixth_expiry_date=null;
       $installment->seventh_installment_amt=null;
       $installment->seventh_expiry_date=null;
       $installment->eighth_installment_amt=null;
       $installment->eighth_expiry_date=null;
       $installment->ninth_installment_amt=null;
       $installment->ninth_expiry_date=null;
       $installment->tenth_installment_amt=null;
       $installment->tenth_expiry_date=null;
       $installment->eleventh_installment_amt=null;
       $installment->eleventh_expiry_date=null;
       $installment->twelveth_installment_amt=null;
       $installment->twelveth_exiry_date=null;

       }elseif($request->no_of_installment==5){

        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;
        $installment->third_installment_amt=$request->third_installment_amt;
        $installment->third_expiry_date=$request->third_expiry_date;
        $installment->fourth_installment_amt=$request->fourth_installment_amt;
        $installment->fourth_expiry_date=$request->fourth_expiry_date;
        $installment->fifth_installment_amt=$request->fifth_installment_amt;
        $installment->fifth_expiry_date=$request->fifth_expiry_date;

       $installment->sixth_installment_amt=null;
       $installment->sixth_expiry_date=null;
       $installment->seventh_installment_amt=null;
       $installment->seventh_expiry_date=null;
       $installment->eighth_installment_amt=null;
       $installment->eighth_expiry_date=null;
       $installment->ninth_installment_amt=null;
       $installment->ninth_expiry_date=null;
       $installment->tenth_installment_amt=null;
       $installment->tenth_expiry_date=null;
       $installment->eleventh_installment_amt=null;
       $installment->eleventh_expiry_date=null;
       $installment->twelveth_installment_amt=null;
       $installment->twelveth_exiry_date=null;

       }elseif($request->no_of_installment==6){

        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;
        $installment->third_installment_amt=$request->third_installment_amt;
        $installment->third_expiry_date=$request->third_expiry_date;
        $installment->fourth_installment_amt=$request->fourth_installment_amt;
        $installment->fourth_expiry_date=$request->fourth_expiry_date;
        $installment->fifth_installment_amt=$request->fifth_installment_amt;
        $installment->fifth_expiry_date=$request->fifth_expiry_date;
        $installment->sixth_installment_amt=$request->sixth_installment_amt;
        $installment->sixth_expiry_date=$request->sixth_expiry_date;

    
       $installment->seventh_installment_amt=null;
       $installment->seventh_expiry_date=null;
       $installment->eighth_installment_amt=null;
       $installment->eighth_expiry_date=null;
       $installment->ninth_installment_amt=null;
       $installment->ninth_expiry_date=null;
       $installment->tenth_installment_amt=null;
       $installment->tenth_expiry_date=null;
       $installment->eleventh_installment_amt=null;
       $installment->eleventh_expiry_date=null;
       $installment->twelveth_installment_amt=null;
       $installment->twelveth_exiry_date=null;

       }elseif($request->no_of_installment==7){

        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;
        $installment->third_installment_amt=$request->third_installment_amt;
        $installment->third_expiry_date=$request->third_expiry_date;
        $installment->fourth_installment_amt=$request->fourth_installment_amt;
        $installment->fourth_expiry_date=$request->fourth_expiry_date;
        $installment->fifth_installment_amt=$request->fifth_installment_amt;
        $installment->fifth_expiry_date=$request->fifth_expiry_date;
        $installment->sixth_installment_amt=$request->sixth_installment_amt;
        $installment->sixth_expiry_date=$request->sixth_expiry_date;
        $installment->seventh_installment_amt=$request->seventh_installment_amt;
        $installment->seventh_expiry_date=$request->seventh_expiry_date;
       
       $installment->eighth_installment_amt=null;
       $installment->eighth_expiry_date=null;
       $installment->ninth_installment_amt=null;
       $installment->ninth_expiry_date=null;
       $installment->tenth_installment_amt=null;
       $installment->tenth_expiry_date=null;
       $installment->eleventh_installment_amt=null;
       $installment->eleventh_expiry_date=null;
       $installment->twelveth_installment_amt=null;
       $installment->twelveth_exiry_date=null;

       }elseif($request->no_of_installment==8){

        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;
        $installment->third_installment_amt=$request->third_installment_amt;
        $installment->third_expiry_date=$request->third_expiry_date;
        $installment->fourth_installment_amt=$request->fourth_installment_amt;
        $installment->fourth_expiry_date=$request->fourth_expiry_date;
        $installment->fifth_installment_amt=$request->fifth_installment_amt;
        $installment->fifth_expiry_date=$request->fifth_expiry_date;
        $installment->sixth_installment_amt=$request->sixth_installment_amt;
        $installment->sixth_expiry_date=$request->sixth_expiry_date;
        $installment->seventh_installment_amt=$request->seventh_installment_amt;
        $installment->seventh_expiry_date=$request->seventh_expiry_date;
        $installment->eighth_installment_amt=$request->eighth_installment_amt;
        $installment->eighth_expiry_date=$request->eighth_expiry_date;

       $installment->ninth_installment_amt=null;
       $installment->ninth_expiry_date=null;
       $installment->tenth_installment_amt=null;
       $installment->tenth_expiry_date=null;
       $installment->eleventh_installment_amt=null;
       $installment->eleventh_expiry_date=null;
       $installment->twelveth_installment_amt=null;
       $installment->twelveth_exiry_date=null;

       }elseif($request->no_of_installment==9){

        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;
        $installment->third_installment_amt=$request->third_installment_amt;
        $installment->third_expiry_date=$request->third_expiry_date;
        $installment->fourth_installment_amt=$request->fourth_installment_amt;
        $installment->fourth_expiry_date=$request->fourth_expiry_date;
        $installment->fifth_installment_amt=$request->fifth_installment_amt;
        $installment->fifth_expiry_date=$request->fifth_expiry_date;
        $installment->sixth_installment_amt=$request->sixth_installment_amt;
        $installment->sixth_expiry_date=$request->sixth_expiry_date;
        $installment->seventh_installment_amt=$request->seventh_installment_amt;
        $installment->seventh_expiry_date=$request->seventh_expiry_date;
        $installment->eighth_installment_amt=$request->eighth_installment_amt;
        $installment->eighth_expiry_date=$request->eighth_expiry_date;
        $installment->ninth_installment_amt=$request->ninth_installment_amt;
        $installment->ninth_expiry_date=$request->ninth_expiry_date;

       $installment->tenth_installment_amt=null;
       $installment->tenth_expiry_date=null;
       $installment->eleventh_installment_amt=null;
       $installment->eleventh_expiry_date=null;
       $installment->twelveth_installment_amt=null;
       $installment->twelveth_exiry_date=null;

       }elseif($request->no_of_installment==10){

        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;
        $installment->third_installment_amt=$request->third_installment_amt;
        $installment->third_expiry_date=$request->third_expiry_date;
        $installment->fourth_installment_amt=$request->fourth_installment_amt;
        $installment->fourth_expiry_date=$request->fourth_expiry_date;
        $installment->fifth_installment_amt=$request->fifth_installment_amt;
        $installment->fifth_expiry_date=$request->fifth_expiry_date;
        $installment->sixth_installment_amt=$request->sixth_installment_amt;
        $installment->sixth_expiry_date=$request->sixth_expiry_date;
        $installment->seventh_installment_amt=$request->seventh_installment_amt;
        $installment->seventh_expiry_date=$request->seventh_expiry_date;
        $installment->eighth_installment_amt=$request->eighth_installment_amt;
        $installment->eighth_expiry_date=$request->eighth_expiry_date;
        $installment->ninth_installment_amt=$request->ninth_installment_amt;
        $installment->ninth_expiry_date=$request->ninth_expiry_date;
        $installment->tenth_installment_amt=$request->tenth_installment_amt;
        $installment->tenth_expiry_date=$request->tenth_expiry_date;

        $installment->eleventh_installment_amt=null;
        $installment->eleventh_expiry_date=null;
        $installment->twelveth_installment_amt=null;
        $installment->twelveth_exiry_date=null;

       }elseif($request->no_of_installment==11){

        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;
        $installment->third_installment_amt=$request->third_installment_amt;
        $installment->third_expiry_date=$request->third_expiry_date;
        $installment->fourth_installment_amt=$request->fourth_installment_amt;
        $installment->fourth_expiry_date=$request->fourth_expiry_date;
        $installment->fifth_installment_amt=$request->fifth_installment_amt;
        $installment->fifth_expiry_date=$request->fifth_expiry_date;
        $installment->sixth_installment_amt=$request->sixth_installment_amt;
        $installment->sixth_expiry_date=$request->sixth_expiry_date;
        $installment->seventh_installment_amt=$request->seventh_installment_amt;
        $installment->seventh_expiry_date=$request->seventh_expiry_date;
        $installment->eighth_installment_amt=$request->eighth_installment_amt;
        $installment->eighth_expiry_date=$request->eighth_expiry_date;
        $installment->ninth_installment_amt=$request->ninth_installment_amt;
        $installment->ninth_expiry_date=$request->ninth_expiry_date;
        $installment->tenth_installment_amt=$request->tenth_installment_amt;
        $installment->tenth_expiry_date=$request->tenth_expiry_date;
        $installment->eleventh_installment_amt=$request->eleventh_installment_amt;
        $installment->eleventh_expiry_date=$request->eleventh_expiry_date;

        $installment->twelveth_installment_amt=null;
        $installment->twelveth_exiry_date=null;

       }elseif($request->no_of_installment==12){

        $installment->first_installment_amt=$request->first_installment_amt;
        $installment->first_expiry_date=$request->first_expiry_date;
        $installment->second_installment_amt=$request->second_installment_amt;
        $installment->second_expiry_date=$request->second_expiry_date;
        $installment->third_installment_amt=$request->third_installment_amt;
        $installment->third_expiry_date=$request->third_expiry_date;
        $installment->fourth_installment_amt=$request->fourth_installment_amt;
        $installment->fourth_expiry_date=$request->fourth_expiry_date;
        $installment->fifth_installment_amt=$request->fifth_installment_amt;
        $installment->fifth_expiry_date=$request->fifth_expiry_date;
        $installment->sixth_installment_amt=$request->sixth_installment_amt;
        $installment->sixth_expiry_date=$request->sixth_expiry_date;
        $installment->seventh_installment_amt=$request->seventh_installment_amt;
        $installment->seventh_expiry_date=$request->seventh_expiry_date;
        $installment->eighth_installment_amt=$request->eighth_installment_amt;
        $installment->eighth_expiry_date=$request->eighth_expiry_date;
        $installment->ninth_installment_amt=$request->ninth_installment_amt;
        $installment->ninth_expiry_date=$request->ninth_expiry_date;
        $installment->tenth_installment_amt=$request->tenth_installment_amt;
        $installment->tenth_expiry_date=$request->tenth_expiry_date;
        $installment->eleventh_installment_amt=$request->eleventh_installment_amt;
        $installment->eleventh_expiry_date=$request->eleventh_expiry_date;
        $installment->twelveth_installment_amt=$request->twelveth_installment_amt;
        $installment->twelveth_exiry_date=$request->twelveth_exiry_date;

       }

       

       $installment->save();
        \Session::flash('success','Installment Information Updated Successfully.');
       return redirect()->back();


    }
}
