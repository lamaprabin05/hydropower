<?php

namespace App\Http\Controllers;

use App\FormShare;
use App\ShareGroup;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        dd(Auth::user()->id);
        $data['usertype'] = Auth::user()->usertype;

        if(Auth::user()->usertype == 0){
            $data['module'] =  'Super Admin';
            $data['total_user'] =  User::where('is_superadmin' , 0)->count();
            $data['user_shareholder'] =  User::where('is_superadmin' , 0)->where('usertype',1)->count();
            $data['user_assets'] =  User::where('is_superadmin' , 0)->where('usertype',2)->count();
            $data['user_dms'] =  User::where('is_superadmin' , 0)->where('usertype',3)->count();

//            dd($data);

        }
        elseif(Auth::user()->usertype == 1){
            $data['user_shareholder'] =  User::where('is_superadmin' , 0)->where('usertype',1)->count();
            $data['share_form'] =  FormShare::where('is_completed' , 0)->count();
            $data['completed_installment'] =  FormShare::where('is_completed' , 1)->count();
            $data['share_group'] =  ShareGroup::where('slug' , 1)->count();

            $data['module'] =  'Shareholder Module';

        }
        elseif(Auth::user()->usertype == 2){

            $data['total_user'] =  User::where('is_superadmin' , 0)->count();
            $data['user_shareholder'] =  User::where('is_superadmin' , 0)->where('usertype',1)->count();
            $data['user_assets'] =  User::where('is_superadmin' , 0)->where('usertype',2)->count();
            $data['user_dms'] =  User::where('is_superadmin' , 0)->where('usertype',3)->count();

            $data['module'] =  'Assets Management Module';

        }
        else{
            $data['total_user'] =  User::where('is_superadmin' , 0)->count();
            $data['user_shareholder'] =  User::where('is_superadmin' , 0)->where('usertype',1)->count();
            $data['user_assets'] =  User::where('is_superadmin' , 0)->where('usertype',2)->count();
            $data['user_dms'] =  User::where('is_superadmin' , 0)->where('usertype',3)->count();

            $data['module'] =  'Document Management Module';
        }

        return view('home',$data);
    }
}
