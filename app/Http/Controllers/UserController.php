<?php

namespace App\Http\Controllers;

use App\AuthGroup;
use App\AuthGroupUser;
use App\Helper\Tools;
use App\Permission;
use App\UserPermission;
use Carbon;
use Illuminate\Http\Request;
use App\User;
use Session;


use \Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index(){
    	$users =User::where('is_superadmin',0)->orderBY('id','desc')->get();

    	//to check  whether there is user or not.
    	$user =User::where('is_superadmin',0)->orderBY('id','desc')->count();
    	if($user == 0){
            $data['action'] = 0;
        }
    	else{
            foreach ($users as $value){
                //permission based on individual user.
                /*$permission_user[] = \DB::table('users as u')
                    ->select('u.*','p.display_name')
                    ->leftjoin('permission_user as pu','u.id','=' , 'pu.user_id')
                    ->leftjoin('permission as p', 'pu.permission_id' ,'=','p.id')
                    ->where('u.is_superadmin',0)
                    ->where('pu.user_id',$value->id)
                    ->first();*/

                $permission_user[] = \DB::table('auth_group_user as agu')
                    ->select('u.*','agu.group_id','ag.name as groupname')
                    ->leftjoin('users as u','agu.user_id','u.id')
                    ->leftjoin('auth_group as ag', 'ag.id','agu.group_id')
                    ->where('u.is_superadmin',0)
                    ->where('agu.user_id',$value->id)
                    ->first();
            }
//            dd($permission_user);


            $data['users'] = $permission_user;
            $data['action'] = 1;

        }
    	return view('user.index',$data);

    }

    //admin panel user edit.
    public function editUser(Request $request){
        $data['users']=User::where('id',$request->id)->first();
        $data['permission'] = Permission::all();
        $data['auth_group'] = AuthGroup::all();

        $data['assigned_group'] = AuthGroupUser::where('user_id',$request->id)->first();

//    	dd($data);
        return view('user.edit_user',$data);

    }

    //admin panel user edit.
    public function post_editUser(Request $request){
        $this->validate($request,[
            'name' => ['required', 'string', 'max:255'],
            'password' => [ 'confirmed'],
            'usertype' => ['required', 'integer'],
            'auth_group' => ['required'],
            'user_id' => 'required',
        ]);


        $user = User::find($request->user_id);
        $user->name = $request->name;

        if(isset($request->password)){
            $user->password = $request->password;

        }

        $user->email = $request->email;
        $user->usertype = $request->usertype;

        $user->updated_at = date('Y-m-d');

        if(isset($request->user_avatar)){
            Storage::delete('public/'.$user->user_avatar);

            $path = $request->file('user_avatar')->store('files/user_images/','public');
            $user->user_avatar = $path;
        }
        $user->save();

        //updating group which is assigned to a user.
        $auth_group_user = AuthGroupUser::where('user_id',$request->user_id)->first();
        $auth_group_user->user_id = $request->user_id;
        $auth_group_user->group_id = $request->auth_group;
        $auth_group_user->save();

        Session::flash('success','User Updated Successfully');
        return redirect()->route('admin.list_user');
    }

    //each logged in user edit
    public function getProfile($key){
//        dd($_GET);
        /*if(isset($_GET['key']))
        {
            $data['key'] = $_GET['key'];
        }
        else{
            $data['key'] = 1;
        }*/

        $data['key'] = $key;

        $data['user'] = User::where('id',\Auth::user()->id)->first();
        return view('user.profile',$data);
    }

    public function editProfile(Request $request){

        //if form is submitted from personal info settings.
        if($request->tab == 3){

            $validator = Validator::make($request->all(),[
                'user_id' => 'required',
                'name' => ['required', 'string', 'max:255'],
                'password' => ['required', 'confirmed','min:6'],
                'old_password' => 'required' ,
            ]);

            if ($validator->fails()) {
                $key = 3;
                Session::flash('error','Error while  updating Profile');
                return redirect()->route('profile.list_profile', $key)
                    ->withErrors($validator);
            }

            $user = User::where('id',$request->user_id)->first();

            //checking old password is correct or not.
            if (Hash::check($request->old_password, $user->password)) {
                $user->name = $request->name;
                $user->password = Hash::make($request->password);
                $user->save();

                $key = 1;
                Session::flash('success','Profile Updated Successfully');

                return redirect()->route('profile.list_profile', $key);
            }
            else{
                //password mismatch
                $key = 3;
                Session::flash('error','Error while  updating Profile');
                return redirect()->route('profile.list_profile', $key)
                    ->withErrors(['old_password' => 'Password is incorrect']);
            }
        }
        else
        {
            //form is submitted from change avatar.
            if (!empty($request->user_image)){
                $path = $request->file('user_image')->store('files/user_images/','public');
                Storage::delete('public/'.Auth::user()->user_avatar);

            }
            else{

                $path = Auth::user()->user_avatar;
            }

            $user = User::where('id',$request->user_id)->first();

            $user->user_avatar = $path;
            $user->save();

            $key = 1;
            Session::flash('success','Avatar Updated Successfully');
            return redirect()->route('profile.list_profile', $key);

        }

    }

    //accesssed from admin
    public function deleteUser(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
//            \Session::flash('error','Error in Deleting Group');
            $responseData = Tools::setResponse('fail', 'Missing Parameter', '', '');
            $status = 422;

            return response()->json($responseData,$status);
        }
        //deleting group permission also
        $auth_permission_user = AuthGroupUser::where('user_id',$request->id)->first();
        $auth_permission_user->delete();

        $data = User::find($request->id);
        Storage::delete('public/'.$data->user_avatar);
        $data->delete();

//        \Session::flash('success','User Deleted Successfully');
        $responseData = Tools::setResponse('success', 'User Deleted Successfully', '', '');
        return response($responseData,200);

    }


}
