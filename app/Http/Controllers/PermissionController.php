<?php

namespace App\Http\Controllers;

use App\AuthGroup;
use App\AuthGroupPermission;
use App\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public static function index()
    {
        $data['group'] = AuthGroup::all();
        return view('permissiongroup.index', $data);
    }

    public static function showGroupForm()
    {
        //if token is set then it is for edit.
        if (isset($_GET['token'])) {
            $token = base64_encode('edit' . $_GET['group']);
            $token = substr($token, 2, 7);

            if ($token == $_GET['token']) {
                if (empty($_GET['id'])) {
                    abort(500);
                }

                $data['group'] = AuthGroup::where('id', $_GET['id'])->first();
//                dd($data);
                $groupPermissions = AuthGroupPermission::where('group_id', $data['group']->id)->get();
//                dd($groupPermissions);
                $data['permissions'] = Permission::all();
//                dd($data);

                $g_perm = array();
                foreach ($groupPermissions as $val) {
                    $g_perm[] = $val->permission;
                }
                $data['groupPermissions'] = $g_perm;
//                dd($data);
                return view('permissiongroup.edit_group_permission', $data);


            } else {
                abort(404);
            }
        } //token is not set it is for add group
        else {
            $data['permissions'] = Permission::all();
            return view('permissiongroup.registergroup', $data);

        }

//        dd($data);


    }

    public static function postGroupForm(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'selected_permission' => 'required',

        ]);

//         dd($request->all());
        if ($request->action == "edit") {
            $auth_group = AuthGroup::find($request->id);
            $auth_group->name = $request->name;
            $auth_group->created_at = date('Y-m-d H:i:s');
            $auth_group->save();

            $auth_group_permission = AuthGroupPermission::where('group_id', $request->id)->delete();
        } else {
            $auth_group = new AuthGroup();
            $auth_group->name = $request->name;
            $auth_group->created_at = date('Y-m-d H:i:s');
            $auth_group->save();
        }
//        dd($request->all());


        foreach ($request->selected_permission as $value) {
            $permission = Permission::find($value);

            $auth_group_permission = new AuthGroupPermission();
            $auth_group_permission->group_id = $auth_group->id;
            $auth_group_permission->permission_id = $permission->id;
            $auth_group_permission->permission = $permission->code_name;
            $auth_group_permission->save();
        }

        return redirect()->route('admin.list_group');

    }

    public static function viewGroup($id)
    {
        $group_permission = AuthGroupPermission::where('group_id',$id)->get();
        foreach ($group_permission as $value){
           $permission_codename[] = Permission::where('code_name',$value->permission)->first(['display_name']);

        }

        $data['permissions'] = $permission_codename;
        $data['group'] = AuthGroup::find($id);

        return view('permissiongroup.view_group_permission',$data);

    }
}
