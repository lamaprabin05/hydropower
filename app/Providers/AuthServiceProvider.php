<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Permission;
use App\User;
use Illuminate\Support\Facades\Schema;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerUserPolicies();
    }

    public function registerUserPolicies(){

        Gate::before(function ($user, $ability) {
            if ($user->is_superadmin == 1) {
                return true;
            }
        });


        $permission= Permission::all();
        if(Schema::hasTable('permission')){

            foreach ($permission as $value){

                $permissions = $value->code_name;
                Gate::define($value->code_name, function($user) use ( $permissions ) {

                    return $user->hasAccess( $permissions );

                });
            }
        }   }


}
