<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodsRequisitionProduct extends Model
{
    protected $table = 'goods_requisition_product';
    public $timestamps = false;

    public function asset_name(){
        return $this->hasOne('App\AssetName','id','asset_name_id');
    }
   
   public function product()
    {
        return $this->hasOne('App\Product','id','product_id');
    }

     public function specification_name()
    {
        return $this->hasOne('App\SpecificationName','id','specification_name_id');
    }
    

}
