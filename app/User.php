<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','is_superadmin', 'usertype','user_avatar','created_by','user_designation'
    ];

    protected $dates = ['last_login'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function  hasAccess($permissions){
        $groupPermissions = $this->checkGroupPermission();
//        $userPermissions = $this->checkUserPermission();

//        if (in_array($permissions,$userPermissions) OR in_array($permissions,$groupPermissions)){
//            return true;
//        }
//        else{
//            return false;
//        }

        if ( in_array($permissions,$groupPermissions)){
            return true;
        }
        else{
            return false;
        }
    }

   /* protected function checkUserPermission(){
        $perm = array();
        $permissions = \DB::table('permission_user')
            ->select('permission')
            ->where('user_id',Auth::user()->id)
            ->get()
            ->toArray();

        foreach ($permissions as $val){
            $perm[] = $val->permission;
        }
        return $perm;
    }*/

    protected function checkGroupPermission(){
        $permissions = \DB::table('auth_group_permission as ap')
            ->select('ap.permission')
            ->join('auth_group_user as au','ap.group_id', '=' , 'au.group_id')
            ->where('au.user_id',Auth::user()->id)
            ->get()
            ->toArray();

        foreach ($permissions as $val){
            $perm[] = $val->permission;
        }
        return $perm;
    }


}
