-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2019 at 12:29 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `binod_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `asset_name`
--

CREATE TABLE `asset_name` (
  `id` int(11) NOT NULL,
  `sub_block_id` int(11) NOT NULL,
  `asset_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_name`
--

INSERT INTO `asset_name` (`id`, `sub_block_id`, `asset_name`) VALUES
(1, 1, 'Photocopy_Paper'),
(2, 1, 'Hard_Cover'),
(3, 2, 'Manager Table');

-- --------------------------------------------------------

--
-- Table structure for table `asset_types`
--

CREATE TABLE `asset_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_types`
--

INSERT INTO `asset_types` (`id`, `name`) VALUES
(1, 'Consumables'),
(2, 'Capital');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_group`
--

INSERT INTO `auth_group` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Requestor Group', '2019-05-30', '2019-05-30'),
(2, 'Approver Group', '2019-05-30', '2019-05-30'),
(3, 'Checker Group', '2019-05-30', '2019-05-30'),
(4, 'Store Keeper', '2019-05-30', '2019-05-30');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permission`
--

CREATE TABLE `auth_group_permission` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `permission` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_group_permission`
--

INSERT INTO `auth_group_permission` (`id`, `group_id`, `permission_id`, `permission`) VALUES
(1, 1, 1, 'requestor'),
(2, 2, 2, 'approver'),
(3, 3, 4, 'checker'),
(4, 4, 3, 'store_keeper');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_user`
--

CREATE TABLE `auth_group_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_group_user`
--

INSERT INTO `auth_group_user` (`id`, `user_id`, `group_id`) VALUES
(1, 2, 1),
(2, 3, 3),
(3, 4, 2),
(4, 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE `blocks` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `block_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`id`, `category_id`, `block_name`) VALUES
(1, 1, 'printing_and_stationary'),
(2, 2, 'furniture_and_fixture'),
(3, 2, 'plant and machinery'),
(4, 2, 'Land And Building'),
(5, 1, 'Food and Clothing');

-- --------------------------------------------------------

--
-- Table structure for table `dispatch_product`
--

CREATE TABLE `dispatch_product` (
  `id` int(11) NOT NULL,
  `handover_id` varchar(20) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `goods_requisition_product_id` bigint(11) NOT NULL,
  `custodian_id` bigint(20) DEFAULT NULL,
  `remarks` text,
  `dispatched_qty` int(11) NOT NULL,
  `handover_date` date DEFAULT NULL,
  `returned_date` date DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `form_share`
--

CREATE TABLE `form_share` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `group_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `share_group_id` int(11) NOT NULL DEFAULT '0',
  `form_status` int(11) DEFAULT NULL,
  `proceeded_for` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_completed` int(11) DEFAULT '0',
  `notification` int(11) NOT NULL,
  `temporary_address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citizenship_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citizenship_issued_date` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citizenship_issued_place` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_place` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grandfather_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `husband_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wife_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominee_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominee_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominee_contact_number` bigint(15) DEFAULT NULL,
  `relation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shareholder_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `permanent_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `registration_number` bigint(20) DEFAULT NULL,
  `pan_number` bigint(20) DEFAULT NULL,
  `website` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citizenship_scan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_certificate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `protected_citizenship` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `goods_purchase_form`
--

CREATE TABLE `goods_purchase_form` (
  `id` int(11) NOT NULL,
  `goods_requisition_product_id` bigint(20) NOT NULL,
  `goods_requisition_number` bigint(11) NOT NULL,
  `assets_name` varchar(255) DEFAULT NULL,
  `specification_name` varchar(255) NOT NULL,
  `form_status` int(11) NOT NULL,
  `proceeded_to` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `requisition_by` int(11) DEFAULT NULL,
  `requestor_remarks` text,
  `checked_by` int(11) DEFAULT NULL,
  `checker_remarks` text,
  `approved_by` int(11) DEFAULT NULL,
  `approver_remarks` text,
  `rejected_status` int(11) DEFAULT '0',
  `rejected_by` int(11) DEFAULT NULL,
  `rejected_reason` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `goods_requisition_form`
--

CREATE TABLE `goods_requisition_form` (
  `id` int(11) NOT NULL,
  `requisition_number` text COLLATE utf8_unicode_ci,
  `purpose` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reason` text COLLATE utf8_unicode_ci,
  `form_status` int(11) NOT NULL,
  `proceeded_to` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_rejected` int(11) DEFAULT NULL,
  `rejected_reason` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `checker_id` int(11) DEFAULT NULL,
  `checker_remarks` text COLLATE utf8_unicode_ci,
  `approver_id` int(11) DEFAULT NULL,
  `approver_remarks` text COLLATE utf8_unicode_ci,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `goods_requisition_product`
--

CREATE TABLE `goods_requisition_product` (
  `id` int(11) NOT NULL,
  `goods_requisition_form_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `asset_name_id` int(11) DEFAULT NULL,
  `specification_name_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `requirement_date` datetime NOT NULL,
  `purchase_requisition` int(11) DEFAULT NULL,
  `proceed_status` tinyint(1) DEFAULT '0',
  `is_rejected` tinyint(4) DEFAULT '0' COMMENT '0 - activated 1-rejected',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `good_requisition_files`
--

CREATE TABLE `good_requisition_files` (
  `id` int(11) NOT NULL,
  `product_requisition_number` bigint(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `requested_qty` int(11) NOT NULL,
  `dispatched_qty` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `required_date` date NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dispatch_status` tinyint(1) NOT NULL DEFAULT '0',
  `request_check_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `handover_product`
--

CREATE TABLE `handover_product` (
  `id` bigint(20) NOT NULL,
  `dispatch_product_id` bigint(20) NOT NULL,
  `custodian_id` int(11) NOT NULL,
  `custodian_name` varchar(50) NOT NULL,
  `handover_date` date NOT NULL,
  `returned_date` date NOT NULL,
  `remarks` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `installment_information`
--

CREATE TABLE `installment_information` (
  `id` int(11) NOT NULL,
  `share_form_id` bigint(11) NOT NULL,
  `no_of_installment` int(11) NOT NULL,
  `remaining_installment` int(11) DEFAULT NULL,
  `committed_amount` int(11) NOT NULL,
  `total_paid_amount` bigint(15) NOT NULL,
  `first_installment_amt` int(11) DEFAULT NULL,
  `first_expiry_date` date DEFAULT NULL,
  `second_installment_amt` int(11) DEFAULT NULL,
  `second_expiry_date` date DEFAULT NULL,
  `third_installment_amt` int(11) DEFAULT NULL,
  `third_expiry_date` date DEFAULT NULL,
  `fourth_installment_amt` int(11) DEFAULT NULL,
  `fourth_expiry_date` date DEFAULT NULL,
  `fifth_installment_amt` int(11) DEFAULT NULL,
  `fifth_expiry_date` date DEFAULT NULL,
  `sixth_installment_amt` int(11) DEFAULT NULL,
  `sixth_expiry_date` date DEFAULT NULL,
  `seventh_installment_amt` int(11) DEFAULT NULL,
  `seventh_expiry_date` date DEFAULT NULL,
  `eighth_installment_amt` int(11) DEFAULT NULL,
  `eighth_expiry_date` date DEFAULT NULL,
  `ninth_installment_amt` int(11) DEFAULT NULL,
  `ninth_expiry_date` date DEFAULT NULL,
  `tenth_installment_amt` int(11) DEFAULT NULL,
  `tenth_expiry_date` date DEFAULT NULL,
  `eleventh_installment_amt` int(11) DEFAULT NULL,
  `eleventh_expiry_date` date DEFAULT NULL,
  `twelveth_installment_amt` int(11) DEFAULT NULL,
  `twelveth_exiry_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_goods_requisition_product`
--

CREATE TABLE `log_goods_requisition_product` (
  `id` int(11) NOT NULL,
  `goods_requisition_product_id` int(11) NOT NULL,
  `goods_requisition_form_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `requirement_date` date NOT NULL,
  `is_rejected` tinyint(4) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_purchase_approved_product`
--

CREATE TABLE `log_purchase_approved_product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `vendor_id` int(11) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `product_unit_cost` double NOT NULL,
  `product_estimated_cost` double NOT NULL,
  `product_image` text NOT NULL,
  `product_bill_image` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `installment_name` varchar(50) NOT NULL,
  `applicant_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `form_id` int(11) NOT NULL,
  `bank_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `branch_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(50) NOT NULL,
  `installment_number` int(11) NOT NULL,
  `installment_nep_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `installment_eng_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `installment_paid_amount` double NOT NULL,
  `advance_payment` double NOT NULL,
  `less_payment` double NOT NULL,
  `bill_image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `installment_name`, `applicant_name`, `form_id`, `bank_name`, `branch_name`, `account_number`, `installment_number`, `installment_nep_date`, `installment_eng_date`, `installment_paid_amount`, `advance_payment`, `less_payment`, `bill_image`, `created_by`, `created_at`) VALUES
(1, 'first', '1', 4, '1', '2', '3', 1, 'सोम, फागुन २७, २०७५', '2019-03-12', 4, 0, 149996, 'files/shareholder/4/payments/50GVaAfVPRQ5dGcxYw2Pkg5nnSsNBHOjLZPTMJAT.jpeg', 1, '2019-03-11');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `display_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `display_name`, `code_name`) VALUES
(1, 'Requestor', 'requestor'),
(2, 'Approver', 'approver'),
(3, 'Store Keeper', 'store_keeper'),
(4, 'Checker', 'checker');

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `permission` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `batch_id` bigint(20) NOT NULL,
  `batch_number` varchar(50) NOT NULL,
  `asset_type_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `sub_block_id` int(11) NOT NULL,
  `asset_name_id` int(11) NOT NULL,
  `specification_name_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `unit_cost` decimal(10,0) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `discount` float DEFAULT NULL,
  `vat` float NOT NULL,
  `expected_life` int(11) NOT NULL,
  `depreciation_method` varchar(255) NOT NULL,
  `depreciation_value` int(11) NOT NULL,
  `residual_price` float NOT NULL,
  `bill_issue_date` datetime DEFAULT NULL,
  `ready_to_use_date` datetime DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `bill` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL,
  `nepali_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_logs`
--

CREATE TABLE `product_logs` (
  `id` int(11) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `batch_id` bigint(20) NOT NULL,
  `batch_number` varchar(50) NOT NULL,
  `asset_type_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `sub_block_id` int(11) NOT NULL,
  `asset_name_id` int(11) NOT NULL,
  `specification_name_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `unit_cost` decimal(10,0) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `discount` float DEFAULT NULL,
  `vat` float NOT NULL,
  `expected_life` int(11) NOT NULL,
  `depreciation_method` varchar(255) NOT NULL,
  `depreciation_value` int(11) NOT NULL,
  `residual_price` float NOT NULL,
  `bill_issue_date` datetime DEFAULT NULL,
  `ready_to_use_date` datetime DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `bill` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL,
  `nepali_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_vendors`
--

CREATE TABLE `product_vendors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact` bigint(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` date NOT NULL,
  `pan_vat_number` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_number` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_vendors`
--

INSERT INTO `product_vendors` (`id`, `name`, `address`, `contact`, `created_at`, `updated_at`, `pan_vat_number`, `email`, `mobile_number`) VALUES
(1, 'Vendor1', 'Vendor Address', 87682421, '2019-04-21 00:00:00', '2019-05-17', 12, 'bino@gmail.com', 12345678),
(2, 'Vendor Name', 'Pharping,Kathamndu', 984116701, '2019-05-16 13:40:43', '2019-05-20', 4455, 'bindasbino@gmail.com', 9861866606),
(3, 'Vendor Check', 'Vendor Address', 98414446714, '2019-05-16 13:47:16', '2019-05-20', 523541, 'bindasbino@gmail.com', 98414116701),
(4, 'Mobile Vendor', 'Vendor address', 9841116701, '2019-05-17 15:06:59', '2019-05-17', 7788, 'bindasbino@gmail.com', 24254252);

-- --------------------------------------------------------

--
-- Table structure for table `share_group`
--

CREATE TABLE `share_group` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` int(50) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `share_group`
--

INSERT INTO `share_group` (`id`, `name`, `slug`, `created_at`, `created_by`) VALUES
(1, 'test', 2, '2019-03-04 23:49:07', 1),
(2, '123', 2, '2019-03-06 05:00:24', 1),
(3, '123', 2, '2019-03-06 05:00:26', 1),
(4, '123', 2, '2019-03-06 05:00:33', 1),
(5, '123', 2, '2019-03-06 05:00:34', 1),
(6, '123', 2, '2019-03-06 05:03:17', 1),
(7, 'Melyssa Garner', 2, '2019-03-06 00:08:54', 8),
(8, 'p', 1, '2019-03-07 06:50:34', 1),
(9, 'prabin', 1, '2019-03-06 21:33:16', 8),
(10, '12', 2, '2019-03-06 21:56:58', 1),
(11, '12', 2, '2019-03-06 21:57:06', 1),
(12, '12', 2, '2019-03-06 21:57:30', 1),
(13, '12', 2, '2019-03-06 22:04:22', 1),
(14, 'test', 2, '2019-03-06 22:14:46', 1),
(15, 'test', 2, '2019-03-06 22:15:33', 1),
(16, '12', 2, '2019-03-06 22:33:34', 1),
(17, '12', 2, '2019-03-06 22:34:11', 1),
(18, 'prabin', 2, '2019-03-06 23:18:22', 1),
(19, 'prabin', 2, '2019-03-06 23:18:25', 1),
(20, 'prabin', 2, '2019-03-06 23:18:28', 1),
(21, 'prabin', 2, '2019-03-06 23:19:28', 1),
(22, '1', 2, '2019-03-10 18:15:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `specification_name`
--

CREATE TABLE `specification_name` (
  `id` int(11) NOT NULL,
  `asset_name_id` int(11) NOT NULL,
  `specification_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `specification_name`
--

INSERT INTO `specification_name` (`id`, `asset_name_id`, `specification_name`) VALUES
(1, 3, 'Steel'),
(2, 3, 'wooden'),
(3, 1, 'a 4 70 mm ');

-- --------------------------------------------------------

--
-- Table structure for table `sub_blocks`
--

CREATE TABLE `sub_blocks` (
  `id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `sub_block_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_blocks`
--

INSERT INTO `sub_blocks` (`id`, `block_id`, `sub_block_name`) VALUES
(1, 1, 'Stationary'),
(2, 2, 'Tables');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_superadmin` int(11) NOT NULL,
  `usertype` int(11) NOT NULL,
  `user_designation` int(11) NOT NULL,
  `user_avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `is_superadmin`, `usertype`, `user_designation`, `user_avatar`, `created_by`, `remember_token`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'superadmin@hydro.com', NULL, '$2y$10$q9/qJSRy5i3nA6b7Iix1auIlJjIZ5Bw213RXkjZKUau2x3NDqeJHu', 1, 0, 0, '', 1, 'WD3xkLtYwThrjk1JXSa9X2AyyKZ5aryumrzaxxLsAKiLCvK2IAJrtCCt5RGQ', '2019-06-04 07:25:36', NULL, '2019-06-04 07:25:36'),
(2, 'requestor', 'requestor@hydro.com', NULL, '$2y$10$smfV2qYq.WWzFPg83mFFvOfKA.y4prEXybYY4bXLIvTjywd4Rk1yC', 0, 2, 1, 'files/user_images//uuYcYVpJ298mT4B4zl2k1y0SKq2uxq03pqcjdH2T.jpeg', 1, 'RWcL23pzCYltkehdj1VQFdarPFlDgkAF1KShRXZXIt1q5yJzWecR6zv9PZ6U', '2019-05-30 11:39:39', '2019-05-30 11:37:49', '2019-05-30 11:39:39'),
(3, 'checker', 'checker@hydro.com', NULL, '$2y$10$zxGy/AyH4zApdKHPwZFt2O0/jlvxzK5xNXtbLe9MrCSGVXPrwygQS', 0, 2, 2, '', 1, 'WmvaEN2W2MExv5fu1LTyiO94Vr2bHXHeMJOSPQ2Fq07fc3F5zEMbkaL2cuvs', '2019-06-04 07:30:54', '2019-05-30 11:38:33', '2019-06-04 07:30:54'),
(4, 'approver', 'approver@hydro.com', NULL, '$2y$10$MstZU4bqfTwFec4Hf4/OFeB5W7P1dHXTH8kseMk0qtJ3RX3q6BG1y', 0, 2, 3, '', 1, 'ESNan4os6ZCAID3PKMuV7iGrKQDvLE11bBJobrH0ufYennGgUnNW7pIfwD9b', '2019-06-04 08:15:21', '2019-05-30 11:39:03', '2019-06-04 08:15:21'),
(5, 'Store Keeper', 'store@hydro.com', NULL, '$2y$10$5zZmSc3NxPONj8CusRxMkukFFCR4Rp6FFj1ECgTefjaXLS4r/O.kG', 0, 2, 4, '', 1, NULL, '2019-06-04 10:28:26', '2019-06-04 10:28:10', '2019-06-04 10:28:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_name`
--
ALTER TABLE `asset_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_types`
--
ALTER TABLE `asset_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group_permission`
--
ALTER TABLE `auth_group_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group_user`
--
ALTER TABLE `auth_group_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dispatch_product`
--
ALTER TABLE `dispatch_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_share`
--
ALTER TABLE `form_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goods_purchase_form`
--
ALTER TABLE `goods_purchase_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goods_requisition_form`
--
ALTER TABLE `goods_requisition_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goods_requisition_product`
--
ALTER TABLE `goods_requisition_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `good_requisition_files`
--
ALTER TABLE `good_requisition_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `handover_product`
--
ALTER TABLE `handover_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installment_information`
--
ALTER TABLE `installment_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_goods_requisition_product`
--
ALTER TABLE `log_goods_requisition_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_purchase_approved_product`
--
ALTER TABLE `log_purchase_approved_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_logs`
--
ALTER TABLE `product_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_vendors`
--
ALTER TABLE `product_vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `share_group`
--
ALTER TABLE `share_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specification_name`
--
ALTER TABLE `specification_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_blocks`
--
ALTER TABLE `sub_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_name`
--
ALTER TABLE `asset_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `asset_types`
--
ALTER TABLE `asset_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `auth_group_permission`
--
ALTER TABLE `auth_group_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `auth_group_user`
--
ALTER TABLE `auth_group_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dispatch_product`
--
ALTER TABLE `dispatch_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `form_share`
--
ALTER TABLE `form_share`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `goods_purchase_form`
--
ALTER TABLE `goods_purchase_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `goods_requisition_form`
--
ALTER TABLE `goods_requisition_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `goods_requisition_product`
--
ALTER TABLE `goods_requisition_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `good_requisition_files`
--
ALTER TABLE `good_requisition_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `handover_product`
--
ALTER TABLE `handover_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `installment_information`
--
ALTER TABLE `installment_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_goods_requisition_product`
--
ALTER TABLE `log_goods_requisition_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_purchase_approved_product`
--
ALTER TABLE `log_purchase_approved_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_logs`
--
ALTER TABLE `product_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_vendors`
--
ALTER TABLE `product_vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `share_group`
--
ALTER TABLE `share_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `specification_name`
--
ALTER TABLE `specification_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sub_blocks`
--
ALTER TABLE `sub_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
