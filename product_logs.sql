-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2019 at 12:49 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hydropower`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_logs`
--

CREATE TABLE `product_logs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `asset_type_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `bill` varchar(255) NOT NULL,
  `unit_cost` decimal(10,0) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `vendor_company_name` varchar(255) NOT NULL,
  `vendor_company_address` varchar(255) NOT NULL,
  `vendor_contact` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `nepali_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_logs`
--

INSERT INTO `product_logs` (`id`, `name`, `description`, `asset_type_id`, `image`, `bill`, `unit_cost`, `quantity`, `amount`, `vendor_name`, `vendor_company_name`, `vendor_company_address`, `vendor_contact`, `created_at`, `nepali_date`) VALUES
(29, 'New Product', 'Sed commodi et iusto', 1, 'files/assets/product/image/2019_04_05/oFML9jutADUVzzD1bc7HPpBErmqwNbNh2ESNVB7F.jpeg', 'files/assets/product/bill/2019_04_05/vqoRXfk1u60nG19YMDcFe1FI4NjyHL0gl9I9j7fC.jpeg', '40', 1234, 77, 'Elliott Macias', 'Mcgee and Heath Trading', 'Macias and Cohen Associates', 95, '2019-04-05', 'शुक्र, चैत २२, २०७५'),
(30, 'New Product', 'Sed commodi et iusto', 1, 'files/assets/product/image/2019_04_05/oFML9jutADUVzzD1bc7HPpBErmqwNbNh2ESNVB7F.jpeg', 'files/assets/product/bill/2019_04_05/vqoRXfk1u60nG19YMDcFe1FI4NjyHL0gl9I9j7fC.jpeg', '40', 1234, 77, 'Elliott Macias', 'Mcgee and Heath Trading', 'Macias and Cohen Associates', 95, '2019-04-05', 'शुक्र, चैत २२, २०७५'),
(31, 'New Product', 'Sed commodi et iusto', 1, 'files/assets/product/image/2019_04_05/oFML9jutADUVzzD1bc7HPpBErmqwNbNh2ESNVB7F.jpeg', 'files/assets/product/bill/2019_04_05/vqoRXfk1u60nG19YMDcFe1FI4NjyHL0gl9I9j7fC.jpeg', '40', 1234, 88, 'Elliott Macias', 'Mcgee and Heath Trading', 'Macias and Cohen Associates', 95, '2019-04-05', 'शुक्र, चैत २२, २०७५');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_logs`
--
ALTER TABLE `product_logs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_logs`
--
ALTER TABLE `product_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
