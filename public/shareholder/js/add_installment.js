$(document).ready(function() {
    $('#individual').hide();
    $('#company').hide();

    $('#accounts_type').on('change', function(e){
        var data = $(this).val();
        if (data =='1'){
            $('#individual').show();
            $('#company').hide();
        }
        else if(data =='2'){
            $('#individual').hide();
            $('#company').show();
        }
    });

    $('#share_group_type').on('change', function(e){
        var data = $(this).val();
        if (data =='individual'){
            $('#individual_name').show();
            $('#group_name').hide();
        }
        else if(data =='group'){
            $('#group_name').show();
            $('#individual_name').hide();
        }
    });
});


$(document).ready(function() {

        $('#installment_1').hide();
        $('#installment_2').hide();
        $('#installment_3').hide();
        $('#installment_4').hide();
        $('#installment_5').hide();
        $('#installment_6').hide();
        $('#installment_7').hide();
        $('#installment_8').hide();
        $('#installment_9').hide();
        $('#installment_10').hide();
        $('#installment_11').hide();
        $('#installment_12').hide();


$('#installment').on('change', function(e){
    var data = $(this).val();

    
    if (data =='1'){
        $('#installment_1').show();
        $('#installment_2').hide();
        $('#installment_3').hide();
        $('#installment_4').hide();
        $('#installment_5').hide();
        $('#installment_6').hide();
        $('#installment_7').hide();
        $('#installment_8').hide();
        $('#installment_9').hide();
        $('#installment_10').hide();
        $('#installment_11').hide();
        $('#installment_12').hide();
        
    }

    else if (data == '2'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').hide();
        $('#installment_4').hide();
        $('#installment_5').hide();
        $('#installment_6').hide();
        $('#installment_7').hide();
        $('#installment_8').hide();
        $('#installment_9').hide();
        $('#installment_10').hide();
        $('#installment_11').hide();
        $('#installment_12').hide();
    }

    else if (data == '3'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').show();
        $('#installment_4').hide();
        $('#installment_5').hide();
        $('#installment_6').hide();
        $('#installment_7').hide();
        $('#installment_8').hide();
        $('#installment_9').hide();
        $('#installment_10').hide();
        $('#installment_11').hide();
        $('#installment_12').hide();
    }

    else if (data == '4'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').show();
        $('#installment_4').show();
        $('#installment_5').hide();
        $('#installment_6').hide();
        $('#installment_7').hide();
        $('#installment_8').hide();
        $('#installment_9').hide();
        $('#installment_10').hide();
        $('#installment_11').hide();
        $('#installment_12').hide();
    }

    else if (data == '5'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').show();
        $('#installment_4').show();
        $('#installment_5').show();
        $('#installment_6').hide();
        $('#installment_7').hide();
        $('#installment_8').hide();
        $('#installment_9').hide();
        $('#installment_10').hide();
        $('#installment_11').hide();
        $('#installment_12').hide();
    }

    else if (data == '6'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').show();
        $('#installment_4').show();
        $('#installment_5').show();
        $('#installment_6').show();
        $('#installment_7').hide();
        $('#installment_8').hide();
        $('#installment_9').hide();
        $('#installment_10').hide();
        $('#installment_11').hide();
        $('#installment_12').hide();
    }

    else if (data == '7'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').show();
        $('#installment_4').show();
        $('#installment_5').show();
        $('#installment_6').show();
        $('#installment_7').show();
        $('#installment_8').hide();
        $('#installment_9').hide();
        $('#installment_10').hide();
        $('#installment_11').hide();
        $('#installment_12').hide();
    }
    else if (data == '8'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').show();
        $('#installment_4').show();
        $('#installment_5').show();
        $('#installment_6').show();
        $('#installment_7').show();
        $('#installment_8').show();
        $('#installment_9').hide();
        $('#installment_10').hide();
        $('#installment_11').hide();
        $('#installment_12').hide();
    }

    else if (data == '9'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').show();
        $('#installment_4').show();
        $('#installment_5').show();
        $('#installment_6').show();
        $('#installment_7').show();
        $('#installment_8').show();
        $('#installment_9').show();
        $('#installment_10').hide();
        $('#installment_11').hide();
        $('#installment_12').hide();
    }

    else if (data == '10'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').show();
        $('#installment_4').show();
        $('#installment_5').show();
        $('#installment_6').show();
        $('#installment_7').show();
        $('#installment_8').show();
        $('#installment_9').show();
        $('#installment_10').show();
        $('#installment_11').hide();
        $('#installment_12').hide();
    }

    else if (data == '11'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').show();
        $('#installment_4').show();
        $('#installment_5').show();
        $('#installment_6').show();
        $('#installment_7').show();
        $('#installment_8').show();
        $('#installment_9').show();
        $('#installment_10').show();
        $('#installment_11').show();
        $('#installment_12').hide();
    }

    else if (data == '12'){
        $('#installment_1').show();
        $('#installment_2').show();
        $('#installment_3').show();
        $('#installment_4').show();
        $('#installment_5').show();
        $('#installment_6').show();
        $('#installment_7').show();
        $('#installment_8').show();
        $('#installment_9').show();
        $('#installment_10').show();
        $('#installment_11').show();
        $('#installment_12').show();
    }

    
});
});

$(document).ready(function() {
    $('#accounts_type').on('change', function(e){
        var data = $(this).val();
        if (data =='1'){
            $('.individual').show();
            $('.company').hide();
        }
        else if(data =='2'){
            $('.individual').hide();
            $('.company').show();
        }
    });

    
});


$(document).ready(function() {

    var data = $('#edit_installment').val();
    myFunction(data);
    $('#edit_installment').on('change', function() {
       var data = $(this).val();
        myFunction(data);
     });

    function myFunction(data){
        var data = data;
        if (data =='1'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').hide();
        $('#edit_installment_3').hide();
        $('#edit_installment_4').hide();
        $('#edit_installment_5').hide();
        $('#edit_installment_6').hide();
        $('#edit_installment_7').hide();
        $('#edit_installment_8').hide();
        $('#edit_installment_9').hide();
        $('#edit_installment_10').hide();
        $('#edit_installment_11').hide();
        $('#edit_installment_12').hide();
        
    }

    else if (data == '2'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').hide();
        $('#edit_installment_4').hide();
        $('#edit_installment_5').hide();
        $('#edit_installment_6').hide();
        $('#edit_installment_7').hide();
        $('#edit_installment_8').hide();
        $('#edit_installment_9').hide();
        $('#edit_installment_10').hide();
        $('#edit_installment_11').hide();
        $('#edit_installment_12').hide();
    }

    else if (data == '3'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').show();
        $('#edit_installment_4').hide();
        $('#edit_installment_5').hide();
        $('#edit_installment_6').hide();
        $('#edit_installment_7').hide();
        $('#edit_installment_8').hide();
        $('#edit_installment_9').hide();
        $('#edit_installment_10').hide();
        $('#edit_installment_11').hide();
        $('#edit_installment_12').hide();
    }

    else if (data == '4'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').show();
        $('#edit_installment_4').show();
        $('#edit_installment_5').hide();
        $('#edit_installment_6').hide();
        $('#edit_installment_7').hide();
        $('#edit_installment_8').hide();
        $('#edit_installment_9').hide();
        $('#edit_installment_10').hide();
        $('#edit_installment_11').hide();
        $('#edit_installment_12').hide();
    }

    else if (data == '5'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').show();
        $('#edit_installment_4').show();
        $('#edit_installment_5').show();
        $('#edit_installment_6').hide();
        $('#edit_installment_7').hide();
        $('#edit_installment_8').hide();
        $('#edit_installment_9').hide();
        $('#edit_installment_10').hide();
        $('#edit_installment_11').hide();
        $('#edit_installment_12').hide();
    }

    else if (data == '6'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').show();
        $('#edit_installment_4').show();
        $('#edit_installment_5').show();
        $('#edit_installment_6').show();
        $('#edit_installment_7').hide();
        $('#edit_installment_8').hide();
        $('#edit_installment_9').hide();
        $('#edit_installment_10').hide();
        $('#edit_installment_11').hide();
        $('#edit_installment_12').hide();
    }

    else if (data == '7'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').show();
        $('#edit_installment_4').show();
        $('#edit_installment_5').show();
        $('#edit_installment_6').show();
        $('#edit_installment_7').show();
        $('#edit_installment_8').hide();
        $('#edit_installment_9').hide();
        $('#edit_installment_10').hide();
        $('#edit_installment_11').hide();
        $('#edit_installment_12').hide();
    }
    else if (data == '8'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').show();
        $('#edit_installment_4').show();
        $('#edit_installment_5').show();
        $('#edit_installment_6').show();
        $('#edit_installment_7').show();
        $('#edit_installment_8').show();
        $('#edit_installment_9').hide();
        $('#edit_installment_10').hide();
        $('#edit_installment_11').hide();
        $('#edit_installment_12').hide();
    }

    else if (data == '9'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').show();
        $('#edit_installment_4').show();
        $('#edit_installment_5').show();
        $('#edit_installment_6').show();
        $('#edit_installment_7').show();
        $('#edit_installment_8').show();
        $('#edit_installment_9').show();
        $('#edit_installment_10').hide();
        $('#edit_installment_11').hide();
        $('#edit_installment_12').hide();
    }

    else if (data == '10'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').show();
        $('#edit_installment_4').show();
        $('#edit_installment_5').show();
        $('#edit_installment_6').show();
        $('#edit_installment_7').show();
        $('#edit_installment_8').show();
        $('#edit_installment_9').show();
        $('#edit_installment_10').show();
        $('#edit_installment_11').hide();
        $('#edit_installment_12').hide();
    }

    else if (data == '11'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').show();
        $('#edit_installment_4').show();
        $('#edit_installment_5').show();
        $('#edit_installment_6').show();
        $('#edit_installment_7').show();
        $('#edit_installment_8').show();
        $('#edit_installment_9').show();
        $('#edit_installment_10').show();
        $('#edit_installment_11').show();
        $('#edit_installment_12').hide();
    }

    else if (data == '12'){
        $('#edit_installment_1').show();
        $('#edit_installment_2').show();
        $('#edit_installment_3').show();
        $('#edit_installment_4').show();
        $('#edit_installment_5').show();
        $('#edit_installment_6').show();
        $('#edit_installment_7').show();
        $('#edit_installment_8').show();
        $('#edit_installment_9').show();
        $('#edit_installment_10').show();
        $('#edit_installment_11').show();
        $('#edit_installment_12').show();
    }
    }

    

});
