<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
//            ['display_name' => 'Share Holder', 'code_name' => 'shareholder'],
//            ['display_name' => 'Assets Management System', 'code_name' => 'ams'],
//            ['display_name' => 'Document Management System', 'code_name' => 'dms'],
//            ['display_name' => 'Share Holder Admin', 'code_name' => 'shareholder_admin'],
//            ['display_name' => 'Can Add Product', 'code_name' => 'add_product'],
            ['display_name' => 'Requestor', 'code_name' => 'requestor'],
            ['display_name' => 'Approver', 'code_name' => 'approver'],
            ['display_name' => 'Store Keeper', 'code_name' => 'store_keeper'],
            ['display_name' => 'Checker', 'code_name' => 'checker'],
//            ['display_name' => 'Can Add User', 'code_name' => 'add_user'],
        ];

        foreach ($permission as $value){
            Permission::create($value);
        }
    }
}
