<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'superadmin',
            'email' =>'superadmin@hydro.com',
            'password' => bcrypt('password'),
            'is_superadmin' => 1,
            'usertype' => 0,
            'created_by' => 1,
            'user_avatar' => '',
            'user_designation'=>1,
        ]);

    }
}
