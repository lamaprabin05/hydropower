<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//if(!defined('STATIC_DIR')) define('STATIC_DIR','/public/');
//defines
if(!defined('STATIC_DIR')) define('STATIC_DIR','/public/');
if(!defined('ROOT_DIR')) define('ROOT_DIR','');
if(!defined('ROOT_DIR_NAME')) define('ROOT_DIR_NAME','');
@require('assets.php');

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/formshareholder', 'ShareHolderController@index')->name('/admin/formshareholder');


Route::group(['prefix' =>ROOT_DIR.'admin', 'middleware' => ['auth'], 'as' => 'admin.'], function () {

    //user register and edit routes
    Route::get('/user', ['uses'=>'UserController@index','as'=>'list_user']);

    Route::get('/user/register',['uses'=>'Auth\RegisterController@showRegistrationForm','as'=>'register']);

    //user edit
    Route::get('/user/edit/{id}', ['uses'=>'UserController@editUser','as'=>'edit_user']);
    Route::post('/user/edit', ['uses'=>'UserController@post_editUser','as'=>'update_user']);

    //delete user
    Route::post('/user/delete', ['uses'=>'UserController@deleteUser','as'=>'delete_user']);

    //list user group permission
    Route::get('/permission/group', ['uses'=>'PermissionController@index','as'=>'list_group']);
    Route::get('/permission/group/create', ['uses'=>'PermissionController@showGroupForm','as'=>'register_group']);
    Route::post('/permission/store', ['uses'=>'PermissionController@postGroupForm','as'=>'store_group']);
    Route::get('/permission/view/{id}', ['uses'=>'PermissionController@viewGroup','as'=>'view_group']);

//    Route::get('/permission/group/edit', ['uses'=>'PermissionController@showGroupForm','as'=>'list_group']);


});

//profile routes
Route::group(['prefix' =>ROOT_DIR.'profile', 'middleware' => ['auth'], 'as' => 'profile.'], function () {

    //user  profile register and edit routes
    Route::get('/{key}',['uses'=>'Usercontroller@getProfile','as'=>'list_profile']);
    Route::post('/edit/profile', ['uses'=>'UserController@editProfile','as'=>'edit']);


});


Route::group(['prefix' =>ROOT_DIR.'shareholder', 'middleware' => ['auth','can:shareholder'], 'as' => 'shareholder.'], function () {

    //share group route
    Route::get('/group', ['uses'=>'ShareHolder\ShareGroupController@index','as'=>'list_group']);
    Route::get('/group/edit/{id}', ['uses'=>'ShareHolder\ShareGroupController@editGroup','as'=>'edit_group']);
    Route::post('/group/add', ['uses'=>'ShareHolder\ShareGroupController@addGroup','as'=>'add_group']);
    Route::post('/group/delete', ['uses'=>'ShareHolder\ShareGroupController@deleteGroup','as'=>'delete_group']);


    //share form
    Route::get('/add_form', ['uses'=>'ShareHolder\ShareHolderController@index','as'=>'add_form']);
    Route::post('/process_form', ['uses'=>'ShareHolder\ShareHolderController@process','as'=>'process_form']);
    Route::get('/list_form', ['uses'=>'ShareHolder\ShareHolderController@list','as'=>'list_form']);
    Route::get('/view/{id}', ['uses'=>'ShareHolder\ShareHolderController@view','as'=>'view']);

    // Edit Form Share and Installment Details

        //Edit Menu
            Route::get('/edit/{id}', ['uses'=>'ShareHolder\ShareHolderController@editMenu','as'=>'edit']);

            Route::get('/edit/form/{id}', ['uses'=>'ShareHolder\ShareHolderController@editForm','as'=>'edit_form']);
            Route::post('/edit/form', ['uses'=>'ShareHolder\ShareHolderController@posteditForm','as'=>'update_form']);

            Route::get('/edit_document/{id}', ['uses'=>'ShareHolder\ShareHolderController@edit_document','as'=>'edit_document']);
            Route::post('/edit_document_form}', ['uses'=>'ShareHolder\ShareHolderController@edit_document_form','as'=>'edit_document_form']);

            Route::get('/edit_installment/{id}', ['uses'=>'ShareHolder\InstallmentController@getInstallmentInformation','as'=>'edit_installment']);
            Route::post('/edit_installment_form/{id}', ['uses'=>'ShareHolder\InstallmentController@editInstallmentInformation','as'=>'edit_installment_form']);


    //end_edit

    //installment
    Route::get('/installment', ['uses'=>'ShareHolder\InstallmentController@index','as'=>'installment']);
    Route::get('/add_installment/{unique_id}', ['uses'=>'ShareHolder\InstallmentController@add_installment','as'=>'add_installment']);
    Route::post('/process_installment', ['uses'=>'ShareHolder\InstallmentController@process_installment','as'=>'process_installment']);

    Route::get('/installment/all/{unique_id}', ['uses'=>'ShareHolder\InstallmentController@view_installment','as'=>'view_installment']);

    Route::get('/installment/detail/{id}', ['uses'=>'ShareHolder\InstallmentController@getSingleInstallment','as'=>'getSingleInstallment']);



    //Notification Routes
    Route::get('/notification', ['uses'=>'ShareHolder\NotificationController@index','as'=>'notification']);
    Route::get('/notiication/message/{id}', ['uses'=>'ShareHolder\NotificationController@showMessageForm','as'=>'notification_message']);
    Route::post('/notiication/message', ['uses'=>'ShareHolder\NotificationController@post_message','as'=>'post_message']);


});


/*Route::group(['prefix' =>ROOT_DIR.'assetsmanagement', 'middleware' => ['auth','can:ams'], 'as' => 'asset.'], function () {*/

    //share group route
    /*Route::get('/add_asset', ['uses'=>'AssetsManagement\ProductController@addAsset','as'=>'add_asset']);*/
    /*Route::post('/add_asset', ['uses'=>'AssetsManagement\ProductController@assetForm','as'=>'add_asset']);
    Route::get('/list_asset', ['uses'=>'AssetsManagement\ProductController@assetList','as'=>'list_asset']);*/
    /*Route::get('/edit_asset/{id}', ['uses'=>'AssetsManagement\ProductController@editAsset','as'=>'edit_asset']);*/
    /*Route::post('/update_asset', ['uses'=>'AssetsManagement\ProductController@updateAsset','as'=>'update_asset']);*/
    /*Route::post('/delete_asset', ['uses'=>'AssetsManagement\ProductController@deleteAsset','as'=>'delete_asset']);*/
    /*Route::get('/view_asset/{id}', ['uses'=>'AssetsManagement\ProductController@viewAsset','as'=>'view_asset']);*/

    /*Route::get('/add_vendor', ['uses'=>'AssetsManagement\ProductController@vendorForm','as'=>'add_vendor']);*/
    /*Route::post('/add_vendor', ['uses'=>'AssetsManagement\ProductController@vendorAdd','as'=>'add_vendor']);*/

   /* Route::get('/view_vendor/{id}', ['uses'=>'AssetsManagement\ProductController@viewVendor','as'=>'view_vendor']);
    Route::get('/edit_vendor/{id}', ['uses'=>'AssetsManagement\ProductController@editVendor','as'=>'edit_vendor']);
    Route::post('/update_vendor', ['uses'=>'AssetsManagement\ProductController@updateVendor','as'=>'update_vendor']);
    Route::post('/delete_vendor', ['uses'=>'AssetsManagement\ProductController@deletVendor','as'=>'delete_vendor']);*/
   


   /* Route::get('/list_vendor', ['uses'=>'AssetsManagement\ProductController@vendorList','as'=>'list_vendor']);

    Route::get('purchase_form', ['uses'=>'AssetsManagement\ProductController@purchaseForm','as'=>'purchase_form']);
    Route::post('purchase_form', ['uses'=>'AssetsManagement\RequisitionFormController@addPurchaseForm','as'=>'purchase_form']);*/


/*});*/

Route::group(['prefix' =>ROOT_DIR.'storekeeper',  'as' => 'store.'], function () {

    //Storekeeper group route

    //add product directly by the store.
    Route::get('/product/add', ['uses'=>'AssetsManagement\ProductController@addProduct','as'=>'add_product']);
    Route::post('/add_product', ['uses'=>'AssetsManagement\ProductController@productForm','as'=>'store_product']);
    Route::get('/list_product', ['uses'=>'AssetsManagement\ProductController@productList','as'=>'list_product']);
    Route::get('/edit_product/{id}', ['uses'=>'AssetsManagement\ProductController@editProduct','as'=>'edit_product']);
    Route::post('/update_product', ['uses'=>'AssetsManagement\ProductController@updateProduct','as'=>'update_product']);
    Route::post('/delete_product', ['uses'=>'AssetsManagement\ProductController@deleteProduct','as'=>'delete_product']);
    Route::get('/view_product/{id}', ['uses'=>'AssetsManagement\ProductController@viewProduct','as'=>'view_product']);

    //end add product route

    // product add after purchase form
    Route::get('/product/purchase/add/{id}', ['uses'=>'AssetsManagement\ProductController@addProductAfterPurchaseForm','as'=>'add_purchase_product']);
    Route::post('/product/purchase/add', ['uses'=>'AssetsManagement\ProductController@storeProductAfterPurchaseForm','as'=>'store_purchase_product']);

    // end product add after purchase form

    //add vendor
    Route::get('/add_vendor', ['uses'=>'AssetsManagement\ProductController@vendorForm','as'=>'add_vendor']);
    Route::post('/add_vendor', ['uses'=>'AssetsManagement\ProductController@vendorAdd','as'=>'add_vendor']);

    Route::get('/view_vendor/{id}', ['uses'=>'AssetsManagement\ProductController@viewVendor','as'=>'view_vendor']);
    Route::get('/edit_vendor/{id}', ['uses'=>'AssetsManagement\ProductController@editVendor','as'=>'edit_vendor']);
    Route::post('/update_vendor', ['uses'=>'AssetsManagement\ProductController@updateVendor','as'=>'update_vendor']);
    Route::post('/delete_vendor', ['uses'=>'AssetsManagement\ProductController@deletVendor','as'=>'delete_vendor']);
    Route::get('/list_vendor', ['uses'=>'AssetsManagement\ProductController@vendorList','as'=>'list_vendor']);
    //end vendor route

    //Product Category list and update
   Route::get('/asset_type_add_list', ['uses'=>'AssetsManagement\ProductController@assetTypeAddList','as'=>'asset_type_add_list']);
   Route::post('/add_asset_type', ['uses'=>'AssetsManagement\ProductController@addAssetType','as'=>'add_asset_type']);
   Route::get('/add_block_type', ['uses'=>'AssetsManagement\ProductController@addBlockTypeList','as'=>'add_block_type']);
   Route::post('/add_block_type', ['uses'=>'AssetsManagement\ProductController@addBlockType','as'=>'add_block_type']);
   Route::get('/add_sub_block_list', ['uses'=>'AssetsManagement\ProductController@addSubBlockList','as'=>'add_sub_block_list']);
   Route::post('/add_sub_block_type', ['uses'=>'AssetsManagement\ProductController@addSubBlockType','as'=>'add_sub_block_type']);
   Route::get('/asset_name_add_list', ['uses'=>'AssetsManagement\ProductController@assetNameAddList','as'=>'asset_name_add_list']);
   Route::post('/add_asset_name', ['uses'=>'AssetsManagement\ProductController@assetName','as'=>'add_asset_name']);
   Route::get('/specification_name_add_list', ['uses'=>'AssetsManagement\ProductController@specificationNameAddList','as'=>'specification_name_add_list']);
   Route::post('/add_specification_name', ['uses'=>'AssetsManagement\ProductController@addSpecificationName','as'=>'add_specification_name']);

   //Product Category Name Edit
   Route::post('/edit_specification_name', ['uses'=>'AssetsManagement\ProductController@editSpecificationName','as'=>'edit_specification_name']);
   Route::post('/edit_asset_name', ['uses'=>'AssetsManagement\ProductController@editAssetName','as'=>'edit_asset_name']);
   Route::post('/edit_sub_block_name', ['uses'=>'AssetsManagement\ProductController@editSubBlockName','as'=>'edit_sub_block_name']);
   Route::post('/edit_block_name', ['uses'=>'AssetsManagement\ProductController@editBlockName','as'=>'edit_block_name']);
   Route::post('/edit_asset_type_name', ['uses'=>'AssetsManagement\ProductController@editAssetTypeName','as'=>'edit_asset_type_name']);
   //End Product Category Name Edit


//   Purchase Requisition Form Routes
    Route::get('purchase_form/{id}', ['uses'=>'AssetsManagement\PurchaseRequisitionController@purchaseForm','as'=>'purchase_form']);
    Route::post('purchase_forms', ['uses'=>'AssetsManagement\PurchaseRequisitionController@addPurchaseForm','as'=>'store_purchase_form']);

    Route::get('/requisition/purchase', ['uses'=>'AssetsManagement\PurchaseRequisitionController@showAllPurchaseRequisition','as'=>'show_purchase_requisition']);
    Route::get('/requisition/purchase/view/{id}', ['uses'=>'AssetsManagement\PurchaseRequisitionController@viewPurchaseRequisition','as'=>'view_purchase_requisition']);

//  End Purchase Requisition Form Routes

    Route::get('purchase_file_list', ['uses'=>'AssetsManagement\PurchaseFileController@purchaseFileList','as'=>'purchase_file_list']);
    Route::get('purchase_file_view/{id}', ['uses'=>'AssetsManagement\PurchaseFileController@purchaseFileView','as'=>'purchase_file_view']);
    Route::get('purchase_file_edit/{id}', ['uses'=>'AssetsManagement\PurchaseFileController@purchaseFileEdit','as'=>'purchase_file_edit']);
    Route::post('purchase_file_update', ['uses'=>'AssetsManagement\PurchaseFileController@purchaseFileUpdate','as'=>'purchase_file_update']);
    Route::post('purchase_file_delete', ['uses'=>'AssetsManagement\PurchaseFileController@purchaseFileDelete','as'=>'purchase_file_delete']);
    Route::post('purchase_file_scan', ['uses'=>'AssetsManagement\PurchaseFileController@purchaseFileScan','as'=>'purchase_file_scan']);
    Route::post('purchase_file_check_list', ['uses'=>'AssetsManagement\PurchaseFileController@purchaseFileCheckList','as'=>'purchase_file_check_list']);
    Route::get('purchase_rejected_file', ['uses'=>'AssetsManagement\PurchaseFileController@purchaseRejectedFile','as'=>'purchase_rejected_file']);

//End Purchase Requisition Form Routes

    Route::get('goods_requisition_file_status', ['uses'=>'AssetsManagement\StorekeeperController@goodsRequisitionFileStatus','as'=>'goods_requisition_file_status']);
    Route::post('goods_requisition_file', ['uses'=>'AssetsManagement\StorekeeperController@goodsRequisitionFile','as'=>'goods_requisition_file']);
    Route::post('goods_dispatch', ['uses'=>'AssetsManagement\StorekeeperController@goodsDispatch','as'=>'goods_dispatch']);
    Route::post('purchase_form1', ['uses'=>'AssetsManagement\PurchaseFormController@purchaseForm1','as'=>'purchase_form1']);
   
   //Purchase Approve
   Route::get('purchase_approved_list', ['uses'=>'AssetsManagement\StorekeeperController@purchaseApprovedList','as'=>'purchase_approved_list']);

   Route::post('purchase_approved_product_update', ['uses'=>'AssetsManagement\StorekeeperController@purchaseApprovedProductUpdate','as'=>'purchase_approved_product_update']);
   Route::post('get_block_list', ['uses'=>'AssetsManagement\ProductController@blockList','as'=>'get_block_list']);
   Route::post('get_sub_block_list', ['uses'=>'AssetsManagement\ProductController@subBlockList','as'=>'get_sub_block_list']);
   Route::post('get_asset_name_list', ['uses'=>'AssetsManagement\ProductController@assetNameList','as'=>'get_asset_name_list']);
   Route::post('get_specification_name_list', ['uses'=>'AssetsManagement\ProductController@specificationNameList','as'=>'get_specification_name_list']);
   Route::post('get_specification_id_count', ['uses'=>'AssetsManagement\ProductController@specificationIdCount','as'=>'get_specification_id_count']);

   //calling to Category controller for all block sub block and other catetgories routes.
   Route::post('get_asset_name_id', ['uses'=>'AssetsManagement\StorekeeperController@getAssetNameId','as'=>'get_asset_name_id']);
   Route::get('category/specification/list', ['uses'=>'AssetsManagement\CategoryController@getAllSpecification','as'=>'get_specification']);
   Route::get('category/asset/name', ['uses'=>'AssetsManagement\CategoryController@getAllAssetName','as'=>'get_assets_name']);

   //after goods requistion request proceed
    Route::get('/requisition/goods', ['uses'=>'AssetsManagement\StorekeeperController@index','as'=>'get_all_goods_requisition']);

    Route::post('/requisition/goods/proceed', ['uses'=>'AssetsManagement\StorekeeperController@proceedGoodsRequisitonForm','as'=>'proceed_goods_form']);

    //show dispatch form
    Route::get('/requisition/goods/proceed/{id}', ['uses'=>'AssetsManagement\StorekeeperController@getProceedForm','as'=>'get_proceed_form']);
    Route::post('/requisition/goods/dispatch', ['uses'=>'AssetsManagement\StorekeeperController@dispatchGoodsRequest','as'=>'dispatch_goods_request']);

    //show dispatched product
    Route::get('/dispatched/product', ['uses'=>'AssetsManagement\DispatchController@showAllDispatchedProduct','as'=>'show_dispatch_product']);
    Route::get('/dispatched/product/handover/{id}',['uses'=>'AssetsManagement\DispatchController@handOverForm','as'=>'show_handover_form']);
    Route::post('/dispatched/product/handover/store',['uses'=>'AssetsManagement\DispatchController@storeHandOverForm','as'=>'store_handover_form']);

    // end show dispatched product

    // show purchase requisition files
    Route::get('/requisition/purchase', ['uses'=>'AssetsManagement\PurchaseRequisitionController@showAllPurchaseRequisition','as'=>'show_purchase_requisition']);

    //end purchase requisition files
});
