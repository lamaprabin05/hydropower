<?php



Route::group(['prefix' =>ROOT_DIR.'assetsmanagement', 'middleware' => ['auth'], 'as' => 'assets.'], function () {

    //Requestor route here.
    Route::group(['prefix' =>ROOT_DIR.'requestor', 'middleware' => ['auth'], 'as' => 'requestor.'], function () {

        //Goods Requisition CRUD operation
        Route::get('/requisition/goods', ['uses'=>'AssetsManagement\RequestorController@getRequisitionForm','as'=>'get_requisition_form']);
        Route::post('/requisition/goods/store', ['uses'=>'AssetsManagement\RequestorController@storeRequisitionForm','as'=>'store_goods_requisition']);

        Route::get('/requisition/files', ['uses'=>'AssetsManagement\RequestorController@getAllRequisitionFile','as'=>'list_all_requisition']);
        Route::get('/requisition/files/view/{id}', ['uses'=>'AssetsManagement\RequestorController@viewRequisitionFile','as'=>'view_all_requisition']);


        //edit requisition and single product wise.
        Route::get('/requisition/files/edit/{id}', ['uses'=>'AssetsManagement\RequestorController@editRequisitionFile','as'=>'edit_all_requisition'])->middleware('can:edit_goods_requisition');
        Route::post('/requisition/files/edit', ['uses'=>'AssetsManagement\RequestorController@updateRequisitionFile','as'=>'update_requisition'])->middleware('can:edit_goods_requisition');

        //delete requisition
        Route::post('/requisition/files/delete', ['uses'=>'AssetsManagement\RequestorController@deleteRequisitionFile','as'=>'delete_requisition'])->middleware('can:delete_goods_requisition');

        //rejected purchase requisition file
        Route::get('/requisition/rejected', ['uses'=>'AssetsManagement\RequestorController@getAllRejectedRequisitionFile','as'=>'list_rejected_requisition']);
        Route::get('/requisition/rejected/proceed/{id}', ['uses'=>'AssetsManagement\RequestorController@getRejectedRequisitionFile','as'=>'get_rejected_requisition']);

    });
    //end requestor route

    //store keeper routes
    Route::group(['prefix' =>ROOT_DIR.'storekeeper', 'middleware' => ['auth'], 'as' => 'store.'], function () {

        //goods requisition routes.
        Route::get('/requisition/goods', ['uses'=>'AssetsManagement\StorekeeperController@index','as'=>'get_all_goods_requisition']);
//        Route::get('/requisition/goods/proceed/{id}', ['uses'=>'AssetsManagement\StorekeeperController@getProceedForm','as'=>'get_proceed_form']);



        //calling to Category controller for all block sub block and other catetgories routes.
//        Route::get('category/specification/list', ['uses'=>'AssetsManagement\CategoryController@getAllSpecification','as'=>'get_specification']);


    });
    //end store keeper routes



    //Checker Routes here
    Route::group(['prefix' =>ROOT_DIR.'checker', 'middleware' => ['auth'], 'as' => 'checker.'], function () {

        //goods requisition routes.
        Route::get('/requisition/goods', ['uses'=>'AssetsManagement\CheckerController@index','as'=>'get_all_goods_requisition']);


        //show proceed, edit or view
        Route::get('/requisition/goods/view/{id}', ['uses'=>'AssetsManagement\CheckerController@viewGoodsRequisition','as'=>'view_goods_requisition']);

        //proceed goods requisition form i.e. change its status
        Route::post('change_requisition_status', ['uses'=>'AssetsManagement\CheckerController@changeGoodsRequisitionStatus','as'=>'change_requisition_status']);

        // edit goods requisition form
        Route::get('/requisition/goods/edit/{id}', ['uses'=>'AssetsManagement\CheckerController@getEditGoodsRequisitionForm','as'=>'get_edit_form']);
        Route::post('/requisition/goods/edit', ['uses'=>'AssetsManagement\CheckerController@editGoodsRequisition','as'=>'edit_goods_requisition']);

        //reject requisition formpurchase_file_edit
        Route::get('/requisition/goods/reject/{id}', ['uses'=>'AssetsManagement\CheckerController@getRejectGoodsRequisitionForm','as'=>'get_reject_form']);
        Route::post('/requisition/goods/reject', ['uses'=>'AssetsManagement\CheckerController@postRejectGoodsRequisitionForm','as'=>'store_reject_form']);

        //purchasee requisition route
        Route::get('purchase_requisition', ['uses'=>'AssetsManagement\PurchaseCheckerController@index','as'=>'purchase_requisition']);
        Route::get('view_purchase_file/{id}', ['uses'=>'AssetsManagement\PurchaseCheckerController@viewPurchaseFile','as'=>'view_purchase_file']);
        Route::post('purchase_file_proceed', ['uses'=>'AssetsManagement\PurchaseCheckerController@proceedPurchaseFile','as'=>'purchase_file_proceed']);
        Route::get('purchase_file_edit/{id}', ['uses'=>'AssetsManagement\PurchaseCheckerController@purchaseFileEdit','as'=>'purchase_file_edit']);
        Route::post('update_purchase_file', ['uses'=>'AssetsManagement\PurchaseCheckerController@updatePurchaseFile','as'=>'update_purchase_file']);
        Route::post('purchase_file_reject', ['uses'=>'AssetsManagement\PurchaseCheckerController@rejectPurchaseFile','as'=>'purchase_file_reject']);
        //end requisition routes.
    });

    //end checker route

    //Approver Routes here
    Route::group(['prefix' =>ROOT_DIR.'approver', 'middleware' => ['auth'], 'as' => 'approver.'], function () {

        //goods requisition routes.
        Route::get('/requisition/goods', ['uses'=>'AssetsManagement\ApproverController@index','as'=>'get_all_goods_requisition']);
        Route::get('/requisition/goods/view/{id}', ['uses'=>'AssetsManagement\ApproverController@viewGoodsRequisition','as'=>'view_goods_requisition']);

        //after proceed i.e change it's status
        Route::post('/requisition/goods/changestatus', ['uses'=>'AssetsManagement\ApproverController@changeGoodsRequisitionStatus','as'=>'change_goodsrequisition_status']);


        //after proceed edit view or delete
       Route::get('/requisition/goods/edit/{id}', ['uses'=>'AssetsManagement\CheckerController@getEditGoodsRequisitionForm','as'=>'get_edit_form']);
       Route::post('/requisition/goods/edit', ['uses'=>'AssetsManagement\CheckerController@editGoodsRequisition','as'=>'edit_goods_requisition']);

        //reject requisition form
        Route::get('/requisition/goods/reject/{id}', ['uses'=>'AssetsManagement\ApproverController@getRejectGoodsRequisitionForm','as'=>'get_reject_form']);
        Route::post('/requisition/goods/reject', ['uses'=>'AssetsManagement\ApproverController@postRejectGoodsRequisitionForm','as'=>'store_reject_form']);
        //end requisition routes.

        //Purchase Requisition Routes
        Route::get('requisition/purchase', ['uses'=>'AssetsManagement\PurchaseApproverController@fetchGoodsPurchaseRequisition','as'=>'fetch_goods_purchase_requisition']);

        Route::get('requisition/purchase/view/{id}', ['uses'=>'AssetsManagement\PurchaseApproverController@viewPurchaseRequisiton','as'=>'view_goods_purchase_requisition']);

        Route::get('edit_goods_purchase_requisition/{goods_requisition_product_id}', ['uses'=>'AssetsManagement\PurchaseApproverController@editGoodsPurchaseRequisiton','as'=>'edit_goods_purchase_requisition']);

        Route::post('update_goods_purchase_requisition', ['uses'=>'AssetsManagement\PurchaseApproverController@updateGoodsPurchaseRequisiton','as'=>'update_goods_purchase_requisition']);

        Route::post('reject_purchase_requisition', ['uses'=>'AssetsManagement\PurchaseApproverController@rejectPurchaseRequisition','as'=>'reject_purchase_requisition']);

        Route::post('proceed_purchase_requisition', ['uses'=>'AssetsManagement\PurchaseApproverController@proceedPurchaseRequisition','as'=>'proceed_purchase_requisition']);

        // End Purchase Requisition Routes

    });

    //end checker route



});

