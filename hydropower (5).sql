-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2019 at 08:27 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hydropower`
--

-- --------------------------------------------------------

--
-- Table structure for table `asset_types`
--

CREATE TABLE `asset_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asset_types`
--

INSERT INTO `asset_types` (`id`, `name`) VALUES
(1, 'Fixed Assets'),
(2, 'Current Asset');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_group`
--

INSERT INTO `auth_group` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Requestor Module', '2019-05-12', '2019-05-12'),
(2, 'Checker Module', '2019-05-12', '2019-05-12'),
(3, 'Approver Group', '2019-05-12', '2019-05-12'),
(4, 'Store Module', '2019-05-12', '2019-05-12'),
(5, 'Admin Assets Management System', '2019-05-13', '2019-05-13');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permission`
--

CREATE TABLE `auth_group_permission` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `permission` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_group_permission`
--

INSERT INTO `auth_group_permission` (`id`, `group_id`, `permission_id`, `permission`) VALUES
(3, 2, 1, 'ams'),
(4, 2, 7, 'checker'),
(5, 3, 1, 'ams'),
(6, 3, 5, 'approver'),
(7, 1, 1, 'ams'),
(8, 1, 4, 'requestor'),
(9, 4, 1, 'ams'),
(10, 4, 6, 'store_keeper'),
(13, 5, 1, 'ams'),
(14, 5, 4, 'requestor');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_user`
--

CREATE TABLE `auth_group_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_group_user`
--

INSERT INTO `auth_group_user` (`id`, `user_id`, `group_id`) VALUES
(1, 2, 5),
(2, 3, 2),
(3, 4, 4),
(4, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `form_share`
--

CREATE TABLE `form_share` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `group_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `share_group_id` int(11) NOT NULL DEFAULT '0',
  `form_status` int(11) DEFAULT NULL,
  `proceeded_for` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_completed` int(11) DEFAULT '0',
  `notification` int(11) NOT NULL,
  `temporary_address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citizenship_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citizenship_issued_date` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citizenship_issued_place` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_place` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grandfather_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `husband_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wife_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominee_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominee_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nominee_contact_number` bigint(15) DEFAULT NULL,
  `relation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shareholder_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `permanent_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `registration_number` bigint(20) DEFAULT NULL,
  `pan_number` bigint(20) DEFAULT NULL,
  `website` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citizenship_scan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_certificate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `protected_citizenship` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `goods_purchase_form`
--

CREATE TABLE `goods_purchase_form` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_description` text NOT NULL,
  `requisition_number` bigint(20) NOT NULL,
  `estimated_cost` float NOT NULL,
  `product_qty` int(11) NOT NULL,
  `proceeded_to` varchar(255) NOT NULL,
  `form_status` tinyint(1) DEFAULT '0',
  `is_rejected` tinyint(1) DEFAULT '0',
  `purchase_file_scan` varchar(255) DEFAULT NULL,
  `rejected_reason` text,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `goods_requisition_form`
--

CREATE TABLE `goods_requisition_form` (
  `id` int(11) NOT NULL,
  `requisition_number` text COLLATE utf8_unicode_ci,
  `purpose` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reason` text COLLATE utf8_unicode_ci,
  `form_status` int(11) NOT NULL,
  `proceeded_to` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_rejected` int(11) DEFAULT NULL,
  `rejected_reason` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `goods_requisition_product`
--

CREATE TABLE `goods_requisition_product` (
  `id` int(11) NOT NULL,
  `goods_requisition_form_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `requirement_date` date NOT NULL,
  `is_rejected` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - activated 1-rejected'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `good_requisition_files`
--

CREATE TABLE `good_requisition_files` (
  `id` int(11) NOT NULL,
  `goods_requisition_product_id` int(11) NOT NULL,
  `requested_qty` int(11) NOT NULL,
  `dispatched_qty` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `dispatch_status` tinyint(1) NOT NULL DEFAULT '0',
  `proceed_status` tinyint(1) NOT NULL DEFAULT '0',
  `request_check_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `good_requisition_files`
--

INSERT INTO `good_requisition_files` (`id`, `goods_requisition_product_id`, `requested_qty`, `dispatched_qty`, `created_at`, `updated_at`, `dispatch_status`, `proceed_status`, `request_check_status`) VALUES
(1, 1, 20, 0, '2019-05-10', '2019-05-10', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `installment_information`
--

CREATE TABLE `installment_information` (
  `id` int(11) NOT NULL,
  `share_form_id` bigint(11) NOT NULL,
  `no_of_installment` int(11) NOT NULL,
  `remaining_installment` int(11) DEFAULT NULL,
  `committed_amount` int(11) NOT NULL,
  `total_paid_amount` bigint(15) NOT NULL,
  `first_installment_amt` int(11) DEFAULT NULL,
  `first_expiry_date` date DEFAULT NULL,
  `second_installment_amt` int(11) DEFAULT NULL,
  `second_expiry_date` date DEFAULT NULL,
  `third_installment_amt` int(11) DEFAULT NULL,
  `third_expiry_date` date DEFAULT NULL,
  `fourth_installment_amt` int(11) DEFAULT NULL,
  `fourth_expiry_date` date DEFAULT NULL,
  `fifth_installment_amt` int(11) DEFAULT NULL,
  `fifth_expiry_date` date DEFAULT NULL,
  `sixth_installment_amt` int(11) DEFAULT NULL,
  `sixth_expiry_date` date DEFAULT NULL,
  `seventh_installment_amt` int(11) DEFAULT NULL,
  `seventh_expiry_date` date DEFAULT NULL,
  `eighth_installment_amt` int(11) DEFAULT NULL,
  `eighth_expiry_date` date DEFAULT NULL,
  `ninth_installment_amt` int(11) DEFAULT NULL,
  `ninth_expiry_date` date DEFAULT NULL,
  `tenth_installment_amt` int(11) DEFAULT NULL,
  `tenth_expiry_date` date DEFAULT NULL,
  `eleventh_installment_amt` int(11) DEFAULT NULL,
  `eleventh_expiry_date` date DEFAULT NULL,
  `twelveth_installment_amt` int(11) DEFAULT NULL,
  `twelveth_exiry_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_goods_requisition_product`
--

CREATE TABLE `log_goods_requisition_product` (
  `id` int(11) NOT NULL,
  `goods_requisition_product_id` int(11) NOT NULL,
  `goods_requisition_form_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `requirement_date` date NOT NULL,
  `is_rejected` tinyint(4) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `installment_name` varchar(50) NOT NULL,
  `applicant_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `form_id` int(11) NOT NULL,
  `bank_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `branch_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(50) NOT NULL,
  `installment_number` int(11) NOT NULL,
  `installment_nep_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `installment_eng_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `installment_paid_amount` double NOT NULL,
  `advance_payment` double NOT NULL,
  `less_payment` double NOT NULL,
  `bill_image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `display_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `display_name`, `code_name`) VALUES
(1, 'Assets Management System', 'ams'),
(3, 'Can Add Product', 'add_product'),
(4, 'Requestor', 'requestor'),
(5, 'Approver', 'approver'),
(6, 'Store Keeper', 'store_keeper'),
(7, 'Checker', 'checker'),
(8, 'Can Add User', 'add_user');

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `permission` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `asset_type_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `bill` varchar(255) NOT NULL,
  `unit_cost` decimal(10,0) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `nepali_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `asset_type_id`, `image`, `bill`, `unit_cost`, `quantity`, `amount`, `vendor_id`, `created_at`, `nepali_date`) VALUES
(1, 'laptop', 'dkfj', 1, 'files/assets/product/image/2019_05_10/5y8yzN3YmrVm4TWSXqM1vB0mDAClYOPU7fpaZC9K.jpeg', 'files/assets/product/bill/2019_05_10/jXsIQ4ILUDJOJAFzlogQLLxtTfYwKlAmmzJcylv5.jpeg', '10', 12, 120, 1, '2019-05-10', 'शुक्र, बैशाख २०, २०७६'),
(2, 'computer', 'i3 5th generations', 1, 'files/assets/product/image/2019_05_10/e5bGwN5WechiouEXpYYX1zhDOI457pqCSNCNTrZg.jpeg', 'files/assets/product/bill/2019_05_10/1dJkgxvR6KJAPSLpBMsdvkwmjTBMMaG6A5POvJan.jpeg', '100', 150, 1500, 1, '2019-05-10', 'शुक्र, बैशाख २७, २०७६'),
(3, 'hard disk', '1 tera byte', 1, 'files/assets/product/image/2019_05_10/VyPI8M7ZwozigFlgZgglcOANDbHrcsoPsDYBaFKc.jpeg', 'files/assets/product/bill/2019_05_10/5PtmxUvY2mhw0pGt59UX4fauODFtNRaicXMOYp5T.jpeg', '7000', 0, 140000, 1, '2019-05-10', 'शुक्र, बैशाख २७, २०७६');

-- --------------------------------------------------------

--
-- Table structure for table `product_logs`
--

CREATE TABLE `product_logs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `asset_type_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `bill` varchar(255) NOT NULL,
  `unit_cost` decimal(10,0) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `vendor_company_name` varchar(255) NOT NULL,
  `vendor_company_address` varchar(255) NOT NULL,
  `vendor_contact` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `nepali_date` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_logs`
--

INSERT INTO `product_logs` (`id`, `name`, `description`, `asset_type_id`, `image`, `bill`, `unit_cost`, `quantity`, `amount`, `vendor_name`, `vendor_company_name`, `vendor_company_address`, `vendor_contact`, `created_at`, `nepali_date`) VALUES
(1, 'computer', 'i3 5th generation', 1, 'files/assets/product/image/2019_05_10/e5bGwN5WechiouEXpYYX1zhDOI457pqCSNCNTrZg.jpeg', 'files/assets/product/bill/2019_05_10/1dJkgxvR6KJAPSLpBMsdvkwmjTBMMaG6A5POvJan.jpeg', '100', 15, 1500, 'Technology Sales', '12', 'lazimpat', 234234, '2019-05-10', 'शुक्र, बैशाख २७, २०७६');

-- --------------------------------------------------------

--
-- Table structure for table `product_vendors`
--

CREATE TABLE `product_vendors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_vendors`
--

INSERT INTO `product_vendors` (`id`, `name`, `company`, `address`, `contact`, `created_at`, `updated_at`) VALUES
(1, 'Technology Sales', '12', 'lazimpat', 234234, '2019-05-10', '2019-05-10');

-- --------------------------------------------------------

--
-- Table structure for table `share_group`
--

CREATE TABLE `share_group` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` int(50) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_superadmin` int(11) NOT NULL,
  `usertype` int(11) NOT NULL,
  `user_avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `is_superadmin`, `usertype`, `user_avatar`, `created_by`, `remember_token`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'superadmin@hydro.com', NULL, '$2y$10$L.s.btsZBt31MfDiMP6PA.wi5bK3S/V2Um2hEyjSiOgdWVSv5b17a', 1, 0, '', 1, 'mHbM7WZ5v1CK2jtGfujOtjNj24cWlaLQLEFQ3sjHrQKXh24WdBlJQKgz1BSf', '2019-05-14 06:14:16', NULL, '2019-05-14 06:14:16'),
(2, 'requestor', 'requestor@hydro.com', NULL, '$2y$10$8q4LR4dRMgFq1tbf/URbmO5AnbJYtP5TdT/NExumsd725qZJQHpK2', 0, 2, 'files/user_images//uOwa21WbIJrNwm6SWZNT3uPkuo7Fuh25tmdmhCQB.jpeg', 1, 'Uaz3kK8GbWF65KD9XC34GuUewTsX6AiMQofKRN8zy821Kl4lh9HNsMvBCbqc', '2019-05-13 11:16:48', '2019-05-12 09:28:15', '2019-05-13 11:16:48'),
(3, 'CheckerModule', 'checker@hydro.com', NULL, '$2y$10$L1ShkgM/XYUyES9iG2Suiue7pPhjPZK.mS4uBh93NJGGqsa./Vs0.', 0, 2, 'files/user_images//0BsdPUAsiiQpyOVqPmcRdm40qM811SeB9SnkNwYg.jpeg', 1, 'GGSaZ2zya06fp5j0wB1UNsm24S4cskJX0MooGW7eKmZ0jDpJAZHFYvJAuUJi', '2019-05-12 09:34:35', '2019-05-12 09:34:04', '2019-05-12 09:34:35'),
(4, 'Store Keeper Module', 'store@hydro.com', NULL, '$2y$10$1sQULG5okNyKCEztiWtLMe7Aari8U/CxHh6s64FAjLj7ZlHpxkyVa', 0, 2, 'files/user_images//iWWeyjxnwKLIlAVhRDdw8csBI2zgkVRgHlxMCyNY.jpeg', 1, NULL, NULL, '2019-05-12 11:59:32', '2019-05-12 11:59:32'),
(5, 'Approver Group', 'approver@hyro.com', NULL, '$2y$10$xD6mI3R0J3DVDUawcgfBSuXr742QAaTK6h.MN7q2KY0WH4eGhl6yG', 0, 2, '', 1, NULL, NULL, '2019-05-12 12:00:38', '2019-05-12 12:00:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_types`
--
ALTER TABLE `asset_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group_permission`
--
ALTER TABLE `auth_group_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group_user`
--
ALTER TABLE `auth_group_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_share`
--
ALTER TABLE `form_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goods_purchase_form`
--
ALTER TABLE `goods_purchase_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goods_requisition_form`
--
ALTER TABLE `goods_requisition_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goods_requisition_product`
--
ALTER TABLE `goods_requisition_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `good_requisition_files`
--
ALTER TABLE `good_requisition_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installment_information`
--
ALTER TABLE `installment_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_goods_requisition_product`
--
ALTER TABLE `log_goods_requisition_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_logs`
--
ALTER TABLE `product_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_vendors`
--
ALTER TABLE `product_vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `share_group`
--
ALTER TABLE `share_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_types`
--
ALTER TABLE `asset_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `auth_group_permission`
--
ALTER TABLE `auth_group_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `auth_group_user`
--
ALTER TABLE `auth_group_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `form_share`
--
ALTER TABLE `form_share`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `goods_purchase_form`
--
ALTER TABLE `goods_purchase_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `goods_requisition_form`
--
ALTER TABLE `goods_requisition_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `goods_requisition_product`
--
ALTER TABLE `goods_requisition_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `good_requisition_files`
--
ALTER TABLE `good_requisition_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `installment_information`
--
ALTER TABLE `installment_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_goods_requisition_product`
--
ALTER TABLE `log_goods_requisition_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_logs`
--
ALTER TABLE `product_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_vendors`
--
ALTER TABLE `product_vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `share_group`
--
ALTER TABLE `share_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
