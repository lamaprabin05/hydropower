@extends('layouts.app')
@section('css')

@endsection('css')

@section('page_title')
    View Permission Group
@endsection('page-title')

@section('page-title')
    View Permission Group
@endsection('page-title')

@section('content')
    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body">
                <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Group Name</label>
                            <input type="text"  readonly value="{{   $group->name }}" name="name" class="form-control">
                        </div>

                        <h2>Assigned Permission</h2>

                            <table class="table  table-hover" >
                                @foreach($permissions as $value)
                                    <tr>
                                        <td>{{$value->display_name}}</td>

                                    </tr>
                                @endforeach
                            </table>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-9">
                                    <a href="{{ URL::previous() }}" class="btn default button-previous">
                                        <i class="m-icon-swapleft"></i> Back
                                    </a>
                                </div>
                            </div>
                        </div>


                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection('scripts')
