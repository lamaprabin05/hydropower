@extends('layouts.app')
@section('css')
 <link href="{{asset(STATIC_DIR.'css/style.css')}}" rel="stylesheet" type="text/css"/>
 <link href="{{ asset(STATIC_DIR.'assets/global/plugins/jquery-multi-select/css/multi-select.css') }}" rel="stylesheet" type="text/css"/>



@endsection('css')

@section('page_title')
     Add New Group
@endsection('page-title')

@section('page-title')
     Add New Group
@endsection('page-title')

@section('content')
    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body">
                <div class="col-md-4">
                    <form action="{{ route('admin.store_group') }}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="action" value="add">

                        <div class="form-group">
                            <label class="control-label">Group Name</label>
                            <input type="text" value="{{   old('name')}}" name="name" class="form-control">

                            @if ($errors->has('name'))
                                    <ul style="margin-left:-25px;">
                                        <li class="text-danger">{{$errors->first('name')}}</li>
                                    </ul>
                            @endif

                        </div>

                        <h2>Assign Permission</h2>

                        <div class="form-group">
                            <div style="padding-bottom: 15px;">
                                All Permissions <span style="margin-left:110px;">Assigned Permissions</span>
                            </div>

                            <select multiple="multiple" class="multi-select" id="assign" name="selected_permission[]" style="width: 500px !important;">
                                @foreach($permissions as $val)
                                    {{--<option value="{{ $val->codename }}" @if(in_array($val->codename,$groupPermissions)) selected @endif>
                                        {{ $val->display_name }}
                                    </option>--}}

                                    <option value="{{ $val->id }}" >
                                        {{ $val->display_name }}
                                    </option>
                                @endforeach

                            </select>

                            @if ($errors->has('selected_permission'))
                                <ul style="margin-left:-25px;">
                                    <li class="text-danger">{{$errors->first('name')}}</li>
                                </ul>
                            @endif

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn green button-submit">
                                        Submit <i class="m-icon-swapright m-icon-white"></i>
                                    </button>
                                    <a href="{{ URL::previous() }}" class="btn default button-previous">
                                        <i class="m-icon-swapleft"></i> Back
                                    </a>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('scripts')

<script src="{{ asset(STATIC_DIR.'assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}" type="text/javascript"></script>


    <script>
        $('#assign').multiSelect();
    </script>

@endsection('scripts')

