
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    List Group Permission
@endsection

@section('page-title')
    Permission Group Listing
@endsection('page-title')


@section('content')


    <div class="row search-form-default">
        <div class="col-md-12">
            <form action="#">
                <div class="input-group">
                    <div class="input-cont">
                        {{--<input type="text" placeholder="Search..." class="form-control"/>--}}
                    </div>
                    <span class="input-group-btn">
                            <a href="{{route('admin.register_group')}}"  class="btn green-haze">
                                <i class="icon-plus "></i> Add  Group &nbsp;
                            </a>
                        </span>
                </div>
            </form>
        </div>
    </div>

    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-advance table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>
                        <th>
                            Name
                        </th>

                        <th>
                            Action
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($group as $value)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$value->name}}</td>
                                <?php
                                $token = base64_encode('edit'.$value->name);
                                $token = substr($token,2,7);
                                ?>
                                <td>
                                    <a href="{{route('admin.view_group', $value->id )  }}" title="Edit" class="btn btn-sm btn-success">
                                        <i class="fa fa-eye"> View</i>
                                    </a>

                                    <a href="{{route('admin.register_group',['token' => $token, 'group' => $value->name,'id' => $value->id ])  }}" title="Edit" class="btn btn-sm btn-primary">
                                        <i class="fa fa-edit"> Edit</i>
                                    </a>


                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('user.modal')
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });

        $('#deleteUser').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var ids = button.data('ids');
            var user = button.data('user');

            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modalfooter #confirm').data('form', form);
            $("#hidden_id").val(ids);
            $(".hidden_title").html(' "' + user + '" ');
        });


        $('#deleteUser').find('#confirm_yes').on('click', function () {

            //set csrf token
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //fetching id
            var id = $("#hidden_id").val();

            $.ajax({
                type: "POST",
                url: "{{ route('admin.delete_user') }}",
                headers: {
                    'XCSRFToken': $('meta[name="csrf-token"]').attr('content')
                },

                data: "id=" + id,
                success: function (msg) {
                    console.log(msg);
                    // alert(msg.error);
                    if(msg.error == false){
                        $("#deleteUser").modal("hide");
                        $('#showMessage').find('.success').show();
                        $("#showMessage").modal("show");
                    }
                    else
                    {
                        $("#deleteUser").modal("hide");
                        $('#showMessage').find('.error').show();
                        $("#showMessage").modal("show");
                    }
                    // window.location.reload();
                }
            });
        });
        // AJAX DELETE PERMISSION END

        // $( "#show_message" ).click(function() {
        //     window.location.reload();
        // });

        $('#show_message').on('click', function () {
            window.location.reload();
        });


    </script>


@endsection('scripts')
