@extends('layouts.app')

@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
@endsection('css')

@section('page_title')
    Send Notification
@endsection

@section('page-title')
    Send Notification
@endsection('page-title')


@section('content')

    <div class=" portlet light bordered" >
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center">Installment Information of  {{ucwords(strtolower($installment->shareholder_name))}}</h3>
            </div>

            <div class="col-md-12" >
                <table class="table table-bordered" >
                    <thead>
                    <tr>
                        <th>Unique Id:</th>
                        <th>Applicant Name:</th>
                        {{--<th>Commited Amount</th>--}}
                        {{--<th>Total Paid Amount</th>--}}
                        {{--<th>Total Installment</th>--}}
                        {{--<th>Remaining Installment</th>--}}
                        <th>Installment</th>
                        <th>Installment Amount</th>
                        <th>Installment Expiry Date</th>
                    </tr>

                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$installment->unique_id}}</td>
                        <td>{{$installment->shareholder_name}}</td>
                        {{--<td>{{$installment->payment_information->committed_amount}}</td>--}}
                        {{--<td>{{$installment->payment_information->total_paid_amount}}</td>--}}
                        {{--<td>{{$installment->payment_information->no_of_installment}}</td>--}}
                        {{--<td>{{$installment->payment_information->remaining_installment}}</td>--}}
                        <td>{{ucfirst($installment->proceeded_for)}} </td>
                        <td>Rs. {{$installment_detail->installment}}</td>
                        <td>{{$installment_detail->expiry_date}}</td>
                    </tr>
                    </tbody>

                </table>
            </div>

        </div>

        <div class="row">
            <form action="{{route('shareholder.post_message')}}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="col-md-12">
                        <h3 class="text-center">Email Notification</h3>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Message:</label>
                                <textarea  name="message" rows="10" style="margin: 0px -194.538px 0px 0px; width: 966px; height: 78px;" class="form-control">Your {{ucfirst($installment->proceeded_for)}} Installment Amount is Rs {{$installment_detail->installment}} and Expiry Date is on {{$installment_detail->expiry_date}}.@if(isset($overdue_days))Your expiry date of installment is finished.You have {{$overdue_days}} overdue days. @else You have {{$remaining_days}} days left to clear it.@endif
                                    </textarea>
                                @if($errors->has('message'))
                                    <span class="help-block" style="color:red;">
                                        * {{ $errors->first('message') }}
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>

                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Email:</label>
                            <input type="text" name="user_email" class="form-control" value="{{$installment->email_address}}">
                            @if($errors->has('user_email'))
                                <span class="help-block" style="color:red;">
                                        * {{ $errors->first('user_email') }}
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="hidden" name="id" value="{{$installment->id}}">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-check"></i> Send Message</button>
                            </div>
                        </div>
                    </div>

            </form>
        </div>

    </div>


@endsection('content')
@section('scripts')



@endsection('scripts')
