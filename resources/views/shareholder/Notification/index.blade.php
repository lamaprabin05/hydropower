@extends('layouts.app')
@section('css')

    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    Notification
@endsection

@section('page-title')
    All Notification
@endsection('page-title')


@section('content')

    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue-madison">
        <div class="portlet-title">
            <div class="caption">
                View Details Of Shareholder
            </div>
        </div>

        <div class="portlet-body">

            <table class="table table-striped table-bordered table-hover" id="group">
                <thead>
                <tr>
                    <th>
                        S.N.
                    </th>

                    <th>
                        Shareholder Name
                    </th>

                    <th>
                        Installment
                    </th>

                    <th>
                        Unique Id
                    </th>

                    <th>
                        Created At
                    </th>

                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                    @if($token == 1)
                        @foreach($form as $element)
                            @if($element->payment_information->remaining_installment != 0)
                            <tr>
                                <td>
                                    {{$loop->iteration}}
                                </td>

                                <td>
                                    {{$element->shareholder_name}}
                                </td>

                                <td>
                                    {{ucfirst($element->proceeded_for)}}
                                </td>

                                <td>
                                    {{$element->unique_id}}
                                </td>
                                <td>
                                    {{$element->created_at}}
                                </td>

                                <td>
                                    <a href="{{ route('shareholder.notification_message',['id' =>$element->id]) }}" class="btn btn-sm btn-primary">
                                        <i class="fa fa-paper-plane"></i> Proceed
                                    </a>
                                </td>
                            </tr>
                            @endif
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->

@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });
    </script>
@endsection('scripts')
