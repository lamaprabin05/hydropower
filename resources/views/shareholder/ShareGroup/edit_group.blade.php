@extends('layouts.app')
@section('css')
    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
@endsection('css')

@section('page_title')
    Edit Share Group
@endsection('page-title')

@section('page-title')
    Edit Group
@endsection('page-title')

@section('content')

    <div class="col-md-4">
        <a href="{{route('shareholder.list_group')}}" class="btn btn-sm  btn-primary"><i class="fa fa-arrow-circle-left"></i> Back</a>
        <hr>
        <div class=" portlet light bordered" >
            <div class="row">
                <form role="form" action="{{ route('shareholder.add_group') }}" method="post">
                    @csrf
                    <div class="form-body">
                        <div class="form-group has-error">
                            <label>Group Name:</label>
                            <input type="text" name="group_name" class="form-control" placeholder="Enter Group Name" value="{{ $group->name ?? '' }}">

                            @if($errors->has('group_name'))
                                <span class="help-block ">
                                    * {{$errors->first('group_name')}}
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="form-actions right">
                        <input type="hidden" value="{{$group->id}}" name="id">
                        <input type="hidden" value="edit" name="action">
                        <button type="submit" class="btn green">
                            <i class="fa fa-plus-circle"></i> Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>




@endsection('content')
@section('scripts')

    {{--    <script src="{{ asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>--}}
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });
    </script>

@endsection('scripts')
