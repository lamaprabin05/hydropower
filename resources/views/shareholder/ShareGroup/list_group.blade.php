@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
@endsection('css')

@section('page_title')
    Share Group
@endsection('page-title')

@section('page-title')
    Group Listing
@endsection('page-title')

@section('content')

    <div class="col-md-8">
        <div class=" portlet light bordered" >
            <div class="row">
                <div class="portlet-body">
                    <table class="table table-striped table-hover table-bordered dataTable no-footer" id="group" role="grid" aria-describedby="sample_editable_1_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" aria-sort="ascending" style="width: 211px;">
                                S.N.
                            </th>

                            <th class="sorting_asc" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" aria-sort="ascending" style="width: 211px;">
                                Group  Name
                            </th>

                            <th class="sorting_asc" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" aria-sort="ascending" style="width: 211px;">
                                Created At
                            </th>

                            <th class="sorting_asc" tabindex="0" aria-controls="sample_editable_1" rowspan="1" colspan="1" aria-sort="ascending" style="width: 211px;">
                                Action
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($group as $value)
                            <tr role="row" class="odd">
                                <td class="sorting_1">
                                    {{$loop->iteration}}
                                </td>

                                <td>
                                    {{$value->name}}
                                </td>

                                <td>
                                    {{$value->created_at}}
                                </td>

                                <td class="center">
                                    <a href="{{route('shareholder.edit_group',$value->id)}}" class="btn btn-sm btn-success">
                                        <i class="fa fa-edit"></i> Edit
                                    </a>

                                    <a href="#deleteGroup" class="btn btn-sm red" data-ids="{{ $value->id }}" data-toggle="modal" data-rel="delete" data-user="{{$value->name}}">
                                    <i class="icon-trash"></i> Delete
                                    </a>
                                    {{--<button class="btn default btn-xs green-stripe"> <i class="fa fa-edit"></i> Edit</button>--}}
                                    {{--<button class="btn default btn-xs red-stripe"> <i class="icon-trash"></i> Delete</button>--}}


                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class=" portlet light bordered" >
            <div class="row">
                <form role="form" action="{{ route('shareholder.add_group') }}" method="post">
                    @csrf
                    <div class="form-body">
                        <div class="form-group has-error">
                            <label>Group Name:</label>
                            <input type="text" name="group_name" class="form-control" placeholder="Enter Group Name">

                            @if($errors->has('group_name'))
                                <span class="help-block ">
                                    * {{$errors->first('group_name')}}
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="form-actions right">
                        <input type="hidden" value="add" name="action">
                        <button type="submit" class="btn green">
                            <i class="icon-plus"></i> Add
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    {{--modal start here.--}}
    <div id="showMessage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="margin-top: 200px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Group Deleted
                    </h4>
                </div>

                <div class="modal-body">
                    <i class="fa fa-check"></i> &nbsp; Group Deleted Successfully .
                </div>
                <input type="hidden" id="hidden_id">
                <div class="modal-footer" style="text-align: center;">
                    <button type="button" class="btn btn-danger green confirm_yes" id="show_message"><i class="icon-check"></i> Ok
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>

    <div id="deleteGroup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently
                    </h4>
                </div>
                <div class="modal-body"> Do you want to delete <span class='hidden_title'>" "</span>?</div>
                <input type="hidden" id="hidden_id">
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
                    </button>
                    <button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                        No
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>




@endsection('content')
@section('scripts')
    <script src="{{ asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });



        $('#deleteGroup').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var ids = button.data('ids');
            var user = button.data('user');

            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modalfooter #confirm').data('form', form);
            $("#hidden_id").val(ids);
            $(".hidden_title").html(' "' + user + '" ');
        });


        $('#deleteGroup').find('#confirm_yes').on('click', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //fetching id
            var id = $("#hidden_id").val();

            $.ajax({
                type: "POST",
                url: "{{ route('shareholder.delete_group') }}",
                headers: {
                    'XCSRFToken': $('meta[name="csrf-token"]').attr('content')
                },

                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg);
                    $("#deleteGroup").modal("hide");
                    $("#showMessage").modal("show");
                    // window.location.reload();
                }
            });
        });
        // AJAX DELETE PERMISSION END

        $( "#show_message" ).click(function() {
            window.location.reload();
        });

    </script>



@endsection('scripts')
