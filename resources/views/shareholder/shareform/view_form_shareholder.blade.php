
@extends('layouts.app')
@section('css')
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
@endsection('css')

@section('page_title')
    View Share Form
@endsection('page-title')

@section('page-title')
    Shareholder Form Information
@endsection('page-title')

@section('content')
<div style="margin-bottom:40px;">
{{--  <a href="{{route('shareholder.list_form')}}" class="btn btn-success pull-right"><i class="m-icon-swapleft m-icon-white"></i> Back</a>--}}
    <a href="{{route('shareholder.list_form')}}" class="btn default  button-previous" style="display: inline-block;">
        <i class="m-icon-swapleft"></i> Back </a>
</div>
   
  <div class=" portlet light bordered" >
    <h3>Shareholder Unique Id: <strong>{{$form->unique_id}}</strong></h3>
    @if($form->account_type == '1')
    <div class="row">Company Registration Certificate
        <div class="col-md-12">
        
        <h3 class="text-center"><strong>Individual  Details</strong></h3>
       <div class="form-group">
            <div class="col-md-3">
                <label>Shareholder Name</label>
                <input value="{{$form->shareholder_name}}" class="form-control" disabled="">
            </div>

            <div class="col-md-3">
                <label>Account Type</label>
                <input value="{{$form->account_type}}" class="form-control" disabled="">
            </div>

            <div class="col-md-3">
                <label>Citizenship Number</label>
                <input value="{{$form->citizenship_number}}" class="form-control" disabled="">
            </div>

            <div class="col-md-3">
                <label>Citizenship Issued Place</label>
                <input value="{{$form->citizenship_issued_place}}" class="form-control" disabled="">
         </div>
      </div>

        

      <div class="form-group">

           <div class="col-md-3">
                <label>Date of Birth</label>
                <input value="{{$form->date_of_birth}}" class="form-control" disabled="">
           </div>

            <div class="col-md-3">
                <label>Birth Place</label>
                <input value="{{$form->birth_place}}" class="form-control" disabled="">
            </div>

            <div class="col-md-3">
                <label>Father Name</label>
                <input value="{{$form->father_name}}" class="form-control" disabled="">
            </div>

            <div class="col-md-3">
                <label>Wife Name</label>
                <input value="{{$form->wife_name}}" class="form-control" disabled="">
            </div>

            
      </div>

      <div class="form-group">

             <div class="col-md-3">
                <label>Grandfather Name</label>
                <input value="{{$form->grandfather_name}}" class="form-control" disabled="">
            </div>  

            <div class="col-md-3">
                <label>Contact Number</label>
                <input value="{{$form->contact}}" class="form-control" disabled="">
            </div>

             <div class="col-md-3">
                <label>Temporary Address</label>
                <input value="{{$form->temporary_address}}" class="form-control" disabled="">
            </div>

            <div class="col-md-3">
                <label>Permanent Address</label>
                <input value="{{$form->permanent_address}}" class="form-control" disabled="">
            </div>

                 
      </div>
      <div class="form-group">

        <div class="col-md-3">
                <label>Email Address</label>
                <input value="{{$form->email_address}}" class="form-control" disabled="">
            </div>

        <div class="col-md-3">
                <label>Nominee Name</label>
                <input value="{{$form->nominee_name }}" class="form-control" disabled="">
            </div>
             <div class="col-md-3">
                <label>Nominee Contact Number</label>
                <input value="{{$form->nominee_contact_number}}" class="form-control" disabled="">
            </div>

           <div class="col-md-3">
                <label>Relation</label>
                <input value="{{$form->relation}}" class="form-control" disabled="">
            </div>

           

      </div>

  
   </div>
   </div>
   @else
    <div class="row">
      <h3 class="text-center"><strong>Company Information</strong></h3>
       <div class="form-group">
      
         <div class="col-md-3">
                 <label>Company Name</label>
                 <input value="{{$form->email_address}}" class="form-control" disabled="">
             </div>
      
         <div class="col-md-3">
                 <label>Registration Numebr</label>
                 <input value="{{$form->registration_number}}" class="form-control" disabled="">
             </div>
              <div class="col-md-3">
                 <label>Permanent Address</label>
                 <input value="{{$form->permanent_address}}" class="form-control" disabled="">
             </div>
      
            <div class="col-md-3">
                 <label>PAN Number</label>
                 <input value="{{$form->pan_number}}" class="form-control" disabled="">
             </div>

             <div class="col-md-3">
                 <label>Contract Person Name</label>
                 <input value="{{$form->contact}}" class="form-control" disabled="">
             </div>

              <div class="col-md-3">
                 <label>Contact Number</label>
                 <input value="{{$form->contact}}" class="form-control" disabled="">
              </div>

              <div class="col-md-3">
                 <label>Website</label>
                 <input value="{{$form->website}}" class="form-control" disabled="">
              </div>

              <div class="col-md-3">
                 <label>Email</label>
                 <input value="{{$form->email_address}}" class="form-control" disabled="">
              </div>
      
      
            
      
       </div>
    </div>
    @endif
 <br>
   <div class="row">
     <div class="col-md-12">
        <h3 class="text-center"><strong>Share Information</strong></h3>
        
       @if($installment->committed_amount !='')
       <h3 style="margin-left:15px;">Committed Amount:<strong>{{$installment->committed_amount}}</strong></h3>
        @endif
  
     </div>
   </div>
  

    <div class="row">
      

        <div class="form-group">

        
        

           @if($installment->first_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >First Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                         <input value="{{$installment->first_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->first_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif


           @if($installment->second_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >Second Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->second_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->second_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif


           @if($installment->third_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >Third Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->third_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->third_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif

           @if($installment->fourth_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >Fourth Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->fourth_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->fourth_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif

           @if($installment->fifth_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >Fifth Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->fifth_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->fifth_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif


           @if($installment->sixth_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >Sixth Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->sixth_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->sixth_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif

           @if($installment->seventh_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >Seventh Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->seventh_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->seventh_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif

           @if($installment->eighth_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >Eight Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->eighth_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->eighth_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif

           @if($installment->ninth_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >Ninth Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->ninth_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->ninth_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif

           @if($installment->tenth_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >Tenth Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->tenth_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->tenth_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif

           @if($installment->eleventh_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3 >Eleventh Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->eleventh_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->eleventh_expiry_date }}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif

           @if($installment->twelveth_installment_amt !='')
          <div class="col-md-3">
            <div class="col-md-12">
              <h3>Twelvth Installment Committed</h3>
              <div class="form-group">
                  <div class="row">
                    <label class="col-md-4 control-label">Amount:</label>
                    <div class="col-md-8">
                      <input value="{{$installment->twelveth_installment_amt}}" class="form-control" disabled="">
                    </div>
                  </div>
                <br>
               <div class="row">
                    <label class="col-md-4 control-label">Pay Date:</label>
                 <div class="col-md-8">
                      <input value="{{$installment->twelveth_expiry_date}}" class="form-control" disabled="">

                 </div>
               </div>
            </div>
             </div>   
           </div>
           @endif
        
        </div>

   </div>



   <div class="row">
      <div class="col-md-12">
        
       <h3 class="text-center"><strong>Account Documents</strong></h3>
      <br>
      <div class="form-group">
        
          <div class="col-md-12">
              <div class="col-md-3">
                @if($form->account_type==1)
                <label>Applicant Photo </label>
                @else
                <label>Company Registration Certificate</label>
                @endif
                <br>
                 @if(empty($form->avatar))
                        <img width="250" height="250" class="img img-thumbnail img-responsive" src="{{asset(STATIC_DIR.'assets/admin/layout/img/nofile.png')}}">
                 @else
                        <img width="250" height="250" class="img img-thumbnail img-responsive" src="{{asset(STATIC_DIR.'storage/'.$form->avatar)}}">
                 @endif
              </div>
            
             <div class="col-md-3">
                
                @if($form->account_type==1)
                <label>Birth Certificate</label>
                @else
                <label>Company Prabanda Certificate</label>
                @endif

                <br>
                @if(empty($form->birth_certificate))
                    <img width="250" height="250" class="img img-thumbnail img-responsive" src="{{asset(STATIC_DIR.'assets/admin/layout/img/nofile.png')}}">
                @else
                        <img width="250" height="250" class="img img-thumbnail img-responsive" src="{{asset(STATIC_DIR.'storage/'.$form->birth_certificate)}}">
                @endif
             </div>
            
             <div class="col-md-3">
                 @if($form->account_type==1)
                <label>Citizenship Scan</label>
                @else
                <label>Company Shareholder Agreement Certificate </label>
                @endif

                <br>
                @if(empty($form->citizenship_scan))
                     <img width="250" height="250" class="img img-thumbnail img-responsive" src="{{asset(STATIC_DIR.'assets/admin/layout/img/nofile.png')}}">
                @else
                     <img width="250" height="250" class="img img-thumbnail img-responsive" src="{{asset(STATIC_DIR.'storage/'.$form->citizenship_scan)}}">
                @endif
             </div>
            
             <div class="col-md-3">
                @if($form->account_type==1)
                <label>Protected Citizenship</label>
                @else
                <label>Company Tax Clearance Certificate</label>
                @endif

                <br>
                @if(empty($form->protected_citizenship))
                    <img width="250" height="250" class="img img-thumbnail img-responsive" src="{{asset(STATIC_DIR.'assets/admin/layout/img/nofile.png')}}">
                @else
                    <img width="250" height="250" class="img img-thumbnail img-responsive" src="{{asset(STATIC_DIR.'storage/'.$form->protected_citizenship)}}">
                @endif
             </div>
          </div>
  
      </div>
      </div>
   </div>


  


  
  </div>

  
@endsection('content')
@section('scripts')
<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/admin/pages/scripts/profile.js')}}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection('scripts')
