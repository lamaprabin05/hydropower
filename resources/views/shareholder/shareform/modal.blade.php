{{--message show--}}
<div id="showMessage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="margin-top: 200px;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Form Upload
                </h4>
            </div>

            <div class="modal-body">
                <div class="error" style="display: none;">
                    <i class="fa fa-close"></i> &nbsp; Error While Uploading Form.
                </div>

                <div class="success" style="display: none;">
                    <i class="fa fa-check"></i> &nbsp; File Uploaded Successfully .
                </div>

            </div>
            <input type="hidden" id="hidden_id">
            <div class="modal-footer success" style="text-align: center; display: none;">
                <button type="button" class="btn btn-danger green confirm_yes" id="show_message"><i class="icon-check"></i> Ok
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
