@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
@endsection('css')

@section('page_title')
    Edit Share Form
@endsection('page-title')

@section('page_title')
    Shareholder Form Information
@endsection('page-title')

@section('content')
<div style="margin-bottom:40px;">
    <a href="{{route('shareholder.edit',$installment->share_form_id)}}" class="btn default  button-previous" style="display: inline-block;">
        <i class="m-icon-swapleft"></i> Back </a>
</div>
   
  <div class=" portlet light bordered" >
    <h3>Shareholder Form Unique Id: <strong>{{$form->unique_id}}</strong></h3>

      <form action="{{route('shareholder.update_form')}}" enctype="multipart/form-data" method="post">
          @csrf
          <input type="hidden" name="form_id" value="{{$form->id}}">
          <div class="row">

              <div class="col-md-12">
                  <div class="form-group">
                      <label class="control-label col-md-3">Share Group Type <span class="required">* </span>
                      </label>

                      <div class="col-md-4">
                          <select  class="form-control" id="share_group_type" name="group_type"  required>
                              <option value="individual" @if($form->group_type == "individual") selected @endif>Individual</option>
                              <option value="group" @if($form->group_type == "group") selected @endif>Group</option>
                          </select>

                      </div>
                  </div>
              </div>



              <br>
          </div>

          <div class="row" style="margin-top:25px;">
              <div class="col-md-12">

                  <div class="form-group" id="group_name"  style="display:none">

                      <label class="control-label col-md-3">Select Group<span class="required">* </span>
                      </label>

                      <div class="col-md-4">
                          <select  class="form-control" name="share_group_id" id="group_name"  >
                              <option value="" hidden >Select One Group</option>
                              @foreach($group as $value)
                                  <option value="{{$value->id}}" @if($value->id == $form->share_group_id) selected @endif>{{ $value->name }}</option>
                              @endforeach
                          </select>

                      </div>
                  </div>

                  <div class="form-group" id="individual_name" style="display:none">
                      <label class="control-label col-md-3">Individual<span class="required">
													* </span>
                      </label>
                      <div class="col-md-4">
                          <input type="hidden" name="individual_id" value="@if($share_group != null) {{$share_group->id}} @endif ">
                          <input type="text" class="form-control" name="individual_group_name" value="@if($share_group != null) {{$share_group->name}}  @endif ">

                      </div>
                  </div>
              </div>
          </div>

          @if($form->account_type == '1')
              <div class="row">
                  <div class="col-md-12">
                      <div class="col-md-3">
                              <h3>Personal  Details</h3>
                      </div>

                  </div>
                  {{--first row--}}
                  <div class="col-md-12">
                      <hr>
                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Shareholder Name</label>
                              <input value="{{ old('shareholder_name') ?? $form->shareholder_name }}" class="form-control" name="shareholder_name" >
                              @if($errors->has('shareholder_name'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('shareholder_name') }}
                                  </span>
                              @endif
                          </div>

                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Account Type</label>
                              <input value="@if($form->account_type == 1) Individual @endif"  class="form-control" name="account_type" readonly>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Date of Birth</label>
                              <input value="{{ old('date_of_birth') ?? $form->date_of_birth}}" class="form-control" name="date_of_birth">
                              @if($errors->has('date_of_birth'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('date_of_birth') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Birth Place</label>
                              <input value="{{  old('birth_place') ??  $form->birth_place}}" class="form-control" name="birth_place">
                              @if($errors->has('birth_place'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('birth_place') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Email Address</label>
                              <input value="{{  old('email_address') ??  $form->email_address}}" class="form-control" name="email_address">
                              @if($errors->has('email_address'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('email_address') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Contact Number</label>
                              <input value="{{  old('contact') ??  $form->contact}}" class="form-control" name="contact">
                              @if($errors->has('contact'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('contact') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Temporary Address</label>
                              <input value="{{  old('temporary_address') ??  $form->temporary_address}}" class="form-control" name="temporary_address" >
                              @if($errors->has('temporary_address'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('temporary_address') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Permanent Address</label>
                              <input value="{{  old('permanent_address') ??  $form->permanent_address}}" class="form-control" name="permanent_address">
                              @if($errors->has('permanent_address'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('permanent_address') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                  </div>
                   {{--end first row--}}

                  {{--second row--}}
                  <div class="col-md-12">
                      <hr>
                      <h3>Document Information</h3>
                      <hr>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Citizenship Number</label>
                              <input value="{{  old('citizenship_number') ??  $form->citizenship_number}}" class="form-control" name="citizenship_number">
                              @if($errors->has('citizenship_number'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('citizenship_number') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Citizenship Issued Date</label>
                              <input value="{{   old('citizenship_issued_date') ??  $form->citizenship_issued_date}}" class="form-control" name="citizenship_issued_date">
                              @if($errors->has('citizenship_issued_date'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('citizenship_issued_date') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Citizenship Issued Place</label>
                              <input value="{{   old('citizenship_issued_place') ??  $form->citizenship_issued_place}}" class="form-control" name="citizenship_issued_place">
                              @if($errors->has('citizenship_issued_place'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('citizenship_issued_place') }}
                                  </span>
                              @endif
                          </div>
                      </div>
                  </div>
                   {{--end second row--}}

                  {{--third row--}}
                  <div class="col-md-12">
                      <hr>
                      <h3>Family Information</h3>
                      <hr>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Father Name</label>
                              <input value="{{   old('father_name') ??  $form->father_name}}" class="form-control" name="father_name" >
                              @if($errors->has('father_name'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('father_name') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Grandfather Name</label>
                              <input value="{{  old('grandfather_name') ??  $form->grandfather_name}}" class="form-control" name="grandfather_name" >
                              @if($errors->has('grandfather_name'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('grandfather_name') }}
                                  </span>
                              @endif
                          </div>

                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Husband Name</label>
                              <input value="{{   old('husband_name') ??  $form->husband_name}}" class="form-control" name="husband_name">
                              @if($errors->has('husband_name'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('husband_name') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Wife Name</label>
                              <input value="{{   old('wife_name') ??  $form->wife_name}}" class="form-control" name="wife_name">
                              @if($errors->has('wife_name'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('wife_name') }}
                                  </span>
                              @endif
                          </div>
                      </div>
                  </div>
                   {{--end third row--}}

                  {{--fourth row--}}
                  <div class="col-md-12">
                      <hr>
                      <h3>Nominee Information</h3>
                      <hr>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Nominee Name</label>
                              <input value="{{   old('nominee_name') ??  $form->nominee_name }}" class="form-control" name="nominee_name" >
                              @if($errors->has('nominee_name'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('nominee_name') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Nominee Contact Number</label>
                              <input value="{{   old('nominee_contact_number') ??  $form->nominee_contact_number}}" class="form-control" name="nominee_contact_number">
                              @if($errors->has('nominee_contact_number'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('nominee_contact_number') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Nominee Address</label>
                              <input value="{{   old('nominee_address') ??  $form->nominee_address}}" class="form-control" name="nominee_address">
                              @if($errors->has('nominee_address'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('nominee_address') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Relation</label>
                              <input value="{{   old('relation') ??  $form->relation}}" class="form-control" name="relation">
                              @if($errors->has('relation'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('relation') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                  </div>
                  {{--fourth row--}}

              </div>
          @else
              <div class="row">
                  <div class="col-md-12">
                      <div class="col-md-3">
                          <h3>Company Information</h3>
                      </div>

                  </div>

                  <div class="col-md-12">
                      <hr>
                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Company Name</label>
                              <input value="{{   old('company_name') ??  $form->shareholder_name}}" class="form-control" name="company_name" >
                              @if($errors->has('company_name'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('company_name') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Registration Number</label>
                              <input value="{{   old('registration_number') ??  $form->registration_number}}" class="form-control"  name="registration_number">
                              @if($errors->has('registration_number'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('registration_number') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Company Permanent Address</label>
                              <input value="{{   old('company_permanent_address') ??  $form->permanent_address}}" class="form-control" name="company_permanent_address">
                              @if($errors->has('company_permanent_address'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('company_permanent_address') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>PAN Number</label>
                              <input value="{{  old('pan_number') ?? $form->pan_number}}" class="form-control" name="pan_number">
                              @if($errors->has('pan_number'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('pan_number') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Contact Person Name*</label>
                              <input value="{{  old('company_contact_name') ?? $form->contact}}" class="form-control" name="company_contact_name">
                              @if($errors->has('company_contact_name'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('company_contact_name') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Contact Number</label>
                              <input value="{{  old('company_contact') ?? $form->contact}}" class="form-control" name="company_contact" >
                              @if($errors->has('company_contact'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('company_contact') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Website</label>
                              <input value="{{  old('website') ?? $form->website}}" class="form-control" name="website">
                              @if($errors->has('website'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('website') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label>Email</label>
                              <input value="{{  old('company_email') ?? $form->email_address}}" class="form-control" name="company_email">
                              @if($errors->has('company_email'))
                                  <span class="help-block" style="color:red;">
                                      * {{ $errors->first('company_email') }}
                                  </span>
                              @endif
                          </div>
                      </div>

                  </div>

              </div>
          @endif

          <br>
          <div class="row">
              <div class="col-md-12">
                  <div class="col-md-3">
                      <div class="form-group">
                          <button type="submit" class="btn green button-next">
                              <i class="fa fa-check"></i>
                              Update
                          </button>
                      </div>
                  </div>
              </div>
          </div>
      </form>



  
  </div>
@endsection('content')
@section('scripts')

    <script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/admin/pages/scripts/profile.js')}}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            let data = $('#share_group_type').val();
            showOption(data);

            $('#share_group_type').on('change', function(e){
                let data = $(this).val();
                showOption(data);


            });

            function showOption(value){
                if (value == 'individual'){
                    $('#individual_name').show();
                    $('#group_name').hide();
                }
                else if(value =='group'){
                    $('#group_name').show();
                    $('#individual_name').hide();
                }
            }
        });
    </script>
@endsection('scripts')
