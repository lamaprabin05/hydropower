
@extends('layouts.app')
@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
@endsection('css')

@section('page-title')
    Shareholder Form Information
@endsection('page-title')

@section('content')
<div style="margin-bottom:40px;">
{{--  <a href="" class="btn btn-success pull-right"><i class="m-icon-swapleft m-icon-white"></i> Back</a>--}}
    <a href="{{route('shareholder.list_form')}}" class="btn default  button-previous" style="display: inline-block;">
        <i class="m-icon-swapleft"></i> Back </a>
</div>
   
  <div class=" portlet light bordered" >
    <h3>Shareholder Unique Id: <strong>{{$form->unique_id}}</strong></h3>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12">
            
            <div class="col-md-4">
                <a href="{{ route('shareholder.edit_form',$form->id) }}" class="btn green">
                    <i class="fa fa-edit"></i> Edit Form
                </a>
            </div>

            <div class="col-md-4">
                <a href="{{ route('shareholder.edit_installment',['id' =>$form->id]) }}" class="btn blue">
                    <i class="fa fa-edit"></i> Edit Installments
                </a>
            </div>

            <div class="col-md-4">
                <a href="{{ route('shareholder.edit_document',['id' =>$form->id]) }}" class="btn green">
                    <i class="fa fa-edit"></i>  Edit Documents
                </a>
            </div>

        </div>
    </div>
  </div>
@endsection('content')
@section('scripts')
    <script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/admin/pages/scripts/profile.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection('scripts')
