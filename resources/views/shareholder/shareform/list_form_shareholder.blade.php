@extends('layouts.app')
@section('css')

<link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    List Form
@endsection('page-title')

@section('page-title')
    Shareholder Information
@endsection('page-title')


@section('content')

<!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box blue-madison">
                        <div class="portlet-title">
                            <div class="caption">
                                View Details Of Shareholder
                            </div>
                            
                        </div>

                        <div class="portlet-body">

                                <table class="table table-striped table-bordered table-hover" id="group">
                            <thead>
                            <tr>
                                <th>
                                    S.N.
                                </th>

                                <th>
                                     Shareholder Name
                                </th>

                                <th>
                                	Group Type
                                </th>

                                <th>
                                     Unique Id
                                </th>

                                <th>
                                     Created At
                                </th>

                                <th>Action</th>
                               
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($form as $element)
                                <tr>
                                    <td>
                                       {{$loop->iteration}}
                                    </td>

                                    <td>
                                      {{$element->shareholder_name}}
                                    </td>

                                    <td>
                                        {{$element->group_type}}
                                    </td>

                                    <td>
                                        {{$element->unique_id}}
                                    </td>
                                    <td>
										{{$element->created_at}}
                                    </td>

                                    <td>
                                        <a href="{{ route('shareholder.view',['id' =>$element->id]) }}" class="btn btn-sm btn-primary">
                                            <i class="fa fa-eye"></i> View
                                        </a>
                                         <a href="{{ route('shareholder.edit',['id' =>$element->id]) }}" class="btn btn-sm btn-danger">
                                             <i class="fa fa-edit"></i> Edit
                                         </a>
                                    </td>
                                </tr>
                                @endforeach
                          

                            </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->

@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });
     </script>


@endsection('scripts')
