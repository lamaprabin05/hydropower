
@extends('layouts.app')
@section('css')
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
@endsection('css')
@section('page-title')
Shareholder Form Information
@endsection('page-title')
@section('content')
<div style="margin-bottom:40px;">
    <a href="{{route('shareholder.edit',$form->id)}}" class="btn default  button-previous" >
        <i class="m-icon-swapleft"></i> Back </a>
</div>
   
  <div class=" portlet light bordered" >
  <h3>Shareholder Unique Id: <strong>{{$form->unique_id}}</strong></h3>

  <form action="{{route('shareholder.edit_document_form')}}" method="post" enctype= multipart/form-data>
    @csrf
    <input type="hidden" value="{{$form->id}}" name="id">
   <div class="row">
      <div class="col-md-12">
        
       <h3 class="text-center"><strong>Account Documents</strong></h3>
      <br>
      <div class="form-group">
        
          <div class="col-md-12">

             <div class="col-md-3">                                        
                @if($form->account_type==1)
                    <label>Applicant Photo </label>
                @else
                    <label>Company Registration Certificate</label>
                @endif
                <br>

                <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                            @if(!empty($form->avatar))
                                <img width="250" height="250"  src="{{asset(STATIC_DIR.'storage/'.$form->avatar)}}">
                            @else
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                            @endif
                        </div>

                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        </div>

                        <div>
                            <span class="btn default btn-file">
                            <span class="fileinput-new">
                            Select image </span>
                            <span class="fileinput-exists">
                            Change </span>
                            <input type="file" name="avatar">
                            </span>
                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                            Remove </a>
                        </div>
                </div>
             </div>

             <div class="col-md-3">
                @if($form->account_type==1)
                    <label>Birth Certificate</label>
                @else
                    <label>Company Prabanda Certificate</label>
                @endif
                <br>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >

                            @if(!empty($form->birth_certificate))
                                <img width="250" height="250"  src="{{asset(STATIC_DIR.'storage/'.$form->birth_certificate)}}">
                            @else
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                            @endif
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        </div>
                        <div>
                            <span class="btn default btn-file">
                            <span class="fileinput-new">
                            Select image </span>
                            <span class="fileinput-exists">
                            Change </span>
                            <input type="file" name="birth_certificate">
                            </span>
                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                            Remove </a>
                        </div>
                    </div>
             </div>
            

             <div class="col-md-3">                                        
                   @if($form->account_type==1)
                <label>Citizenship Scan</label>
                @else
                <label>Company Shareholder Agreement Certificate </label>
                @endif
                <br>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                            @if(!empty($form->birth_certificate))
                                <img width="250" height="250"  src="{{asset(STATIC_DIR.'storage/'.$form->citizenship_scan)}}">
                            @else
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                            @endif
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        </div>
                        <div>
                            <span class="btn default btn-file">
                            <span class="fileinput-new">
                            Select image </span>
                            <span class="fileinput-exists">
                            Change </span>
                            <input type="file" name="citizenship_scan">
                            </span>
                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                            Remove </a>
                        </div>
                    </div>
             </div>

            
             
             <div class="col-md-3">                                        
                    @if($form->account_type==1)
                <label>Protected Citizenship</label>
                @else
                <label>Company Tax Clearance Certificate</label>
                @endif
                <br>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                            @if(!empty($form->birth_certificate))
                                <img width="250" height="250"  src="{{asset(STATIC_DIR.'storage/'.$form->protected_citizenship)}}">
                            @else
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                            @endif
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        </div>
                        <div>
                            <span class="btn default btn-file">
                            <span class="fileinput-new">
                            Select image </span>
                            <span class="fileinput-exists">
                            Change </span>
                            <input type="file" name="protected_citizenship">
                            </span>
                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                            Remove </a>
                        </div>
                    </div>
             </div>
          </div>
  
      </div>
      </div>
   </div>
      <button type="submit" class="btn green-haze">
          <i class="fa fa-check"></i> Submit
      </button>
</form>

  </div>
@endsection('content')
@section('scripts')
<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/admin/pages/scripts/profile.js')}}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection('scripts')
