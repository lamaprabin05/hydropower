@extends('layouts.app')
@section('css')
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<!-- <link href="{{asset(STATIC_DIR.'assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
 --><link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
 //custom english calander css
<link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>

@endsection('css')
@section('page_title')
    Add New Form
@endsection('page-title')

@section('page-title')
    Add Form
@endsection('page-title')

@section('content')
   
<!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box blue" id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Shareholder Form FillUp - <span class="step-title">
                                Step 1 of 5 </span>
                            </div>

                        </div>
                        <div class="portlet-body form">
                            <form action="{{ route('shareholder.process_form') }}" class="form-horizontal" id="submit_form" method="POST" enctype='multipart/form-data'>

                                <div class="form-wizard">
                                    <div class="form-body">
                                        <ul class="nav nav-pills nav-justified steps">
                                            <li>
                                                <a href="#tab1" data-toggle="tab" class="step">
                                                <span class="number">
                                                1 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> Account Setup </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab2" data-toggle="tab" class="step">
                                                <span class="number">
                                                2 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> Account Details </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab3" data-toggle="tab" class="step active">
                                                <span class="number">
                                                3 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> File Upload </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab4" data-toggle="tab" class="step active">
                                                <span class="number">
                                                4 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> Installment </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab5" data-toggle="tab" class="step">
                                                <span class="number">
                                                5 </span>
                                                <span class="desc">
                                                <i class="fa fa-check"></i> Confirm </span>
                                                </a>
                                            </li>
                                        </ul>

                                        <div id="bar" class="progress progress-striped" role="progressbar">
                                            <div class="progress-bar progress-bar-success">
                                            </div>
                                        </div>

                                        <div class="tab-content">
                                            <div class="alert alert-danger display-none">
                                                <button class="close" data-dismiss="alert"></button>
                                                You have some form errors. Please check below.
                                            </div>

                                            <div class="alert alert-success display-none">
                                                <button class="close" data-dismiss="alert"></button>
                                                Your form validation is successful!
                                            </div>

                                            <div class="tab-pane active" id="tab1">
                                                <h3 class="block">Provide your Account Details</h3>

                                                <div class="form-group">
                                                  <label class="control-label col-md-3">Account Type <span class="required">* </span>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <select  class="form-control" id="accounts_type" name="account_type"  required>
                                                            <option  value="" hidden >Select Account Type</option>
                                                            <option value='1' >Personal Account</option>
                                                            <option value='2'>Company Account</option>
                                                        </select>
                                                        <span class="help-block">
                                                            Choose Any Account Type
                                                        </span>
                                                    </div>

                                                </div>

												<div class="form-group">
                                                  <label class="control-label col-md-3">Share Group Type <span class="required">* </span>
                                                    </label>

													<div class="col-md-4">
														<select  class="form-control" id="share_group_type" name="group_type"  required>
															<option value="" hidden >Select Group Type</option>
															<option value="individual">Individual</option>
															<option value="group">Group</option>
														</select>
														<span class="help-block">
                                                        Choose Any Share Group Type </span>
                                                    </div>
                                                </div>

                                                <div class="form-group" id="group_name"  style="display:none">
													<label class="control-label col-md-3">Select Group<span class="required">* </span>
													</label>
													<div class="col-md-4">
														<select  class="form-control" name="share_group_id" id="group_name"  >
															<option value="" hidden >Select One Group</option>
                                                            @foreach($group as $value)
                                                                <option value="{{$value->id}}">{{ $value->name }}</option>
                                                            @endforeach
														</select>
														<span class="help-block">
                                                        Choose Any Account Type </span>
													</div>
												</div>

												  <div class="form-group" id="individual_name" style="display:none">
													<label class="control-label col-md-3">Individual<span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="individual_group_name"  required="">
														<span class="help-block">
														Enter Individual Name </span>
													</div>
												</div>

                                            </div>
                                            <!-- Tab1 End -->

                                            <!-- Tab2 Start -->
                                            <div class="tab-pane" id="tab2">
                                                <!-- <h3 class="block">Applicant details</h3> -->

                                                <div id="individual">
                                                    <h3 class="text-center text-white">Fill Individual Details</h3>
                                                    <hr>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label>Applicant Name:</label>
                                                            <input type="text" name="individual_name" class="form-control" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Temporary Addresss:</label>
                                                            <input type="text" name="temporary_address" class="form-control" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Permanent Addresss:</label>
                                                            <input type="text" name="individual_permanent_address" class="form-control" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Citizenship Number:</label>
                                                            <input type="text" name="citizenship_number" class="form-control" required="">
                                                        </div>
                                                     </div>
                                                    
                                                     <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label>Citizenship Issued Place:</label>
                                                            <input type="text" name="citizenship_issued_place" class="form-control" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Citizenship Issued Date:</label>
                                                            <input type="text" name="citizenship_issued_date" class="form-control datepicker" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Applicant Date Of Birth(DOB)</label>
                                                            <input type="text" name="date_of_birth" class="form-control datepicker" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Applicant Birth place *</label>
                                                            <input type="text" name="birth_place" class="form-control" required="">
                                                        </div>
                                                     </div>
                                                    
                                                     <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label>Contact Number:</label>
                                                            <input type="text" name="individual_contact" class="form-control" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Email Addresss:</label>
                                                            <input type="text" name="individual_email_address" class="form-control" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Father Name:</label>
                                                            <input type="text" name="father_name" class="form-control" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>GrandFather Name:</label>
                                                            <input type="text" name="grandfather_name" class="form-control" required="">
                                                        </div>
                                                     </div>
                                                    
                                                     <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label>Husband Name: *</label>
                                                            <input type="text" name="husband_name" class="form-control" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Wife Name:</label>
                                                            <input type="text" name="wife_name" class="form-control" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Nominee Name: *</label>
                                                            <input type="text" name="nominee_name" class="form-control" required="">
                                                        </div>

                                                         <div class="col-md-3">
                                                            <label>Relationship * </label>
                                                            <select class="form-control" name="relation" required="">
                                                                <option selected disabled>Select Any One</option>
                                                                <option>Son</option>
                                                                <option>Daughter</option>
                                                               
                                                            </select>
                                                         </div>
                                                        
                                                      </div>
                                                    
                                                      <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label>Nominee Address: *</label>
                                                            <input type="text" name="nominee_address" class="form-control" required="">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label> Nominee Contact Number: *</label>
                                                            <input type="text" name="nominee_contact_number" class="form-control" required="">
                                                        </div>
                                                        
                                                     </div>
                                                </div>

                                                <!-- Company Details -->
                                                <div id="company" >
                                                    <h3 class=" text-center text-white " >Fill Company Details</h3>
                                                    <hr>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label>Company Name:</label>
                                                            <input type="text" name="shareholder_name" class="form-control" required="">
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label>Registration Number:</label>
                                                            <input type="text" name="registration_number" class="form-control" required="">
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label>Company Address:</label>
                                                            <input type="text" name="company_permanent_address" class="form-control" required="">
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label>Contact Number:</label>
                                                            <input type="number" min="0" name="company_contact" class="form-control" required="">
                                                        </div>

                                                     </div>

                                                     <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label>Pan Number:</label>
                                                            <input type="text" name="pan_number" class="form-control" required="">
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label>Contact Person Name: *</label>
                                                            <input type="" name="" class="form-control" required="">
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label>Website:</label>
                                                            <input type="text" name="website" class="form-control" >
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label>Email:</label>
                                                            <input type="text" name="company_email_address" class="form-control" >
                                                        </div>

                                                     </div>
                                                </div>
                                                <!-- Company Details End -->
                                            
                                            </div>
                                            <!-- Tab2 End -->

                                            <!-- Tab3 Start -->
                                            <div class="tab-pane" id="tab3">
                                                <h3 class="block">Uplaod Images</h3>
                                                 <div class="form-group">
                                                    <div class="col-md-3">
                                                        <label class="individual">Upload Citizenship</label>
                                                        <label class="company">Company Registration Certificate</label>
                                                    <br>
														<div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
																	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
																</div>
																<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
																</div>
																<div>
																	<span class="btn default btn-file">
																	<span class="fileinput-new ">
																	Select image </span>
																	<span class="fileinput-exists ">
																	Change </span>
                                                                    <input type="file" name="citizenship_scan" required="" >
																	
																	</span>
																	<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
																	Remove </a>
																</div>
														</div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        
                                                        <label class="individual">Upload Photo</label>
                                                        <label class="company">Company Prabanda Certificate </label>
                                                    <br>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                            </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                <span class="fileinput-new">
                                                                Select image </span>
                                                                <span class="fileinput-exists">
                                                                Change </span>
                                                                <input type="file" name="avatar" required="" >
                                                                </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                                Remove </a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="individual">Birth Certificate</label>
                                                        <label class="company">Company Shareholder Agreement Certificate </label>
                                                    <br>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                            </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                <span class="fileinput-new ">
                                                                Select image </span>
                                                                <span class="fileinput-exists">
                                                                Change </span>
                                                                <input type="file" name="birth_certificate" required="" >
                                                                </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                                Remove </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="individual">Protector Citizenship</label>
                                                        <label class="company">Company Tax Clearance Certificate </label>
                                                        
                                                    <br>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">

                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                            </div>
                                                            <div>
                                                                
                                                                <span class="btn default btn-file">
                                                                <span class="fileinput-new ">
                                                                Select image </span>
                                                                <span class="fileinput-exists">
                                                                Change </span>
                                                                <input type="file" name="protected_citizenship" required="" >
                                                                </span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                                Remove </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                 </div>
                                                
                                            </div>
                                            <!-- Tab3 End -->
                                            
                                            <!-- Tab4 Start -->
                                            <div class="tab-pane" id="tab4">
                                                <h3 class="block">Fill You Installment Details</h3>

												<div class="form-group">
													<label class="control-label col-md-3">Commited Amount <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="number" min="0" class="form-control" required="" min="0" name="committed_amount"/>
														<span class="help-block">
														Provide your commited amount in digiit </span>
													</div>
												</div>

												{{--<div class="form-group">--}}
													{{--<label class="control-label col-md-3">Commited Amount In Figure <span class="required">--}}
													{{--* </span>--}}
													{{--</label>--}}
                                                    {{--<div class="col-md-4">--}}
                                                        {{--<input type="text" class="form-control" name="password" id="submit_form_password"/>--}}
                                                        {{--<span class="help-block">--}}
                                                        {{--Provide your commited amount in figure </span>--}}
                                                    {{--</div>--}}
												{{--</div>--}}
                                                <div class="form-group">
                                                  <label class="control-label col-md-3">Choose Installment <span class="required">* </span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <select  class="form-control" id="installment" name="no_of_installment" >
                                                            <option value="0" hidden>Select</option> 
                                                            <option value="1" >1</option>
                                                            <option value="2" >2</option>
                                                            <option value="3" >3</option>
                                                            <option value="4" >4</option>
                                                            <option value="5" >5</option>
                                                            <option value="6" >6</option>
                                                            <option value="7" >7</option>
                                                            <option value="8" >8</option>
                                                            <option value="9" >9</option>
                                                            <option value="10" >10</option>
                                                            <option value="11" >11</option>
                                                            <option value="12" >12</option>
                                                        </select>
                                                        <span class="help-block">
                                                        Choose Applicant Installment Type </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-3" id="installment_1">
                                                        <h3 class="text-center">Installment 1</h3>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0"  name="first_installment_amt" class="form-control" placeholder="Enter amount in digit" required="" >
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="first_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input  name="nep_date1" class="bod-picker  form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}


                                                        {{--</div>--}}
                                                        {{--</div>--}}

                                                     </div>

                                                        

                                                    <div class="col-md-3" id="installment_2">
                                                        <h3 class="text-center">Installment 2</h3>
                                                        
                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0"   class="form-control" placeholder="Enter amount in digit" name="second_installment_amt" required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="second_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date2" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>

                                                    <div class="col-md-3" id="installment_3">
                                                        <h3 class="text-center">Installment 3</h3>
                                                        
                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0"  class="form-control" placeholder="Enter amount in digit" name="third_installment_amt" required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="third_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date3" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>

                                                    <div class="col-md-3" id="installment_4">
                                                        <h3 class="text-center">Installment 4</h3>
                                                        
                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0"  class="form-control" placeholder="Enter amount in digit" name="fourth_installment_amt" required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="fourth_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date4" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-3" id="installment_5">
                                                        <h3 class="text-center">Installment 5</h3>
                                                        
                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0" class="form-control" placeholder="Enter amount in digit" name="fifth_installment_amt" required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="fifth_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date5" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                    <div class="col-md-3" id="installment_6">
                                                        <h3 class="text-center">Installment 6</h3>
                                                        
                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0" class="form-control" placeholder="Enter amount in digit" name="sixth_installment_amt"  required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="sixth_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date6" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                    <div class="col-md-3" id="installment_7">
                                                        <h3 class="text-center">Installment 7</h3>
                                                        
                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0" class="form-control" placeholder="Enter amount in digit" name="seventh_installment_amt" required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="seventh_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date7" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                    <div class="col-md-3" id="installment_8">
                                                        <h3 class="text-center">Installment 8</h3>
                                                        
                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0" class="form-control" placeholder="Enter amount in digit" name="eighth_installment_amt" required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="eighth_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date8" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-3" id="installment_9">
                                                        <h3 class="text-center">Installment 9</h3>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0" class="form-control" placeholder="Enter amount in digit" name="ninth_installment_amt" required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="ninth_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="" >
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date9" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}

                                                     </div>

                                                        

                                                    <div class="col-md-3" id="installment_10">
                                                        <h3 class="text-center">Installment 10</h3>
                                                        
                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0" class="form-control" placeholder="Enter amount in digit" name="tenth_installment_amt" required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="tenth_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date10" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>

                                                    <div class="col-md-3" id="installment_11">
                                                        <h3 class="text-center">Installment 11</h3>
                                                        
                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0" class="form-control" placeholder="Enter amount in digit" name="eleventh_installment_amt" required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="eleventh_expiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date11" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>

                                                    <div class="col-md-3" id="installment_12">
                                                        <h3 class="text-center">Installment 12</h3>
                                                        
                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount:</label>
                                                        <div class="col-md-8">
                                                            <input type="number" min="0" class="form-control" placeholder="Enter amount in digit" name="twelveth_installment_amt" required="">
                                                        </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="col-md-4 control-label">Eng Pay Date:</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="twelveth_exiry_date" class="form-control datepicker" placeholder="Enter amount in digit" required="">
                                                        </div>
                                                        </div>

                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="col-md-4 control-label">Nep Pay Date:</label>--}}
                                                        {{--<div class="col-md-8">--}}
                                                            {{--<input type="text" name="nep_date12" class="bod-picker input-box form-control"   placeholder="मिति चयन गर्नुहोस"  required="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                                
                                                
                                             </div>

                                             <div class="tab-pane" id="tab5">
                                                <h3 class="block text-center">Confirm your account And Submit</h3>
                                             </div>
                                        </div>

                                        <div>
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <a href="javascript:;" class="btn default button-previous">
                                                    <i class="m-icon-swapleft"></i> Back </a>
                                                    <a href="javascript:;" class="btn blue button-next">
                                                    Continue <i class="m-icon-swapright m-icon-white"></i>
                                                    </a>
                                                    <button type="submit" id="submit" class="btn green button-submit">
                                                    Submit <i class="m-icon-swapright m-icon-white"></i>

                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
@include('shareholder.shareform.modal')
@endsection('content')
@section('scripts')

<script>
    jQuery(document).ready(function() {
       FormWizard.init();
    });

$('#submit_form .button-submit').click(function () {
    
    $.ajax({
        type: "POST",
        url: "{{ route('shareholder.process_form') }}",
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: new FormData($('#submit_form')[0]),
        processData: false,
        contentType: false,
        success: function (data) {
            //check if there is no error then reload the page and reset form.
           /* if(data['error'] == 'false'){
                alert('Your data has been successfully submitted');
                window.location.href = "{{route('shareholder.list_form')}}";
                // window.location.reload();

            }*/

            if(data['error'] == 'false'){
                $('#showMessage').find('.success').show();
                $("#showMessage").modal("show");
            }
            else
            {
                $('#showMessage').find('.error').show();
                $("#showMessage").modal("show");
            }
        }
    });
}).hide();

    $('#show_message').on('click', function () {
        window.location.href = "{{route('shareholder.list_form')}}";
    });

    $(document).ready(function () {
                $("#date1").on('click',function(){
                    $('#date1').remove();
                });

                $(".bod-picker").nepaliDatePicker({
                    dateFormat: "%D, %M %d, %y",
                    closeOnDateSelect:true
                    //minDate : "सोम, जेठ १०, २०७३",
                    //maxDate : "मंगल, जेठ ३२, २०७३"
                });
            });


           
       

</script>

<script src="{{ asset(STATIC_DIR.'shareholder/js/add_installment.js') }}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>

<script src="{{asset(STATIC_DIR.'assets/admin/pages/scripts/profile.js')}}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

<script src="{{asset(STATIC_DIR.'assets/admin/pages/scripts/form-wizard.js')}}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
{{--<script src="{{asset(STATIC_DIR.'assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>--}}

<script src="{{asset(STATIC_DIR.'assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
{{--<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>--}}
{{--<!-- <script src="{{asset(STATIC_DIR.'assets/admin/pages/scripts/profile.js')}}" type="text/javascript"></script> -->--}}
{{--<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>--}}


@endsection('scripts')
