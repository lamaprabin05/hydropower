@extends('layouts.app')
@section('css')

<link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    Installment
@endsection('page-title')

@section('page-title')
    Search Installment Payee
@endsection('page-title')


@section('content')

<!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box blue-madison">
                        <div class="portlet-title">
                            <div class="caption">
                                Search and Add Installment
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{route('shareholder.installment')}}" method="GET">
                                    @csrf
                                    <div class="form-group">

                                        <div class="col-md-4">
                                            <select  class="form-control" id="share_group_type" name="search_type"  required>
                                                <option value="" hidden >Select Search Type</option>
                                                <option value="individual">Individual</option>
                                                <option value="group">Group</option>
                                            </select>

                                        </div>

                                        <button type="submit" class="btn red"> <i class="fa fa-search-minus"></i> Search</button>


                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="portlet-body">
                            @if($token == 'all')
                                <h3>All Listing</h3>
                            @elseif($token == 'group')
                                <h3>Searching through Group</h3>
                            @else
                                <h3>Searching through Individual</h3>
                            @endif
                                <table class="table table-striped table-bordered table-hover" id="group">
                            <thead>
                            <tr>
                                <th>
                                    S.N.
                                </th>

                                <th>
                                     Shareholder Name
                                </th>
                                <th>
                                     Unique ID
                                </th>
                                <th>
                                     Total Installment:
                                </th>

                                <th>
                                     Remaining Installment
                                </th>

                                <th>Action</th>
                               
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($form_share as $value)
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>

                                    <td>
                                        {{$value->shareholder_name}}
                                    </td>

                                    <td>
                                        {{$value->unique_id}}
                                    </td>

                                    <td>
                                        {{$value->payment_information->no_of_installment}}
                                    </td>
                                    <td>
                                        {{$value->payment_information->remaining_installment}}
                                    </td>

                                    <td>
                                        @if($value->payment_information->remaining_installment != 0)
                                            <a href="{{route('shareholder.add_installment',$value->id)}}" class="btn btn-sm  btn-success">
                                                {{--<i class="fa fa-plus-circle"></i> Add</a>--}}
                                            <i class="icon-plus"></i> Add</a>

                                        @endif
                                            <a href="{{ route('shareholder.view_installment',$value->id) }}" class="btn btn-sm btn-primary">
                                                <i class="fa fa-eye"></i> View
                                            </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->

@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });
    </script>

@endsection('scripts')
