@extends('layouts.app')
@section('css')
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

{{-- custom english calander css --}}
<link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>

@endsection('css')
@section('page_title')
    Add Installment
@endsection('page-title')

@section('page-title')
    Add Installment
@endsection('page-title')


@section('content')
<div style="margin-bottom:50px;">
    <a href="{{route('shareholder.installment')}}" class="btn btn-sm btn-success pull-left"><i class="m-icon-swapleft m-icon-white"></i> Back</a>

</div>

<div class=" portlet light bordered" >
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">Installment Information:</h4>
        </div>
        <div class="col-md-12" >

            <table class="table table-bordered" >


                <thead>
                <tr>

                    <th>Commited Amount</th>
                    <th>Total Paid Amount</th>
                    <th>Total Installment</th>
                    <th>Remaining Installment</th>
                    @if($installment->payment_information->remaining_installment != 0)
                        <th>Installment</th>
                        <th>Installment Amount</th>
                        <th>Installment Expiry Date</th>
                    @endif
                </tr>

                </thead>
                <tbody>
                <tr>

                    <td>{{$installment->payment_information->committed_amount}}</td>
                    <td>{{$installment->payment_information->total_paid_amount}}</td>
                    <td>{{$installment->payment_information->no_of_installment}}</td>
                    <td>{{$installment->payment_information->remaining_installment}}</td>
                    @if($installment->payment_information->remaining_installment != 0)
                        <td>{{ucfirst($installment->proceeded_for)}} Payment</td>
                        <td>{{$installment_detail->installment}}</td>
                        <td>{{$installment_detail->expiry_date}}</td>
                    @endif
                </tr>
                </tbody>

            </table>
        </div>

    </div>



</div>

<div class=" portlet light bordered" >
    <div class="row">
        @if($installment->payment_information->remaining_installment != 0)
        <div class="col-md-12"  >
            <form action="{{route('shareholder.process_installment')}}" method="post" enctype='multipart/form-data'>
                {{csrf_field()}}
                <div>
                <div class="col-md-12">
                    {{--<a href="" class="pull-right btn btn-info "><i class="fa fa-print"></i> Print</a>--}}

                    <h3 class="text-center">Add New Installment</h3>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Applicant Name:</label>
                            <input type="text" name="applicant_name" value="{{$installment->shareholder_name}}" readonly="" class="form-control">
                            @if($errors->has('applicant_name'))
                                <span class="help-block" style="color:red;">
                                * {{ $errors->first('applicant_name') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Form Id:</label>
                            <input type="text" name="form_id" value="{{$installment->id}}" readonly="" class="form-control">
                            @if($errors->has('form_id'))
                                <span class="help-block" style="color:red;">
                                * {{ $errors->first('form_id') }}
                            </span>
                            @endif
                        </div>

                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Bank Name:</label>
                            <input type="text" name="bank_name" class="form-control" value="{{old('bank_name')}}" autocomplete="off" required="">
                            @if($errors->has('bank_name'))
                                <span class="help-block" style="color:red;">
                                {{ $errors->first('bank_name') }}

                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Branch Name:</label>
                            <input type="text" name="branch_name" class="form-control" value="{{old('branch_name')}}" autocomplete="off" required="">
                            @if($errors->has('branch_name'))
                                <span class="help-block" style="color:red;">
                                    {{ $errors->first('branch_name') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>


                <div class="col-md-12">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Account Number:</label>
                            <input type="number" name="account_number" class="form-control" value="{{old('account_number')}}" autocomplete="off" required="">
                            @if($errors->has('account_number'))
                                <span class="help-block" style="color:red;">
                                * {{ $errors->first('account_number') }}
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Installment Paid Amount:</label>
                            <input type="number" min="0" name="installment_paid_amount" class="form-control" value="{{old('installment_paid_amount')}}" autocomplete="off" required="">
                            @if($errors->has('installment_paid_amount'))
                                <span class="help-block" style="color:red;">
                                * {{ $errors->first('installment_paid_amount') }}
                            </span>
                            @endif
                        </div>

                    </div>



                    <div class="col-md-3">

                        <div class="form-group">

                            <label class="control-label">Choose Installment <span class="required">* </span></label>
                            <select name="installment_number" class="form-control" required="">
                                    <option hidden >Select Installment</option>

                                    <option value="1" @if(old('installment_number')=='1') selected @endif>
                                        First Installment
                                    </option>

                                    <option value="2" @if(old('installment_number')=='2') selected @endif>
                                        Second Installment
                                    </option>

                                    <option value="3" @if(old('installment_number')=='3') selected @endif>
                                        Third Installment
                                    </option>

                                    <option value="4" @if(old('installment_number')=='4') selected @endif>
                                        Fourth Installment
                                    </option>

                                    <option value="5" @if(old('installment_number')=='5') selected @endif>
                                        Fifth Installment
                                    </option>

                                    <option value="6" @if(old('installment_number')=='6') selected @endif>
                                        Sixth Installment
                                    </option>

                                    <option value="7" @if(old('installment_number')=='7') selected @endif>
                                        Seventh Installment
                                    </option>

                                    <option value="8" @if(old('installment_number')=='8') selected @endif>
                                        Eighth Installment
                                    </option>

                                    <option value="9" @if(old('installment_number')=='9') selected @endif>
                                        Ninth Installment
                                    </option>

                                    <option value="10" @if(old('installment_number')=='10') selected @endif>
                                        Tenth Installment
                                    </option>

                                    <option value="9" @if(old('installment_number')=='11') selected @endif>
                                        Eleventh Installment
                                    </option>

                                    <option value="10" @if(old('installment_number')=='12') selected @endif>
                                        Twelveth Installment
                                    </option>

                            </select>



                            </select>
                            @if($errors->has('installment_number'))
                                <span class="help-block" style="color:red;">
                                   * {{ $errors->first('installment_number') }}
                                </span>
                            @endif
                        </div>
                    </div>

                     {{--<div class="col-md-3">--}}
                       {{--<div class="form-group">--}}
                           {{--<label>Advance Amount</label>--}}
                           {{--<input type="number" name="advance_payment" class="form-control">--}}
                           {{--@if($errors->has('advance_payment'))--}}
                               {{--<span class="help-block" style="color:red;">--}}
                                {{-- * {{ $errors->first('advance_payment') }}--}}
                            {{--</span>--}}
                           {{--@endif--}}
                       {{--</div>--}}
                    {{--</div>--}}

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>English Payment Date:</label>
                            <input type="text" name="installment_eng_date" class="form-control datepicker" value="{{old('installment_eng_date')}}" required="" autocomplete="off">
                            @if($errors->has('installment_eng_date'))
                                <span class="help-block" style="color:red;">
                               * {{ $errors->first('installment_eng_date') }}
                            </span>
                            @endif
                        </div>

                    </div>

                </div>

                <div class="col-md-12">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Nepali Payment Date:</label>
                            <input  name="installment_nep_date" class="bod-picker  form-control"   placeholder="मिति चयन गर्नुहोस"  required="" value="{{old('installment_nep_date')}}">
                            @if($errors->has('installment_nep_date'))
                                <span class="help-block" style="color:red;">
                                * {{ $errors->first('installment_nep_date') }}
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Upload Bill Image:</label>
                            <br>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                </div>
                                <div>
                                    <span class="btn default btn-file">
                                    <span class="fileinput-new">
                                    Select image </span>
                                    <span class="fileinput-exists">
                                    Change </span>
                                    <input type="file" name="bill_image" value="{{old('bill_image')}}">
                                    </span>
                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                        Remove </a>

                                    @if($errors->has('bill_image'))
                                        <span class="help-block" style="color:red;">
                                           * {{ $errors->first('bill_image') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>

                </div>


             </div>
             <div class="row">
                <div class="col-md-3 col-md-offset-4">
                    <button type="submit" class="btn green-haze">
                        <i class="fa fa-check"></i> Submit
                    </button>
                </div>

             </div>

            </form>

        </div>
        @else
            <p>Your Installment is complete</p>

        @endif


    </div>

</div>

@endsection('content')
@section('scripts')
<script>

$(document).ready(function () {
            $("#date1").on('click',function(){
                $('#date1').remove();
            });

            $(".datepicker").datepicker({
                dateFormat:"yy-mm-dd",
                changeMonth:true,
                changeYear:true
            });

            $(".bod-picker").nepaliDatePicker({
                dateFormat: "%D, %M %d, %y",
                closeOnDateSelect:true
                //minDate : "सोम, जेठ १०, २०७३",
                //maxDate : "मंगल, जेठ ३२, २०७३"
            });
        });


</script>


<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>



@endsection('scripts')
