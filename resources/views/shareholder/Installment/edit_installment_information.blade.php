
@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    //custom english calander css
    <link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>
@endsection('css')

@section('page-title')
    Shareholder Form Information
@endsection('page-title')

@section('content')
<div style="margin-bottom:40px;">
    <a href="{{route('shareholder.edit',$form->id)}}" class="btn default  button-previous" style="display: inline-block;">
        <i class="m-icon-swapleft"></i> Back </a>
</div>
   
  <div class=" portlet light bordered" >
    <h3>Shareholder Form Unique Id: <strong>{{$form->unique_id}}</strong></h3>

 <form action="{{route('shareholder.edit_installment_form',['id'=>$installment->share_form_id])}}" method="post">
  @csrf
   <div class="row">
     <div class="col-md-12">
        <h3 class="text-center"><strong>Share Information</strong></h3>
        
       @if($installment->committed_amount !='')
            <h3 style="margin-left:15px;">Committed Amount: <strong><input type="number" name="committed_amount" value="{{$installment->committed_amount}}"></strong></h3>
       @endif
  
     </div>
   </div>

  <div class="row">
     <div class="col-md-12">
          <div class='col-md-3'>
             <h3>Edit Installment</h3>

              <select  class="form-control" id="edit_installment" name="no_of_installment">
                <option value="{{$installment->no_of_installment}}" hidden="" selected="">{{$installment->no_of_installment}}</option>
                @for($i= 1; $i<=12;$i++)
                    <option value="{{$i}}">{{$i}}</option>
                @endfor
            </select>

          </div>
     </div>
   </div>

    <div class="row">
     
        <div class="form-group">
            <div id="edit_installment_1">
                <div class="col-md-3">
                    <div class="col-md-12">
                        <h3 >First Installment Committed</h3>

                        <div class="form-group">
                           <div class="row">
                             <label class="col-md-4 control-label">Amount:</label>
                             <div class="col-md-8">
                                  <input type="number" name="first_installment_amt" value="{{$installment->first_installment_amt}}" class="form-control"  >
                             </div>
                           </div>

                        <br>
                        <div class="row">
                             <label class="col-md-4 control-label">Pay Date:</label>
                          <div class="col-md-8">
                               <input type="text" name="first_expiry_date" value="{{$installment->first_expiry_date }}" class="form-control datepicker"   >

                          </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>


          <div id="edit_installment_2"> 
              <div class="col-md-3">
                <div class="col-md-12">
                    <h3 >Second Installment Committed</h3>

                    <div class="form-group">
                       <div class="row">
                            <label class="col-md-4 control-label">Amount:</label>
                            <div class="col-md-8">
                                <input type="number" name="second_installment_amt" value="{{$installment->second_installment_amt}}" class="form-control"  >
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <label class="col-md-4 control-label">Pay Date:</label>
                            <div class="col-md-8">
                                <input type="text" name="second_expiry_date" value="{{$installment->second_expiry_date }}" class="form-control datepicker"   >
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </div>


           <div id="edit_installment_3">
           
                       <div class="col-md-3">
              <div class="col-md-12">
                <h3 >Third Installment Committed</h3>
                <div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label">Amount:</label>
                      <div class="col-md-8">
                        <input type="number" name="third_installment_amt" value="{{$installment->third_installment_amt}}" class="form-control"  >
                      </div>
                    </div>
                  <br>
                 <div class="row">
                      <label class="col-md-4 control-label">Pay Date:</label>
                   <div class="col-md-8">
                        <input type="text" name="third_expiry_date"  value="{{$installment->third_expiry_date }}" class="form-control datepicker"  >
             
                   </div>
                 </div>
              </div>
               </div>   
             </div>
             
           </div>

           <div id="edit_installment_4">
            
                       <div class="col-md-3">
              <div class="col-md-12">
                <h3 >Fourth Installment Committed</h3>
                <div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label">Amount:</label>
                      <div class="col-md-8">
                        <input  type="number" name="fourth_installment_amt"  value="{{$installment->fourth_installment_amt}}" class="form-control" >
                      </div>
                    </div>
                  <br>
                 <div class="row">
                      <label class="col-md-4 control-label">Pay Date:</label>
                   <div class="col-md-8">
                        <input type="text" name="fourth_expiry_date" value="{{$installment->fourth_expiry_date }}" class="form-control datepicker"  >
             
                   </div>
                 </div>
              </div>
               </div>   
             </div>
             
           </div>

           <div id="edit_installment_5">
          
                       <div class="col-md-3">
              <div class="col-md-12">
                <h3 >Fifth Installment Committed</h3>
                <div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label">Amount:</label>
                      <div class="col-md-8">
                        <input type="number" name="fifth_installment_amt" value="{{$installment->fifth_installment_amt}}" class="form-control"  >
                      </div>
                    </div>
                  <br>
                 <div class="row">
                      <label class="col-md-4 control-label">Pay Date:</label>
                   <div class="col-md-8">
                        <input type="text" name="fifth_expiry_date" value="{{$installment->fifth_expiry_date }}" class="form-control datepicker" >
             
                   </div>
                 </div>
              </div>
               </div>   
             </div>
             
           </div>


           <div id="edit_installment_6">
           
                       <div class="col-md-3">
              <div class="col-md-12">
                <h3 >Sixth Installment Committed</h3>
                <div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label">Amount:</label>
                      <div class="col-md-8">
                        <input type="number" name="sixth_installment_amt" value="{{$installment->sixth_installment_amt}}" class="form-control"  >
                      </div>
                    </div>
                  <br>
                 <div class="row">
                      <label class="col-md-4 control-label">Pay Date:</label>
                   <div class="col-md-8">
                        <input type="text" name="sixth_expiry_date" value="{{$installment->sixth_expiry_date }}" class="form-control datepicker" >
             
                   </div>
                 </div>
              </div>
               </div>   
             </div>
            
           </div>

           <div id="edit_installment_7">
          
                       <div class="col-md-3">
              <div class="col-md-12">
                <h3 >Seventh Installment Committed</h3>
                <div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label">Amount:</label>
                      <div class="col-md-8">
                        <input  type="number" name="seventh_installment_amt" value="{{$installment->seventh_installment_amt}}" class="form-control"  >
                      </div>
                    </div>
                  <br>
                 <div class="row">
                      <label class="col-md-4 control-label">Pay Date:</label>
                   <div class="col-md-8">
                        <input type="text" name="seventh_expiry_date" value="{{$installment->seventh_expiry_date }}" class="form-control datepicker"  >
             
                   </div>
                 </div>
              </div>
               </div>   
             </div>
            
           </div>

           <div id="edit_installment_8">
           
                       <div class="col-md-3">
              <div class="col-md-12">
                <h3 >Eight Installment Committed</h3>
                <div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label">Amount:</label>
                      <div class="col-md-8">
                        <input type="number" name="eighth_installment_amt"  value="{{$installment->eighth_installment_amt}}" class="form-control"  >
                      </div>
                    </div>
                  <br>
                 <div class="row">
                      <label class="col-md-4 control-label">Pay Date:</label>
                   <div class="col-md-8">
                        <input type="text" name="eighth_expiry_date"  value="{{$installment->eighth_expiry_date }}" class="form-control datepicker"  >
             
                   </div>
                 </div>
              </div>
               </div>   
             </div>
             
           </div>

           <div id="edit_installment_9">
            
                       <div class="col-md-3">
              <div class="col-md-12">
                <h3 >Ninth Installment Committed</h3>
                <div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label">Amount:</label>
                      <div class="col-md-8">
                        <input type="number" name="ninth_installment_amt" value="{{$installment->ninth_installment_amt}}" class="form-control"  >
                      </div>
                    </div>
                  <br>
                 <div class="row">
                      <label class="col-md-4 control-label">Pay Date:</label>
                   <div class="col-md-8">
                        <input type="text" name="ninth_expiry_date" value="{{$installment->ninth_expiry_date }}" class="form-control datepicker"  >
             
                   </div>
                 </div>
              </div>
               </div>   
             </div>
            
           </div>

           <div id="edit_installment_10">
           
                       <div class="col-md-3">
              <div class="col-md-12">
                <h3 >Tenth Installment Committed</h3>
                <div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label">Amount:</label>
                      <div class="col-md-8">
                        <input type="number" name="tenth_installment_amt" value="{{$installment->tenth_installment_amt}}" class="form-control"  >
                      </div>
                    </div>
                  <br>
                 <div class="row">
                      <label class="col-md-4 control-label">Pay Date:</label>
                   <div class="col-md-8">
                        <input type="text" name="tenth_expiry_date"  value="{{$installment->tenth_expiry_date }}" class="form-control datepicker"  >
             
                   </div>
                 </div>
              </div>
               </div>   
             </div>
             
           </div>

           <div id="edit_installment_11">
            
                       <div class="col-md-3">
              <div class="col-md-12">
                <h3 >Eleventh Installment Committed</h3>
                <div class="form-group">
                    <div class="row">
                      <label class="col-md-4 control-label">Amount:</label>
                      <div class="col-md-8">
                        <input type="number" name="eleventh_installment_amt" value="{{$installment->eleventh_installment_amt}}" class="form-control"  >
                      </div>
                    </div>
                  <br>
                 <div class="row">
                      <label class="col-md-4 control-label">Pay Date:</label>
                   <div class="col-md-8">
                        <input type="text" name="eleventh_expiry_date" value="{{$installment->eleventh_expiry_date }}" class="form-control datepicker"  >
             
                   </div>
                 </div>
              </div>
               </div>   
             </div>
            
           </div>

          <div id="edit_installment_12">
          
                      <div class="col-md-3">
             <div class="col-md-12">
               <h3>Twelveth Installment Committed</h3>
               <div class="form-group">
                   <div class="row">
                     <label class="col-md-4 control-label">Amount:</label>
                     <div class="col-md-8">
                       <input type="number" name="twelveth_installment_amt" value="{{$installment->twelveth_installment_amt}}" class="form-control"  >
                     </div>
                   </div>
                 <br>
                <div class="row">
                     <label class="col-md-4 control-label">Pay Date:</label>
                  <div class="col-md-8">
                       <input type="text" name="twelveth_exiry_date"  value="{{$installment->twelveth_expiry_date}}" class="form-control datepicker"   >
            
                  </div>
                </div>
             </div>
              </div>   
            </div>
            
          </div>
        
        </div>

   </div>
     <button type="submit" class="btn green-haze">
             <i class="fa fa-check"></i> Submit
     </button>
     </form>



   
  </div>
@endsection('content')
@section('scripts')
<script>

$(document).ready(function () {
           
            $(".datepicker").datepicker({
                dateFormat:"yy-mm-dd",
                changeMonth:true,
                changeYear:true
            });

            
        });


</script>
<script src="{{ asset(STATIC_DIR.'shareholder/js/add_installment.js') }}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
@endsection('scripts')

