@extends('layouts.app')

@section('css')
    <link href="{{asset(STATIC_DIR.'shareholder/css/installment_print.css')}}"  media="print" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 600px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
     Payment Details
@endsection

@section('page-title')
    Payment Information
@endsection('page-title')


@section('content')

    <div class="portlet box ">
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form class="form-horizontal " role="form">
                <div class="form-body print_page">
                    <h3 class="form-section">{{ucfirst($payment->installment_name)}} Installment</h3>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Applicant Name:</label>
                                <div class="col-md-8">
                                    <p class="form-control-static">
                                        {{$payment->applicant_name}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Payment Date (Nepali):</label>
                                <div class="col-md-8">
                                    <p class="form-control-static">
                                        {{$payment->installment_nep_date}}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Payment Date(English) :</label>
                                <div class="col-md-8">
                                    <p class="form-control-static">
                                        {{$payment->installment_eng_date}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section">Bank Details</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Bank Name:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        {{$payment->bank_name}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Branch Name:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        {{$payment->branch_name}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Account Number:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        {{$payment->account_number}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section">Amount Details</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Paid Amount:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        Rs. {{$payment->installment_paid_amount}}
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Advance Amount:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        Rs.  {{$payment->advance_payment}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Due Amount:</label>
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                       Rs. {{$payment->less_payment}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>

                    <h3 class="form-section hide-on-print">Uploaded Bank Receipt :</h3>
                    <div class="row hide-on-print">
                        <div class="col-md-6">
                            <a href="{{ asset(STATIC_DIR.'storage/'.$payment->bill_image) }}" target="_blank">
                            <img width="50" height="50"  class="responsive" width="600" height="400" src="{{asset(STATIC_DIR.'storage/'.$payment->bill_image)}} " alt="payments">
                            </a>
                        </div>
                    </div>


                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                                        <i class="fa fa-print"></i>
                                            Print
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
    </div>


@endsection('content')
@section('scripts')



@endsection('scripts')
