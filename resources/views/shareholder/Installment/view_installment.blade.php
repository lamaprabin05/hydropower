@extends('layouts.app')

@section('css')

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Statement
@endsection

@section('page-title')
     Installment Report
@endsection('page-title')


@section('content')

    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div style="margin-bottom:50px;">
          <a href="{{route('shareholder.installment')}}" class="btn btn-sm btn-success pull-left"><i class="m-icon-swapleft m-icon-white"></i> Back</a>

    </div>
    
    <div class="portlet box blue-madison">
        <div class="portlet-title">
            <div class="caption">
                  Installment Information
            </div>

        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                <tr>
                    <th>
                        Shareholder Name
                    </th>
                    <th>
                        Form ID
                    </th>
                    <th>
                        Group Type
                    </th>
{{--                    @if($info->group_type == 'group')--}}
                    <th>
                        Group Name
                    </th>
                    {{--@endif--}}
                    <th>
                        Committed Amount
                    </th>

                    <th>
                        Total Paid Amount
                    </th>
                    <th>
                        Total Remaining
                    </th>
                    <th>
                        Total Installment
                    </th>
                    <th>
                       Remaining Installment
                    </th>



                </tr>
                </thead>
                <tbody>
                        <tr>
                            <td>
                                {{$info->shareholder_name}}
                            </td>
                            <td>
                                {{$info->unique_id}}
                            </td>
                            <td>
                                {{$info->group_type}}
                            </td>

                            {{--@if($info->group_type == 'group')--}}
                                <td>
                                    {{$group_name->name}}
                                </td>
                            {{--@endif--}}
                            <td>
                                Rs {{$info->payment_information->committed_amount}}
                            </td>
                            <td>
                                Rs {{$info->payment_information->total_paid_amount}}
                            </td>
                            <td>
                                Rs {{$info->payment_information->committed_amount - $info->payment_information->total_paid_amount}}
                            </td>

                            <td>
                                {{$info->payment_information->no_of_installment}}
                            </td>

                            <td>
                                {{$info->payment_information->remaining_installment}}
                            </td>

                        </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="portlet box blue-madison">
        <div class="portlet-title">
            <div class="caption">
                All Paid Installment Information
            </div>

        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" >
                <thead>
                <tr>
                    <th>
                        S.N.
                    </th>

                    <th>
                        Installment
                    </th>
                    <th>
                        Payment Date
                    </th>

                    <th>
                        Paid Amount
                    </th>

                    <th>
                        Due Amount
                    </th>

                    <th>
                        Advance Amount
                    </th>


                    <th>Status</th>
                    <th>Action</th>


                </tr>
                </thead>
                <tbody>
                @foreach($payments as $value)
                    <tr>
                        <td>
                            {{$loop->iteration}}
                        </td>

                        <td>
                            {{ucfirst($value->installment_name)}} Installment
                        </td>
                        <td>
                            {{$value->installment_nep_date}}
                        </td>
                        <td>
                            {{$value->installment_paid_amount}}
                        </td>

                        <td>
                            {{$value->less_payment}}
                        </td>

                        <td>
                            {{$value->advance_payment}}
                        </td>

                        <td>
                            paid
                        </td>
                        <td>
                            <a href="{{route('shareholder.getSingleInstallment',$value->id)}}" class="btn btn-sm green">View Details</a>

                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->

@endsection('content')
@section('scripts')

   {{-- <script src="{{asset(STATIC_DIR.'assets/admin/pages/scripts/table-advanced.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {

            Demo.init(); // init demo features
            TableAdvanced.init();
        });
    </script>--}}

@endsection('scripts')
