@extends('layouts.app')
@section('css')

<link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    Requisition Form
@endsection


@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<!-- BEGIN SAMPLE FORM PORTLET-->
                    <div >
                        
                        <div class="portlet-body form"  >
                            <form class="form-horizontal" role="form" action="{{route('store.purchase_form')}}" method="post" enctype="multipart/form-data" >
                                @csrf
                                <div class="form-body" id="product">
                                    <h3 class="text-center">Purchase Requisition Form Details</h3>
                                    <hr>


                                    <div class="form-group">
                                        
                                        <div class="col-md-12">
                                           <!-- BEGIN BORDERED TABLE PORTLET-->
                    <div class="portlet box yellow">
                        <div class="portlet-title center">
                            <div class="caption">
                                Requisition Form
                            </div>
                          
                        </div>

                        <div class="portlet-body">
                                   
                            <div class="table-scrollable">

                                <table class="table table-bordered table-hover" id="purchase_table">

                                <thead>
                                <tr>
                                    
                                    <th> S.N</th>
                                    <th> Product Name</th>
                                    <th> Product Description</th>
                                    <th> Product Qty</th>
                                    <th> Requisition No.</th>
                                    <th> Estimated Cost.</th>
                                    <th class="action_column"> Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr >

                                    <!-- <td >
                                         1
                                    </td>
                                     <td class="text-center">
                                        <div>
                                            <input type="text" name="input_name1" placeholder="Enter Product Name"  class="form-control" autocomplete="off" id="input_name" >
                                        </div>

                                    <div id="select_name">
                                            <select class="form-control input-lg list_asset" name="select_name1" id="select_name" >
                                                <option vlaue="">1</option>
                                                <option vlaue="">2</option>


                                            </select>
                                        </div>
                                        <br>

                                       <input type="button" class="btn btn-sm btn-danger" value="Toggle Selection" id="toggle_product">
                                        <button type="button" class="btn btn-success" id="toggle_product"><i class="fa fa-exchange"></i></button>



                                    </td>
                                     <td>
                                         <textarea class="form-control" name="description1" required=""></textarea>
                                    </td>
                                     <td>
                                         <input type="text" name="requisition_number1"  placeholder="Enter Product requisition_number" required=""  class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <input type="number" name="cost1" placeholder="Enter Product Name" required="" class="form-control" autocomplete="off">
                                     </td>
                                     <td class="action_column">

                                    </td>

                                    <input type="hidden" name="number" value="1"> -->

  
                                </tr>
                                </tbody>


                                </table>

                            </div>
                            <div class="text-center">

                                <button type="button" class="btn btn-sm btn-success " id="add_button2"><i class="fa fa-plus"></i> Choose Product</button>
                            </div>


                            
                        </div>
                        <button type="button" class="btn btn-sm btn-danger pull-right" style="margin-top:10px;" id="add_button1"><i class="fa fa-plus"></i> Manual Product </button>


                    </div>
                    <!-- END BORDERED TABLE PORTLET-->
                                        </div>
                                        
                                            
                                    </div>


                               <div class="form-actions text-center">
                                    <button type="reset" class="btn default" id=cancel>Cancel</button>
                                    <button type="submit" class="btn green " id="submit">Submit</button>
                                </div>     
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->


@endsection('content')
@section('scripts')

<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $('#input_name').hide();
    $('#toggle_product').click(function(e){
    e.preventDefault();
     $('#select_name').hide();
     $('#input_name').show();
     $('#select_name').val('');
     $('#input_name').prop("required",true);
     
    });
    
    $("#toggle_product").on('dblclick', function(e) {
     e.preventDefault();
     $('#input_name').hide();
     $('#select_name').show();
     $('#input_name').val('');
     $('#select_name').prop("required",false);
     
    });
   
</script>
<script>
    i=1;
    $(document).ready(function(){
        $(".action_column").hide();
        $("#add_button1").on("click", function(e){
            e.preventDefault();
            var row ='<tr id="row_show_hide">'+
                '<td>'+i+'</td>'+
                '<td><input type="text" name="input_name'+i+'" placeholder="Enter Product Name" required="" class="form-control" autocomplete="off"></td>'+
                '<td><textarea class="form-control"  name="description'+i+'" required></textarea></td>'+
                '<td><input type="number" name="product_qty'+i+'"  placeholder="Enter Product Quantity" required="" autocomplete="off"  class="form-control" autocomplete="off"</td>'+
                '<td><input type="number" name="requisition_number'+i+'"  placeholder="Enter Product requisition_number" required="" autocomplete="off"  class="form-control" autocomplete="off"</td>'+
                '<td><input type="number" name="cost'+i+'" placeholder="Enter Product Name" required="" class="form-control" autocomplete="off"></td>'+
                '<td><button type="button" class="btn btn-sm btn-danger" id="delete_row" ><i class="fa fa-trash"></i></button></td>'+
                '<input type="hidden" type="text" name="number" value="'+i+'">'
            '</tr>';
            i++;
            $("#purchase_table").append(row);
        });
    });

    $("#purchase_table").on("click", "button", function() {
        $(this).closest("tr").remove();
    });

    $(document).ready(function(){
        $(".action_column").hide();
        $("#add_button2").on("click", function(e){
            e.preventDefault();
            var row ='<tr id="row_show_hide">'+
                '<td>'+i+'</td>'+
                '<td><select class="form-control input-lg list_asset" name="select_name'+i+'"><option value="" disabled selected></option>@foreach($product as $value)<option value="1">{{$value->name}}</option>@endforeach</select></td>'+
                '<td><textarea class="form-control"  name="description'+i+'" required></textarea></td>'+
                '<td><input type="number" name="product_qty'+i+'"  placeholder="Enter Product Quantity" required="" autocomplete="off"  class="form-control" autocomplete="off"</td>'+
                '<td><input type="number" name="requisition_number'+i+'"  placeholder="Enter Product requisition_number" required="" autocomplete="off"  class="form-control" autocomplete="off"</td>'+
                '<td><input type="number" name="cost'+i+'" placeholder="Enter Product Name" required="" class="form-control" autocomplete="off"></td>'+
                '<td><button type="button" class="btn btn-sm btn-danger" id="delete_row" ><i class="fa fa-trash"></i></button></td>'+
                '<input type="hidden" type="text" name="number" value="'+i+'">'
            '</tr>';
            i++;
            $("#purchase_table").append(row);
        });
    });

    $("#purchase_table").on("click", "button", function() {
        $(this).closest("tr").remove();
        i=i-1;
    });

</script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $(".list_asset").select2({
                placeholder: "Select",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });  
</script>
@endsection('scripts')
