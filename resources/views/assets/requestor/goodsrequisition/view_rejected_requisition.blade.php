@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/goods_requisition_print.css') }}">

@endsection('css')

@section('page_title')
    View Rejected Goods Requisition Form
@endsection('page_title')


@section('content')

    <div class="portlet light bordered">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <a href="{{route('assets.requestor.list_rejected_requisition')}}"  class="btn btn-sm btn-default ">
                        <i class="fa fa-arrow-circle-left"></i>
                        Back
                    </a>
                </div>
            </div>
                <div class="col-md-3 col-md-offset-6 text-right">
                    <div class="form-group">
                        <span><label for="">Requisition Number: {{$form->requisition_number}}</label>
                         </span>
                    </div>
                </div>
        </div>

        @if($form->is_rejected == 1)
            <table class="table table-responsive table-bordered  table-striped">
                <thead>
                <tr>
                    <th colspan="2" style="color:red;" >Rejected Reason:</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="2">
                        {{ucfirst($form->rejected_reason)}}
                    </td>
                </tr>

                <tr>
                    <th style="color:red;" >Rejected By: </th>

                </tr>

                <tr>
                    <td>{{$rejected_user->name}}</td>
                </tr>
                </tbody>
            </table>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12 col-md-offset-4">
                    <h3>Vision Energy and Power Pvt. Ltd. </h3> </br>
                    <h4>New Baneshwor, Kathmandu</h4>
                    </br>
                </div>
                <hr>
            </div>
            <br>
        </div>

    <form action="{{ route('assets.requestor.update_requisition') }}" method="POST" >
        @csrf
        <input type="hidden" name="requisition_id" value="{{$form->id}}">
        <input type="hidden" name="action" value="reject_redirect">


        <table id="myTable" class=" table table-bordered table-hover order-list">
            <thead>
            <tr>
                <th>S.No.</th>
                <th>Product Name</th>
                <th>Product Description</th>
                <th>Quantity</th>
                <th>Date of Requirement</th>
                <th>Status</th>
                {{--<th>Action</th>--}}
            </tr>
            </thead>
            <tbody>

            @foreach($form->requisition as $value)
                <input type="hidden" class="product_id" name="goods_requisition_product_id{{ $loop->iteration}}" value="{{$value->id}}">

                <tr>
                        <td class="col-sm-1">
                            <p>{{$loop->iteration}}</p>
                        </td>

                        <td class="col-sm-4 text-center">

                            <input type="text" id="test" value="{{$value->name}}" name="name{{$loop->iteration}}" required readonly  class="form-control"/>

                            {{--<select class="form-control" name="product_id" >
                                <option value="@if(empty($value->name)){{$value->product['id']}}@else
                                {{$value->id}}
                                @endif" selected >@if(empty($value->name)){{$value->product['name']}}@else
                                        {{$value->name}}
                                    @endif</option>
                                @foreach($product_list as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                            </select>--}}
                        </td>

                        <td class="col-sm-3">
                            <input type="text" name="description{{$loop->iteration}}" value="{{$value->description}}"  required  class="form-control"/>
                        </td>

                        <td class="col-sm-2">
                            <input type="number" name="quantity{{ $loop->iteration }}" value="{{$value->quantity}}" required  class="form-control"/>
                        </td>

                        <td class="col-sm-3">
                            <input type="text" name="requirement_date{{ $loop->iteration }}" value="{{$value->requirement_date}}" required class="form-control datepicker"/>
                        </td>

                        <td style="text-align: center;">
                            @if($value->is_rejected == 0)
                                <i  style="font-size: 20px; color: green; " class="fa fa-check-circle"></i>
                            @else
                                <i style="font-size: 20px; color: red;" class="fa fa-times-circle"></i>
                            @endif
                        </td>

                        {{--<td class="col-sm-2">--}}
                            {{--<input type="hidden" class="product_id" name="goods_requisition_product_id" value="{{$value->id}}">--}}
                            {{--<button  type="submit">--}}
                                {{--<i class="fa fa-pencil"></i>--}}
                            {{--</button>--}}

                        {{--</td>--}}


                </tr>
            @endforeach
            </tbody>
            <tfoot>

            </tfoot>
        </table>


            @if($errors->has('purpose'))
                <span class="help-block" style="color:red;">
                      * {{ $errors->first('reason') }}
                </span>
            @endif
            @if($errors->any())
                <span class="help-block" style="color:red;">
                                      * {{ $errors->first() }}
                </span>
            @endif

            <div class="row">
                {{--<div class="col-md-12">--}}

                    <div class="col-md-3">
                        <div class="form-group">
                            <h4>Purpose of such Goods/Service:</h4>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <select name="purpose" id="purpose" class="form-control" required>
                            <option value="Office Use" @if($form->purpose == "Office Use") selected @endif>Office Use</option>
                            <option value="Site Office Use" @if($form->purpose == "Site Office Use") selected @endif>Site Office Use</option>
                            <option value="others" @if($form->purpose != "Office Use" AND $form->purpose != "Site Office Use") selected @endif>Others</option>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <input @if($form->purpose != "Office Use" AND $form->purpose != "Site Office Use") value="{{   old('others') ?? $form->purpose }}" @endif class="form-control" id="others" name="others" style="display: none;">
                    </div>
                    <div class="col-md-3"></div>

                    <div class="col-md-3">
                        @if($errors->has('purpose'))
                            <span class="help-block" style="color:red;">
                              * {{ $errors->first('purpose') }}
                        </span>
                        @endif
                    </div>


                </div>
            {{--end purpose of goods row--}}

            {{--reason row--}}
            <div class="row">

                <div class="col-md-12">
                    <hr>
                    <h4>Reason for Requisition:</h4>
                </div>

                <div class="col-md-8">
                    <div class="form-group">
                        <textarea class="form-control" name="reason"  rows="7" >{{ old('reason') ?? $form->reason }}</textarea>
                    </div>

                </div>

            </div>
            {{-- end reason row--}}

            @if($form->is_rejected == 1)
            <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn green button-next">
                                <i class="fa fa-check"></i>
                                Update
                            </button>
                        </div>
                    </div>
            </div>
            @else
                <div class="alert alert-warning">
                    Goods Requisition File has been proceeded to {{ucfirst($form->proceeded_to)}}
                </div>
            @endif


        </form>
    </div>

@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

    <script>

        $(document).ready(function () {
            let counter = 1;

            $('#getCounter').val(counter);

            //purpose show or shide
            if($('#purpose').val() == 'others'){
                $("#others").show();
            }

            $('#purpose').on('change', function(e){
                let purpose = $('#purpose').val();

                if(purpose == 'others'){
                    $("#others").show();
                }
                else{
                    $("#others").hide();
                }


            });
            //end purpose show or hide

            //date picker
            $(".datepicker").datepicker({
                dateFormat:"yy-mm-dd",
                changeMonth:true,
                changeYear:true
            });
            //end date picker


        });



    </script>
@endsection('scripts')
