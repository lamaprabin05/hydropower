@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/goods_requisition_print.css') }}">

@endsection('css')

@section('page_title')
    View Goods Requisition
@endsection('page_title')


@section('content')
    <div class="portlet light bordered">
        <div class="row hide-on-print">
            <div class="col-md-12">
                <div class="form-group">
                    <a href="{{route('assets.requestor.list_all_requisition')}}"  class="btn btn-sm btn-default ">
                    <i class="fa fa-arrow-circle-left"></i>
                        Back
                    </a>
                </div>
            </div>

            <div class="col-md-12 " >
                <div class="col-md-12 col-md-offset-4">
                    <h3>Vision Energy and Power Pvt. Ltd. </h3> </br>
                    <h4>New Baneshwor, Kathmandu</h4>
                    </br>
                </div>
            </div>
        </div>

        @include('layouts._partials.print-head')

            {{--getting number of row--}}
            <input type="hidden" name="counter" id="getCounter" >
            @if($errors->has('upload_form'))
                <span class="help-block" style="color:red;">
                    * {{ $errors->first('upload_form') }}
                </span>
            @endif
            <table id="myTable" class=" table table-bordered order-list">
                <thead>

                <tr>
                    <th colspan="6" style="text-align: center">Goods or Service Requisition Form</th>
                </tr>

                <tr>
                    <th>S.No.</th>
                    <th>Product Name</th>
                    <th>Product Description</th>
                    <th>Quantity</th>
                    <th>Date of Requirement</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>

                @foreach($form->requisition as $value)


                <tr>
                    <td class="col-sm-1">
                        <p>{{$loop->iteration}}</p>
                    </td>

                    <td class="col-sm-4">
                        @if(empty($value->name))
                        <p>{{$value->product['name']}}</p>
                        @else
                        <p>{{$value->name}}</p>
                        @endif
                    </td>

                    <td class="col-sm-3">
                        <p>{{$value->description}}</p>
                    </td>

                    <td class="col-sm-2">
                        <p>{{$value->quantity}}</p>
                    </td>

                    <td class="col-sm-3">
                        <p>{{$value->requirement_date}}</p>
                    </td>

                    <td>
                        @if($value->is_rejected == 0)
                            <i  style="font-size: 20px; color: green; " class="fa fa-check-circle"></i>
                        @else
                            <i style="font-size: 20px; color: red;" class="fa fa-times-circle"></i>
                        @endif
                    </td>
                </tr>
               
                @endforeach
                <tr>
                    <td colspan="2" ><p>Purpose of such Goods/Service:</p></td>
                    <th colspan="4"><p> {{ucfirst($form->purpose)}} </p></th>
                </tr>
                @if($form->reason)
                    <tr>
                        <td colspan="2"><p> Reason for Requisition: </p></td>
                        <td colspan="4" ><p> {{ucfirst($form->reason)}} </p></td>
                    </tr>
                @endif

                </tbody>
                <tfoot>

                </tfoot>
            </table>

        <div class="alert alert-warning">
            Goods Requisition File has been proceeded to {{ucfirst($form->proceeded_to)}}
        </div>

        {{--show only if checker edit this form--}}
    @if($form->log_goods_requisition->count() != 0)
        <table id="myTable" class=" table table-bordered order-list">
            <thead>

            <tr>
                <th colspan="6" style="color:red;">Log History of Goods Requisition Form</th>
            </tr>

            <tr>
                <th colspan="6" style="text-align: center">Goods or Service Requisition Form</th>
            </tr>

            <tr>
                <th>S.No.</th>
                <th>Product Name</th>
                <th>Product Description</th>
                <th>Quantity</th>
                <th>Date of Requirement</th>
                <th>Updated By</th>
            </tr>
            </thead>
            <tbody>

            @foreach($log as $value)

                <tr>
                    <td class="col-sm-1">
                        <p>{{$loop->iteration}}</p>
                    </td>

                    <td class="col-sm-3">
                        @if(empty($value->name))
                            <p>{{$value->product['name']}}</p>
                        @else
                            <p>{{$value->name}}</p>
                        @endif
                    </td>

                    <td class="col-sm-3">
                        <p>{{$value->description}}</p>
                    </td>

                    <td class="col-sm-2">
                        <p>{{$value->quantity}}</p>
                    </td>

                    <td class="col-sm-2">
                        <p>{{$value->requirement_date}}</p>
                    </td>

                    <td class="col-sm-3">
                        {{$user[$value->updated_by]}}
                    </td>


                </tr>

            @endforeach

            </tbody>
            <tfoot>

            </tfoot>
        </table>
    @endif

    </div>

@include('assets.requestor.goodsrequisition.modal')

@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>


@endsection('scripts')
