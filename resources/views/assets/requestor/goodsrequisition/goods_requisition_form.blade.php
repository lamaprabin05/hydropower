@extends('layouts.app')
@section('css')
<link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
<!-- <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/> -->
<link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>
@endsection('css')

@section('page_title')
    Requisition Form
@endsection


@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<!-- BEGIN SAMPLE FORM PORTLET-->
                    <div>
                        
                        <div class="portlet-body form"  >
                            <form class="form-horizontal" role="form" 
                            action="{{route('assets.requestor.store_goods_requisition') }}" method="post"  >
                                @csrf
                                <div class="form-body" id="product">
                                    <h3 class="text-center">Goods Requisition Form Details</h3>
                                    <hr>


                                    <div class="form-group">
                                        
                                        <div class="col-md-12">
                                           <!-- BEGIN BORDERED TABLE PORTLET-->
                    <div class="portlet box yellow">
                        <div class="portlet-title center">
                            <div class="caption">
                               Goods Requisition Form
                            </div>
                          
                        </div>

                        <div class="portlet-body">
                                   
                            <div class="table-scrollable">

                                <table class="table table-bordered table-hover" id="purchase_table">

                                <thead>
                                <tr>
                                    
                                    <th> S.N</th>
                                    <th> Product Name</th>
                                    <th> Product Description</th>
                                    <th> Quantity</th>
                                    <th> Date Of Requirement</th>
                                    <th class="action_column">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr >
                                   
                                
  
                                </tr>
                                </tbody>


                                </table>

                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-sm btn-success " id="add_button2"><i class="fa fa-plus"></i> Select Product</button>
                            </div>

                            
                        </div>

                    </div>
                                            <button type="button" class="btn btn-sm btn-danger pull-right" style="margin-top:-20px;" id="add_button1"><i class="fa fa-plus"></i> Manual Product</button>
                    <div class="row">
                        <div class="col-md-4 ">Purpose Of Such Goods/Service:</div>
                        <div class="col-md-4 ">
                            <select class="form-control" required="" name="purpose">
                                <option value="" selected="" disabled="">Select Purpose</option>
                                <option value="Office Use">Office Use</option>
                                <option value="Site Office Use">Site Office Use</option>
                                <option value="others">Others</option>
                            </select>
                        </div>
                        <div class="col-md-4"></div>
                    </div>

                   <div class="row">

                    <div class="col-md-12">
                         <h4>Reason for Requisition:</h4>
                        <div class="form-group">
                            <textarea class="form-control" style="background-color:rgba(0,0,0,0.25);width:60%;cursor:auto" name="reason" rows="3" ></textarea>
                        </div>

                        @if($errors->has('reason'))
                            <span class="help-block" style="color:red;">
                                      * {{ $errors->first('reason') }}
                                  </span>
                        @endif
                    </div>

                </div>

            </div>
                    <!-- END BORDERED TABLE PORTLET-->
                                        </div>
                                        
                                            
                                    </div>


                               <div class="form-actions text-center">
                                    <button type="reset" class="btn default" id=cancel>Cancel</button>
                                    <button type="submit" class="btn green " id="submit">Submit</button>
                                </div>     
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->


@endsection('content')
@section('scripts')

<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $('#input_name').hide();
    $('#toggle_product').click(function(e){
    e.preventDefault();
     $('#select_name').hide();
     $('#input_name').show();
     $('#select_name').val('');
     $('#input_name').prop("required",true);
     
    });
    
    $("#toggle_product").on('dblclick', function(e) {
     e.preventDefault();
     $('#input_name').hide();
     $('#select_name').show();
     $('#input_name').val('');
     $('#select_name').prop("required",false);
     
    }); 
</script>
<script type="text/javascript">
            $('body').on('focus',".date_picker", function(){
                $(this).datepicker({
                    dateFormat: "yy-mm-dd",
                    changeMonth: true,
                    changeYear: true
                });
            });
</script>
<script type="text/javascript">
    i=1;
$(document).ready(function(){
        $(".action_column").hide();
       $("#add_button1").on("click", function(e){
        e.preventDefault();
         var row ='<tr id="row_show_hide">'+
         '<td>'+i+'</td>'+
         '<td><input type="text" name="name'+i+'" placeholder="Enter Product Name" required="" class="form-control" autocomplete="off"></td>'+
         '<td><textarea class="form-control"  name="description'+i+'" required placeholder="Enter Product Description"></textarea></td>'+
         '<td><input type="number" min=0 name="quantity'+i+'"  placeholder="Enter Product Quantity" required="" autocomplete="off"  class="form-control" autocomplete="off"</td>'+
         '<td><input type="text"  name="requirement_date'+i+'" required class="form-control date_picker" autocomplete="off" /></td>'+
         '<td><button type="button" class="btn btn-sm btn-danger" id="delete_row" ><i class="fa fa-trash"></i></button></td>'+
          '<input type="hidden" type="text" name="number" value="'+i+'">'
          '</tr>';
          i++;
         $("#purchase_table").append(row);
         });
     });

$("#purchase_table").on("click", "button", function() {
   $(this).closest("tr").remove(); 
});

$(document).ready(function(){
        $(".action_column").hide();
       $("#add_button2").on("click", function(e){
        e.preventDefault();
         var row ='<tr id="row_show_hide">'+
         '<td>'+i+'</td>'+
         '<td><select type="number" class="form-control input-lg list_product" name="name'+i+'" ><option value="" disabled selected ></option>@foreach($product as $value)<option value="{{$value->id}}">{{$value->name}}</option>@endforeach</select></td>'+
         '<td><textarea class="form-control"  name="description'+i+'" required placeholder="Enter Product Description"></textarea></td>'+
         '<td><input type="number" min=0 name="quantity'+i+'"  placeholder="Enter Product Quantity" required="" autocomplete="off"  class="form-control" autocomplete="off"</td>'+
         '<td><input type="text"  name="requirement_date'+i+'" required class="form-control date_picker" autocomplete="off" /></td>'+
         '<td><button type="button" class="btn btn-sm btn-danger" id="delete_row" ><i class="fa fa-trash"></i></button></td>'+
          '<input type="hidden" type="text" name="number" value="'+i+'">'
          '</tr>';
          i++;
         $("#purchase_table").append(row);
         });
     });

$("#purchase_table").on("click", "button", function() {
   $(this).closest("tr").remove();
   i=i-1; 
});
</script>
<script type="text/javascript">

    $(".list_product").select2({
        placeholder: "Select",
        allowClear: true,
        escapeMarkup: function (m) {
            return m;
        }
    });

</script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
@endsection('scripts')
