@extends('layouts.app')

@section('css')
@endsection('css')

@section('page_title')
    Edit Goods Requisition Form
@endsection('page-title')


@section('content')
    <div class="portlet light bordered">

        <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <a href="{{route('assets.requestor.list_all_requisition')}}"  class="btn btn-sm btn-default ">
                            <i class="fa fa-arrow-circle-left"></i>
                            Back
                        </a>
                    </div>
                </div>

                <div class="col-md-3 col-md-offset-6 text-right">
                    <div class="form-group">
                        <span><label for="">Requisition Number: {{$form->requisition_number}}</label>
                         </span>
                    </div>
                </div>

            <div class="col-md-12">
                <div class="col-md-12 col-md-offset-4">
                    <h3>Vision Energy and Power Pvt. Ltd. </h3> </br>
                    <h4>New Baneshwor, Kathmandu</h4>
                    </br>
                </div>
                <hr>
            </div>
            <br>
        </div>

        <form action="{{ route('assets.requestor.update_requisition') }}" method="POST" >
            <input type="hidden" name="requisition_id" value="{{$form->id}}">
            <input type="hidden" name="action" value="edit_requisition">
            @if($errors->any())
                <span class="help-block" style="color:red;">
                    * {{ $errors->first() }}
                </span>
            @endif
            <table id="myTable" class=" table table-bordered order-list">
                <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Product Name</th>
                    <th>Product Description</th>
                    <th>Quantity</th>
                    <th>Date of Requirement</th>
                    {{--<td>Action</td>--}}
                </tr>
                </thead>
                <tbody>

                @foreach($form->requisition as $value)
                    <tr>
                        @csrf
                        <input type="hidden" class="product_id" name="goods_requisition_product_id{{ $loop->iteration}}" value="{{$value->id}}">
                        <td class="col-sm-1">
                            <input type="text" value="{{$loop->iteration}}" class="form-control" readonly >
                        </td>

                        <td class="col-sm-4">
                            <input type="text" value="{{$value->name}}" class="form-control" name="name{{$loop->iteration}}" readonly>

                            {{--<select class="form-control" name="product_id{{ $loop->iteration }}" readonly="">--}}
                                {{--@if(empty($value->product_id))--}}
                                    {{--<option value="0">{{$value->name}}</option>--}}
                                {{--@endif--}}

                                {{--@foreach($product_list as $val)--}}
                                    {{--<option value="{{$val->id}}" @if($value->product['id'] == $val->id)  selected @endif>{{$val->name}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}

                        </td>

                        <td class="col-sm-3">
                            <input type="text" name="description{{$loop->iteration}}" value="{{$value->description}}"  required  class="form-control"/>
                        </td>

                        <td class="col-sm-2">
                            <input type="number" name="quantity{{ $loop->iteration }}" value="{{$value->quantity}}" required  class="form-control"/>
                        </td>

                        <td class="col-sm-3">
                            <input type="text" name="requirement_date{{ $loop->iteration }}" value="{{$value->requirement_date}}" required class="form-control datepicker"/>
                        </td>

                    </tr>
                @endforeach
                </tbody>
                <tfoot>

                </tfoot>
            </table>


            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <h4>Purpose of such Goods/Service:</h4>
                    </div>
                </div>

                <div class="col-md-3">
                    <select name="purpose" id="purpose" class="form-control" required>
                        <option value="Office Use" @if($form->purpose == "Office Use") selected @endif>Office Use</option>
                        <option value="Site Office Use" @if($form->purpose == "Site Office Use") selected @endif>Site Office Use</option>
                        <option value="others" @if($form->purpose != "Office Use" AND $form->purpose != "Site Office Use") selected @endif>Others</option>
                    </select>
                </div>

                <div class="col-md-3">
                    <input @if($form->purpose != "Office Use" AND $form->purpose != "Site Office Use") value="{{   old('others') ?? $form->purpose }}" @endif class="form-control" id="others" name="others" style="display: none;">
                </div>
                <div class="col-md-3"></div>

                <div class="col-md-3">
                    @if($errors->has('purpose'))
                        <span class="help-block" style="color:red;">
                              * {{ $errors->first('purpose') }}
                        </span>
                    @endif
                </div>

            </div>
            {{--end purpose of goods row--}}

            {{--reason row--}}
            <div class="row">
                <div class="col-md-12">
                    <hr>
                    <h4>Reason for Requisition:</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <textarea class="form-control" name="reason"  rows="7" >{{ old('reason') ?? $form->reason }}</textarea>
                    </div>

                </div>
            </div>
            {{-- end reason row--}}

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <button type="submit" class="btn green button-next">
                            <i class="fa fa-check"></i>
                            Update
                        </button>
                    </div>
                </div>
            </div>

        </form>

    </div>

{{--@include('assets.requestor.goodsrequisition.modal')--}}
@endsection('content')
@section('scripts')

    <script>

        $(document).ready(function () {
            let counter = 1;

            $('#getCounter').val(counter);

            //purpose show or shide
            if($('#purpose').val() == 'others'){
                $("#others").show();
            }

            $('#purpose').on('change', function(e){
                let purpose = $('#purpose').val();

                if(purpose == 'others'){
                    $("#others").show();
                }
                else{
                    $("#others").hide();
                }


            });
            //end purpose show or hide

            //date picker
            $(".datepicker").datepicker({
                dateFormat:"yy-mm-dd",
                changeMonth:true,
                changeYear:true
            });
            //end date picker


        });



    </script>

    {{--<script>--}}
        {{--$('#select').hide();--}}
        {{--$('#button_option').on('click', function(e){--}}
            {{--$('#select').show();--}}
        {{--});--}}
        {{--$('#button_option').on('dblclick', function(e){--}}
            {{--$('#select').hide();--}}
        {{--});--}}

    {{--</script>--}}
@endsection('scripts')
