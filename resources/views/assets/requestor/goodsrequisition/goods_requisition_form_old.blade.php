@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>

    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    Edit Share Form
@endsection('page-title')

@section('page_title')
    Shareholder Form Information
@endsection('page-title')

@section('content')
    <div class="portlet light bordered">

        <div class="row">
            <div class="col-md-12">
                <hr>
                <div class="col-md-12 col-md-offset-4">
                    <h3>Vision Energy and Power Pvt. Ltd. </h3> </br>
                    <h4>New Baneshwor, Kathmandu</h4>
                    </br>
                </div>
                <hr>
            </div>
            <br>
        </div>

        <form action="{{ route('assets.requestor.store_goods_requisition') }}" method="post">
            @csrf

            {{--getting number of row--}}
            <input type="hidden" name="counter" id="getCounter" >

            @if($errors->has('error_empty'))
                <span class="help-block" style="color:red;">
                      * {{$errors->first('error_empty')}}
                </span>

            @elseif($errors->any())
                <span class="help-block" style="color:red;">
                      * All the field are compulsory. Please Fill it.
                </span>
            @endif
            <table id="myTable" class="table table-bordered order-list">
                <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Product Name</th>
                    <th>Product Description</th>
                    <th>Quantity</th>
                    <th>Date of Requirement</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" style="text-align: left;">
                            <input type="button" class="btn btn-lg btn-block " id="addselectrow" value="Add Listed Product " />
                        </td>

                        <td colspan="2">
                            <input type="button" class="btn btn-lg btn-block " id="addmanualrow" value="Add Manual Product" />

                        </td>
                    </tr>
                <tr>
                </tr>
                </tfoot>
            </table>

            <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <h4>Purpose of such Goods/Service:</h4>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <select name="purpose" id="purpose" class="form-control" required>
                            <option value="Office Use" @if(old('purpose ') == 'Office Use') selected @endif>Office Use</option>
                            <option value="Site Office Use" @if(old('purpose ') == 'Site Office Use') selected @endif>Site Office Use</option>
                            <option value="others" @if(old('purpose ') == 'others') selected @endif>Others</option>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <input value="{{   old('others') ??  ''}}" class="form-control" id="others" name="others" style="display: none;">
                    </div>

                    <div class="col-md-3"></div>

                        <div class="col-md-3">
                            @if($errors->has('purpose'))
                                <span class="help-block" style="color:red;">
                                      * {{ $errors->first('purpose') }}
                                  </span>
                            @endif
                        </div>

            </div>

            
            <div class="row">
                    <div class="col-md-12">
                        <hr>
                        <h4>Reason for Requisition:</h4>
                    </div>


                    <div class="col-md-8">
                        <div class="form-group">
                            <textarea class="form-control" name="reason"  rows="7">{{old('reason')}}</textarea>
                        </div>

                        @if($errors->has('reason'))
                            <span class="help-block" style="color:red;">
                                      * {{ $errors->first('reason') }}
                                  </span>
                        @endif
                    </div>

            </div>

            <br>
            <div class="row">
                <div class="col-md-3">
                    <label for="">Proceeded To Checker:</label>
            
                </div>
                <div class="col-md-3">
                    <select class="form-control input-md list_asset" name="proceeded_user_id"  required="" >
                        <option  value="" ></option>
                        @foreach($checker_list as $element)
                                <option value="{{$element->id}}">
                                   {{$element->name}}
                                </option>
                                @endforeach

                    </select>
                </div>
            </div>
            <br>
            <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn green button-next">
                                <i class="fa fa-check"></i>
                                Submit
                            </button>
                        </div>
                    </div>
            </div>

        </form>
    </div>

@endsection
@section('scripts')
    <script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $(".datepicker").datepicker({
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true
            });

            $(".list_asset").select2({
                placeholder: "Select",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });

            $('body').on('focus',".date_picker", function(){
                $(this).datepicker({
                    dateFormat: "yy-mm-dd",
                    changeMonth: true,
                    changeYear: true
                });
            });

            //datepicker works
            let counter = 1;

            $('#getCounter').val(counter);


            $('#purpose').on('change', function(e){
                let purpose = $('#purpose').val();
                if(purpose == 'others'){
                    $("#others").show();
                }
                else{
                    $("#others").hide();
                }


            });



            $("#addselectrow").on("click", function () {

                // $('#hello').find('.datepicker').attr("ID","newInput"+counter);

                var newRow = $("<tr>");
                var cols = "";

                cols += '<td><input type="number" class="form-control" required name="id' + counter + '"/></td>';

                cols += '<td class="col-sm-4"> ' +
                                '<select id="test'+counter+'" class="form-control input-lg " name="asset_name_id'+counter +'" > ' +
                                    '<option value=""></option> @foreach($product as $value) <option value="{{$value->asset_name->id}}">{{$value->asset_name->asset_name}}</option> @endforeach</select>'+
                        '</td>';

                cols += '<td><input type="text" class="form-control" required name="description' + counter + '"/></td>';

                cols += '<td><input type="number" class="form-control" required name="quantity' + counter + '"/></td>';

                cols += '<td><input type="text"  name="requirement_date' + counter + '" required class="form-control date_picker" /></td>';

                cols += '<td><i class="fa fa-trash ibtnDel btn btn-md btn-danger"></i></td>';
                newRow.append(cols);
                $("table.order-list").append(newRow);

                getSelectInitialize('test'+counter);

                counter++;

                $('#hello').find('.datepicker').attr("ID","newInput"+counter);

                $('#getCounter').val(counter);


            });

            //add manual row
            $("#addmanualrow").on("click", function () {

                // $('#hello').find('.datepicker').attr("ID","newInput"+counter);

                var newRow = $("<tr>");
                var cols = "";

                cols += '<td><input type="number" class="form-control" required name="id' + counter + '"/></td>';


                cols += '<td class="col-sm-4"  id="product_input'+ counter +'"> ' +
                                '<input type="text" class="form-control" id="name'+ counter + '" required name="name' + counter + '"/> ' +
                        '</td>';

                // cols += '<td><input type="text" class="form-control" required name="name' + counter + '"/></td>';
                cols += '<td><input type="text" class="form-control" required name="description' + counter + '"/></td>';

                cols += '<td><input type="number" class="form-control" required name="quantity' + counter + '"/></td>';

                cols += '<td><input type="text"  name="requirement_date' + counter + '" required class="form-control date_picker" /></td>';

                cols += '<td><i class="fa fa-trash ibtnDel btn btn-md btn-danger"></i></td>';

                newRow.append(cols);
                $("table.order-list").append(newRow);

                getSelectInitialize('test'+counter);

                counter++;

                $('#hello').find('.datepicker').attr("ID","newInput"+counter);

                $('#getCounter').val(counter);


            });

            function getSelectInitialize(id) {
                $("#"+id).select2({
                    placeholder: "Select Any Product",
                    allowClear: true


                });
            }

            $("table.order-list").on("click", ".ibtnDel", function (event) {
                $(this).closest("tr").remove();
                counter -= 1
                $('#test').val(counter);

            });




        });

    </script>
@endsection
