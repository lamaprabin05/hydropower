
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection
{{--@section('page-title')--}}
    {{--Goods Requisition Files--}}
{{--@endsection('page-title')--}}

@section('content')

    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                {{--<table class="table table-striped table-bordered table-advance table-hover" id="sample_1">--}}
                <table class="table table-striped table-bordered table-advance table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>

                        <th>
                            Requisition Number
                        </th>

                        <th>
                            Purpose
                        </th>

                        <th>
                            Proceeded To:
                        </th>

                        <th>
                            Date of Issue
                        </th>

                        <th>
                            Action
                        </th>

                    </tr>
                    </thead>
                    <tbody >
                        @foreach($form as $value)
                            <tr>

                                <td>
                                    {{$loop->iteration}}
                                </td>

                                <td>
                                    {{$value->requisition_number}}
                                </td>

                                <td>
                                    {{$value->purpose}}
                                </td>

                                <td>
                                    {{$value->proceeded_to}}
                                </td>

                                <td>
                                    {{\Carbon\Carbon::parse($value->created_at)->diffForHumans()}}
                                </td>


                                <td>

                                    <a class="btn default btn-xs green-stripe" href="{{ route('assets.requestor.view_all_requisition',['id' => $value->id]) }}">
                                        <i class="fa fa-eye"></i> View
                                    </a>

                                    @can('edit_goods_requisition')
                                    <a class="btn default btn-xs blue-stripe" href="{{ route('assets.requestor.edit_all_requisition',['id' => $value->id]) }}">
                                        <i class="fa fa-pencil"></i> Edit
                                    </a>
                                    @endcan

                                    @can('delete_goods_requisition')
                                    <a  href="#deleteRequisition" class="btn default btn-xs red-stripe" data-ids="{{ $value->id }}" data-toggle="modal" data-rel="delete" data-user="{{$value->requisition_number}}" >
                                        <i class="fa fa-trash"></i> Delete
                                    </a>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach



                    </tbody>
                </table>
            </div>
        </div>
    </div>
@include('assets.requestor.goodsrequisition.modal')
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });


        $('#deleteRequisition').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var ids = button.data('ids');
            var user = button.data('user');

            $("#hidden_id").val(ids);
            $(".hidden_title").html(' "' + user + '" ');
        });

        $('#deleteRequisition').find('#confirm_yes').on('click', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //fetching id
            var id = $("#hidden_id").val();

            $.ajax({
                type: "POST",
                url: "{{ route('assets.requestor.delete_requisition') }}",
                headers: {
                    'XCSRFToken': $('meta[name="csrf-token"]').attr('content')
                },

                data: "id=" + id,
                success: function (msg) {
                    if(msg.error == false){
                        $("#deleteRequisition").modal("hide");
                        $('#showMessage').find('.success').show();
                        $("#showMessage").modal("show");
                    }
                    else
                    {
                        $("#deleteRequisition").modal("hide");
                        $('#showMessage').find('.error').show();
                        $("#showMessage").modal("show");
                    }
                    // window.location.reload();
                }
            });

        });

        $('#show_message').on('click', function () {
            window.location.reload();
        });
    </script>


@endsection('scripts')
