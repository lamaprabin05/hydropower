<div id="deleteRequisition" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content"  style="margin-top: 200px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently
                </h4>
            </div>

            <div class="modal-body"> Do you want to delete <span class='hidden_title'></span>?</div>
            <input type="hidden" id="hidden_id">

            <div class="modal-footer">
                <button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                    No
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<div id="showMessage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="margin-top: 200px;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Requisition File Deleted
                </h4>
            </div>

            <div class="modal-body">
                <div class="error" style="display: none;">
                    <i class="fa fa-close"></i> &nbsp; Error While Deleting &nbsp;Purchase File .
                </div>

                <div class="success" style="display: none;">
                    <i class="fa fa-check"></i> &nbsp; Purchase File Deleted Successfully .
                </div>

            </div>
            <input type="hidden" id="hidden_id">
            <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-danger green confirm_yes" id="show_message"><i class="icon-check"></i> Ok
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

{{--upload goods requisiton signed one--}}
<div id="uploadGoodsRequisition" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content"  style="margin-top: 200px;">
            <form action="{{route('store.purchase_file_scan')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Upload Goods Requisition
                    </h4>
                </div>

                <div class="modal-body">

                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        </div>
                        <div>
                                                <span class="btn default btn-file">
                                                <span class="fileinput-new ">
                                                Select image </span>
                                                <span class="fileinput-exists ">
                                                Change </span>
                                                <input type="file" name="purchase_file_scan" required="" >

                                                </span>
                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                Remove </a>
                        </div>

                        @foreach($file_check_list as $value)
                          <input type="hidden" name="list_value[]" value='{{$value->id}}'>
                        @endforeach

                        @if($errors->has('upload_form'))
                            <span class="help-block" style="color:red;">
                                      * {{ $errors->first('upload_form') }}
                            </span>
                        @endif
                    </div>

                   
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="fa fa-cloud-upload"></i> Upload
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                        Cancel
                    </button>
                </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
</div>