
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

{{--@section('page-title')--}}
{{--Goods Requisition Files--}}
{{--@endsection('page-title')--}}


@section('content')


    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                {{--<table class="table table-striped table-bordered table-advance table-hover" id="sample_1">--}}
                <table class="table table-striped table-bordered table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>

                        <th>
                            Requisition Number
                        </th>

                        <th>
                            Product Name
                        </th>

                        <th>
                            Product Qty
                        </th>

                        <th>
                            Date of Issue
                        </th>

                        <th>
                            Action
                        </th>

                        <th>Status</th>


                    </tr>
                    </thead>
                    <tbody >
                    @foreach($file_list as $value)
                        <tr>

                            <td>
                               {{$loop->iteration}}
                            </td>

                            <td>
                               {{$value->requisition_number}}
                            </td>

                            <td>
                                @if(empty($value->product_name))
                                    {{$value->file_products->name}}
                                @else
                                    {{$value->product_name}}
                                @endif

                            </td>

                            <td>
                                {{$value->product_qty}}
                            </td>

                            <td>

                                {{\Carbon\Carbon::parse($value->created_at)->diffForHumans()}}
                            </td>


                            <td>
                                <a class="btn default btn-xs green-stripe" href="{{route('store.purchase_file_view',['id'=>$value->id])}}">
                                    <i class="fa fa-eye"></i> View
                                </a>

                                <a class="btn default btn-xs blue-stripe" href="{{route('store.purchase_file_edit',['id'=>$value->id])}}">
                                    <i class="fa fa-pencil"></i> Edit
                                </a>


                                <a href="#deleteGroup" class="btn default btn-xs red-stripe" data-ids="{{$value->id}}" data-toggle="modal" data-rel="delete" data-user="@if(empty($value->product_name))
                                {{$value->file_products->name}}
                                @else
                                {{$value->product_name}}
                                @endif">
                                    <i class="icon-trash"></i> Delete
                                </a>

                            </td>
                            <td>
                                @if($value->form_status==0)
                                    <a class="btn btn-xs btn-info" href="{{route('store.purchase_file_view',['id'=>$value->id])}}">
                                        Procced
                                    </a>

                                @else
                                    <button class="btn btn-xs btn-success">Procceded</button>
                                @endif
                            </td>

                        </tr>
                    @endforeach



                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{--modal start here.--}}
    <div id="showMessage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="margin-top: 200px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Asset Deleted Successfully
                    </h4>
                </div>

                <div class="modal-body">
                    <i class="fa fa-check"></i> &nbsp; Asset Deleted Successfully .
                </div>
                <input type="hidden" id="hidden_id">
                <div class="modal-footer" style="text-align: center;">
                    <button type="button" class="btn btn-danger green confirm_yes" id="show_message"><i class="icon-check"></i> Ok
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>

    <div id="deleteGroup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently
                    </h4>
                </div>
                <div class="modal-body"> Do you want to delete <span class='hidden_title'>" "</span>?</div>
                <input type="hidden" id="hidden_id">
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
                    </button>
                    <button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                        No
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });
    </script>

    <script>

        $('#deleteGroup').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var ids = button.data('ids');
            var user = button.data('user');
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modalfooter #confirm').data('form', form);
            $("#hidden_id").val(ids);
            $(".hidden_title").html(' "' + user + '" ');
        });


        $('#deleteGroup').find('#confirm_yes').on('click', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //fetching id
            var id = $("#hidden_id").val();


            $.ajax({
                type:"POST",
                url: "{{ route('store.purchase_file_delete') }}",

                headers: {
                    'XCSRFToken': $('meta[name="csrf-token"]').attr('content')
                },

                data: "id=" + id,
                success: function (msg) {

                    $("#deleteGroup").modal("hide");
                    $("#showMessage").modal("show");
                    // window.location.reload();
                }
            });
        });
        // AJAX DELETE PERMISSION END

        $( "#show_message" ).click(function() {
            window.location.reload();
        });

    </script>


@endsection('scripts')
