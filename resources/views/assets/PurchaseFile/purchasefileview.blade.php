@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'css/goods_requisition_print.css') }}">

@endsection('css')

@section('page_title')
    Print Goods Requisition
@endsection('page_title')


@section('content')
    <div class="portlet light bordered">
        <div class="row hide-on-print">
            <div class="col-md-12">
                <div class="form-group">
                    <a href=""  class="btn btn-sm btn-default ">
                        <i class="fa fa-arrow-circle-left"></i>
                        Back
                    </a>
                </div>
            </div>

            <div class="col-md-12 " >
                <div class="col-md-12 col-md-offset-4">
                    <h3>Vision Energy and Power Pvt. Ltd. </h3> </br>
                    <h4>New Baneshwor, Kathmandu</h4>
                    </br>
                </div>
            </div>
        </div>

        @include('layouts._partials.print-head')

        {{--getting number of row--}}
        <input type="hidden" name="counter" id="getCounter" >
        @if($errors->has('upload_form'))
            <span class="help-block" style="color:red;">
                    * {{ $errors->first('upload_form') }}
                </span>
        @endif
        <table id="myTable" class=" table table-bordered order-list">
            <thead>

            <tr>
                <th colspan="5" style="text-align: center">Goods or Service Requisition Form</th>
            </tr>

            <tr>
                <th>S.No.</th>
                <th>Product Name</th>
                <th>Product Description</th>
                <th>Quantity</th>
                <th>Issue Date</th>
            </tr>
            </thead>
            <tbody>




                <tr>
                    <td class="col-sm-1">
                        <p>{{1}}</p>
                    </td>

                    <td class="col-sm-4">
                        @if(empty($file_view->product_name))
                            <p>{{$file_view->product_name}}</p>
                        @else

                            {{$file_view->file_products->name}}
                        @endif

                    </td>

                    <td class="col-sm-3">
                        <p>{{$file_view->product_description}}</p>
                    </td>

                    <td class="col-sm-2">
                        <p>{{$file_view->product_qty}}</p>
                    </td>

                    <td class="col-sm-3">
                        <p>{{$file_view->created_at}}</p>
                    </td>


                </tr>





            </tbody>
            <tfoot>

            </tfoot>
        </table>


        <div class="row print-page" style="display: none;">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-3">
                <p>......................</p>
                <p>Requested by:</p>
                <p>Name:</p>
                <p>Designation:</p>
                <p>Date:</p>

            </div>

            <div class="col-sm-3">
                <p>......................</p>
                <p>Requested by:</p>
                <p>Name:</p>
                <p>Designation:</p>
                <p>Date:</p>

            </div>

            <div class="col-sm-3">
                <p>......................</p>
                <p>Requested by:</p>
                <p>Name:</p>
                <p>Designation:</p>
                <p>Date:</p>

            </div>

        </div>

     {{--   @if($file_view->form_status == 0)
            <div class="row hide-on-print">
                <div class="col-md-3">
                    <button class="btn btn-sm btn-danger" onclick="window.print()">
                        <i class="fa fa-print"> </i>  Print
                    </button>

                    <a  href="#uploadGoodsRequisition" class="btn default btn-sm blue-stripe"  data-toggle="modal" >
                        <i class="fa fa-cloud-upload"> </i>  Upload
                    </a>
                </div>
            </div>
        @endif--}}

    </div>
    {{--@include('assets.PurchaseFile.modal')--}}
@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection('scripts')
