@extends('layouts.app')
@section('css')

    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    Goods Purchase Form Update
@endsection


@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div >

        <div class="portlet-body form"  >
            <form class="form-horizontal" role="form" action="{{route('store.purchase_file_update')}}" method="post" enctype="multipart/form-data" >
                @csrf

                <h3 class="text-center">Goods Purchase Form Update</h3>


                    <div class="form-group">

                        <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet box yellow">
                                <div class="portlet-title center">
                                    <div class="caption">
                                        Requisition Form Update
                                    </div>

                                </div>

                                <div class="portlet-body">

                                    <div class="table-scrollable">

                                        <table class="table table-bordered table-hover" id="purchase_table">

                                            <thead>
                                            <tr>

                                                <th> S.N</th>
                                                <th> Product Name</th>
                                                <th> Product Description</th>
                                                <th> Requisition No.</th>
                                                <th> Product Qty</th>
                                                <th> Estimated Cost.</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr >

                                                 <td >
                                                     1
                                                </td>
                                                 <td class="text-center">
                                                    
                                                     <div>
                                                        <input type="text" name="product_name" placeholder="Enter Product Name" value="@if(!empty($file_edit->product_name)){{$file_edit->product_name}}@endif" class="form-control" autocomplete="off" id="product_name" >
                                                    </div>
                                                     <input type="hidden" name="product_id" value="@if(empty($file_edit->product_id))@else{{$file_edit->file_products->id}}@endif" id="product_id1">
                                                    <div>
                                                        <select class="form-control input-lg list_asset" name="product_id" id="product_id" >
                                                            <option value="@if(empty($file_edit->product_id))@else{{$file_edit->file_products->id}}@endif" disabled="" selected="" id="value2">@if(empty($file_edit->product_id))@else{{$file_edit->file_products->name}}@endif</option>
                                                            @foreach($product_list as $value)
                                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                   
                                                   

                                                
                                                     <button type="button" class="btn btn-success" id="toggle_product"><i class="fa fa-exchange pull-right"></i></button>
                                                </td>
                                                 <td>
                                                     <textarea class="form-control" name="product_description"  required="">{{$file_edit->product_description}}</textarea>
                                                </td>
                                                 <td>
                                                     <input type="number" min=1 name="requisition_number"  value="{{$file_edit->requisition_number}}" placeholder="Enter Product requisition_number" required=""  class="form-control" autocomplete="off">
                                                </td>
                                                 <td>
                                                     <input type="number" min=1 name="product_qty" placeholder="Enter Product Qty" value="{{$file_edit->product_qty}}" required="" class="form-control" autocomplete="off">
                                                 </td>
                                                <td>
                                                    <input type="number" min=0 name="estimated_cost" placeholder="Enter Estimated Cost" value="{{$file_edit->estimated_cost}}" required="" class="form-control" autocomplete="off">

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>


                    </div>
                <input type="hidden" name="goods_purchase_form_id" value="{{$file_edit->id}}">
                    <div class="form-actions text-center">
                        <button type="reset" class="btn default" id=cancel>Cancel</button>
                        <button type="submit" class="btn green " id="submit">Submit</button>
                    </div>
            </form>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->


@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
    <script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
    <script>
        var a=$('#value2').val();
        if(a!=''){
            $('#product_name').hide();
        }else{
            $('#product_id').hide();
        }
        
        // alert(a !='');

        $('#toggle_product').click(function(e){
            e.preventDefault();
            $('#product_name').show();
            $('#product_id').hide();
            $('#product_id').val('');
            $('#product_id1').val('');

        });
        $("#toggle_product").on('dblclick', function(e) {
            e.preventDefault();
            $('#product_id').show();
            $('#product_name').hide();
            $('#product_name').val('');


        });
    </script>
@endsection('scripts')
