
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Purchase Approved List
@endsection

{{--@section('page-title')--}}
{{--Goods Requisition Files--}}
{{--@endsection('page-title')--}}

@section('content')
    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">
                <table class="table table-striped table-bordered table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>
                        <th>
                            Requisition Number
                        </th>
                        <th>Product Name</th>
                        <th>Product Qty</th>

                        <th>
                            Issued Date
                        </th>

                        <th>
                            Action
                        </th>
                    </tr>
                    </thead>
                    <tbody >
                        @foreach($purchaseApprovedList as $value)
                        <tr>
                            <td>
                                {{$loop->iteration}}
                            </td>
                            <td>
                                {{$value->goods_requisition_number}}
                            </td>
                            <td>
                                {{$value->specification_name}}
                                
                            </td>
                            <td>{{$value->quantity}}</td>
                           
                            <td>
                                {{$value->created_at}}
                            </td>
                            <td>
                                 <a href="#" class="btn btn-info btn-xs"
                                                   data-toggle="modal"
                                                   data-target="#update_asset"
                                                   data-goods_purchase_form_id="{{$value->id}}"
                                                  
                                                >Update Assets</a>               
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Request Form Modal Call -->
<div class="modal fade" id="update_asset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold" id="product_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">

                    <form class="form form-horizontal" action="{{route('store.purchase_approved_product_update')}}"  method="post"   enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <label><b>Product Qty</b></label>
                        <br>
                        <input type="number" min=0 class="form-control" name="product_qty" id="product_qty">

                        <label><b>Unit Cost</b></label>
                        <br>
                        <input type="number" min=0 class="form-control" name="product_unit_cost" id="product_unit_cost" placeholder="Enter Product Qty" autocomplete="off">
                       
                        <label><b>Total Cost</b></label>
                        <br>
                        <input type="hidden" name="product_id" id="product_id">
                        <input type="hidden" name="goods_purchase_form_id" id="goods_purchase_form_id">
                        <input type="hidden" name="vendor_id" id="vendor_id">
                        <input type="number" min=0 class="form-control" name="product_estimated_cost" id="product_estimated_cost">
                        <input type="hidden" name="goods_requisition_file_id" id="goods_requisition_file_id">
                        <label><b>Select Vendor</b></label>
                        <br>
                        <select class="form-control" name="vendor_id" id="">
                            <option id="vendor_name" disabled selected></option>
                             @foreach($vendor_list as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                             @endforeach
                        </select>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label><b>Product Image</b></label>
                                <br>
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                <span class="fileinput-new ">
                                                Select image </span>
                                                <span class="fileinput-exists ">
                                                Change </span>
                                                <input type="file" name="product_image" required="" >
                                                
                                                </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                Remove </a>
                                            </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label><b>Product Bill</b></label>
                                <br>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                <span class="fileinput-new ">
                                                Select image </span>
                                                <span class="fileinput-exists ">
                                                Change </span>
                                                <input type="file" name="product_bill_image" required="" >
                                                
                                                </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                Remove </a>
                                            </div>
                                </div>
                                        
                            </div>
                        </div>
                        <br>
                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Request Form Modal -->
@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });
    </script>

     <script>
        $(document).ready(function() {
            $('#update_asset').on('show.bs.modal', function(e) {
                var button     = $(e.relatedTarget);
                var product_id = button.data('product_id');
                var vendor_id = button.data('vendor_id');
                var vendor_name = button.data('vendor_name');
                var requisition_number=button.data('requisition_number');
                var product_qty=button.data('product_qty');
                var product_unit_cost=button.data('product_unit_cost');
                var product_estimated_cost=button.data('product_estimated_cost');
                var product_requested_date=button.data('product_requested_date');
                var product_id=button.data('product_id');
                var product_title=button.data('product_title');
                var goods_purchase_form_id=button.data('goods_purchase_form_id');
                var goods_requisition_file_id=button.data('goods_requisition_file_id');



                $("#product_id").val(product_id);
                $("#requisition_number").val(requisition_number);
                $("#product_qty").val(product_qty);
                $("#product_unit_cost").val(product_unit_cost);
                $("#product_estimated_cost").val(product_estimated_cost);
                $("#product_requested_date").val(product_requested_date);
                $("#goods_purchase_form_id").val(goods_purchase_form_id);
                $("#goods_requisition_file_id").val(goods_requisition_file_id);
                $("#product_title").html("Enter Product Details Of "+product_title);
                $("#vendor_id").val(vendor_id);
                $("#vendor_name").html(vendor_name);

            });
        });
    </script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>

@endsection('scripts')
