@extends('layouts.app')
@section('css')

    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    Product Add
@endsection

@section('content')

    <div class=" portlet light bordered" >
        <div class="row" style="padding-left: 15px;">
            <div class="portlet-body col-md-12 col-sm-12">
                <form class="form-horizontal" role="form" action="{{route('store.store_product')}}" method="post" enctype="multipart/form-data" >
                    @csrf


                    {{--start 1st row--}}
                    <div class="row" >
                        <hr>
                        <h4>Product Category and Its Sub Category</h4>
                        <hr>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Asset Type</label>
                                <select class="form-control list_asset" name="asset_type_id" required="" id="asset_type_id">
                                    <option  value="" ></option>
                                    @foreach($asset_type as $element)
                                        <option value="{{$element->id}}" >{{$element->name}}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('asset_type_id'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('asset_type_id') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">Block Type</label>

                                <select class="form-control input-sm list_asset" name="block_id" id="block_list">
                                    <option  value="" ></option>
                                </select>
                                @if($errors->has('block_id'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('block_id') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">Sub-Block Type</label>
                                <select class="form-control input-sm  list_asset" name="sub_block_id"  id='sub_block_list'>
                                    <option  value="" ></option>
                                </select>
                                @if($errors->has('sub_block_id'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('sub_block_id') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                    </div>
                    {{--end 1st row--}}

                    {{--2nd row--}}
                    <div class="row">
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <label class=" control-label">Asset Name</label>
                                <select class="form-control input-sm list_asset" name="asset_name_id"  id='asset_name_list'>
                                    <option  value="" ></option>
                                </select>
                                @if($errors->has('asset_name_id'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('asset_name_id') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">Specification</label>
                                <select class="form-control input-sm list_asset" name="specification_name_id"  id='specification_name_list'>
                                    <option  value="" ></option>
                                </select>
                                @if($errors->has('specification_name_id'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('specification_name_id') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class=" control-label">Unit:</label>
                                <select name="unit" class = "form-control" >
                                    <option value="" selected>Select Any One Unit</option>
                                    <option value="rim" {{ (old('unit') == 'rim' ? 'selected' : '') }}>Rim</option>
                                    <option value="packet" {{ (old('unit') == 'packet' ? 'selected' : '') }}>Packet</option>
                                    <option value="piece" {{ (old('unit') == 'piece' ? 'selected' : '') }}>Piece</option>
                                    <option value="unit" {{ (old('unit') == 'unit' ? 'selected' : '') }}>Unit</option>
                                    <option value="kg" {{ (old('unit') == 'kg' ? 'selected' : '') }}>Kg</option>
                                </select>
                                @if($errors->has('unit'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('unit') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                    </div>
                    {{--end 2nd row--}}

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class=" control-label">Select Vendor</label>
                                <select class="form-control input-md list_asset" name="vendor_id"  required="" >
                                    <option  value="" ></option>
                                    @foreach($vendor_list as $element)
                                        <option  value="{{$element->id}}" @if(old('vendor_id') == $element->id) selected @endif>{{$element->name}}</option>
                                    @endforeach

                                </select>
                                @if($errors->has('vendor_id'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('vendor_id') }}
                                  </span>
                                @endif

                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class=" control-label">Batch Number</label>
                                <input type="text" name="batch_number" class="form-control" id='batch_number' readonly=""  >
                                @if($errors->has('batch_number'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('vendor_id') }}
                                  </span>
                                @endif

                            </div>
                        </div>

                    </div>

                    {{--start 3rd row--}}
                    <div class="row">
                        <hr>
                        <h4>Product Information</h4>
                        <hr>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class=" control-label">Quantity</label>
                                <input type="number" name="quantity" class="form-control" placeholder="Enter Product Quantity" required="" id="quantity" value="{{old('quantity')}}">
                                @if($errors->has('quantity'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('quantity') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">Per Unit Cost</label>
                                <input type="number" name="unit_cost" class="form-control" placeholder="Enter Product Unit Cost" required="" id="unit_cost" value="{{old('unit_cost')}}">
                                @if($errors->has('unit_cost'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('unit_cost') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">Amount</label>
                                <input type="number" name="amount" class="form-control" placeholder="Enter Product Amount" required="" id="amount" value="{{old('amount')}}">
                                @if($errors->has('amount'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('amount') }}
                                  </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    {{--end 3rd row--}}

                    {{--start 4th row--}}
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Discount</label>
                                <input type="text" name="discount" class="form-control" placeholder="Enter Discount Percent" required="" id="amount" value="{{old('discount')}}">
                                @if($errors->has('discount'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('discount') }}
                                  </span>
                                @endif
                            </div>

                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">V.A.T</label>
                                <input type="number" name="vat" class="form-control" placeholder="Enter VAT Percent" required="" id="amount" value="{{old('vat')}}">
                                @if($errors->has('vat'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('vat') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">Expected Life(yrs)</label>
                                <input type="number" name="expected_life" id="expected_life" class="form-control" placeholder="Enter Expected Life" required="" autocomplete="off" value="{{old('expected_life')}}">
                                @if($errors->has('expected_life'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('expected_life') }}
                                  </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    {{--end 4th row--}}

                    {{--start 5th row--}}
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class=" control-label">Depreciation Method</label>
                                <select name="depreciation_method" id="depreciation_method" class="form-control" >
                                    <option value="" selected>Select  One Depreciation Method</option>
                                    <option value="fifo" {{ (old('depreciation_method') == 'fifo' ? 'selected' : '') }}>FIFO</option>
                                    <option value="wdv" {{ (old('depreciation_method') == 'wdv' ? 'selected' : '') }}>WDV</option>
                                    <option value="slm" {{ (old('depreciation_method') == 'slm' ? 'selected' : '') }}>SLM</option>
                                </select>
                                @if($errors->has('depreciation_method'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('depreciation_method') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                        {{--<div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">Depreciation Rate:</label>
                                    <input type="number" name="depreciation_rate" id="depreciation_rate" class="form-control" placeholder="Depreciation Rate" required="" autocomplete="off" value="{{old('depreciation_rate')}}">
                                    @if($errors->has('depreciation_rate'))
                                        <span class="help-block" style="color:red;">
                                          * {{ $errors->first('depreciation_rate') }}
                                      </span>
                                    @endif
                            </div>
                        </div>--}}

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class="control-label">Residual Value (% Of Purchase Price)</label>
                                <input type="number" name="residual_price" id="residual_value" class="form-control" placeholder="Residual Value Of Purchase Price" required="" autocomplete="off" value="{{old('residual_price')}}">
                                @if($errors->has('residual_price'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('residual_price') }}
                                  </span>
                                @endif
                            </div>
                        </div>


                    </div>
                    {{--end 5th row--}}

                    {{--start 6th row--}}
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class=" control-label">Bill Issued Date</label>
                                <input type="text"  name="bill_date"  placeholder="Bill Issue Date"  value="{{old('bill_date')}}"class="form-control datepicker">
                                @if($errors->has('bill_date'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('bill_date') }}
                                  </span>
                                @endif

                                @if($errors->has('required_date'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('required_date') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class=" control-label">Ready to use Date</label>
                                <input type="text"  name="ready_to_use" placeholder="Ready to use product" value="{{old('ready_to_use')}}"  class="form-control datepicker"/>
                                @if($errors->has('ready_to_use'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('ready_to_use') }}
                                  </span>
                                @endif

                                @if($errors->has('required_date'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('required_date') }}
                                  </span>
                                @endif
                            </div>
                        </div>

                    </div>
                    {{--end 6th row--}}

                    {{--start 6th row--}}
                    <div class="row">
                        <hr>
                        <h4>File Uploads</h4>
                        <hr>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Bill Image</label>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                    </div>
                                    <div>
                                                <span class="btn default btn-file">
                                                <span class="fileinput-new ">
                                                Select image </span>
                                                <span class="fileinput-exists ">
                                                Change </span>
                                                <input type="file" name="bill" >

                                                </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                            Remove </a>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="col-md-3 col-md-offset-1">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Product Image</label>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                    </div>
                                    <div>
                                                <span class="btn default btn-file">
                                                <span class="fileinput-new ">
                                                Select image </span>
                                                <span class="fileinput-exists ">
                                                Change </span>
                                                <input type="file" name="image" >

                                                </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                            Remove </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    {{--end 6th row--}}

                    {{--7th row--}}
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class=" control-label">Product Entry Date</label>
                                <input type="text" name="nepali_date" class="form-control bod-picker" placeholder="Enter Nepali Date" required="" value="{{old('nepali_date')}}">
                                @if($errors->has('nepali_date'))
                                    <span class="help-block" style="color:red;">
                                      * {{ $errors->first('nepali_date') }}
                                  </span>
                                @endif
                            </div>

                        </div>
                    </div>
                    {{--7th row end--}}

                    {{--8th row--}}
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="submit" class="btn green " id="submit">Submit</button>
                                <button type="reset" class="btn default" id=cancel>Clear</button>
                            </div>
                        </div>
                    </div>
                    {{--end 8th row--}}

                </form>

            </div>
            {{--<div class="form-group">--}}
            {{--<label class="col-md-3 control-label">First Year Depreciation(% of year)</label>--}}
            {{--<div class="col-md-9">--}}
            {{--<input type="number" name="depreciation_amount" class="form-control" placeholder="First Year Depreciation Year" required="" autocomplete="off">--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->


@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
    <script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $("#date1").on('click',function(){
                $('#date1').remove();
            });

            $(".datepicker").datepicker({
                dateFormat:"yy-mm-dd",
                changeMonth:true,
                changeYear:true
            });

            $(".bod-picker").nepaliDatePicker({
                dateFormat: "%D, %M %d, %y",
                closeOnDateSelect:true
                //minDate : "सोम, जेठ १०, २०७३",
                //maxDate : "मंगल, जेठ ३२, २०७३"
            });

            $(".list_asset").select2({
                placeholder: "Select",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });


        });
    </script>

    <script>
        {{--javascript to calculate the amount after adding unit cost and  quantity--}}
        $('#unit_cost').on("keyup", function(){
            var unit_cost=$('#unit_cost').val();
            var quantity=$('#quantity').val();
            var amount=unit_cost*quantity;
            $('#amount').val(amount);
        });

        $('#quantity').on("keyup", function(){
            var unit_cost=$('#unit_cost').val();
            var quantity=$('#quantity').val();
            var amount=unit_cost*quantity;
            $('#amount').val(amount);
        });
        {{-- end javascript to calculate the amount after adding unit cost and  quantity--}}

        {{--retrieve assets type on change through ajax post--}}
        $('#asset_type_id').change(function(){

            var asset_type_id=$(this).val();

            //set value initially.
            if(asset_type_id == 1){
                $('#residual_value').val(0);
                $('#depreciation_method').val('fifo');
                $('#expected_life').val(0);
            }
            else{
                $('#residual_value').val('');
                $('#depreciation_method').val('');
                $('#expected_life').val('');
            }

            // console.log($('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                type:"POST",
                url: "{{route('store.get_block_list')}}",
                data: "asset_type_id=" + asset_type_id,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (msg) {
                    var option1='';
                    $.each(msg,function(key,value){
                        option1+='<option value="'+value['id']+'" >'+value['block_name']+'</option>'
                    });
                    $('#block_list').children().remove();
                    $("#block_list").append('<option value selected></option>');
                    $('#select2-chosen-2').val = '';

                    $("#block_list").append(option1);

                }
            });
        });
        {{-- end retrieve assets type on change through ajax post--}}


        //on block list change retrieve its sub block through ajax post.
        $('#block_list').change(function(){
            var sub_block_id=$("#block_list option:selected").val();
            // $('#sub_block_list').children().remove();

            $.ajax({
                type:"POST",
                url: "{{route('store.get_sub_block_list')}}",
                data: "block_id=" + sub_block_id,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (msg) {
                    var option2='';
                    $.each(msg,function(key,value){
                        option2+='<option value="'+value['id']+'" >'+value['sub_block_name']+'</option>'
                    });

                    //removing previous data.
                    $('#sub_block_list').children().remove();
                    $("#sub_block_list").append('<option value></option>');

                    $("#sub_block_list").append(option2);
                }
            });
        });
        //end on block list change retrieve its sub block through ajax post.

    </script>

    <script type="text/javascript">

        $('#sub_block_list').change(function(){
            var sub_block_id=$("#sub_block_list option:selected").val();

            $.ajax({
                type:"POST",
                url: "{{route('store.get_asset_name_list')}}",
                data: "sub_block_id=" + sub_block_id,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (msg) {
                    var option3='';
                    $.each(msg,function(key,value){
                        option3+='<option value="'+value['id']+'" >'+value['asset_name']+'</option>'
                    });

                    //removing previous data.
                    $('#asset_name_list').children().remove();
                    $("#asset_name_list").append('<option value></option>');

                    $("#asset_name_list").append(option3);
                }
            });
        });

    </script>

    <script type="text/javascript">

        $('#asset_name_list').change(function(){
            var asset_name_id=$("#asset_name_list option:selected").val();
            $.ajax({
                type:"POST",
                url: "{{route('store.get_specification_name_list')}}",
                data: "asset_name_id=" + asset_name_id,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (msg) {
                    var option4='';
                    $.each(msg,function(key,value){
                        option4+='<option value="'+value['id']+'" >'+value['specification_name']+'</option>'
                    });
                    //removing previous data.
                    $('#specification_name_list').children().remove();
                    $("#specification_name_list").append('<option value></option>');
                    $("#specification_name_list").append(option4);
                }
            });
        });

    </script>


    <script type="text/javascript">

        $('#specification_name_list').change(function(){
            var get_specification_id_count=$("#specification_name_list option:selected").val();

            $.ajax({
                type:"POST",
                url: "{{route('store.get_specification_id_count')}}",
                data: "get_specification_id_count=" + get_specification_id_count,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (msg) {
                    $('#batch_number').val('batch_'+(msg+1));

                }
            });
        });

    </script>

@endsection('scripts')
