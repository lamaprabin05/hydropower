@extends('layouts.app')

@section('page_title')
    View Product
@endsection

@section('content')
<!-- BEGIN SAMPLE FORM PORTLET-->
<div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">
                <div class="row">
                        <div class="form-group">
                            <a href="{{route('store.list_product')}}" class="btn btn-sm btn-default ">
                                <i class="fa fa-arrow-circle-left"></i>
                                Back
                            </a>
                        </div>
                </div>
            {{--start 1st row--}}
                <div class="row">
                    <h2>Product Information:</h2>
                    <hr>

                    <h4>Product Category and Its Sub Category</h4>
                    <hr>

                    <div class="col-md-3">
                        <div class="form-group">
                            <strong > Category</strong>
                            <p class="form-control">  {{ucfirst($product->asset_types->name)}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong > Block</strong>
                            <p class="form-control">  {{ucfirst($product->product_block->block_name)}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong > Sub Block</strong>
                            <p class="form-control">  {{ucfirst($product->product_sub_block->sub_block_name)}}</p>
                        </div>
                    </div>

                </div>
            {{--end 1st row--}}

                {{--2nd row--}}
                <div class="row">
                    <div class="col-md-3 ">
                        <div class="form-group">
                            <strong >Product Name:</strong>
                            <p class="form-control">  {{$product->asset_name->asset_name}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Specification</strong>
                            <p class="form-control">  {{$product->specification_name->specification_name}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Unit</strong>
                            <p class="form-control">  {{$product->unit}}</p>
                        </div>
                    </div>

                </div>
            {{--end 2nd row--}}

            {{--start 3rd row--}}
                <div class="row">
                    <hr>
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong >Quantity</strong>
                            <p class="form-control">  {{$product->quantity}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Per Unit Cost</strong>
                            <p class="form-control">  {{$product->unit_cost}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Amount</strong>
                            <p class="form-control">  {{$product->amount}}</p>
                        </div>
                    </div>
                </div>
            {{--end 3rd row--}}

            {{--start 4th row--}}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong >Discount</strong>
                            <p class="form-control">  {{$product->discount}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >VAT</strong>
                            <p class="form-control">  {{$product->vat}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Expected Life</strong>
                            <p class="form-control">  {{$product->expected_life}}</p>
                        </div>
                    </div>
                </div>
            {{--end 4th row--}}

            {{--start 5th row--}}
                <div class="row">
                    <hr>
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong >Depreciation Method</strong>
                            <p class="form-control">  {{$product->depreciation_method}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Depreciation Value</strong>
                            <p class="form-control">  {{$product->depreciation_value}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Residual Price</strong>
                            <p class="form-control">  {{$product->residual_price}}</p>
                        </div>
                    </div>
            </div>
            {{--end 5th row--}}

            {{--start 6th row--}}
                <div class="row">
                    <hr>
                    @if(isset($product->bill_issue_date))
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong >Bill Issue Date</strong>
                            <p class="form-control">{{ date('Y-m-d',strtotime($product->bill_issue_date)) }}</p>
                        </div>
                    </div>
                    @endif

                    @if(isset($product->ready_to_use_date))
                    <div class="col-md-3 @if(!empty($product->bill_issue_date))col-md-offset-1  @endif">
                        <div class="form-group">
                            <strong >Ready To Use Date</strong>
                            <p class="form-control">{{ date('Y-m-d',strtotime($product->ready_to_use_date)) }} </p>
                        </div>
                    </div>
                    @endif

                </div>
            {{--end 6th row--}}

            {{--start 7th row--}}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong >Product Created Date</strong>
                            <p class="form-control">  {{$product->created_at}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Created Date Nepali</strong>
                            <p class="form-control">  {{$product->nepali_date}}</p>
                        </div>
                    </div>


                </div>
            {{--end 7th row--}}

            {{--start 8th row--}}
             @if(!empty($product->image) && !empty($product->bill))
                <div class="row">
                    <hr>
                    <h4>Uploaded Files</h4>
                    <hr>
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong >Product Image</strong>
{{--                            <p class="form-control">  {{$product->image}}</p>--}}
                            <img src="{{asset(STATIC_DIR.$product->image)}}" class="img-responsive" alt="">
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Bill Image</strong>
{{--                            <p class="form-control">  {{$product->bill}}</p>--}}
                            <img src="{{asset(STATIC_DIR.$product->image)}}" class="img-responsive" alt="">

                        </div>
                    </div>

                </div>
             @endif
            {{--end 8th row--}}

            {{--start 9th row--}}
                <div class="row">
                    <hr>
                    <h4>Vendor Information:</h4>
                    <hr>
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong >Vendor Name</strong>
                            <p class="form-control">  {{$product->product_vendors->name}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Email</strong>
                            <p class="form-control">  {{$product->product_vendors->email}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Pan Vat Number</strong>
                            <p class="form-control">  {{$product->product_vendors->pan_vat_number}}</p>
                        </div>
                    </div>
                </div>
            {{--end 9th row--}}

            {{--start 10th row--}}
                <div class="row">
                    <hr>
                    <div class="col-md-3">
                        <div class="form-group">
                            <strong >Vendor Address</strong>
                            <p class="form-control">  {{$product->product_vendors->address}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Vendor Contact</strong>
                            <p class="form-control">  {{$product->product_vendors->contact}}</p>
                        </div>
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <div class="form-group">
                            <strong >Mobile Number:</strong>
                            <p class="form-control">  {{$product->product_vendors->mobile_number}}</p>
                        </div>
                    </div>
                </div>
            {{--end 10th row--}}

            </div>


        </div>
</div>


@endsection('content')
@section('scripts')
 
@endsection('scripts')
