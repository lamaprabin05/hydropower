
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Product Listing
@endsection

{{--@section('page-title')--}}
        {{--Goods Requisition Files--}}
{{--@endsection('page-title')--}}


@section('content')


    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                {{--<table class="table table-striped table-bordered table-advance table-hover" id="sample_1">--}}
                <table class="table table-striped table-bordered table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>
                        <th>
                            Product Name
                        </th>
                        <th>Specification Name</th>

                    

                        <th>
                            Asset Type
                        </th>
                        <th>Quantitiy</th>

                        <th>
                            Vendor
                        </th>

                        <th>Created Date</th>

                        <th>
                            Action
                        </th>


                    </tr>
                    </thead>
                    <tbody >
                    @foreach($list_assets as $element)
                                <tr>
                                    <td>
                                         {{$loop->iteration}}
                                    </td>
                                    <td>
                                         {{$element->asset_name->asset_name}}
                                    </td>
                                    <td>{{$element->specification_name->specification_name}}</td>
                                     <td>
                                         {{$element->asset_types->name}}
                                    </td>
                                    <td>
                                         {{$element->quantity}}
                                    </td>
                                    <td>
                                         {{$element->product_vendors->name}}
                                    </td>
                                    <td>{{$element->created_at}}</td>
                                    <td>
                                        
                                        <a class="btn default btn-xs blue-stripe" href="{{route('store.edit_product',['id'=>$element->id])}}">
                                        <i class="fa fa-pencil"></i> Edit
                                         </a>
                                    
                                    <a class="btn default btn-xs green-stripe" href="{{route('store.view_product',['id'=>$element->id])}}">
                                        <i class="fa fa-eye"></i> View
                                    </a>
                                        
                                        
                                    </td>
                                </tr>
                                @endforeach



                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });



    </script>


@endsection('scripts')
