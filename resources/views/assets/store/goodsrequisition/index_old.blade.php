
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

@section('content')

    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                {{--<table class="table table-striped table-bordered table-advance table-hover" id="sample_1">--}}

                    <table class="table table-striped table-bordered table-hover" id="pagination_search">
                    <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Requisition Number</th>
                        <th>Product Name</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Required Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody >
                  @foreach($goods_requisition_product as $element)
                      <tr>
                          <td>{{$loop->iteration}}</td>
                          <td>{{$element->goods_requisition_form_id}}</td>
                          <td>{{$element->name}}</td>
                          <td>
                            <textarea>{{$element->description}}</textarea>
                          </td>
                          <td>{{$element->quantity}}</td>
                          <td>{{ date('Y-m-d',strtotime($element->requirement_date)) }}</td>
                          <td>
                              @if($element->product_id == null)
                                <a href="#" class="btn btn-xs btn-info"
                                                   data-toggle="modal"
                                                   data-target="#product_modal"
                                                   data-product_requisition_number="{{$element->goods_requisition_form_id}}",
                                                   data-product_id="{{$element->id}}",
                                                   data-product_name="{{$element->name}}",
                                                   data-product_description="{{$element->description}}",
                                                   data-product_quantity="{{$element->quantity}}",
                                                   data-product_required_date="{{$element->requirement_date}}",
                                                   data-product_requisition_product_id="{{$element->id}}",

                                                ><i class="fa fa-paper-plane"></i> Choose & Proceed</a>
                              @else
                                  <a href="{{route('assets.store.get_proceed_form',$element->id)}}" class="btn btn-danger btn-xs">Proceed to Dispatch</a>
                              @endif
                          </td>


                      </tr>
                  @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Disptach Modal Call -->
<div class="modal fade" id="product_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"></h4>
                
               
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">

                    <form class="form form-horizontal" action="{{route('store.goods_requisition_file')}}" id="driver_form" method="post">
                        {{ csrf_field() }}
                        <div >
                            <label><b>Choose Product</b></label>
                            <br>
                            <select class="form-control list_asset" name="product_id"  >
                                <option value=""></option>
                                @foreach($product_name as $element)
                            <option value="{{$element->id}}">{{$element->asset_name->asset_name}}-({{$element->specification_name->specification_name}})</option>
                                @endforeach
                            </select>

                        </div>
                        <br>

                       

                        <input type="hidden" name="product_requisition_number" id="product_requisition_number">
                        <input type="hidden" name="product_quantity" id="product_quantity">
                        <input type="hidden" name="product_required_date" id="product_required_date">
                        <input type="hidden" name="product_requisition_product_id" id="product_requisition_product_id">

                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">
                       

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Modal Call -->
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
            $('#pagination_search').DataTable();
        });
    </script>
<script>

$(".list_asset").select2({
                placeholder: "Select",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });      


</script>

<script>
  $(document).ready(function() {
  $('#product_modal').on('show.bs.modal', function(e) {
    var button     = $(e.relatedTarget);
    
    var product_requisition_number = button.data('product_requisition_number');
    var product_id = button.data('product_id');
    var product_name = button.data('product_name');
    var product_quantity=button.data('product_quantity');
    var product_required_date=button.data('product_required_date');
    var product_requisition_product_id=button.data('product_requisition_product_id');
    


  
    $("#product_requisition_number").val(product_requisition_number);
    $("#product_name").val(product_name);
    $("#product_quantity").val(product_quantity);
    $("#product_id").val( product_id);
    $("#product_required_date").val( product_required_date);
    $("#product_requisition_product_id").val( product_requisition_product_id);

});
});
</script>
@endsection('scripts')
