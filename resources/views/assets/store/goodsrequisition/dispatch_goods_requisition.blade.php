@extends('layouts.app')

@section('css')
@endsection('css')

@section('page_title')
     Dispatch Goods Requisition
@endsection('page_title')


@section('content')
    <div class="portlet light bordered">
        <div class="row hide-on-print">
            <div class="col-md-12">
                <div class="form-group">
                    <a href="{{route('assets.store.get_all_goods_requisition')}}"  class="btn btn-sm btn-default ">
                        <i class="fa fa-arrow-circle-left"></i>
                        Back
                    </a>
                </div>
            </div>

        </div>
        @if(isset($store->id))
            <div class="row">
                <div class="col-md-12">
                    <table id="myTable" class=" table table-bordered order-list">
                        <thead>

                        <tr>
                            <th colspan="8" style="text-align:left">Dispatch Requested Product</th>
                        </tr>

                        <tr>
                            <th>S.N.</th>
                            <th>Requisition <br>Number</th>
                            <th>Product <br> Name</th>
                            <th>Product <br> Specification</th>
                            <th>Requested<br>Qty</th>
                            <th>Stock  <br>Qty</th>
                            <th>Required<br>Date</th>
                            <th>Batch</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td class="col-sm-1">
                                <p>1</p>
                            </td>

                            <td><p>{{$product->goods_requisition_form_id}}</p></td>

                            <td class="col-sm-4">
                                <p>{{$product->name}}</p>
                            </td>

                            <td class="col-sm-3">
                                <p>{{$product->description}}</p>
                            </td>

                            <td class="col-sm-2">
                                <p>{{$product->quantity}}</p>
                            </td>

                            <td class="col-sm-3" style="color:#B22222;">
                                <p>{{$store->quantity}}</p>
                            </td>

                            <td class="col-sm-3">
                                <p>{{date('Y-m-d',strtotime($product->requirement_date))}}</p>
                            </td>

                            <td style="color:#B22222;">
                                {{$store->batch_number}}
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <table id="myTable" class=" table table-bordered order-list">
                        <thead>

                        <tr>
                            <th colspan="6" style="text-align: center">Requested Products</th>
                        </tr>

                        <tr>
                            <th>S.No.</th>
                            <th>Product Name</th>
                            <th>Product Description</th>
                            <th>Quantity</th>
                            <th>Date of Requirement</th>
                        </tr>


                        </thead>
                        <tbody>

                        <tr>
                            <td class="col-sm-1">
                                <p>1</p>
                            </td>

                            <td class="col-sm-4">
                                <p>{{$product->name}}</p>
                            </td>

                            <td class="col-sm-3">
                                <p>{{$product->description}}</p>
                            </td>

                            <td class="col-sm-2">
                                <p>{{$product->quantity}}</p>
                            </td>

                            <td class="col-sm-3">
                                <p>{{$product->requirement_date}}</p>
                            </td>


                        </tr>

                        <tr>
                            <td colspan="5" style="text-align: left;">
                                No Product Available in Store.
                            </td>
                        </tr>

                        <tr>
                            <td colspan="5" style="text-align: center;">
                                @if($product->purchase_requisition == 1)
                                    <span style="color:red;">File Proceeded For Purchase Requisition</span>
                                @else
                                    <a href="{{route('store.purchase_form',$product->id) }}">Proceed to Purchase Requisition Form</a>
                                @endif
                            </td>
                        </tr>

                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            </div>
        @endif

        @if(isset($store->id))

        <form class="form" action="{{route('store.dispatch_goods_request')}}" id="driver_form" method="post"   >
            {{ csrf_field() }}
            <input type="hidden" name="store_id" value="{{$store->id}}">
            <input type="hidden" name="goods_requisition_product_id" value="{{$product->id}}">

            <div class="row" >
                <hr>
                <h4 style="padding-left: 15px;">Dispatch Form</h4>
                <hr>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Product Name</label>
                        <p class="form-control">  {{ucfirst($store->asset_name->asset_name)}}</p>

                    </div>
                </div>

                <div class="col-md-3 col-md-offset-1">
                    <div class="form-group">
                        <label class="control-label">Product Specification </label>
                        <p class="form-control">  {{ucfirst($store->specification_name->specification_name)}}</p>
                    </div>
                </div>

                <div class="col-md-3 col-md-offset-1">
                    <div class="form-group">
                        <label class="control-label">Product Batch </label>
                        <p class="form-control">  {{ucfirst($store->batch_number)}}</p>
                    </div>
                </div>

            </div>

            <div class="row" >
                <hr>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label" style="color:#B22222;">Product Quantity Available </label>
                        <p class="form-control" >{{$store->quantity}}</p>
                        <input type="hidden" value="{{$store->quantity}}" id="store_product">
                    </div>
                </div>

                <div class="col-md-3 col-md-offset-1">
                    <div class="form-group">
                        <label class="control-label" style="color:#B22222;">Requested Quantity</label>
                        <p class="form-control" >{{$product->quantity}}</p>
                        <input type="hidden" value="{{$product->quantity}}" name="requested_quantity">

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 ">
                    <div class="form-group" style="color:#B22222;">
                        <label class="control-label">Enter Dispatching Quantity  </label>
                        <input type="text" name="dispatch_quantity" class="form-control" id="dispatch_quantity" value="{{old('dispatch_quantity')}}">
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="form-group">
                    @if($errors->has('dispatch_quantity'))
                        <span class="control-label" style="color:red;">
                              * {{ $errors->first('dispatch_quantity') }}
                        </span>
                    @endif
                </div>


            </div>

            <input type="submit" class="btn btn-sm  btn-primary" value="Dispatch"  id="submit">
        </form>

        @endif

        {{--start modal --}}
        <div class="modal fade" id="showMessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content" style="margin-top: 90px;">
                    <div class="modal-header">
                        <h5 class="modal-title">Error</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>You cannot enter value more than in stock.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        {{--end modal--}}
    </div>

@endsection('content')
@section('scripts')
    {{--<script>--}}
        {{--$('#dispatch_quantity').keyup(function(){--}}
            {{--let store_product = $('#store_product').val();--}}
            {{--let dispatch_quantity = $('#dispatch_quantity').val();--}}

            {{--if (store_product < dispatch_quantity ){--}}
                {{--$('#dispatch_quantity').val('');--}}

                {{--$('#showMessage').modal('show');--}}
            {{--}--}}
        {{--});--}}
    {{--</script>--}}
@endsection('scripts')
