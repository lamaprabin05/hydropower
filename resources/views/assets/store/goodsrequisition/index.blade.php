
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>


@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

@section('content')

    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                <table class="table table-striped table-bordered table-hover" id="pagination_search">
                    <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Requisition Number</th>
                            <th>Product Name</th>
                            <th>Description</th>
                            <th>Quantity</th>
                            <th>Required Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody >
                        @foreach($goods_requisition_product as $element)
                          <tr>
                              <td>{{$loop->iteration}}</td>
                              <td>{{$element->goods_requisition_form_id}}</td>
                              <td>{{$element->name}}</td>
                              <td>
                                {{$element->description}}
                              </td>
                              <td>{{$element->quantity}}</td>
                              <td>{{ date('Y-m-d',strtotime($element->requirement_date)) }}  </td>
                              <td>
                                  @if($element->specification_name_id == null)

                                          @if($element->asset_name_id == null)

                                            <a href="#" class="btn btn-xs btn-info"
                                                               data-toggle="modal"
                                                               data-target="#product_modal"
                                                               data-asset_name="{{$element->name}}",
                                                               data-asset_description="{{$element->description}}",
                                                               data-asset_name_id="0",
                                                               data-product_id="{{$element->id}}",


                                            ><i class="fa fa-paper-plane"></i> Manual Choose & Proceed
                                            </a>
                                          @else
                                              <a href="#" class="btn btn-xs btn-info"
                                                 data-toggle="modal"
                                                 data-target="#product_modal"
                                                 data-asset_name="{{$element->name}}",
                                                 data-asset_description="{{$element->description}}",
                                                 data-asset_name_id="{{$element->asset_name['id']}}",
                                                 data-product_id="{{$element->id}}",

                                              ><i class="fa fa-paper-plane"></i>  Proceed</a>
            {{--                                  <a href="{{route('assets.store.get_proceed_form',$element->id)}}" class="btn btn-danger btn-xs">Proceed to Dispatch</a>--}}
                                          @endif
                                  @else
                                        <a href="{{route('store.get_proceed_form',$element->id)}}" class="btn btn-success btn-xs"><i class="fa fa-eye"></i> View</a>
                                  @endif

                              </td>
                          </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!--Modal to select assets name and specification-->
<div class="modal fade" id="product_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"></h4>
                
               
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">

                    <form class="form form-horizontal" method="POST" action="{{route('store.proceed_goods_form')}}" id="driver_form" >
                        {{ csrf_field() }}
                        <div class="row">
                            <h3>Product Dispatch</h3>
                            <hr>
                            <div class="col-md-4 col-md-offset-1">

                                <div class="form-group">
                                    <label class="control-label">Product Name</label>
                                    <p id="asset_name" class="form-control"></p>

                                </div>


                            </div>

                            <div class="col-md-4 col-md-offset-1">
                                <div class="form-group">
                                    <label class="control-label">Specification </label>
                                    <p id="asset_description" class="form-control"></p>

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6 col-md-offset-1">


                                <div class="form-group" id="show_asset_name">
                                    <label class="control-label">Choose Store Product</label>
                                    <select class="form-control list_asset" id="asset_id" name="asset_name_id"  >
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label class="control-label">Choose Specification Name</label>
                                    <select class="form-control list_asset" id="specification_id" name="specification_id"  >
                                    </select>

                                </div>

                                <div class="form-group">
                                    <input type="hidden" name="product_id" id="product_id" >
                                    <input type="hidden" name="token" id="token" >
                                    <input type="submit" class="btn btn-sm  btn-success" value="Submit"  id="submit">
                                    <button type="button" class="btn  btn-sm btn-secondary " data-dismiss="modal">Close</button>
                                </div>


                            </div>
                        </div>


                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Modal Call -->
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
            $('#pagination_search').DataTable();
        });

        $(".list_asset").select2({
            placeholder: "Select",
            allowClear: true,
            escapeMarkup: function (m) {
                return m;
            }
        });

</script>

<script>
  $(document).ready(function() {
      $('#product_modal').on('show.bs.modal', function(e) {

          //show default  value to select boxes.
          $( "#s2id_asset_id" ).removeClass("list_asset select2-allowclear");
          $("#select2-chosen-1").html("Select");

          $("#s2id_specification_id").removeClass("list_asset select2-allowclear");
          $('#select2-chosen-2').html("Select");

        var button = $(e.relatedTarget);
        var asset_name = button.data('asset_name');
        var asset_description = button.data('asset_description');
        var asset_name_id = button.data('asset_name_id');


        var product_id = button.data('product_id');
        $("#product_id").val( product_id);

        //adding value in p tag in modal
        $("#asset_name").empty('');
        $("#asset_name").append(asset_name);

        $("#asset_description").empty('');
        $("#asset_description").append(asset_description);

        //show assets name menu only when it is manual product entry
        if(asset_name_id == 0){
            $('#show_asset_name').show();
            $('#token').val('manual');
        }
        else{
            $('#show_asset_name').hide();
            $('#token').val('auto');

        }

        //fetch assets list
          $.ajax({
              type:"get",
              url: "{{route('store.get_assets_name')}}",
              dataType: 'json',
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function (msg) {
                  var option='';
                  $.each(msg,function(key,value){
                      option+='<option value="'+value['id']+'" >'+value['asset_name']+'</option>'
                  });

                  //removing previous data.
                  $('#asset_id').children().remove();
                  $("#asset_id").append('<option value></option>');
                  $("#asset_id").append(option);
              }
          });


          //fetch specification list based on assets name id or list all
          $.ajax({
              type:"get",
              url: "{{route('store.get_specification')}}",
              // data: "asset_name_id=" + asset_name_id,
              dataType: 'json',
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function (msg) {

                  var option='';
                  $.each(msg,function(key,value){
                      if(asset_name_id == value['asset_name_id']){
                          option+='<option value="'+value['id']+'" >'+value['specification_name']+'</option>'
                      }
                  });

                  //removing previous data.
                  $('#specification_id').children().remove();
                  $("#specification_id").append('<option value></option>');
                  $("#specification_id").append(option);
              }
          });



      });
  });

</script>



<script type="text/javascript">
    $('#product_modal').on('show.bs.modal',function(e){
      var button = $(e.relatedTarget);
         var asset_name=$("#product_name").val();
            $.ajax({
                type:"POST",
                url: "{{route('store.get_asset_name_id')}}",
                data: "asset_name=" + asset_name,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                
            });
          });
</script>

<script>
    $('#asset_id').change(function(){
        // $('#asset_id').val();
        let asset_name_id = $('#asset_id').val();

        $.ajax({
            type:"get",
            url: "{{route('store.get_specification')}}",
            // data: "asset_name_id=" + asset_name_id,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (msg) {

                var option='';
                $.each(msg,function(key,value){
                    if(asset_name_id == value['asset_name_id']){
                        option+='<option value="'+value['id']+'" >'+value['specification_name']+'</option>'
                    }
                });

                //removing previous data.
                // $('#show_specification').show();
                $('#specification_id').children().remove();
                $("#specification_id").append('<option value></option>');
                $("#specification_id").append(option);
            }
        });
    });
</script>

@endsection('scripts')
