
@extends('layouts.app')
@section('css')
@endsection('css')

@section('page_title')
    Dispatched Products
@endsection


@section('content')


    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <a href="{{route('store.show_dispatch_product')}}" class="btn btn-sm btn-default ">
                                <i class="fa fa-arrow-circle-left"></i>
                                Back
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h3>Product Dispatched Information</h3>
                        <table class="table table-striped table-bordered table-hover" id="group">
                            <thead>
                            <tr>
                                <th>Badge Number</th>
                                <th>Goods Requisition Number</th>
                                <th>Product Name</th>
                                <th>Specification Name</th>
                                <th>Dispatched Quantitiy</th>
                                <th>Dispatched Date</th>
                            </tr>
                            </thead>
                            <tbody >
                            <tr>
                                <td>{{$dispatch->product->batch_number}}</td>
                                <td>{{$dispatch->goods_requisition_product->goods_requisition_form_id}}</td>
                                <td>{{$dispatch->product->asset_name->asset_name}}</td>
                                <td>{{$dispatch->product->specification_name->specification_name}}</td>
                                <td>{{$dispatch->dispatched_qty}}</td>
                                <td>{{date('Y-m-d',strtotime($dispatch->created_at))}}</td>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h3>Hand Over Information</h3>
                        <table class="table table-striped table-bordered table-hover" id="group">
                            <thead>
                            <tr>
                                <th>Custodian Id</th>
                                <th>Custodian Name</th>
                                <th>Hand Over Date</th>
                                <th>Returned Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody >
                            <tr>
                                <td>{{$dispatch->custodian_id}}</td>
                                <td>{{ucfirst($custodian_name->name)}}</td>
                                <td>{{$dispatch->handover_date}}</td>
                                <td>{{$dispatch->returned_date}}</td>
                                <td>
                                    Change Custodian
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                @if(!empty($dispatch->remarks))
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Remarks</label>
                        <p class="form-control">{{$dispatch->remarks}}</p>
                    </div>
                    </div>
                 @endif


            </div>
        </div>
    </div>
@endsection('content')
@section('scripts')

@endsection('scripts')
