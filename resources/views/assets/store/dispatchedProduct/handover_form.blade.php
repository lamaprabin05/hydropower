
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    Product Hand Over
@endsection


@section('content')
    {{--<div class=" portlet light bordered">--}}
        {{--<div class="row">--}}
                {{--<div class="portlet-body col-md-12 col-sm-12">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-3">--}}
                            {{--<div class="form-group">--}}
                                {{--<a href="{{route('store.show_dispatch_product')}}" class="btn btn-sm btn-default ">--}}
                                    {{--<i class="fa fa-arrow-circle-left"></i>--}}
                                    {{--Back--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-6">--}}
                            {{--<h2>Product Hand Over Form</h2>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <a href="{{route('store.show_dispatch_product')}}" class="btn btn-sm btn-default ">
                                <i class="fa fa-arrow-circle-left"></i>
                                Back
                            </a>
                        </div>
                    </div>

                </div>

                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        <form   action="{{route('store.store_handover_form')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <input type="hidden" name="dispatch_product_id" value="{{$dispatch->id}}">
                            <div class="row" >
                                <hr>
                                <div class="col-md-3">
                                   <h4>Product Hand Over Form</h4>
                               </div>
                            </div>

                            <div class="row">
                                <hr>

                                <div class="col-md-3 ">
                                    <div class="form-group">
                                        <label class=" control-label">Handover / Issue Date</label>
                                        <input type="text" name="handover_date" class="form-control datepicker">
                                        @if($errors->has('handover_date'))
                                            <span class="help-block" style="color:red;">
                                                * {{ $errors->first('handover_date') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" control-label">Hand Over To</label>
                                        <select class="form-control input-sm list_asset" name="custodian_id">
                                            <option  value="" ></option>
                                            @foreach($users as $value)
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('custodian_id'))
                                            <span class="help-block" style="color:red;">
                                  * {{ $errors->first('custodian_id') }}
                              </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" control-label">Returned Date</label>
                                        <input type="text" name="returned_date" class="form-control datepicker">

                                        @if($errors->has('returned_date'))
                                            <span class="help-block" style="color:red;">
                                      * {{ $errors->first('returned_date') }}
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class=" control-label">Remarks</label>
                                        <textarea name="remarks" id="" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="submit" value="Submit" class="btn btn-success">
                                    </div>
                                </div>
                            </div>

                        </form>

                    {{--</div>--}}
                {{--</div>--}}

            </div>
        </div>
    </div>
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>


    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();

            $(".list_asset").select2({
                placeholder: "Select",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });
        });


    </script>

@endsection('scripts')
