
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
@endsection('css')

@section('page_title')
    Dispatched Products
@endsection


@section('content')


    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                <table class="table table-striped table-bordered table-hover" id="group">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Badge Number</th>
                            <th>Goods Requisition Number</th>
                            <th>Product Name</th>
                            <th>Specification Name</th>
                            <th>Dispatched Quantitiy</th>
                            <th>Dispatched Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody >
                        @foreach($dispatch as $value)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$value->product->batch_number}}</td>
                                <td>{{$value->goods_requisition_product->goods_requisition_form_id}}</td>
                                <td>{{$value->product->asset_name->asset_name}}</td>
                                <td>{{$value->product->specification_name->specification_name}}</td>
                                <td>{{$value->dispatched_qty}}</td>
                                <td>{{date('Y-m-d',strtotime($value->created_at))}}</td>
                                <td>
                                    @if(empty($value->handover_id))
                                    <a href="{{route('store.show_handover_form',$value->id)}}">
                                        Proceed
                                    </a>
                                    @else
                                    <a href="{{route('store.show_handover_form',$value->id)}}">
                                        View
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });


    </script>

@endsection('scripts')
