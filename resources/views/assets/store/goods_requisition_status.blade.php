
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')
@section('page_title')
    Goods Requisition Files
@endsection
@section('content')
    <div class="portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">
                {{--<table class="table table-striped table-bordered table-advance table-hover" id="sample_1">--}}
                <table class="table table-striped table-bordered table-hover" id="group">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Requisition Number</th>
                        <th>Product Name</th>
                        <th>Product Qty</th>
                        <th>Product Purpose</th>
                        <th>Description</th>
                        <th>Date of Requirement</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody >
                   @foreach($requisitionrequest->requisition as $element)
                        <tr>
                            <td>
                             {{$loop->iteration}}
                            </td>

                            <td>
                               {{$requisitionrequest->requisition_number}}
                            </td>

                            <td>{{$element->name}}</td>

                            <td>
                              
                                {{$element->quantity}}
                                
                            </td>

                            <td>
                              {{$requisitionrequest->purpose}}

                            </td>

                            <td>
                                {{$element->description}}

                            </td>

                            <td>{{$element->requirement_date}}</td>
                            <td>
                                <a class="btn btn-sm btn-success" href="{{route('assets.store.get_all_goods_requisition')}}">
                                    <i class="fa fa-paper-plane"></i> Proceed
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });

    </script>
@endsection('scripts')
