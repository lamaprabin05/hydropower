
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection


@section('content')


    <div class=" portlet light bordered" >
        <div class="row">
            <div class="pull-right">
                 <a href="{{route('assets.store.get_all_goods_requisition')}}"  class="btn btn-sm btn-default left">
                            <i class="fa fa-arrow-circle-left"></i>
                            Back
            </a>
            </div>

            <div class="col-md-12">
             <div class="row text-center">
                <div class="col-md-4">
                 <a href="" style="pa">Running</a>
                </div>

                <div class="col-md-4">
                 <a href="">New Request</a>
                 
                </div>

                <div class="col-md-4">
                 <a href="">Complete</a>
                 
                </div>
             </div>
            </div>
          
        </div>
    </div>
 

<!-- Modal Call -->
<div class="modal fade" id="print_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Enter Disptaching Details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          
            <form class="form form-horizontal" action="" id="driver_form" method="get"   >
                {{ csrf_field() }}
                   
                   

                     <label><b>Quantity In Stock</b></label>
                     <br>
                     
                            
                         <input class="form-control"   value="243"  disabled="" >
                    
                    <label><b>Dispatch Quantity</b></label>
                     <br>
                    
                            
                         <input class="form-control" name="dispatched_qty" placeholder="Enter Dispatching Quantity" required="">
                       
                    <br> 
  
                    
                    
                    <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-sm  btn-info  " value="Submit" id="submit">

            
           </form>
        </div>
      </div>
      
    </div>
  </div>
</div>
<!-- End Modal Call -->


@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
            $('#pagination_search').DataTable();
        });



    </script>

@endsection('scripts')
