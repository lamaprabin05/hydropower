
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class=" portlet light bordered" >
        <div class="row">
            <div class="pull-right">
                 <a href="{{route('assets.store.get_all_goods_requisition')}}"  class="btn btn-sm btn-default left">
                            <i class="fa fa-arrow-circle-left"></i>
                            Back
            </a>
            </div>

            <div class="portlet-body col-md-12 col-sm-12">
                <br>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab2">
                        <table class="table table-striped table-bordered table-hover" id="pagination_search">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Requisition <br>Number</th>
                        <th>Product <br> Name</th>
                        <th>Stock <br> Qty</th>
                        <th>Requested <br>Qty</th>
                        <th>Dispatched <br>Qty</th>
                        <th>Pending <br>Qty</th>
                        <th>Issued <br> Date</th>
                        <th>Required<br>Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody >
                 @foreach($goods_requisition_file as $element)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>
                                {{$element->product_requisition_number}}
                            </td>
                            <td> 
                           {{$element->product_name}}
                            </td>
                            <td>
                                {{$element->product_specification}}
                               
                            </td>

                            <td>
                                {{$element->requested_qty}}
                                                      
                            </td>           

                            <td >
                                {{$element->dispatched_qty}}
                                
                            </td>

                            <td class="text-danger">
                                {{$element->requested_qty-$element->dispatched_qty}}
                            </td>
                            <td>{{\Carbon\Carbon::parse($element->created_at)->diffForHumans()}}</td>
                            <td>{{$element->product->requirement_date}}</td>
                            <td>
                                @if(($element->product->quantity-$element->dispatched_qty)!=0)
                                @if(empty($element->product->name))
                                        @if(($element->product->product->quantity)==0)
                                            @if(($element->request_check_status)==1)
                                                <button class="btn btn-xs btn-success">Request Sent</button>
                                            @else
                                                <a href="#" class="btn btn-danger btn-xs"
                                                   data-toggle="modal"
                                                   data-target="#request_modal"
                                                   data-product_id="{{$element->product->product->id}}",
                                                   data-product_name="{{$element->product->product->name}}"
                                                   data-requisition_number="{{$element->product->goods_requisition_form_id}}",
                                                   data-good_requisition_file_id="{{$element->id}}",
                                                   data-vendor_id="{{$element->product->product->vendor_id}}",
                                                >Request Item</a>
                                            @endif
                                        @else
                                            <a class="btn btn-sm btn-info" href="#"
                                               data-toggle="modal"
                                               data-target="#print_modal"
                                               data-product_id="{{$element->product->product->id}}"
                                               data-goods_requisition_file_id="{{$element->id}}"
                                               data-product_name="{{$element->product->product->name}}"
                                               data-product_quantity="{{$element->product->product->quantity}}"
                                               data-pending_qty="{{$element->requested_qty-$element->dispatched_qty}}"
                                               data-check="product_id"
                                            >
                                                <i class="fa fa-paper-plane"></i>Dispatch
                                                <!-- if all dispatched then  sucess (dispatched all) -->
                                            </a>
                                        @endif
                                @else
                                 <a class="btn btn-sm btn-info" href="#" 
                                    data-toggle="modal"
                                    data-target="#print_modal"
                                    data-check="product_name"
                                    data-goods_requisition_file_id="{{$element->id}}"
                                    data-pending_qty="{{$element->requested_qty-$element->dispatched_qty}}"
                                     >
                                    <i class="fa fa-paper-plane"></i>Dispatch 
                                    <!-- if all dispatched then  sucess (dispatched all) -->
                                </a>
                               
                                @endif
                                    
                                @else

                                <button class="btn btn-sm btn-success">Complete</button>

                                @endif


                                   <!-- Button trigger modal -->
                            </td>
                        </tr>              
                    @endforeach
                    </tbody>
                </table>
                     </div>
          
                     </div>          
            </div>
        </div>
    </div>
 

<!-- Disptach Modal Call -->
<div class="modal fade" id="print_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Enter Disptaching Details</h4>
                
               
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">

                    <form class="form form-horizontal" action="{{route('store.goods_dispatch')}}" id="driver_form" method="post"   >
                        {{ csrf_field() }}

                        <div id="select">
                            <label><b>Choose Product</b></label>
                            <br>
                            <select class="form-control list_asset" name="product_id" id="select_product_id" >
                                <option value=""></option>
                                @foreach($product_quantity as $element)
                                    <option value="{{$element->id}}">{{$element->name}} - ({{$element->quantity}})</option>
                                @endforeach
                            </select>

                            <label><b>Dispatch Quantity</b></label>
                            <br>
                            <input class="form-control" name="dispatched_qty"  placeholder="Enter Dispatching Quantity"  autocomplete="off" id="select_product_qty">
                            <br>
                        </div>

                        <div id="selected">
                            <label><b>Product Name</b></label>
                            <br>
                            <input type="text" class="form-control" readonly   id="product_name">
                            <br>
                            <label><b>Product Quantity Available</b></label>
                            <br>
                            <input type="text" class="form-control" readonly   id="product_quantity">
                            <br>
                            <label><b>Enter Dispatching Quantity</b></label>
                            <br>
                            <input type="text" class="form-control" name="dispatched_qty" placeholder="Enter Dispatching Quantity"  autocomplete="off" id="input_product_qty">

                            <br>
                            <input type="hidden" name="product_id" id='input_product_id'>
                            <input type="hidden" name="pending_qty" id='pending_qty'>

                        </div>
                        <input type="hidden" name="goods_requisition_file_id" id="goods_requisition_file_id">
                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                         {{--<button type="button" class="btn  btn-sm btn-secondary pull-left" data-dismiss="modal">Purchase Form <i class="fa fa-paper-plane"></i></button>--}}
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">
                       

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Modal Call -->


<!-- Request Form Modal Call -->
<div class="modal fade" id="request_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Enter Purchase Details</h4>


                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">

                    <form class="form form-horizontal" action="{{route('store.purchase_form1')}}"  method="post"   >
                        {{ csrf_field() }}

                        <label><b>Product Requirement Description</b></label>
                        <br>
                        <input type="text" class="form-control" name="product_description" placeholder="Enter prodcut description" autocomplete="off">
                        <br>
                        <label><b>Product Qty</b></label>
                        <br>
                        <input type="number" min=0 class="form-control" name="product_qty" placeholder="Enter Product Qty" autocomplete="off">
                        <br>
                        <label><b>Estimated Cost</b></label>
                        <br>
                        <input type="number" min=0 class="form-control" name="estimated_cost" placeholder="Enter Estimated Cost" autocomplete="off">
                        <br>

                        <input type="hidden" value="" name="requisition_number" id="requisition_number">
                        <input type="hidden" value="" name="product_name" id="product_name">
                        <input type="hidden" value="" name="product_id" id="product_id">
                        <input type="hidden" value="" name="good_requisition_file_id" id="good_requisition_file_id">
                        <input type="hidden" value="" name="vendor_id" id="vendor_id">


                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                        {{--<button type="button" class="btn  btn-sm btn-secondary pull-left" data-dismiss="modal">Purchase Form <i class="fa fa-paper-plane"></i></button>--}}
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">


                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Request Form Modal -->
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>


    <script>
        jQuery(document).ready(function() {
            $('#pagination_search').DataTable();
        });
    </script>
<script>

$(".list_asset").select2({
                placeholder: "Select",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });      


</script>

<script>
  $(document).ready(function() {
  $('#print_modal').on('show.bs.modal', function(e) {
    var button     = $(e.relatedTarget);
    var product_id = button.data('product_id');
    var product_name = button.data('product_name');
    var product_quantity = button.data('product_quantity');
    var product_check=button.data('check');
    var goods_requisition_file_id=button.data('goods_requisition_file_id');
    var pending_qty=button.data('pending_qty');
    

    // alert(goods_requisition_file_id);

    if(product_check=='product_id'){
      $('#select').hide();
      $('#selected').show();
      $('#select_product_id').remove();
      $('#select_product_qty').remove();
      
    }else{
      $('#select').show();
      $('#selected').hide();
      $('#input_product_qty').remove();
      $('#input_product_id').remove();
    }

    $("#product_id").val(product_id);
    $("#product_name").val(product_name);
    $("#product_quantity").val(product_quantity);
    $("#input_product_id").val(product_id);
    $("#goods_requisition_file_id").val(goods_requisition_file_id);
    $("#pending_qty").val(pending_qty);

});
});
</script>

    <script>
        $(document).ready(function() {
            $('#request_modal').on('show.bs.modal', function(e) {
                var button     = $(e.relatedTarget);
                var product_id = button.data('product_id');
                var product_name = button.data('product_name');
                var requisition_number = button.data('requisition_number');
                var good_requisition_file_id = button.data('good_requisition_file_id');
                var vendor_id = button.data('vendor_id');

                $("#product_id").val(product_id);
                $("#product_name").val(product_name);
                $("#requisition_number").val(requisition_number);
                $("#good_requisition_file_id").val(good_requisition_file_id);
                $("#vendor_id").val(vendor_id);

            });
        });
    </script>

<script type="text/javascript">
   $('#submit').on('click', function () {
       $( "form" ).submit();
    });
</script>



@endsection('scripts')
