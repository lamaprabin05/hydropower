
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class=" portlet light bordered" >
        <div class="row">
            <div class="pull-right">
                 <a href="{{route('assets.store.get_all_goods_requisition')}}"  class="btn btn-sm btn-default left">
                            <i class="fa fa-arrow-circle-left"></i>
                            Back
            </a>
            </div>

            <div class="portlet-body col-md-12 col-sm-12">
                <br>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab2">
                        <table class="table table-striped table-bordered table-hover" id="pagination_search">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Requisition <br>Number</th>
                        <th>Product <br> Name</th>
                        <th>Product <br> Specification</th>
                        <th>Stock <br>Qty</th>
                        <th>Requested <br>Qty</th>
                        <th>Dispatched <br>Qty</th>
                        <th>Pending <br>Qty</th>
                        <th>Issued <br> Date</th>
                        <th>Required<br>Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody >
                 @foreach($goods_requisition_file as $element)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>
                                {{$element->product_requisition_number}}
                            </td>
                            <td> 
                           {{$element->product->asset_name->asset_name}}
                            </td>
                            <td>
                                {{$element->product->specification_name->specification_name}}
                               
                            </td>
                            <td class="text-danger">{{$element->product->quantity}}</td>

                            <td>
                                {{$element->requested_qty}}
                                                      
                            </td>           

                            <td >
                                {{$element->dispatched_qty}}
                                
                            </td>

                            <td class="text-danger">
                                {{$element->requested_qty-$element->dispatched_qty}}
                            </td>
                            <td>{{\Carbon\Carbon::parse($element->created_at)->diffForHumans()}}</td>
                            
                            <td>
                        {{$element->required_date}}
                            </td>
                        <td>
                            @if(($element->request_check_status)==0)
                            @if(($element->product->quantity)==0)
                           <a href="#" class="btn btn-danger btn-xs"
                                                   data-toggle="modal"
                                                   data-target="#request_modal"
                                                   data-requisition_file_id1="{{$element->id}}",
                                                   data-product_id1="{{$element->product_id}}",
                                                   data-product_name1="{{$element->product->asset_name->asset_name}}"
                                                   data-product_specification1="{{$element->product->specification_name->specification_name}}"
                                                   data-product_availability_qty1="{{$element->product->quantity}}"
                                                   data-product_requisition_number1="{{$element->product_requisition_number}}"
                                                >Request</a>
                            @else
                            <a href="#" class="btn btn-info btn-xs"
                                                   data-toggle="modal"
                                                   data-target="#dispatch_modal"
                                                   data-requisition_file_id="{{$element->id}}",
                                                   data-product_id="{{$element->product_id}}",
                                                   data-product_name="{{$element->product->asset_name->asset_name}}"
                                                   data-product_specification="{{$element->product->specification_name->specification_name}}"
                                                   data-product_availability_qty="{{$element->product->quantity}}"
                                                   data-pending_qty="{{$element->requested_qty-$element->dispatched_qty}}"
                                                >Dispatch</a>

                            @endif
                            @else
                            <button class="btn btn-xs btn-warning">Request Sent</button>
                            @endif

                        </td>
                        </tr>              
                    @endforeach
                    </tbody>
                </table>
                     </div>
          
                     </div>          
            </div>
        </div>
    </div>
 
 <!-- Disptach Modal Call -->
<div class="modal fade" id="dispatch_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Enter Disptaching Details</h4>
                
               
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">

                <form class="form form-horizontal" action="{{route('store.goods_dispatch')}}" id="driver_form" method="post"   >
                        {{ csrf_field() }}
                        <div>
                            <label><b>Product Name</b></label>
                            <br>
                            <input type="text" class="form-control" readonly   id="product_name">
                            <br>
                            <label><b>Product Specification</b></label>
                            <br>
                            <input type="text" class="form-control" readonly   id="product_specification">
                            <br>
                            <label><b>Product Quantity Available</b></label>
                            <br>
                            <input type="text" class="form-control" readonly   id="product_availability_qty">
                            <br>
                            <div id="dispatch_id">
                                <label><b>Enter Dispatching Quantity</b></label>
                            <br>
                            <input type="number" min=0 class="form-control" name="dispatched_qty" placeholder="Enter Dispatching Quantity"  autocomplete="off" id="input_product_qty">
                            </div>
                            <br>
                            <input type="hidden" name="product_id" id='product_id'>
                            <input type="hidden" name="requisition_file_id" id='requisition_file_id'>
                            <input type="hidden" name="pending_qty" id='pending_qty'>

                        </div>

                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Modal Call -->

 <!-- Disptach Modal Call -->
<div class="modal fade" id="request_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Enter Request Details</h4>
                
               
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">

                <form class="form form-horizontal" action="{{route('store.purchase_form1')}}" id="driver_form" method="post"   >
                        {{ csrf_field() }}
                        <div>
                            <label><b>Product Name</b></label>
                            <br>
                            <input type="text" class="form-control" readonly   id="product_name1">
                            <br>
                            <label><b>Product Specification</b></label>
                            <br>
                            <input type="text" class="form-control" readonly   id="product_specification1">
                            <br>
                            <div >
                                <label><b>Enter Requested Quantity</b></label>
                            <br>
                            <input type="number" min=0 class="form-control" name="requested_qty1" placeholder="Enter Requesting Quantity"  autocomplete="off" >
                            </div>
                            <br>

                            <div >
                                <label><b>Estimated Cost</b></label>
                            <br>
                            <input type="number" min=0 class="form-control" name="estimated_cost1" placeholder="Enter Estimated Quantity"  autocomplete="off" >
                            </div>
                            <br>
                            <label><b>Product Requirement Description</b></label>
                            <br>
                             <input type="text" class="form-control" name="product_description1" placeholder="Enter prodcut description" autocomplete="off">
                            <br>
                            <br>
                            <input type="hidden" name="product_id1" id='product_id1'>
                            <input type="hidden" name="requisition_file_id1" id='requisition_file_id1'>
                            <input type="hidden" name="product_requisition_number1" id='product_requisition_number1'>


                        </div>

                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Modal Call -->


@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>


    <script>
        jQuery(document).ready(function() {
            $('#pagination_search').DataTable();
        });
    </script>
<script>

$(".list_asset").select2({
                placeholder: "Select",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });      


</script>

<script type="text/javascript">
   $('#submit').on('click', function () {
       $( "form" ).submit();
    });
</script>

<script>
        $(document).ready(function() {
            $('#dispatch_modal').on('show.bs.modal', function(e) {
                var button     = $(e.relatedTarget);
                var product_id = button.data('product_id');
                var requisition_file_id = button.data('requisition_file_id');
                var product_name = button.data('product_name');
                var product_availability_qty = button.data('product_availability_qty');
                var product_specification = button.data('product_specification');
                var pending_qty = button.data('pending_qty');
            

                $("#product_id").val(product_id);
                $("#requisition_file_id").val(requisition_file_id);
                $("#product_name").val(product_name);
                $("#product_availability_qty").val(product_availability_qty);
                $("#product_specification").val(product_specification);
                $("#pending_qty").val(pending_qty);

                
            });
        });
</script>

<script>
        $(document).ready(function() {
            $('#request_modal').on('show.bs.modal', function(e) {
                var button     = $(e.relatedTarget);
                var product_id1 = button.data('product_id1');
                var requisition_file_id1 = button.data('requisition_file_id1');
                var product_name1 = button.data('product_name1');
                var product_specification1 = button.data('product_specification1');
                var product_requisition_number1 = button.data('product_requisition_number1');

                $("#product_id1").val(product_id1);
                $("#requisition_file_id1").val(requisition_file_id1);
                $("#product_name1").val(product_name1);
                $("#product_specification1").val(product_specification1);
                $("#product_requisition_number1").val(product_requisition_number1);
                
               

                
            });
        });
</script>



@endsection('scripts')
