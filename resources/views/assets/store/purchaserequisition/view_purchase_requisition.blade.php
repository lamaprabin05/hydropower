
@extends('layouts.app')
@section('css')
@endsection('css')

@section('page_title')
    View Purchase Requisition
@endsection


@section('content')


    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <a href="{{route('store.show_purchase_requisition')}}" class="btn btn-sm btn-default ">
                                <i class="fa fa-arrow-circle-left"></i>
                                Back
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h3>Goods Purchase Information</h3>
                        <table class="table table-striped table-bordered table-hover" id="group">
                            <thead>
                            <tr>
                                <th>Goods Requisition Number</th>
                                <th>Product Name</th>
                                <th>Specification Name</th>
                                <th>Requested Quantitiy</th>
                                <th>Created Date</th>
                            </tr>
                            </thead>

                            <tbody >
                            <tr>
                                <td>{{$files->goods_requisition_number}}</td>
                                <td>{{$files->assets_name}}</td>
                                <td>{{$files->specification_name}}</td>
                                <td>{{$files->quantity}}</td>
                                <td>{{date('Y-m-d',strtotime($files->created_at))}}</td>
                            </tr>


                            </tbody>
                        </table>
                    </div>
                </div>

                {{--requestor remarks--}}
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover" id="group">
                            <tbody>

                            <tr >
                                <th colspan="2">Requestor Remarks:</th>
                                <td rowspan="2" colspan="3" style="width: 50%;" >{{$files->requestor_remarks}}</td>
                            </tr>

                            <tr >
                                <td colspan="1">Requested By</td>
                                <td colspan="1">{{ucfirst($files->requisition_user->name)}}</td>
                                {{--                                <td rowspan="2" colspan="3" >{{$files->requestor_remarks}}</td>--}}
                            </tr>

                            {{--<tr>--}}
                                {{--<td colspan="2" style="color:red; text-align: center;">File Proceeded to {{ucfirst($files->proceeded_to)}} </td>--}}
                            {{--</tr>--}}
                            </tbody>
                        </table>
                    </div>
                </div>
                {{--end requestor remarks--}}

                @if($files->checked_by != null)
                {{--checker remarks--}}
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover" id="group">
                            <tbody>

                            <tr >
                                <th colspan="2">Checker Remarks:</th>
                                @if($files->checker_remarks == null)
                                    <td rowspan="2" colspan="3" style="width: 50%;" >No Remarks Added</td>
                                @else
                                    <td rowspan="2" colspan="3" style="width: 50%;" >{{$files->checker_remarks}}</td>
                                @endif
                            </tr>

                            <tr >
                                <td colspan="1">Checked By</td>
                                <td colspan="1">{{$files->checker_user->name}}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                {{--end checker remarks--}}
                @endif

                @if($files->approved_by != null)
                {{--checker remarks--}}
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover" id="group">
                            <tbody>

                            <tr >
                                <th colspan="2">Approver Remarks:</th>
                                @if($files->approver_remarks == null)
                                    <td rowspan="2" colspan="3" style="width: 50%;" >No Remarks Added</td>
                                @else
                                    <td rowspan="2" colspan="3" style="width: 50%;" >{{$files->approver_remarks}}</td>
                                @endif
                            </tr>

                            <tr >
                                <td colspan="1">Approved By</td>
                                <td colspan="1">{{$files->approver_user->name}}</td>
                                {{--                                <td rowspan="2" colspan="3" >{{$files->requestor_remarks}}</td>--}}
                            </tr>

                            {{--<tr>--}}
                            {{--<td colspan="2" style="color:red; text-align: center;">File Proceeded to {{ucfirst($files->proceeded_to)}} </td>--}}
                            {{--</tr>--}}
                            </tbody>
                        </table>
                    </div>
                </div>
                {{--end checker remarks--}}
                @endif

                <div class="alert alert-warning">
                    Purchase Requisition File has been proceeded to {{ucfirst($files->proceeded_to)}}
                </div>

            </div>
        </div>
    </div>
@endsection('content')
@section('scripts')

@endsection('scripts')
