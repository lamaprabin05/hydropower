@extends('layouts.app')

@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
@endsection('css')
@section('page_title')
    View Goods Requisition
@endsection('page-title')

@section('content')
    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                <div class="row">
                    <div class="col-md-12">
                        <hr>
                        <h2>Purchase Requisition Form</h2>
                        <hr>

                        <table id="myTable" class=" table table-bordered order-list">
                            <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Requisition Number</th>
                                <th>Product Name</th>
                                <th>Specification Name</th>
                                <th>Required Date</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    1
                                </td>

                                <td>
                                    <p class="form-control">{{$product->goods_requisition_form_id}}</p>

                                </td>

                                <td>
                                    {{--<input type="text" name="assets" class="form-control" value="{{$product->goods_requisition_form_id}}" style="border:none;" readonly="">--}}
                                    <p class="form-control">{{$product->asset_name->asset_name}}</p>
                                </td>

                                <td>
                                    <p class="form-control">{{$product->specification_name->specification_name}}</p>

                                </td>

                                <td >
                                    <p class="form-control">{{date('Y-m-d',strtotime($product->requirement_date))}}</p>
                                </td>


                            </tr>

                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>
                </div>


                <form  action="{{route('store.store_purchase_form')}}" id="driver_form" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="goods_requisition_product_id" value="{{$product->id}}">
                    <div class="row">
                        <hr>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="control-label">Quantity</label>
                                <input type="number" min=0 name="quantity" class="form-control" placeholder="Please Enter quantity">

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="control-label">Requestor Remarks</label>
                                <textarea name="requestor_remarks" class="form-control" id="" cols="30" rows="7" style="width:50%;"  ></textarea>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">

                            <div class="form-group">
                                <label for="" class="control-label">Proceed To Checker</label>

                                    <select class="form-control input-sm list_asset" name="user_id">
                                        <option  value="" ></option>
                                        @foreach($checker_list as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="submit" class="btn btn-sm btn-success pull-left"  name="" value="submit" >
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
</div>



    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="alert alert-warning">--}}
                {{--Goods Requisition File has been proceeded to--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}



@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            $(".list_asset").select2({
                placeholder: "Select",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });


        });
    </script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>

@endsection('scripts')
