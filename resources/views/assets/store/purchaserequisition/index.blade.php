
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

@section('content')


    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">
                <hr>
                <h2>Purchase Requisition Files</h2>
                <hr>
                <table class="table table-striped table-bordered table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>
                        <th>
                            Requisition Number
                        </th>
                        <th>
                            Product Name
                        </th>

                        <th>
                            Specification
                        </th>

                        <th>
                            Quantity
                        </th>

                        <th>
                            Action
                        </th>
                    </tr>
                    </thead>
                    <tbody >
                    @foreach($files as $value)
                        <tr>
                            <td>
                                {{$loop->iteration}}
                            </td>

                            <td>
                                {{$value->goods_requisition_number}}
                            </td>

                            <td>
                                {{$value->assets_name}}
                            </td>

                            <td>
                                {{$value->specification_name}}
                            </td>

                            <td>
                                {{$value->quantity}}
                            </td>

                            <td>
                                {{--<a href="#" class="btn btn-primary btn-xs">Update Product</a>--}}
                                <a href="{{route("store.view_purchase_requisition",$value->id)}}" class="btn btn-primary btn-xs"> View Purchase Requisition</a>
                                @if($value->form_status == 4)
                                <a href="{{route("store.add_purchase_product",$value->id)}}" class="btn btn-success btn-xs"> <i class="fa fa-plus-circle"></i> Add Product</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });



    </script>


@endsection('scripts')
