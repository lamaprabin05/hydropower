@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    View Goods Requisition
@endsection('page-title')

@section('content')
    <div class="portlet light bordered">

        <div class="row">
            <div class="col-md-12">
                    <div class="form-group">
                        <a href="{{route('assets.checker.get_all_goods_requisition')}}"  class="btn btn-sm btn-default ">
                            <i class="fa fa-arrow-circle-left"></i>
                            Back
                        </a>
                    </div>
            </div>

            <div class="col-md-12">
                <div class="col-md-12 col-md-offset-4">
                    <h3>Vision Energy and Power Pvt. Ltd. </h3> </br>
                    <h4>New Baneshwor, Kathmandu</h4>
                    </br>
                </div>
            </div>
            <br>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table id="myTable" class=" table table-bordered order-list">
                    <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Product Name</th>
                        <th>Product Description</th>
                        <th>Quantity</th>
                        <th>Date of Requirement</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($form->requisition as $value)
                        <tr>
                            <td class="col-sm-1">
                                <p>{{$loop->iteration}}</p>
                            </td>

                            <td class="col-sm-4">
                                </p>
                                @if(empty($value->name))
                                   <p>{{$value->product->name}}</p>
                                @else
                                   <p>{{$value->name}}</p>
                                
                                @endif
                            </td>

                            <td class="col-sm-3">
                                <p>{{$value->description}}</p>
                            </td>

                            <td class="col-sm-2">
                                <p>{{$value->quantity}}</p>
                            </td>

                            <td class="col-sm-3">
                                <p>{{$value->requirement_date}}</p>
                            </td>

                            <td>
                                @if($value->is_rejected == 0)
                                    <i  style="font-size: 20px; color: green; " class="fa fa-check-circle"></i>
                                @else
                                    <i style="font-size: 20px; color: red;" class="fa fa-times-circle"></i>
                                @endif
                            </td>

                        </tr>
                    @endforeach

                    <tr>
                        <td colspan="2" style="text-align: center;">Purpose of such Goods/Service:</td>
                        <th colspan="4" style="text-align: center;">{{ucfirst($form->purpose)}}</th>
                    </tr>

                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>

        @if($form->reason)
            <div class="row">
                    <div class="col-md-12">
                        <h4>Reason for Requisition:</h4>
                    </div>

                <div class="col-md-12">
                        <div class="form-group">
                            <table class=" table table-bordered order-list">
                                <tr>
                                    <td>{{$form->reason}}</td>
                                </tr>
                            </table>
                        </div>
                </div>

            </div>
        @endif

 @if($form->form_status == 2)
    <div class="row">
        <div class="col-md-12">
        <form  method="post" action="{{route('assets.approver.change_goodsrequisition_status')}}">
            @csrf
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <h4><b>Approver Remarks</b></h4>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea name="approver_remarks" id="" cols="30" rows="7" style="width:50%;" autofocus=""></textarea>
                        </div>
                    </div>

                </div>
            </div>
               
        
        </div>
            </div>
        <input type="hidden" name="form_id" value="{{$form->id}}">
        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
         <input type="submit" class="btn btn-sm btn-success pull-left"  name="" value="Procceed" required="">
       </form>
       <hr style="margin-top:50px;">  
       @if($form->form_status == 2)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                     
                        <a href="{{ route('assets.approver.edit_goods_purchase_requisition',$form->id) }}" class="btn btn-sm blue button-next">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a href="{{ route('assets.approver.get_reject_form',$form->id) }}" class="btn btn-sm red button-next">
                            <i class="fa fa-times"></i>
                            Reject
                        </a>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-warning">{{$form->status}}
                        Goods Requisition File has been proceeded to {{ucfirst($form->proceeded_to)}}
                    </div>
                </div>
            </div>
        @endif               
    </div>
    <br>

@endif

        


        {{--show only if checker edit this form--}}
        @if($form->log_goods_requisition->count() != 0)
            <table id="myTable" class=" table table-bordered order-list">
                <thead>

                <tr>
                    <th colspan="6" style="color:red;">Log History of Goods Requisition Form</th>
                </tr>

                <tr>
                    <th colspan="6" style="text-align: center">Goods or Service Requisition Form</th>
                </tr>

                <tr>
                    <th>S.No.</th>
                    <th>Product Name</th>
                    <th>Product Description</th>
                    <th>Quantity</th>
                    <th>Date of Requirement</th>
                    <th>Updated By</th>
                </tr>
                </thead>
                <tbody>

                @foreach($log as $value)

                    <tr>
                        <td class="col-sm-1">
                            <p>{{$loop->iteration}}</p>
                        </td>

                        <td class="col-sm-3">
                            @if(empty($value->name))
                                <p>{{$value->product['name']}}</p>
                            @else
                                <p>{{$value->name}}</p>
                            @endif
                        </td>

                        <td class="col-sm-3">
                            <p>{{$value->description}}</p>
                        </td>

                        <td class="col-sm-2">
                            <p>{{$value->quantity}}</p>
                        </td>

                        <td class="col-sm-2">
                            <p>{{$value->requirement_date}}</p>
                        </td>

                        <td class="col-sm-3">
                            {{$user[$value->updated_by]}}
                        </td>


                    </tr>

                @endforeach

                </tbody>
                <tfoot>

                </tfoot>
            </table>
        @endif



    </div>

@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection('scripts')
