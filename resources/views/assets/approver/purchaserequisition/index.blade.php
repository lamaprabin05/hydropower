
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

{{--@section('page-title')--}}
{{--Goods Requisition Files--}}
{{--@endsection('page-title')--}}


@section('content')


    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                {{--<table class="table table-striped table-bordered table-advance table-hover" id="sample_1">--}}
                <table class="table table-striped table-bordered table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>
                        <th>
                            Requisition Number
                        </th>
                        <th>Specification Name</th>
                        <th>
                           Quantity
                        </th>

                        <th>
                           Date Of Issue
                        </th>

                        <th>
                           Action
                        </th>
                    </tr>
                    </thead>
                    <tbody >
                        @foreach($goods_purchase_form as $value)
                        <tr>
                            <td>
                                {{$loop->iteration}}
                            </td>
                            <td>
                                {{$value->goods_requisition_number}}
                            </td>
                            <td>
                                {{$value->specification_name}}
                            </td>
                            <td>
                               {{$value->quantity}}
                            </td>
                            <td>
                                {{$value->created_at}}
                            </td>
                            <td>
                                 <a class="btn default btn-xs blue-stripe" href="{{route('assets.approver.view_goods_purchase_requisition',['id' => $value->id]) }}">
                                    <i class="fa fa-paper-plane"></i> View and Proceed
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });



    </script>


@endsection('scripts')
