@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    View Goods Requisition
@endsection('page-title')

@section('content')
    <div class="portlet light bordered">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <a href="{{route('assets.approver.fetch_goods_purchase_requisition')}}"  class="btn btn-sm btn-default ">
                        <i class="fa fa-arrow-circle-left"></i>
                        Back
                    </a>
                </div>
            </div>

            <div class="col-md-12">
                <h3>Purchase Requisition  </h3>
                <hr>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <table id="myTable" class=" table table-bordered order-list">
                    <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Requisition Number</th>
                        <th>Specification Name</th>
                        <th>Quantity</th>
                        <th>Date Of Issue</th>
                    </tr>
                    </thead>
                    <tbody>


                        <tr>
                            <td class="col-sm-1">
                                <p>1</p>
                            </td>
                            <td>{{$viewGoodsPurchaseRequisiton->goods_requisition_number}}</td>

                            <td class="col-sm-3">
                               {{$viewGoodsPurchaseRequisiton->specification_name}}
                        
                            </td>
                            <td class="col-sm-3">
                                <p>{{$viewGoodsPurchaseRequisiton->quantity}}</p>
                            </td>


                            <td class="col-sm-3">
                                <p>{{date('Y-m-d',strtotime($viewGoodsPurchaseRequisiton->created_at))}}</p>
                            </td>


                        </tr>

                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>

        <form  method="post" action="{{route('assets.approver.proceed_purchase_requisition')}}">
            @csrf
            <input type="hidden" name="purchase_form_id" value="{{$viewGoodsPurchaseRequisiton->id}}">


            <div class="row">
                <hr>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="" class="control-label">Approver Remarks</label>
                        <textarea name="approver_remarks" class="form-control" id="" cols="30" rows="7" style="width:50%;"  ></textarea>
                        @if($errors->has('approver_remarks'))
                            <span class="help-block" style="color:red;">
                                      * {{ $errors->first('approver_remarks') }}
                            </span>
                        @endif
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">

                        <input type="submit" class="btn btn-sm btn-success "  name="" value="Proceed" required="">

                        {{--<a href="{{route('assets.approver.edit_goods_purchase_requisition',['goods_requisition_product_id' => $viewGoodsPurchaseRequisiton->id])}}" class="btn btn-sm blue button-next">--}}
                            {{--<i class="fa fa-edit"></i>--}}
                            {{--Edit--}}
                        {{--</a>--}}


                        <a href="#" class="btn btn-danger btn-sm"
                           data-toggle="modal"
                           data-target="#reject_purchase_file"

                        >Reject</a>
                    </div>
                </div>
            </div>
       </form>



    </div>

    @include('assets.approver.purchaserequisition.modal')

@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
     <script>
        $(document).ready(function() {
            $('#reject_purchase_file').on('show.bs.modal', function(e) {
                var button     = $(e.relatedTarget);
            });
        });
    </script>
@endsection('scripts')
