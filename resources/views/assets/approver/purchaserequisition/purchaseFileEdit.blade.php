@extends('layouts.app')

@section('css')
@endsection('css')

@section('page_title')
    Edit Goods Requisition Form
@endsection('page-title')

@section('content')
    <div class="portlet light bordered">

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <a href="{{route('assets.approver.fetch_goods_purchase_requisition')}}"  class="btn btn-sm btn-default ">
                        <i class="fa fa-arrow-circle-left"></i>
                        Back
                    </a>
                </div>
            </div>



            <div class="col-md-12 col-md-offset-4">
                <h3>Vision Energy and Power Pvt. Ltd. </h3> </br>
                <h4>New Baneshwor, Kathmandu</h4>
                </br>
            </div>

            <br>
        </div>

            @if($errors->any())
                <span class="help-block" style="color:red;">
                    * {{ $errors->first() }}
                </span>
            @endif
            <table id="myTable" class=" table table-bordered order-list">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>Goods Requisition Number</th>
                    <th>Specification Name</th>
                    <th>Quantity</th>
                    <th>Requested Date</th>
                </tr>
                </thead>
                <tbody>
                <form action="{{route('assets.approver.update_goods_purchase_requisition')}}" method="post" >
                    @csrf
                    <div class="form-actions">
                        <tr>
                            <td>1</td>
                            <td><input type="text" class="form-control"   value="{{$editGoodsPurchaseRequisiton->goods_requisition_number}}" readonly=""></td>
                            <td>
                                 <input type="text" class="form-control"   value="{{$editGoodsPurchaseRequisiton->specification_name}}" readonly="">
                            </td>
                            <td>
                                 <input type="text" class="form-control"  name="quantity" value="{{$editGoodsPurchaseRequisiton->quantity}}">
                            </td>

                            <td>
                                <input type="text" class="form-control"  value="{{$editGoodsPurchaseRequisiton->created_at}}" readonly="">

                            </td>
                        </tr>
                    </div>






                </tbody>
            </table>
        <input type="hidden" name="goods_purchase_form_id" value="{{$editGoodsPurchaseRequisiton->id}}">
                <div class="form-actions text-center">
                    <button type="reset" class="btn btn-sm" id="cancel">Cancel</button>
                    <button type="submit" class="btn btn-sm green" id="submit">Update</button>
                </div>

        </form>

    </div>



@endsection('content')
@section('scripts')

    <script>



            //end purpose show or hide

            //date picker
            $(".datepicker").datepicker({
                dateFormat:"yy-mm-dd",
                changeMonth:true,
                changeYear:true
            });
            //end date picker


        });



    </script>
@endsection('scripts')
