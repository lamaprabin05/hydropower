@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}"/>

@endsection('css')

@section('page_title')
    Reject Goods Requisition Form
@endsection('page_title')

@section('content')
    <div class="portlet light bordered">

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <a href="{{route('assets.checker.view_goods_requisition',$form->id)}}"  class="btn btn-sm btn-default ">
                        <i class="fa fa-arrow-circle-left"></i>
                        Back
                    </a>
                </div>
            </div>

            <div class="col-md-3 col-md-offset-6 text-right">
                <div class="form-group">
                        <span><label for="">Requisition Number: {{$form->requisition_number}}</label>
                         </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-md-offset-4">
                <h3>Vision Energy and Power Pvt. Ltd. </h3> </br>
                <h4>New Baneshwor, Kathmandu</h4>
                </br>
            </div>
            <hr>
            <br>
        </div>

        <form action="{{ route('assets.checker.store_reject_form') }}" method="POST" >
            @csrf

            @if($errors->any())
                <span class="help-block" style="color:red;">
                    * {{ $errors->first() }}
                </span>
            @endif

            <table id="myTable" class="table table-bordered order-list">
                <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Product Name</th>
                    <th>Product Description</th>
                    <th>Quantity</th>
                    <th>Date of Requirement</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>

                @foreach($form->requisition as $value)
                    <tr>
                        <input type="hidden" class="product_id" name="product_id{{ $loop->iteration}}" value="{{$value->id}}">

                        <td class="col-sm-1">
                            {{--<p>{{$loop->iteration}}</p>--}}
                            <input type="text" value="{{$loop->iteration}}" class="form-control" readonly >
                        </td>

                        <td class="col-sm-4">
                            <input type="text" id="test" name="name{{ $loop->iteration }}" value="{{$value->name}}" required  class="form-control"/>

                        </td>

                        <td class="col-sm-3">
                            <input type="text" name="description{{$loop->iteration}}" value="{{$value->description}}"  required  class="form-control"/>
                        </td>

                        <td class="col-sm-2">
                            <input type="number" name="quantity{{ $loop->iteration }}" value="{{$value->quantity}}" required  class="form-control"/>
                        </td>

                        <td class="col-sm-3">
                            <input type="text" name="requirement_date{{ $loop->iteration }}" value="{{$value->requirement_date}}" required class="form-control datepicker"/>{{($value->is_rejected == 2) }}
                        </td>

                        <td>
                            <div class="col-sm-3">
                                <input type="checkbox" name="switch{{$loop->iteration}}" class="make-switch" @if($value->is_rejected == 0)  checked @endif data-on="success" data-off="warning">
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>

            <input type="hidden" name="requisition_id" value="{{$form->id}}">

        {{--reason row--}}
        <div class="row">
            <div class="col-md-12">
                <h4>Reject Reason: <span style="color:red;">*</span></h4>
            </div>
        </div>

        <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <textarea class="form-control" name="reject_reason"  rows="7" ></textarea>
                    </div>

                </div>
        </div>
        {{-- end reason row--}}

        <div class="row">
            <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn green button-next">
                            <i class="fa fa-check"></i>
                            Submit
                        </button>
                    </div>
            </div>
        </div>

    </form>
    </div>

@endsection('content')
@section('scripts')
    <script src="{{ asset(STATIC_DIR.'assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>


    <script>

        $(document).ready(function () {
            let counter = 1;

            $('#getCounter').val(counter);

            //purpose show or shide
            if($('#purpose').val() == 'others'){
                $("#others").show();
            }

            $('#purpose').on('change', function(e){
                let purpose = $('#purpose').val();

                if(purpose == 'others'){
                    $("#others").show();
                }
                else{
                    $("#others").hide();
                }


            });
            //end purpose show or hide

            //date picker
            $(".datepicker").datepicker({
                dateFormat:"yy-mm-dd",
                changeMonth:true,
                changeYear:true
            });
            //end date picker


        });



    </script>
@endsection('scripts')
