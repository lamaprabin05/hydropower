

@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    View Goods Requisition
@endsection('page-title')

@section('content')
    <div class="portlet light bordered">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <a href="{{route('assets.checker.get_all_goods_requisition')}}"  class="btn btn-sm btn-default ">
                        <i class="fa fa-arrow-circle-left"></i>
                        Back
                    </a>
                </div>
            </div>

            <div class="col-md-12">
                <div class="col-md-12 col-md-offset-4">
                    <h3>Vision Energy and Power Pvt. Ltd. </h3> </br>
                    <h4>New Baneshwor, Kathmandu</h4>
                    </br>
                </div>
            </div>
            <br>
        </div>


        <div class="row">
            <div class="col-md-12">
                <table id="myTable" class=" table table-bordered order-list">
                    <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Product Name</th>
                        <th>Product Qty</th>
                        <th>Requested Date</th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($purchasing_view as $value)
                        <tr>
                            <td class="col-sm-1">
                                <p>{{$loop->iteration}}</p>
                            </td>

                            <td class="col-sm-4">

                              <p>{{$loop->product_name}}</p>
                            </td>

                            <td class="col-sm-3">
                                <p>{{$value->product_qty}}</p>
                            </td>

                            <td class="col-sm-3">
                                <p>{{$value->created_at}}</p>
                            </td>

                        </tr>
                    @endforeach

                    <tr>
                        <td colspan="2" style="text-align: center;">Purpose of such Goods/Service:</td>
                        <th colspan="3" style="text-align: center;">{{--{{ucfirst($form->purpose)}}--}}</th>
                    </tr>

                    <tr>
                        <th colspan="5">Uploaded Goods Requisition</th>
                    </tr>

                    <tr>
                        <td colspan="5">
                            <img src="{{ asset(STATIC_DIR.'storage/'.$value->purchase_file_scan) }}" alt="Goods Purchase File" class="img-responsive img-rounded" width="250">
                        </td>
                    </tr>


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>

        @if($form->reason)
            <div class="row">
                <div class="col-md-12">
                    <h4>Reason for Requisition:</h4>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <table class=" table table-bordered order-list">
                            <tr>
                                <td>{{--{{$form->reason}}--}}</td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{--<a href="#uploadGoodsRequisition" class="btn green button-next" data-toogle="modal">--}}

                    {{--</a>--}}

                    <a  href="#uploadGoodsRequisition" class="btn btn-sm green button-next"  data-toggle="modal" >
                        <i class="fa fa-check"></i>
                        Proceed
                    </a>

                    {{--<a href="{{ route('assets.checker.get_edit_form',$form->id) }}" class="btn btn-sm blue button-next">--}}
                        <i class="fa fa-edit"></i>
                        Edit
                    </a>

                    {{--<a href="{{ route('assets.checker.get_reject_form',$form->id) }}" class="btn btn-sm red button-next">--}}
                        <i class="fa fa-times"></i>
                        Reject
                    </a>
                </div>
            </div>
        </div>

    </div>
    @include('assets.checker.goodsrequisition.modal')

@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection('scripts')
