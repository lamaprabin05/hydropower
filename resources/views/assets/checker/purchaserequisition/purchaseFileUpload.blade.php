@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    View Goods Requisition
@endsection('page-title')

@section('content')
    <div class="portlet light bordered">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <a href="#"  class="btn btn-sm btn-default ">
                        <i class="fa fa-arrow-circle-left"></i>
                        Back
                    </a>
                </div>
            </div>

            <div class="col-md-12">
                <div class="col-md-12 col-md-offset-4">
                    <h3>Vision Energy and Power Pvt. Ltd. </h3> </br>
                    <h4>New Baneshwor, Kathmandu</h4>
                    </br>
                </div>
            </div>
            <br>
        </div>


        <div class="row">
            <div class="col-md-12">
                <table id="myTable" class=" table table-bordered order-list">
                    <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Product Name</th>
                        <th>Product Description</th>
                        <th>Quantity</th>
                        <th>Requested Date</th>
                    </tr>
                    </thead>
                    <tbody>


                        <tr>
                            <td class="col-sm-1">
                                <p>1</p>
                            </td>

                            <td class="col-sm-3">
                                jhjk
                            </td>

                            <td class="col-sm-3">
                                dgh
                            </td>

                            <td class="col-sm-3">
                                gdsag
                            </td>



                            <td class="col-sm-3">
                               gdsa
                            </td>


                        </tr>




                    <tr>
                        <th colspan="5">Uploaded Purchase Requisition</th>
                    </tr>

                    <tr>
                        <td colspan="5">
                            <img src="" alt="Goods Requisition" class="img-responsive img-rounded" width="250">
                        </td>
                    </tr>


                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="form-group">

                    <a  href="#uploadGoodsRequisition" class="btn btn-sm green button-next"  data-toggle="modal" >
                        <i class="fa fa-check"></i>
                        Proceed
                    </a>

                    <a href="{{--{{ route('assets.checker.get_edit_form',$form->id) }}--}}" class="btn btn-sm blue button-next">
                        <i class="fa fa-edit"></i>
                        Edit
                    </a>

                    <a href="{{--{{ route('assets.checker.get_reject_form',$form->id) }}--}}" class="btn btn-sm red button-next">
                        <i class="fa fa-times"></i>
                        Reject
                    </a>
                </div>
            </div>
        </div>

    </div>
    @include('assets.checker.goodsrequisition.modal')

@endsection('content')
@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection('scripts')
