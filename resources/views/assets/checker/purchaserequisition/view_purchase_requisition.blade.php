@extends('layouts.app')

@section('css')

@endsection('css')

@section('page_title')
    View Purchase  Requisition
@endsection('page-title')

@section('content')
    <div class="portlet light bordered">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <a href="{{route('assets.checker.purchase_requisition')}}"  class="btn btn-sm btn-default ">
                        <i class="fa fa-arrow-circle-left"></i>
                        Back
                    </a>
                </div>
            </div>

                <div class="col-md-12">
                    <h3>Purchase Requisition  </h3> </br>

                    </br>
                </div>
            <br>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table id="myTable" class=" table table-bordered order-list">
                    <thead>
                    <tr>
                        <th>Goods Requisition Number</th>
                        <th>Product Name</th>
                        <th>Specification Name</th>
                        <th>Quantity</th>
                        <th>Requested Date</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td class="col-sm-2">{{$goods_purchase_form->goods_requisition_number}}</td>

                            <td class="col-sm-3">
                               {{$goods_purchase_form->specification_name}}
                            </td>

                            <td class="col-sm-3">
                                {{$goods_purchase_form->quantity}}
                            </td>

                            <td class="col-sm-2">
                                {{date('Y-m-d',strtotime($goods_purchase_form->created_at))}}
                            </td>

                            <td>
                                {{date('Y-m-d',strtotime($goods_purchase_form->created_at))}}
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>
 
            <form  method="post" action="{{ route('assets.checker.purchase_file_proceed') }}">
                @csrf
                <input type="hidden" name="purchase_form_id" value="{{$goods_purchase_form->id}}">

                <div class="row">
                    <hr>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Checker Remarks</label>
                            <textarea name="checker_remarks" class="form-control" id="" cols="30" rows="7" style="width:50%;"  ></textarea>
                            @if($errors->has('checker_remarks'))
                                <span class="help-block" style="color:red;">
                                      * {{ $errors->first('checker_remarks') }}
                                  </span>
                            @endif
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">

                        <div class="form-group">
                            <label for="" class="control-label">Proceed To Approver</label>

                            <select class="form-control input-sm list_asset" name="approver_id">
                                <option  value="" ></option>
                                @foreach($approver_list  as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach

                            </select>
                            @if($errors->has('approver_id'))
                                <span class="help-block" style="color:red;">
                                      * {{ $errors->first('approver_id') }}
                                  </span>
                            @endif
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-sm btn-success "  name="" value="Proceed" required="">
                            <a href="{{ route('assets.checker.purchase_file_edit',$goods_purchase_form->id) }}" class="btn btn-sm blue button-next">
                                <i class="fa fa-edit"></i>
                                Edit
                            </a>
                            <a href="#" class="btn btn-danger btn-sm"
                               data-toggle="modal"
                               data-target="#reject_purchase_file"

                            >Reject</a>

                        </div>
                    </div>
                </div>


           </form>

    </div>

    @include('assets.checker.purchaserequisition.reject_modal')

@endsection('content')
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#reject_purchase_file').on('show.bs.modal', function(e) {
                var button     = $(e.relatedTarget);
            });
        });
    </script>
@endsection('scripts')
