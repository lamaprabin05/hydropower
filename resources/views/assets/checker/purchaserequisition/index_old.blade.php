@extends('layouts.app')

@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
@endsection('css')
@section('page_title')
    View Goods Requisition
@endsection('page-title')

@section('content')
    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">
            <form class="form form-horizontal" action="{{route('store.purchase_form')}}" id="driver_form" method="post">
                {{ csrf_field() }}
                <table id="myTable" class=" table table-bordered order-list">
                    <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Requisition Number</th>
                        <th>Specification Name</th>
                        <th>Quantity</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                               1
                            </td>

                            <td>
                     <input type="text" name="goods_requisition_number" class="form-control" value="1234" style="border:none;" readonly="">
                            </td>

                            <td>
                               <input type="text" name="specification_name" class="form-control" value="A4-paper" style="border:none;" readonly="">
                            </td>

                            <td >
                                <input type="text" name="quantity" class="form-control" value="123" >
                            </td>

                            
                        </tr>
                  
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>

            <div class="row">
                    <div class="col-md-12">
                        <h4>Checker Remarks</h4>
                        <textarea name="requestor_remarks" id="" cols="30" rows="7" style="width:50%;" required=""></textarea>
                    </div>
            </div>
   
    <div class="row">
        <div class="col-md-12">
         <label ><b>Procced To Approver</b></label>
        <div class="form-group">
               <div class="col-md-3">
                    <select class="form-control input-lg list_asset" name="user_id">
                       <option  value="" disabled=""></option>
                       @foreach($checker_list as $value)
                        <option value="{{$value->id}}">{{$value->name}}</option>
                       @endforeach
                    </select>
                </div>  
        </div>
        <input type="hidden" name="specification_id" value="1">
         <input type="submit" class="btn btn-sm btn-success pull-left" style="margin-top:-2px;" name="" value="Procceed" required="">
        
       </div>               
    </div>
    <br>
        </div>
      
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-warning">
                        Goods Requisition File has been proceeded to 
                    </div>
                </div>
            </div>
      

    </div>
     </form>

@endsection('content')
@section('scripts')
<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script>
$(document).ready(function () {
            
            $(".list_asset").select2({
                placeholder: "Select",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });  


        });
</script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>

@endsection('scripts')
