<!-- Request Form Modal Call -->
<div class="modal fade" id="reject_purchase_file" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Enter Reason To Reject</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">

                    <form class="form form-horizontal" action="{{route('assets.checker.purchase_file_reject')}}"  method="post"   >
                        {{ csrf_field() }}
                        <label><b>Reject Reason</label>
                        <br>
                        <textarea class="form-control" name="reject_reason" placeholder="Enter prodcut description" autocomplete="off" rows="5" ></textarea>
                        <br>
                        <input type="hidden" name="purchase_form_id" value="{{$goods_purchase_form->id}}">
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Request Form Modal -->