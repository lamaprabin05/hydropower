<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="">

    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset(STATIC_DIR.'assets/admin/layout/img/fav_icon.png') }}">
    <title>Hydropower | Page Not Found </title>

    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>

    {{--error css.--}}
    <link href="{{asset(STATIC_DIR.'css/errors/style.css')}}" rel="stylesheet">
    <link href="{{asset(STATIC_DIR.'css/errors/custom.css')}}" rel="stylesheet">
    <link href="{{asset(STATIC_DIR.'css/errors/default.css')}}" rel="stylesheet">

</head>


<body>
<!-- Preloader -->
<section id="wrapper" class="error-page">
    <div class="error-box">
        <div class="error-body text-center">
            <h1 class="text-info">403</h1>
            <h3 class="text-uppercase">Forbidden Error</h3>
            <p class="text-muted m-t-30 m-b-30 text-uppercase">You don't have permission to access on this server.</p>

            <a href="{{route('home')}}" class="btn btn-info btn-rounded waves-effect waves-light m-b-40">Back to home</a> </div>

    </div>

</section>

<script src="{{asset(STATIC_DIR.'assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>


</body>
</html>







