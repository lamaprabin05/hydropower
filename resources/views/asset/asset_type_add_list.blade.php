
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

@section('content')
    <div class="portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-8 col-sm-8">
                <table class="table table-striped table-bordered table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>
                        <th>
                            Asset Name
                        </th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($asset_type_list as $element)
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td>
                                        {{$element->name}}
                                    </td>
                                 <td>  <a href="#" 
                                        class="btn default btn-xs blue-stripe"
                                        data-toggle="modal",
                                        data-target="#edit_modal",
                                        data-asset_type_id="{{$element->id}}",
                                        data-asset_type_name="{{$element->name}}",
                                        >Edit</a><a href="" class="btn default btn-xs red-stripe">Delete</a></td> 
                                   
                        </td>
                                                                
                                </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="portlet-body col-md-4 col-sm-4">
                <div class="text-center" style="margin-bottom:2px;">
                    <button class="btn btn-sm btn-info">Add Asset Name</button>
                </div>
                <div style="border:1px solid black;padding:20px;">
                 <form class="form form-horizontal" action="{{route('store.add_asset_type')}}" id="driver_form" method="post">
                        {{ csrf_field() }}
                        <div >
                            <label><b>Add Asset Name</b></label>
                            <br>
                            <input class="form-control" type="text" name="asset_type_name" autocomplete="off" placeholder="Enter Asset Name">
                        </div>
                        <br>
                    
                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal Call -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                    <form class="form form-horizontal" action="{{route('store.edit_asset_type_name')}}" method="post">
                        {{ csrf_field() }}
                        <div>
                            <label><b>Edit Asset Type Name</b></label>
                            <br>
                            <input type="text" class="form-control" name="asset_type_name" id="asset_type_name">
                        </div>
                        <br>
                        <input type="hidden" class="form-control" name="asset_type_id" id="asset_type_id">
                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Modal Call -->

@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript">
    </script>
    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });
    </script>

<script>
jQuery(document).ready(function() {
FormEditable.init();
});
</script>

<script>
  $('#edit_modal').on('show.bs.modal', function(e) {
    var button     = $(e.relatedTarget);
    var asset_type_name = button.data('asset_type_name');

    $("#asset_type_name").val(asset_type_name);
    var asset_type_id = button.data('asset_type_id');
    $("#asset_type_id").val(asset_type_id);
});
</script>
@endsection('scripts')
