
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

@section('content')

    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">
                <table class="table table-striped table-bordered table-advance table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>

                        <th>
                            Requisition Number
                        </th>



                        <th>
                            Rejected By

                        </th>

                        <th>
                            Date of Issue
                        </th>

                        <th>
                            Action
                        </th>

                    </tr>
                    </thead>
                    <tbody >
                    @foreach($purchase_rejected_file as $value)
                        <tr>

                            <td>
                                {{$loop->iteration}}
                            </td>

                            <td>
                                {{$value->requisition_number}}
                            </td>



                            <td>
                                {{$value->proceeded_to}}
                            </td>

                            <td>
                                {{\Carbon\Carbon::parse($value->created_at)->diffForHumans()}}
                            </td>


                            <td>
                                <input type="button" class="btn btn-sm btn-success" value="Proceed">&nbsp
                                <input type="button" class="btn btn-sm btn-danger" value="Delete">

                            </td>
                        </tr>
                    @endforeach



                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('assets.requestor.goodsrequisition.modal')
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });

    </script>


@endsection('scripts')
