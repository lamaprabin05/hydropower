
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

@section('content')
    <div class="portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-8 col-sm-8">
                <table class="table table-striped table-bordered table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>
                        <th>
                          Sub Block Name
                        </th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($sub_block_list as $element)
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td>
                                        {{$element->sub_block_name}}
                                    </td>
                                    <td><a href="" class="btn default btn-xs blue-stripe"
                                         data-toggle="modal",
                                         data-target="#edit_modal",
                                         data-sub_block_id="{{$element->id}}",
                                         data-sub_block_name="{{$element->sub_block_name}}",

                                        >Edit</a><a href="" class="btn default btn-xs red-stripe">Delete</a></td>                   
                                </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="portlet-body col-md-4 col-sm-4">
                <div class="text-center" style="margin-bottom:2px;">
                    <button class="btn btn-sm btn-info">Add Sub-Block Name</button>
                </div>
               

                
                <div style="border:1px solid black;padding:20px;">
               <form class="form form-horizontal" action="{{route('store.add_sub_block_type')}}" id="driver_form" method="post">
                        {{ csrf_field() }}
                        <div>
                            <label><b>Choose Block Type</b></label>
                            <select class="form-control" name="block_type_id">
                                @foreach($block_list as $element)
                                <option value="{{$element->id}}">{{$element->block_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>    
                        <div>
                            <label><b>Add Sub-Block Type Name</b></label>
                            <br>
                            <input class="form-control" type="text" name="sub_block_type_name" autocomplete="off" placeholder="Enter Sub-Block Type Name">
                        </div>
                        <br>
                    
                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">
                    </form>
                </div>
         </div>
    </div>
</div>

<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                    <form class="form form-horizontal" action="{{route('store.edit_sub_block_name')}}" method="post">
                        {{ csrf_field() }}
                        <div>
                            <label><b>Specification Name</b></label>
                            <br>
                            <input type="text" class="form-control" name="sub_block_name" id="sub_block_name">
                        </div>
                        <br>
                        <input type="hidden" class="form-control" name="sub_block_id" id="sub_block_id">
                        <button type="button" class="btn  btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-sm  btn-info" value="Submit"  id="submit">
                    </form>
                </div>
            </div>

        </div>
</div>
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });
    </script>

<script>
  $(document).ready(function() {
  $('#edit_modal').on('show.bs.modal', function(e) {
    var button     = $(e.relatedTarget);
    var sub_block_name = button.data('sub_block_name');
    $("#sub_block_name").val(sub_block_name);
    var sub_block_id = button.data('sub_block_id');
    $("#sub_block_id").val(sub_block_id);
});
});
</script>
@endsection('scripts')
