@extends('layouts.app')

@section('page_title')
    View Product
@endsection

@section('content')
<!-- BEGIN SAMPLE FORM PORTLET-->
<div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">
                       
                            <form class="form-horizontal" role="form" action="{{route('store.update_product')}}" method="post" enctype="multipart/form-data" >
                                @csrf
                                <div class="form-body" id="product">
                                    <h3 class="text-center">Product Details</h3>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Product Name</label>
                                        <div class="col-md-9">
                                            <input type="text"  value="{{$asset->name}}" class="form-control input-lg" disabled="" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Product Description</label>
                                        <div class="col-md-9">
                                            <input type="text"  value="{{$asset->description}}" class="form-control" disabled="">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Asset Type</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control"  value="{{$asset_type_name->asset_types->name}}" disabled="">
                                           
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Product Description</label>
                                        <div class="col-md-9">
                                            <input type="text"  value="{{$asset->description}}" class="form-control" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Quantity</label>
                                        <div class="col-md-9">
                                            <input type="number"  value="{{$asset->quantity}}" class="form-control" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Unit Cost</label>
                                        <div class="col-md-9">
                                            <input type="number"  value="{{$asset->unit_cost}}" class="form-control" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Amount</label>
                                        <div class="col-md-9">
                                            <input type="number"   value="{{$asset->amount}}" class="form-control" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nepali Date</label>
                                        <div class="col-md-9">
                                            <input type="text" name="nepali_date" value="{{$asset->nepali_date}}" class="form-control bod-picker" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Bill Image</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail"  >
                                                <img  style="max-width: 200px; max-height: 150px;"  class="img img-responsive"  src="{{ url('storage/app/public/'.$asset->bill) }} " alt="product image">
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Product Image</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail"  >
                                                <img   style="max-width: 200px; max-height: 150px;"  src="{{ url('storage/app/public/'.$asset->image) }} " alt="product image">
                                            </div>
                                           
                                        </div>
                                    </div>

                                  

                                    
                                 
                                </div>

                                <div class="form-body" id="vendor">
                                    <h3 class="text-center">Vendor Details</h3>
                                    <hr>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Name</label>
                                        <div class="col-md-9">
                                            <input type="text"  value="{{$vendor->product_vendors->name}}" class="form-control input-lg" disabled="" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Company Name</label>
                                        <div class="col-md-9">
                                            <input type="text"  value="{{$vendor->product_vendors->company}}" class="form-control input-lg" disabled="" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Address</label>
                                        <div class="col-md-9">
                                            <input type="text"  value="{{$vendor->product_vendors->address}}" class="form-control input-lg" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Contact</label>
                                        <div class="col-md-9">
                                            <input type="number"  value="{{$vendor->product_vendors->contact}}" class="form-control input-lg" disabled="">
                                        </div>
                                    </div>

                                </div>

                               
                                 
                                
                            </form>
                        </div>
                    </div>
                </div>
                    <!-- END SAMPLE FORM PORTLET-->


@endsection('content')
@section('scripts')
 
@endsection('scripts')
