@extends('layouts.app')
@section('css')

<link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>
@endsection('css')

@section('page_title')
    Product Add
@endsection

@section('content')

<div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">
                            <form class="form-horizontal" role="form" action="{{route('store.add_product')}}" method="post" enctype="multipart/form-data" >
                                @csrf
                                <div class="form-body" id="product">
                                    <h3 class="text-center">Add Product Details</h3>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Asset Type</label>
                                        <div class="col-md-9">
                                            <select class="form-control input-lg list_asset" name="asset_type_id"  id="asset_type_id">
                                               <option  value="" ></option>
                                                @foreach($asset_type as $element)
                                                <option value="{{$element->id}}">{{$element->name}}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('asset_type_id'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('asset_type_id') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Block Type</label>
                                        <div class="col-md-9">
                                        <select class="form-control input-lg list_asset" name="block_id" id="block_list">
                                            <option  value="" ></option>
                                       </select>
                                       @if($errors->has('block_id'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('block_id') }}
                                                </span>
                                       @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Sub-Block Type</label>
                                        <div class="col-md-9">
                                            <select class="form-control input-lg list_asset" name="sub_block_id"  id='sub_block_list'>
                                               <option  value="" ></option>
                                            </select>
                                            @if($errors->has('sub_block_id'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('sub_block_id') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Asset Name</label>
                                        <div class="col-md-9" >
                                             <select class="form-control input-lg list_asset" name="asset_name_id"  id='asset_name_list'>
                                               <option  value="" ></option>
                                            </select>
                                            @if($errors->has('asset_name_id'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('asset_name_id') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Specification Name</label>
                                        <div class="col-md-9" >
                                             <select class="form-control input-lg list_asset" name="specification_name_id"  id='specification_name_list'>
                                               <option  value="" ></option>
                                            </select>
                                            @if($errors->has('specification_name_id'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('specification_name_id') }}
                                                </span>
                                            @endif

                                        </div>
                                    </div> 
  
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Specification Unit Name</label>
                                        <div class="col-md-9">
                                            <input type="text" name="specification_unit_name" class="form-control" placeholder="Enter Product Unit" value="{{old('specification_unit_name')}}" autocomplete="off">
                                            @if($errors->has('specification_unit_name'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('specification_unit_name') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Expected Life(yrs)</label>
                                        <div class="col-md-9">
                                            <input type="number" name="expected_life" value="{{old('expected_life')}}" class="form-control" placeholder="Enter Expected Life"  autocomplete="off">
                                            @if($errors->has('expected_life'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('expected_life') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Method Of Depreciation/Stock Valuation</label>
                                        <div class="col-md-9">
                                            <input type="number" name="depreciation_value" class="form-control" placeholder="Enter Depreciation/Stock Valuation" value="{{old('depreciation_value')}}" autocomplete="off">
                                            @if($errors->has('depreciation_value'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('depreciation_value') }}
                                                </span>
                                            @endif

                                        </div>
                                     </div>

                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Residual Value (% Of Purchase Price)</label>
                                        <div class="col-md-9">
                                            <input type="number" name="residual_price" class="form-control" placeholder="Residual Value Of Purchase Price"  value="{{old('residual_price')}}" autocomplete="off">
                                            @if($errors->has('residual_price'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('residual_price') }}
                                                </span>
                                            @endif
                                        </div>
                                     </div>

                                     <div class="form-group">
                                        <label class="col-md-3 control-label">First Year Depreciation(% of year)</label>
                                        <div class="col-md-9">
                                            <input type="number" name="depreciation_amount" class="form-control" placeholder="First Year Depreciation Year"  value="{{old('depreciation_amount')}}" autocomplete="off">
                                            @if($errors->has('depreciation_amount'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('depreciation_amount') }}
                                                </span>
                                            @endif
                                        </div>
                                     </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Quantity</label>
                                        <div class="col-md-9">
                                            <input type="number" name="quantity" value="{{old('quantity')}}" class="form-control" placeholder="Enter Product Quantity"  id="quantity">
                                            @if($errors->has('quantity'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('quantity') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Unit Cost</label>
                                        <div class="col-md-9">
                                            <input type="number" name="unit_cost" value="{{old('unit_cost')}}" class="form-control" placeholder="Enter Product Unit Cost"  id="unit_cost">
                                            @if($errors->has('unit_cost'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('unit_cost') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Amount</label>
                                        <div class="col-md-9">
                                            <input type="number" name="amount" value="{{old('amount')}}" class="form-control" placeholder="Enter Product Amount"  id="amount">
                                            @if($errors->has('amount'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('amount') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nepali Date</label>
                                        <div class="col-md-9">
                                            <input type="text" name="nepali_date" value="{{old('nepali_date')}}" class="form-control bod-picker" placeholder="Enter Nepali Date" >
                                            @if($errors->has('nepali_date'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('nepali_date') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Bill Image</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                <span class="fileinput-new ">
                                                Select image </span>
                                                <span class="fileinput-exists ">
                                                Change </span>
                                                <input type="file" name="bill"  >
                                                
                                                </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                Remove </a>
                                            </div>
                                            @if($errors->has('bill'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('bill') }}
                                                </span>
                                            @endif
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Product Image</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                <span class="fileinput-new ">
                                                Select image </span>
                                                <span class="fileinput-exists ">
                                                Change </span>
                                                <input type="file" name="image"  >
                                                </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                Remove </a>
                                            </div>
                                            @if($errors->has('image'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('image') }}
                                                </span>
                                             @endif
                                        </div>
                                        
                                    </div>

                                </div>

                                <div class="form-group">
                                        <label class="col-md-3 control-label">Select Vendor</label>
                                        <div class="col-md-9">
                                            <select class="form-control input-lg list_asset" name="vendor_id"   style="width:300px;">
                                                <option  value=""></option>
                                               @foreach($vendor_list as $element)
                                                 <option  value="{{$element->id}}" >{{$element->name}}</option>
                                                @endforeach
                                               
                                            </select>
                                            @if($errors->has('vendor_id'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('vendor_id') }}
                                                </span>
                                            @endif

                                        </div>
                                </div>
                                <div class="form-actions text-center">
                                    <button type="reset" class="btn default" id=cancel>Cancel</button>
                                    <button type="submit" class="btn green " id="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->


@endsection('content')
@section('scripts')

<script>
$(document).ready(function () {
            $("#date1").on('click',function(){
                $('#date1').remove();
            });

            $(".datepicker").datepicker({
                dateFormat:"yy-mm-dd",
                changeMonth:true,
                changeYear:true
            });

            $(".bod-picker").nepaliDatePicker({
                dateFormat: "%D, %M %d, %y",
                closeOnDateSelect:true
                //minDate : "सोम, जेठ १०, २०७३",
                //maxDate : "मंगल, जेठ ३२, २०७३"
            });

            $(".list_asset").select2({
                placeholder: "Select",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });  


        });
</script>


<script>
    $('#unit_cost').on("keyup", function(){
        var unit_cost=$('#unit_cost').val();
        var quantity=$('#quantity').val();
        var amount=unit_cost*quantity;
        $('#amount').val(amount);
    });

</script>

<script type="text/javascript">
    $('#asset_type_id').change(function(){
        var asset_type_id=$(this).val();
       // console.log($('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                type:"POST",
                url: "{{route('store.get_block_list')}}",
                data: "asset_type_id=" + asset_type_id,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (msg) {
                    var option1='';
                     $.each(msg,function(key,value){
                         option1+='<option value="'+value['id']+'" >'+value['block_name']+'</option>'
                    });
                     $("#block_list").append(option1);

                }
            });
        });
</script>

<script type="text/javascript">
       
    $('#block_list').change(function(){
         var sub_block_id=$("#block_list option:selected").val();
       // console.log($('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                type:"POST",
                url: "{{route('store.get_sub_block_list')}}",
                data: "block_id=" + sub_block_id,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (msg) {
                    var option2='';
                     $.each(msg,function(key,value){
                         option2+='<option value="'+value['id']+'" >'+value['sub_block_name']+'</option>'
                    });
                     $("#sub_block_list").append(option2);
                }
            });
        });
        
</script>

<script type="text/javascript">
       
    $('#sub_block_list').change(function(){
         var sub_block_id=$("#sub_block_list option:selected").val();
        
            $.ajax({
                type:"POST",
                url: "{{route('store.get_asset_name_list')}}",
                data: "sub_block_id=" + sub_block_id,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (msg) {
                    var option3='';
                     $.each(msg,function(key,value){
                         option3+='<option value="'+value['id']+'" >'+value['asset_name']+'</option>'
                    });
                     $("#asset_name_list").append(option3);
                }
            });
        });
        
</script>

<script type="text/javascript">
       
    $('#asset_name_list').change(function(){
         var asset_name_id=$("#asset_name_list option:selected").val();
            $.ajax({
                type:"POST",
                url: "{{route('store.get_specification_name_list')}}",
                data: "asset_name_id=" + asset_name_id,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (msg) {
                    var option4='';
                     $.each(msg,function(key,value){
                         option4+='<option value="'+value['id']+'" >'+value['specification_name']+'</option>'
                    });
                     $("#specification_name_list").append(option4);
                }
            });
        });
        
</script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>


 
@endsection('scripts')
