@extends('layouts.app')
@section('css')

<link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    Requisition Form
@endsection




@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box red ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Product Entry Form
                            </div>
                         
                        </div>
                        <div class="portlet-body form"  >
                            <form class="form-horizontal" role="form" action="{{route('asset.purchase_form')}}" method="post" enctype="multipart/form-data" >
                                @csrf
                                <div class="form-body" id="product">
                                    <h3 class="text-center">Purchase Requisition Form Details</h3>
                                    <hr>

                                    <div class="form-group">
                                        
                                        <div class="col-md-12">
                                           <!-- BEGIN BORDERED TABLE PORTLET-->
                    <div class="portlet box yellow">
                        <div class="portlet-title center">
                            <div class="caption">
                                Requisition Form
                            </div>
                          
                        </div>

                        <div class="portlet-body">

                            <div class="col-md-4">Specify Number of Product Entry Required:</div>
                            <div class="col-md-8" style="margin-left:-80px;">
                                <select  class="form-control" id="row" name="number" style="width:100px;height:30px;" >
                                       <option >Select</option>
                                       @for($i=1;$i<=6;$i++)

                                       <option value="{{$i}}">{{$i}}</option>
                                       @endfor                    
                                </select>
                                <br>
                            </div>
                             
                                       
                            <div class="table-scrollable">

                                <table class="table table-bordered table-hover">

                                <thead>
                                <tr>
                                    
                                    <th> S.N</th>
                                    <th> Product Name</th>
                                    <th> Product Description</th>
                                    <th> Requisition No.</th>
                                    <th> Estimated Cost.</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id="row1">
                                   
                                    <td >
                                         1
                                    </td>
                                     <td>
                                         <input type="text" name="name1" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <textarea class="form-control" name="description1"></textarea>
                                    </td>
                                     <td>
                                         <input type="text" name="requisition_number1"  placeholder="Enter Product requisition_number" autocomplete="off"  class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <input type="number" name="cost1" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>

                                    
                                </tr>

                                <tr id="row2">
                                   
                                    <td>
                                         2
                                    </td>
                                     <td>
                                         <input type="text" name="name2" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <textarea class="form-control" name="description2"></textarea>
                                    </td>
                                     <td>
                                         <input type="text" name="requisition_number2"  placeholder="Enter Product requisition_number" autocomplete="off"  class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <input type="number" name="cost2" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>

                                    
                                </tr>

                                <tr id="row3">
                                   
                                    <td>
                                         3
                                    </td>
                                     <td>
                                         <input type="text" name="name3" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <textarea class="form-control" name="description3"></textarea>
                                    </td>
                                     <td>
                                         <input type="text" name="requisition_number3"  placeholder="Enter Product requisition_number" autocomplete="off"  class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <input type="number" name="cost3" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>

                                    
                                </tr>

                                <tr id="row4">
                                   
                                    <td>
                                         4
                                    </td>
                                     <td>
                                         <input type="text" name="name4" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <textarea class="form-control" name="description4"></textarea>
                                    </td>
                                     <td>
                                         <input type="text" name="requisition_number4"  placeholder="Enter Product requisition_number" autocomplete="off"  class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <input type="number" name="cost4" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>

                                    
                                </tr>

                                <tr id="row5">
                                   
                                    <td>
                                         5
                                    </td>
                                     <td>
                                         <input type="text" name="name5" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <textarea class="form-control" name="description5"></textarea>
                                    </td>
                                     <td>
                                         <input type="text" name="requisition_number5"  placeholder="Enter Product requisition_number" autocomplete="off"  class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <input type="number" name="cost5" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>

                                    
                                </tr>

                                <tr id="row6">
                                   
                                    <td>
                                         6
                                    </td>
                                     <td>
                                         <input type="text" name="name6" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <textarea class="form-control" name="description6"></textarea>
                                    </td>
                                     <td>
                                         <input type="text" name="requisition_number6"  placeholder="Enter Product requisition_number" autocomplete="off"  class="form-control" autocomplete="off">
                                    </td>
                                     <td>
                                         <input type="number" name="cost6" placeholder="Enter Product Name" class="form-control" autocomplete="off">
                                    </td>

                                    
                                </tr>

                                
                                
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END BORDERED TABLE PORTLET-->
                                        </div>
                                        
                                            
                                    </div>


                               <div class="form-actions text-center">
                                    <button type="reset" class="btn default" id=cancel>Cancel</button>
                                    <button type="submit" class="btn green " id="submit">Submit</button>
                                </div>     
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->


@endsection('content')
@section('scripts')
<script src="{{ asset(STATIC_DIR.'product/js/add_product.js') }}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>


 
@endsection('scripts')
