<!DOCTYPE html>
<html lang="en">
@include('layouts._partials.head')


<!-- BEGIN BODY -->
{{--<body class="page-header-fixed page-quick-sidebar-over-content">--}}
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid">

    @include('layouts._partials.nav')

<div class="clearfix">
</div>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('layouts._partials.sidebar')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <h3 class="page-title">
               <b>@yield('page-title')</b> 
                <hr>
            </h3>

            <div class="row">
                <div class="col-md-12">
                    @include('layouts._partials.messages')
                </div>
            </div>

            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    @yield('content')
                {{--</div>--}}
            {{--</div>--}}
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner ">
        2019 &copy; Developed By Technology Sales Pvt Ltd.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>


@include('layouts._partials.scripts')
</body>

</html>
