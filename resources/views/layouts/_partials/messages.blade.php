@if(Session::has('success'))
    <div class="alert alert-success display-none test" style="display: block;">
        <button class="close" data-dismiss="alert"></button>
        <i class="fa fa-check "></i> &nbsp;{{Session::get('success')}}
    </div>

@elseif(Session::has('error'))
    <div class="alert alert-danger display-none test" style="display: block;">
        <button class="close" data-dismiss="alert"></button>
        <i class="fa fa-warning"></i>&nbsp; {{Session::get('error')}}
    </div>
@elseif(Session::has('delete'))
    <div class="alert alert-danger display-none test" style="display: block;">
        <button class="close" data-dismiss="alert"></button>
        <i class="fa fa-warning"></i>&nbsp; {{Session::get('delete')}}
    </div>
@endif

<script>
        setTimeout(function(){$('.test').slideUp();},3000);  //slidup after 3 second
</script>
