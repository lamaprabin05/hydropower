<?php
    $count = App\Helper\Tools::count_file();
?>
<div class="page-sidebar-wrapper">

    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->

        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>

{{--            @can('update-post')--}}
                <li class="tooltips @if(Request::is('/')) active @endif">
                    <a href="{{url('/')}}">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                    </a>
                </li>
            {{--@endcan--}}

{{--            @can('add_user')--}}
                <li class="@if(Request::is('admin/*')) active @endif">
                    <a href="javascript:;">
                        {{--<i class="icon-docs"></i>--}}
                        <i class="icon-users"></i>
                        <span class="title">Users</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        @if (Route::has('register'))
                            <li class=" tooltips @if(Request::is('admin/register')) active @endif">
                                <a href="{{ route('admin.register') }}">
                                    <i class="fa fa-plus-circle"></i>
                                    Add User</a>
                            </li>
                        @endif

                        <li class=" tooltips @if(Request::is('admin/user')) active @endif">
                            <a href="{{ route('admin.list_user') }}">
                                <i class="fa fa-list"></i>
                                List User</a>
                        </li>

                        <li class=" tooltips @if(Request::is('admin/permission/group')) active @endif">
                            <a href="{{ route('admin.list_group') }}">
                                <i class="fa fa-lock"></i>
                                 Permission Group</a>
                        </li>
                    </ul>
                </li>
            {{--@endcan--}}

            <li class="tooltips @if(Request::is('profile/*')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                <a href="{{route('profile.list_profile',1)}}" >
                    <i class="icon-settings"></i>
                    <span class="title">
                        My Profile
                    </span>
                </a>
            </li>


           {{--@can('shareholder')
                <li class="heading">
                    <h3 class="uppercase">Share Holder</h3>
                </li>

                <li class="tooltips @if(Request::is('shareholder/group')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                    <a href="{{route('shareholder.list_group')}}" >
                        <i class="icon-user"></i>
                        <span class="title">
                        Share Group
                    </span>
                    </a>
                </li>

                <li class="tooltips @if(Request::is('shareholder/add_form')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                    <a href="{{route('shareholder.add_form')}}" >
                        <i class="icon-plus"></i>
                        <span class="title">
					    Add Form
                    </span>
                    </a>
                </li>

                <li class="tooltips @if(Request::is('shareholder/list_form')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                    <a href="{{route('shareholder.list_form')}}" >
                        <i class="icon-docs"></i>
                        <span class="title">
                        List Form
                    </span>
                    </a>
                </li>

                <li class="@if(Request::is('shareholder/installment')) active @endif">
                    <a href="javascript:;">
                        --}}{{--<i class="icon-docs"></i>--}}{{--
                        <i class="fa fa-dollar"></i>
                        <span class="title">Installment</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li class=" tooltips @if(Request::is('shareholder/installment')) active @endif">
                            <a href="{{route('shareholder.installment')}}">
                                <i class="fa fa-plus-circle"></i>
                                Paid Installment</a>
                        </li>
                    </ul>
                </li>

                <li class="tooltips @if(Request::is('shareholder/notification')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                    <a href="{{route('shareholder.notification')}}" >
                        <i class="fa fa-bell"></i>
                        <span class="title">
                            Notification
                        </span>

                        <span class="badge badge-warning">{{$count}}</span>
                    </a>
                </li>
            @endcan--}}

{{--           @can('ams')--}}
                {{--<li class="heading">--}}
                    {{--<h3 class="uppercase">Assets Management</h3>--}}
                {{--</li>--}}

                {{--store keeper permission--}}
                @can('store_keeper')

                    <li class="heading">
                        <h3 class="uppercase">Store Keeper Module</h3>
                    </li>

                    <li class="tooltips @if(Request::is('assetsmanagement/storekeeper/requisition/goods')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('assets.store.get_all_goods_requisition')}}" >
                            <i class="fa fa-file"></i>
                            <span class="title">
                            <small>Goods Requisition Request</small>
                            </span>
                            @if($count['goods_requisition_request'] > 0)
                                <span class="badge badge-primary">{{$count['goods_requisition_request'] ?? ''}}</span>
                            @endif
                        </a>
                    </li>

                    <li class="tooltips @if(Request::is('storekeeper/goods_requisition_file_status')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">

                        <a href="{{route('store.goods_requisition_file_status')}}">
                            <i class="fa fa-file"></i>
                            <span class="title">
                                   Goods Requisition Files
                            </span>
                        </a>
                    </li>

                    <li class="tooltips @if(Request::is('storekeeper/dispatched/product')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('store.show_dispatch_product')}}">
                            {{--<i class="fa fa-file"></i>--}}
                            <i class="fa fa-exclamation-circle"></i>
                            <span class="title">
                                   Dispatched Products
                            </span>
                        </a>
                    </li>

                    <li class="tooltips @if(Request::is('storekeeper/requisition/purchase')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('store.show_purchase_requisition')}}">
                            {{--<i class="fa fa-file"></i>--}}
                            <i class="fa fa-exclamation-circle"></i>
                            <span class="title">
                                       Purchase Requisition Files
                                </span>
                        </a>
                    </li>

                     {{--<li>
                        <a href="{{route('store.purchase_form')}}">
                                    <i class="fa fa-file"></i>
                            <span class="title">
                                 Goods Purchase  Form
                            </span>
                        </a>
                    </li>--}}

                   {{-- <li>
                        <a href="{{route('store.purchase_file_list')}}">
                            <i class="fa fa-file"></i>
                            Goods Purchase  Files
                        </a>
                    </li>--}}

                    <li class="tooltips @if(Request::is('storekeeper/purchase_approved_list')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">

                        <a href="{{route('store.purchase_approved_list')}}">
                            <i class="fa fa-file"></i>
                            <span class="title">
                                  Purchase Approved Files
                            </span>
                          
                        </a>
                    </li>


                    <li class="tooltips @if(Request::is('storekeeper/purchase_rejected_file')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('store.purchase_rejected_file')}}">
                            <i class="fa fa-file"></i>
                            <span class="title">
                                  Purchase Rejected Files
                            </span>
                        </a>
                    </li>


                    <li class="@if(Request::is('storekeeper/product/add') || Request::is('storekeeper/list_product') ||
                    Request::is('storekeeper/add_vendor') || Request::is('storekeeper/list_vendor') ||
                    Request::is('storekeeper/asset_type_add_list')  || Request::is('storekeeper/add_block_type') ||
                    Request::is('storekeeper/add_sub_block_list') || Request::is('storekeeper/asset_name_add_list') ||
                    Request::is('storekeeper/specification_name_add_list') ) active @endif">
                        
                        <a href="javascript:;">
                            {{--<i class="icon-docs"></i>--}}
                            <i class="fa fa-dollar"></i>
                            <span class="title">Product</span>
                            <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu">
                            <li class=" tooltips @if(Request::is('storekeeper/product/add')) active @endif">
                                <a href="{{route('store.add_product')}}">
                                    <i class="fa fa-plus-circle"></i>
                                    Add Product</a>
                            </li>

                            <li class=" tooltips @if(Request::is('storekeeper/list_product')) active @endif">
                                <a href="{{route('store.list_product')}}">
                                    <i class="fa fa-plus-circle"></i>
                                    List Products</a>
                            </li>

                            <li class=" tooltips @if(Request::is('storekeeper/add_vendor')) active @endif">
                                <a href="{{route('store.add_vendor')}}">
                                    <i class="fa fa-plus-circle"></i>
                                    Add Vendor</a>
                            </li>

                            <li class=" tooltips @if(Request::is('storekeeper/list_vendor')) active @endif">
                                <a href="{{route('store.list_vendor')}}">
                                    <i class="fa fa-plus-circle"></i>
                                    List Vendor</a>
                            </li>

                            <li class=" tooltips @if(Request::is('storekeeper/asset_type_add_list')) active @endif">
                                <a href="{{route('store.asset_type_add_list')}}">
                                    <i class="fa fa-plus-circle"></i>
                                    Asset Type(Add/List)</a>
                            </li>

                            <li class=" tooltips @if(Request::is('storekeeper/add_block_type')) active @endif">
                                <a href="{{route('store.add_block_type')}}">
                                    <i class="fa fa-plus-circle"></i>
                                    Block Type(Add/List)</a>
                            </li>

                            <li class=" tooltips @if(Request::is('storekeeper/add_sub_block_list')) active @endif">
                                <a href="{{route('store.add_sub_block_list')}}">
                                    <i class="fa fa-plus-circle"></i>
                                    SUbBlock Type(Add/List)</a>
                            </li>

                            <li class=" tooltips @if(Request::is('storekeeper/asset_name_add_list')) active @endif">
                                <a href="{{route('store.asset_name_add_list')}}">
                                    <i class="fa fa-plus-circle"></i>
                                 Asset Name(Add/List)</a>
                            </li>

                            <li class=" tooltips @if(Request::is('storekeeper/specification_name_add_list')) active @endif">
                                <a href="{{route('store.specification_name_add_list')}}">
                                    <i class="fa fa-plus-circle"></i>
                                 Specification Name(Add/List)</a>
                            </li>
                        </ul>
                    </li>
                @endcan

                @can('requestor')

                    <li class="heading">
                        <h3 class="uppercase">Requestor Module</h3>
                    </li>

                    <li class="tooltips @if(Request::is('assetsmanagement/requestor/requisition/goods')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('assets.requestor.get_requisition_form')}}" >
                            <i class="fa fa-file"></i>
                            <span class="title">
                            Goods Requisition Form
                        </span>

                        </a>
                    </li>

                    <li class="tooltips @if(Request::is('assetsmanagement/requestor/requisition/files')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('assets.requestor.list_all_requisition')}}" >
                            <i class="fa fa-file"></i>
                            <span class="title">
                            Files
                        </span>
                            @if($count['requestor'] > 0)
                                <span class="badge badge-warning">{{$count['requestor'] ?? ''}}</span>
                            @endif
                        </a>
                    </li>

                    <li class="tooltips @if(Request::is('assetsmanagement/requestor/requisition/rejected')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('assets.requestor.list_rejected_requisition')}}" >
                            <i class="fa fa-file"></i>
                            <span class="title">
                            Rejected Files
                        </span>
                            @if($count['rejected'] > 0)
                                    <span class="badge badge-danger">{{$count['rejected'] ?? ''}}</span>

                            @endif
                        </a>
                    </li>

                @endcan

                @can('checker')

                    <li class="heading">
                        <h3 class="uppercase">Checker Module</h3>
                    </li>

                    <li class="tooltips @if(Request::is('assetsmanagement/checker/requisition/goods')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('assets.checker.get_all_goods_requisition')}}" >
                            <i class="fa fa-file"></i>
                            <span class="title">
                            Goods Requisition
                        </span>
                            @if($count['checker'] > 0)
                                <span class="badge badge-info">{{$count['checker'] ?? ''}}</span>
                            @endif
                        </a>
                    </li>

                    <li class="tooltips @if(Request::is('assetsmanagement/checker/purchase_requisition') || Request::is('assetsmanagement/checker/view_purchase_file/*')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('assets.checker.purchase_requisition')}}" >
                            <i class="fa fa-file"></i>
                            <span class="title">
                      Purchase Requisition
                        </span>
                        
                        </a>
                    </li>

                @endcan

                @can('approver')

                    <li class="heading">
                        <h3 class="uppercase">Approver Module</h3>
                    </li>

                    <li class="tooltips @if(Request::is('assetsmanagement/approver/requisition/goods')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('assets.approver.get_all_goods_requisition')}}" >
                            <i class="fa fa-file"></i>
                            <span class="title">
                            Goods Requisition
                        </span>

                            @if($count['approver'] > 0)
                                <span class="badge badge-info">{{$count['approver'] ?? ''}}</span>
                            @endif
                        </a>
                    </li>

                    <li class="tooltips @if(Request::is('assetsmanagement/approver/requisition')) active @endif" data-container="body" data-placement="right" data-html="true" data-original-title="">
                        <a href="{{route('assets.approver.fetch_goods_purchase_requisition')}}" >
                            <i class="fa fa-file"></i>
                            <span class="title">
                            Purchase Requisition
                             </span>
                           
                             

                          
                        </a>
                    </li>

                @endcan


           {{--@endcan--}}
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>