<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Hydropower | @yield('page_title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->


@yield('css-top')
    <!-- BEGIN THEME STYLES -->
    <link href="{{ asset(STATIC_DIR.'assets/global/css/components.css') }}" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/admin/layout/css/layout.css') }}" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="{{ asset(STATIC_DIR.'assets/admin/layout/css/themes/darkblue.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'assets/admin/layout/css/custom.css') }}" rel="stylesheet" type="text/css"/>

    {{--custom calander css--}}
    <link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>

    <!-- END THEME STYLES -->
    {{--<link rel="shortcut icon" href="{{ asset(STATIC_DIR.'assets/admin/layout/img/fav_icon.png') }}"/>--}}
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset(STATIC_DIR.'assets/admin/layout/img/fav_icon.png') }}">


    @yield('css')
    <style>

        body{
            font-family: Avantgarde, TeX Gyre Adventor, URW Gothic L, sans-serif;
            font-size: 14px;
            font-weight: normal;
            font-style: normal
        }

    </style>
</head>
<!-- END HEAD -->