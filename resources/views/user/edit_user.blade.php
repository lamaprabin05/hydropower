@extends('layouts.app')
@section('css')
 <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    Edit User
@endsection('page-title')

@section('page-title')
    Edit User
@endsection('page-title')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center"></div>
                    <br>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.update_user') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') ?? $users->name }}"  >

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback text-danger" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" readonly type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') ?? $users->email }}" >

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback text-danger" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback text-danger" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                                </div>
                            </div>

                            <div class="form-group row">

                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('UserType') }} </label>
                                <div class="col-md-6">
                                    <select class="form-control" name="usertype">
                                        <option value="" hidden>Select UserType</option>
                                        <option value="1" @if($users->usertype == 1)  selected @endif>Shareholder </option>
                                        <option value="2" @if($users->usertype == 2)  selected @endif>Assets Management</option>
                                        <option value="3" @if($users->usertype == 3)  selected @endif>Document Management</option>
                                    </select>
                                    @if ($errors->has('usertype'))
                                        <span class="invalid-feedback text-danger" role="alert">
                                            <strong>{{ $errors->first('usertype') }}</strong>
                                        </span>
                                    @endif

                                </div>

                            </div>

                            <div class="form-group row">

                                <label for="image-upload" class="col-md-4 col-form-label text-md-right">{{ __('Upload Image') }}</label>
                                <div class="col-md-6">
                                    <?php
                                        if($users->user_avatar != null){
                                            $class = "fileinput fileinput-exists";
                                        }
                                        else {
                                            $class = "fileinput fileinput-new";
                                        }

                                     ?>
                                    <div class="{{$class}}" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">

                                                <img src="{{asset(STATIC_DIR.'storage/'.$users->user_avatar)}}" style="max-height: 140px;">
                                        </div>
                                        <div>

                                        <span class="btn default btn-file">
                                            <span class="fileinput-new">
                                                Choose Image
                                            </span>

                                            <span class="fileinput-exists">
                                                Change
                                            </span>

                                            <input type="file" name="user_avatar">
                                        </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                Remove </a>
                                        </div>
                                    </div>
                                    <br>


                                </div>



                            </div>

                            <div class="form-group row">

                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Group Permission') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="auth_group">
                                        <option value="" hidden>Assign Permission</option>
                                        @foreach($auth_group as $value)
                                            <option value="{{$value->id}}" @if($assigned_group->group_id == $value->id) selected @endif>{{$value->name}}</option>
                                        @endforeach

                                    </select>
                                    @if ($errors->has('auth_group'))
                                        <span class="invalid-feedback text-danger" role="alert">
                                            <strong>{{ $errors->first('auth_group') }}</strong>
                                        </span>
                                    @endif

                                </div>

                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 col-md-offset-4 ">
                                    <input type="hidden" value="{{$users->id}}" name="user_id">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>

    </script>

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>


@endsection('scripts')

