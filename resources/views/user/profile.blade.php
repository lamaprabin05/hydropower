@extends('layouts.app')

@section('css')
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{asset(STATIC_DIR.'assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
{{--    <link href="{{asset(STATIC_DIR.'assets/admin/pages/css/tasks.css"')}}" rel="stylesheet" type="text/css"/>--}}
    <!-- END PAGE LEVEL STYLES -->
@endsection

@section('page_title')
    Profile
@endsection

@section('page-title')
    My Profile
@endsection

@section('content')
            <div class="row margin-top-20">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="profile-sidebar">
                        <!-- PORTLET MAIN -->
                        <div class="portlet light profile-sidebar-portlet">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                @if($user->user_avatar != null)
                                    <img src="{{asset(STATIC_DIR.'storage/'.$user->user_avatar)}}" class="img-responsive" alt="">
                                @else
                                    <img src="{{asset(STATIC_DIR.'assets/uploads/user.png')}}" class="img-responsive" alt="">
                                @endif
                            </div>
                            <!-- END SIDEBAR USERPIC -->
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name">
                                    {{$user->name}}
                                </div>
                                <div class="profile-usertitle-job">
                                    @if($user->is_superadmin == 1)
                                        Admin
                                    @else
                                        User
                                    @endif
                                </div>
                            </div>
                            <!-- END MENU -->
                        </div>
                        <!-- END PORTLET MAIN -->

                    </div>
                    <!-- END BEGIN PROFILE SIDEBAR -->

                    <!-- BEGIN PROFILE CONTENT -->
                    <div class="profile-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class=" {{ $key == 1 ? 'active' : ''  }}">
                                                <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                            </li>

                                            <li class=" {{ $key == 2 ? 'active' : ''  }}">
                                                <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                                            </li>

                                            <li class=" {{ $key == 3 ? 'active' : ''  }}">
                                                <a href="#tab_1_3 " data-toggle="tab">Personal Info Settings</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tab-content">

                                            <!-- PERSONAL INFO TAB -->
                                            <div class="tab-pane {{ $key == 1 ? 'active' : '' }}" id="tab_1_1">
                                                <form role="form" action="#">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label"><strong>Full Name</strong></label>
                                                                <p>{{$user->name}}</p>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label"><strong>Email</strong></label>
                                                                <p>lamaprabin05@gmail.com</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label"><strong>User Type</strong></label>
                                                                <p class="profile-usertitle-job">
                                                                    @if($user->usertype == 1)
                                                                        Share Holder
                                                                    @elseif($user->usertype == 2)
                                                                        Assets Management
                                                                    @else
                                                                        Document Management
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label"><strong>Joined Date:</strong></label>
                                                                <p>2019-01-05</p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </form>
                                            </div>
                                            <!-- END PERSONAL INFO TAB -->

                                            <!-- CHANGE AVATAR TAB -->
                                            <div class="tab-pane {{ $key == 2 ? 'active' : '' }}" id="tab_1_2">

                                                <form action="{{route('profile.edit')}}" role="form" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <input type="hidden" value="{{$user->id}}" name="user_id">
                                                    <input type="hidden" value="2" name="tab">

                                                    <div class="form-group">
                                                        <?php
                                                            if($user->user_avatar != null){
                                                                $class = "fileinput fileinput-exists";
                                                            }
                                                            else {
                                                                $class = "fileinput fileinput-new";
                                                            }
                                                        ?>
                                                        <div class="{{$class}}" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                                <img src="{{asset(STATIC_DIR.'storage/'.$user->user_avatar)}}" style="max-height: 140px;">
                                                            </div>
                                                            <div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="user_image">
																</span>
                                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                                    Remove </a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="margin-top-10">
                                                        <button type="submit" class="btn green-haze">
                                                            <i class="fa fa-check"></i> Submit
                                                        </button>

                                                    </div>
                                                </form>
                                            </div>
                                            <!-- END CHANGE AVATAR TAB -->

                                            <!-- PRIVACY SETTINGS TAB -->
                                            <div class="tab-pane {{ $key == 3 ? 'active' : '' }}" id="tab_1_3" >
                                                <form role="form" action="{{route('profile.edit')}}" method="post" enctype="multipart/form-data">
                                                    @csrf

                                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                                    <input type="hidden" value="3" name="tab">


                                                    <div class="form-group">
                                                        <label class="control-label">Name</label>
                                                        <input type="text" value="{{ old('name') ?? $user->name }}" name="name" class="form-control"/>
                                                        @if ($errors->has('name'))
                                                            <span class="invalid-feedback text-danger" role="alert">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label">Email</label>
                                                        <input type="text" value="{{ $user->email }}" readonly class="form-control"/>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label">Current Password</label>
                                                        <input type="password" name="old_password" class="form-control"/>
                                                        @if ($errors->has('old_password'))
                                                            <span class="invalid-feedback text-danger" role="alert">
                                                                <strong>{{ $errors->first('old_password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label">New Password</label>
                                                        <input type="password" value="{{ old('password') }}" name="password" placeholder="Enter New Password" class="form-control"/>
                                                        @if ($errors->has('password'))
                                                            <span class="invalid-feedback text-danger" role="alert">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="password-confirm"  class="control-label">Confirm New Password</label>
                                                        <input type="password" id="password-confirm"   value="{{ old('password_confirmation') }}" name="password_confirmation" placeholder="Enter Password Again" class="form-control"/>
                                                    </div>

                                                    <div class="margiv-top-10">
                                                        <button type="submit" class="btn green-haze">
                                                            <i class="fa fa-check"></i> Save Changes </button>

                                                    </div>
                                                </form>

                                            </div>
                                            <!-- END PRIVACY SETTINGS TAB -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
@endsection('content')

@section('scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{asset(STATIC_DIR.'assets/admin/pages/scripts/profile.js')}}"></script>
        <script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"></script>
    <!-- END PAGE LEVEL PLUGINS -->


    <script src="{{asset(STATIC_DIR.'assets/global/plugins/jquery.sparkline.min.js')}}"></script>

    <script>
        jQuery(document).ready(function() {
            Profile.init(); // init page demo
        });
    </script>
{{--
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-37564768-1', 'keenthemes.com');
        ga('send', 'pageview');
    </script>--}}
@endsection('scripts')
