
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    List User
@endsection

@section('page-title')
    User Listing
@endsection('page-title')


@section('content')


        <div class="row search-form-default">
            <div class="col-md-12">
                <form action="#">
                    <div class="input-group">
                        <div class="input-cont">
                            {{--<input type="text" placeholder="Search..." class="form-control"/>--}}
                        </div>
                        <span class="input-group-btn">

                            <a href="{{route('admin.register')}}"  class="btn green-haze">
												Add User &nbsp; <i class="icon-plus "></i>
                            </a>
                        </span>
                    </div>
                </form>
            </div>
        </div>

        <div class=" portlet light bordered" >
            <div class="row">
                <div class="portlet-body col-md-12 col-sm-12">

                    {{--<table class="table table-striped table-bordered table-advance table-hover" id="sample_1">--}}
                    <table class="table table-striped table-bordered table-advance table-hover" id="group">
                        <thead>
                        <tr>
                            <th>
                                SN
                            </th>
                            <th>
                                Photo
                            </th>
                            <th>
                                Fullname
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Last Login
                            </th>
                            <th>
                               User Type
                            </th>
                            <th>User <br> Designation</th>
                            <th>
                                Permission Group
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody >
                        @if($action == 1)
                            @foreach($users as $element)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td style="width:200px;">
                                    @if(!empty($element->user_avatar !=''))
                                            <img  class="responsive" width="50" height="50"  src="{{asset(STATIC_DIR.'storage/'.$element->user_avatar)}} " alt="user image">
                                        @else
                                            <img class="responsive" src="{{asset(STATIC_DIR.'assets/admin/layout/img/no_image.png')}}">

                                        @endif
                                    </td>
                                    <td>
                                        {{ucfirst($element->name)}}
                                    </td>
                                    <td>
                                        {{$element->email}}
                                    </td>
                                    <td>
                                        {{\Carbon\Carbon::parse($element->last_login)->diffForHumans()}}
                                    </td>
                                    <td>
                                        @if($element->usertype == 1)
                                            <span class="label label-sm label-success">
                                                Share Holder
                                            </span>
                                        @elseif($element->usertype == 2)
                                            <span class="label label-sm label-danger">
                                                Assets Management
                                            </span>
                                        @else
                                            <span class="label label-sm label-primary">
                                                Document Management
                                            </span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($element->user_designation == 1)
                                            <span class="label label-sm label-warning">
                                                Requestor
                                            </span>
                                        @elseif($element->user_designation == 2)
                                            <span class="label label-sm label-info">
                                                Checker
                                            </span>
                                        @elseif($element->user_designation == 3)
                                        <span class="label label-sm label-success">
                                            Approver
                                        </span>
                                        
                                        @endif
                                        
                                    </td>

                                    <td>
                                    <span class="label label-sm label-success">
                                        {{$element->groupname}}

                                            </span>
                                    <td>

                                        <a class="btn default btn-xs green-stripe" href="{{ route('admin.edit_user',['id' => $element->id]) }}">
                                            <i class="fa fa-pencil"></i> Edit
                                        </a>

                                        <a  href="#deleteUser" class="btn default btn-xs red-stripe"data-ids="{{ $element->id }}" data-toggle="modal" data-rel="delete" data-user="{{$element->name}}" >
                                            <i class="fa fa-trash"></i> Delete
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                        @endif



                        </tbody>
                    </table>
                </div>
            </div>
        </div>

@include('user.modal')
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });

        $('#deleteUser').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var ids = button.data('ids');
            var user = button.data('user');

            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modalfooter #confirm').data('form', form);
            $("#hidden_id").val(ids);
            $(".hidden_title").html(' "' + user + '" ');
        });


        $('#deleteUser').find('#confirm_yes').on('click', function () {

            //set csrf token
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //fetching id
            var id = $("#hidden_id").val();

            $.ajax({
                type: "POST",
                url: "{{ route('admin.delete_user') }}",
                headers: {
                    'XCSRFToken': $('meta[name="csrf-token"]').attr('content')
                },

                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg);
                    // alert(msg.error);
                    if(msg.error == false){
                        $("#deleteUser").modal("hide");
                        $('#showMessage').find('.success').show();
                        $("#showMessage").modal("show");
                    }
                    else
                    {
                        $("#deleteUser").modal("hide");
                        $('#showMessage').find('.error').show();
                        $("#showMessage").modal("show");
                    }
                    // window.location.reload();
                }
            });
        });
        // AJAX DELETE PERMISSION END

        // $( "#show_message" ).click(function() {
        //     window.location.reload();
        // });

        $('#show_message').on('click', function () {
            window.location.reload();
        });


    </script>


@endsection('scripts')
