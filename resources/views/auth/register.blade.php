@extends('layouts.app')
@section('css')
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<!-- <link href="{{asset(STATIC_DIR.'assets/admin/pages/css/profile.css')}}" rel="stylesheet" type="text/css"/>
 --><link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
     Add User
@endsection('page-title')

@section('page-title')
    Register Account
@endsection('page-title')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center"></div>
                <br>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required="" >

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                            </div>
                        </div>

                         <div class="form-group row">
                        
                             <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('UserType') }}</label>
                           <div class="col-md-6">
                             <select class="form-control" name="usertype">
                                <option value="" hidden>Select UserType</option>
                               <option value="1" @if(old('usertype') == 1) selected @endif>Shareholder Account</option>
                               <option value="2" @if(old('usertype') == 2) selected @endif>Assets Account</option>
                               <option value="3" @if(old('usertype') == 3) selected @endif>DMS Account</option>
                             </select>
                           @if ($errors->has('usertype'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $errors->first('usertype') }}</strong>
                                    </span>
                                @endif
                               
                           </div>
                          
                        </div>

                        <div class="form-group row">
                        
                             <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('User Designation') }}</label>
                           <div class="col-md-6">
                             <select class="form-control" name="user_designation">
                                <option value="" hidden>Select User Designation</option>
                               <option value="1" @if(old('usertype') == 1) selected @endif>Requestor</option>
                               <option value="2" @if(old('usertype') == 2) selected @endif>Checker</option>
                               <option value="3" @if(old('usertype') == 3) selected @endif>Approver</option>
                               <option value="4" @if(old('usertype') == 4) selected @endif>Store</option>
                             </select>
                           @if ($errors->has('user_designation'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $errors->first('user_designation') }}</strong>
                                    </span>
                                @endif
                               
                           </div>
                          
                        </div>

                        <div class="form-group row">
                        
                             <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Upload Image') }}</label>
                           <div class="col-md-6">
                              <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" >
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/ >
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                    </div>
                                    <div>
                                        <span class="btn default btn-file">
                                        <span class="fileinput-new">
                                        Select image </span>
                                        <span class="fileinput-exists">
                                        Change </span>
                                        <input type="file" name="user_avatar">
                                        </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                        Remove </a>
                                    </div>
                                </div>
                                 <br>

                             
                           </div>


                          
                        </div>

                        <div class="form-group row">

                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Group Permission') }}</label>
                            <div class="col-md-6">
                                <select class="form-control" name="auth_group">
                                    <option value="" hidden>Assign Group Permission</option>
                                    @foreach($group as $value)
                                        <option value="{{$value->id}}" @if( old('auth_group') == $value->id ) selected @endif>{{$value->name}}</option>
                                    @endforeach

                                </select>
                                @if ($errors->has('auth_group'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                        <strong>{{ $errors->first('auth_group') }}</strong>
                                    </span>
                                @endif

                            </div>

                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-paper-plane"></i>  {{ __('Register') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')


<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>


@endsection('scripts')

