@extends('layouts.app')
@section('css')

<link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
    View Vendor
@endsection

@section('content')

<!-- BEGIN SAMPLE FORM PORTLET-->
    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">
                            <form class="form-horizontal" role="form">
                                <div class="form-body">
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Name</label>
                                        <div class="col-md-6">
                                            <input type="text" name="vendor_name" value="{{$vendor->name}}"  class="form-control" placeholder="Enter Product Description" disabled="">
                                        </div>
                                    </div>
                                   
                                    

                                  

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Addres</label>
                                        <div class="col-md-6">
                                            <input type="text" name="vendor_company_address" value="{{$vendor->address}}" class="form-control" placeholder="Enter Product Unit Cost" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Contact</label>
                                        <div class="col-md-6">
                                            <input type="number" name="vendor_contact"   class="form-control" value="{{$vendor->contact}}" placeholder="Enter Product Amount" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Mobile Number</label>
                                        <div class="col-md-6">
                                            <input type="number" name="mobile_number"   class="form-control" value="{{$vendor->mobile_number}}" placeholder="Enter Product Amount" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Email</label>
                                        <div class="col-md-6">
                                            <input type="email" name="email"   class="form-control" value="{{$vendor->email}}" placeholder="Enter Product Amount" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Pan/Vat Number</label>
                                        <div class="col-md-6">
                                            <input type="number" name="pan_vat_number"   class="form-control" value="{{$vendor->pan_vat_number}}" placeholder="Enter pan vat number" disabled="">
                                        </div>
                                    </div>
                                </div>
                               
                            </form>
                        </div>
                    </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->


@endsection('content')

