
@extends('layouts.app')
@section('css')
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .responsive {
            width: 100%;
            max-width: 100px;
            height: auto;
        }
    </style>
@endsection('css')

@section('page_title')
    Goods Requisition Files
@endsection

{{--@section('page-title')--}}
        {{--Goods Requisition Files--}}
{{--@endsection('page-title')--}}


@section('content')


    <div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

            
                <table class="table table-striped table-bordered table-hover" id="group">
                    <thead>
                    <tr>
                        <th>
                            SN
                        </th>

                        <th>
                            Vendor Name
                        </th>

                        <th>
                           Vendor Address
                        </th>

                        <th>
                            Vendor Mobile Number
                        </th>

                        <th>
                            Vendor Contact
                        </th>

                        <th>
                            Action
                        </th>


                    </tr>
                    </thead>
                    <tbody >
                     @foreach($vendor_list as $element)
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td>
                                        {{$element->name}}
                                         
                                    </td>
                                    <td>
                                        {{$element->address}}

                                         
                                    </td>
                                    <td>
                                        {{$element->mobile_number}}
                                         
                                    </td>
                                    
                                    
                                    <td>
                                        {{$element->contact}}
                                    </td>
                                    <td>
                                         <a class="btn default btn-xs blue-stripe" href="{{route('store.edit_vendor',['id'=>$element->id])}}">
                                        <i class="fa fa-pencil"></i> Edit
                                         </a>
                                       
                                       <!--  <a href="" class="btn btn-sm red" data-ids="" data-toggle="modal" data-rel="delete" data-user="">
                                                                               <i class="icon-trash"></i> Delete
                                                                               </a> --> 
                                       <!-- <a href="#deleteGroup" class="btn btn-sm red" data-ids="{{ $element->id }}" data-toggle="modal" data-rel="delete" data-user="{{$element->name}}">
                                        <i class="icon-trash"></i> Delete
                                        </a> -->
                                       <a class="btn default btn-xs green-stripe" href="{{route('store.view_vendor',['id'=>$element->id])}}">
                                        <i class="fa fa-eye"></i> View
                                      </a>
                                        
                                    </td>
                                </tr>
                                @endforeach



                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection('content')
@section('scripts')

    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
            $('#group').DataTable();
        });



    </script>


@endsection('scripts')
