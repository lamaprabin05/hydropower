@extends('layouts.app')
@section('css')

<link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>

@endsection('css')

@section('page_title')
   Add Vendor
@endsection

@section('content')
<!-- 
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif -->

<div class=" portlet light bordered" >
        <div class="row">
            <div class="portlet-body col-md-12 col-sm-12">

                            <form class="form-horizontal" role="form" action="{{route('store.add_vendor')}}" method="post" enctype="multipart/form-data" >
                                @csrf
                                <div class="form-body" id="product">

                                    <h3 class="text-center">Add Vendor</h3>
                                    <hr>
                             
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Name</label>
                                        <div class="col-md-9">
                                            <input type="text" name="vendor_name"  class="form-control" placeholder="Enter Vendor Name"  autocomplete="off" value="{{old('vendor_name')}}">
                                            @if($errors->has('vendor_name'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('vendor_name') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Address</label>
                                        <div class="col-md-9">
                                            <input type="text" name="vendor_company_address"  class="form-control" placeholder="Enter Vendor Address"  autocomplete="off" value="{{old('vendor_company_address')}}">
                                            @if($errors->has('vendor_company_address'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('vendor_company_address') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Contact</label>
                                        <div class="col-md-9">
                                            <input type="number" name="vendor_contact"   class="form-control" placeholder="Enter Vendor Contact"  autocomplete="off" value="{{old('vendor_contact')}}">
                                            @if($errors->has('vendor_contact'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('vendor_contact') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">PAN/VAT No</label>
                                        <div class="col-md-9">
                                            <input type="number" name="pan_vat_number"   class="form-control" placeholder="Enter Vendor Contact"  autocomplete="off" value="{{old('pan_vat_number')}}">
                                            @if($errors->has('pan_vat_number'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('pan_vat_number') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Email</label>
                                        <div class="col-md-9">
                                            <input type="email" name="email"   class="form-control" placeholder="Enter Vendor Email"  autocomplete="off" value="{{old('email')}}" >
                                            @if($errors->has('email'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('email') }}
                                                </span>
                                            @endif

                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Mobile Number</label>
                                        <div class="col-md-9">
                                            <input type="tel" name="mobile_number"   class="form-control" placeholder="Enter Vendor Mobile Number"  autocomplete="off" value="{{old('mobile_number')}}">
                                            @if($errors->has('mobile_number'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('mobile_number') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                   <div class="form-actions text-center">
                                    <button type="reset" class="btn btn-sm" id="cancel">Cancel</button>
                                    <button type="submit" class="btn btn-sm green" id="submit">Submit</button>
                                </div> 
                                 
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                    <!-- END SAMPLE FORM PORTLET-->

 



@endsection('content')
@section('scripts')

<script>
$(document).ready(function () {
            $(".bod-picker").nepaliDatePicker({
                dateFormat: "%D, %M %d, %y",
                closeOnDateSelect:true
                //minDate : "सोम, जेठ १०, २०७३",
                //maxDate : "मंगल, जेठ ३२, २०७३"
            });


        });
</script>


<script src="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'js/nepalidatepicker.js') }}" type="text/javascript"></script>
<script src="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.min.js')}}" type="text/javascript"></script>

 
@endsection('scripts')
