
@extends('layouts.app')
@section('css')

@endsection('css')

@section('page_title')
    Vendors List
@endsection

@section('page-title')
    Vendor List 
@endsection('page-title')


@section('content')


<!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                            Vendor Lists
                            </div>
                           
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>
                                         #
                                    </th>
                                    <th>
                                         Vendor Name
                                    </th>
                                    <th>
                                         Vendor Company
                                    </th>
                                    <th>
                                         Vendor Address
                                    </th>
                                    <th>
                                    	Vendor Contact
                                    	
                                    </th>
                                    <th>
                                         Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                   @foreach($vendor_list as $element)
                                <tr>
                                    <td>
                                        {{$loop->iteration}}
                                    </td>
                                    <td>
                                    	{{$element->name}}
                                         
                                    </td>
                                    <td>
                                    	{{$element->company}}

                                        
                                    </td>
                                    <td>
                                    	{{$element->address}}

                                         
                                    </td>
                                    <td>
                                    	{{$element->contact}}
                                    </td>
                                    <td>
                                        <a href="{{route('store.edit_vendor',['id'=>$element->id])}}" class="btn btn-sm btn-success"> <i class="icon-pencil"></i> Edit</a>
                                       
                                       <!--  <a href="" class="btn btn-sm red" data-ids="" data-toggle="modal" data-rel="delete" data-user="">
                                                                               <i class="icon-trash"></i> Delete
                                                                               </a> --> 
                                       <a href="#deleteGroup" class="btn btn-sm red" data-ids="{{ $element->id }}" data-toggle="modal" data-rel="delete" data-user="{{$element->name}}">
                                        <i class="icon-trash"></i> Delete
                                        </a>
                                      
                                        
                                        <a href="{{route('store.view_vendor',['id'=>$element->id])}}" class="btn btn-sm btn-primary"> <i class="icon-eye"></i> View</a>
                                    </td>
                                </tr>
                                @endforeach
                                
                                
                                
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->

                    {{--modal start here.--}}
    <div id="showMessage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="margin-top: 200px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Vendor Deleted Successfully
                    </h4>
                </div>

                <div class="modal-body">
                    <i class="fa fa-check"></i> &nbsp; Vendor Deleted Successfully .
                </div>
                <input type="hidden" id="hidden_id">
                <div class="modal-footer" style="text-align: center;">
                    <button type="button" class="btn btn-danger green confirm_yes" id="show_message"><i class="icon-check"></i> Ok
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>

    <div id="deleteGroup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently
                    </h4>
                </div>
                <div class="modal-body"> Do you want to delete <span class='hidden_title'>" "</span>?</div>
                <input type="hidden" id="hidden_id">
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
                    </button>
                    <button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                        No
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>

  
@endsection('content')
@section('scripts')


<script>
      
        $('#deleteGroup').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var ids = button.data('ids');
            var user = button.data('user');

            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modalfooter #confirm').data('form', form);
            $("#hidden_id").val(ids);
            $(".hidden_title").html(' "' + user + '" ');
        });


        $('#deleteGroup').find('#confirm_yes').on('click', function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //fetching id
            var id = $("#hidden_id").val();
                $.ajax({
                type:"POST",
                url: "{{ route('store.delete_vendor') }}",
               
                headers: {
                    'XCSRFToken': $('meta[name="csrf-token"]').attr('content')
                },

                data: "id=" + id,
                success: function (msg) {
                
                    $("#deleteGroup").modal("hide");
                    $("#showMessage").modal("show");
                    // window.location.reload();
                }
            });
        });
        // AJAX DELETE PERMISSION END

        $( "#show_message" ).click(function() {
            window.location.reload();
        });

    </script>
 
@endsection('scripts')
