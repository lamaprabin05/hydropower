@extends('layouts.app')
@section('css')

<link href="{{asset(STATIC_DIR.'assets/global/plugins/select2/select2.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/nepalidatepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{asset(STATIC_DIR.'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ asset(STATIC_DIR.'css/custom_calendar.css') }}" rel="stylesheet" type="text/css"/>
@endsection('css')
@section('page_title')
    Edit Vendor
@endsection
@section('content')
<!-- BEGIN SAMPLE FORM PORTLET-->
                  <div class=" portlet light bordered" >
                    <div class="row">
                        <div class="portlet-body col-md-12 col-sm-12">
                            <h3 class="text-center"><b>Edit Vendor</b></h3>
                            <form class="form-horizontal" role="form" action="{{route('store.update_vendor')}}" method="post" enctype="multipart/form-data" >
                                @csrf
                                <div class="form-body">
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Name</label>
                                        <div class="col-md-6">
                                            <input type="text" name="vendor_name" value="{{$vendor->name}}"  class="form-control" placeholder="Enter Product Description" >
                                            @if($errors->has('vendor_name'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('vendor_name') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Addres</label>
                                        <div class="col-md-6">
                                            <input type="text" name="vendor_company_address" value="{{$vendor->address}}" class="form-control" placeholder="Enter Product Unit Cost" >
                                            @if($errors->has('vendor_company_address'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('vendor_company_address') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Contact</label>
                                        <div class="col-md-6">
                                            <input type="number" name="vendor_contact"   class="form-control" value="{{$vendor->contact}}" placeholder="Enter Product Amount" >
                                            @if($errors->has('vendor_contact'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('vendor_contact') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor PAN/VAT Number</label>
                                        <div class="col-md-6">
                                            <input type="number" name="pan_vat_number"   class="form-control" value="{{$vendor->pan_vat_number}}" placeholder="Enter Vendor PAN/VAT Number" >
                                            @if($errors->has('pan_vat_number'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('pan_vat_number') }}
                                                </span>
                                            @endif
                                        </div>
                                     </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Email</label>
                                        <div class="col-md-6">
                                            <input type="email" name="vendor_email"   class="form-control" value="{{$vendor->email}}" placeholder="Enter Vendor Email" >
                                            @if($errors->has('vendor_email'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('vendor_email') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Vendor Mobile Number</label>
                                        <div class="col-md-6">
                                            <input type="text" name="mobile_number"   class="form-control" value="{{$vendor->mobile_number}}" placeholder="Enter Vendor Mobile Number">
                                            @if($errors->has('mobile_number'))
                                                <span class="help-block" style="color:red;">
                                                * {{ $errors->first('mobile_number') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <input type="hidden" name="id" value="{{$vendor->id}}">

                                    <div class="form-actions text-center">
                                        <button type="reset" class="btn btn-sm" id="cancel">Cancel</button>
                                        <button type="submit" class="btn btn-sm green" id="submit">Submit</button>
                                    </div>
                                </div>
                               
                            </form>
                        </div>
                    </div>
                </div>
                    <!-- END SAMPLE FORM PORTLET-->
@endsection('content')

