@extends('layouts.app')

@section('page_title')
    Dashboard
@endsection

@section('page-title')
    Dashboard || {{$module}}
@endsection
@section('content')
    {{--superadmin--}}
    @if($usertype == 0)
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue-madison">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{$total_user}}
                    </div>

                    <div class="desc">
                        Total Users
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat red-intense">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{$user_shareholder}}

                    </div>
                    <div class="desc">
                            Share Holder User
                    </div>
                </div>

            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat green-haze">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{$user_assets}}
                    </div>
                    <div class="desc">
                            Assets Management User
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat purple-plum">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {{$user_dms}}
                    </div>
                    <div class="desc">
                            Document Management User
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Share Holder --}}
    @elseif($usertype == 1)
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat blue-madison">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$user_shareholder}}
                        </div>

                        <div class="desc">
                            Users
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat green-haze">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$share_group}}
                        </div>
                        <div class="desc">
                            Share Group
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat red-intense">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$share_form}}

                        </div>
                        <div class="desc">
                                Total Share Form
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat purple-plum">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$completed_installment}}
                        </div>
                        <div class="desc">
                            Completed Installment
                        </div>
                    </div>

                </div>
            </div>
        </div>

    {{-- Assets Management--}}
    @elseif($usertype == 2)
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat blue-madison">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$total_user}}
                        </div>

                        <div class="desc">
                            Total Users
                        </div>
                    </div>
                    <a class="more" href="{{route('admin.list_user')}}">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat red-intense">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$user_shareholder}}

                        </div>
                        <div class="desc">
                            @if($usertype == 0)
                                Share Holder User
                            @else
                                Total Share Form
                            @endif
                        </div>
                    </div>
                    <a class="more" href="{{route('shareholder.list_form')}}">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat green-haze">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$user_assets}}
                        </div>
                        <div class="desc">
                            @if($usertype == 0)
                                Assets Management User
                            @else
                                Total Share Group
                            @endif

                        </div>
                    </div>
                    <a class="more" href="{{route('shareholder.list_group')}}">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat purple-plum">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$user_dms}}
                        </div>
                        <div class="desc">
                            @if($usertype == 0)
                                Document Management User
                            @else
                                Total Completed Installment
                            @endif

                        </div>
                    </div>
                    <a class="more" href="javascript:;">
                        {{--<i class="m-icon-swapright m-icon-white"></i>--}}
                    </a>
                </div>
            </div>
        </div>

    {{-- DMS--}}
    @else
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat blue-madison">
                    <div class="visual">
                        <i class="fa fa-comments"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$total_user}}
                        </div>

                        <div class="desc">
                            Total Users
                        </div>
                    </div>
                    <a class="more" href="{{route('admin.list_user')}}">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat red-intense">
                    <div class="visual">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$user_shareholder}}

                        </div>
                        <div class="desc">
                            @if($usertype == 0)
                                Share Holder User
                            @else
                                Total Share Form
                            @endif
                        </div>
                    </div>
                    <a class="more" href="{{route('shareholder.list_form')}}">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat green-haze">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$user_assets}}
                        </div>
                        <div class="desc">
                            @if($usertype == 0)
                                Assets Management User
                            @else
                                Total Share Group
                            @endif

                        </div>
                    </div>
                    <a class="more" href="{{route('shareholder.list_group')}}">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat purple-plum">
                    <div class="visual">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{$user_dms}}
                        </div>
                        <div class="desc">
                            @if($usertype == 0)
                                Document Management User
                            @else
                                Total Completed Installment
                            @endif

                        </div>
                    </div>
                    <a class="more" href="javascript:;">
                        {{--<i class="m-icon-swapright m-icon-white"></i>--}}
                    </a>
                </div>
            </div>
        </div>
    @endif
@endsection
